package net.megamil.leilao24h.Utils.ToolBox;


import android.app.Activity;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.megamil.leilao24h.R;


public class ToolBox_Carossel {

    private Activity activity;
    private int PaginaAtual = 0;
    private int TempoDeTrocaDeImagem = 10; // segundos
    private int TempoDeRestartDeTrocaDeImagem = 30; // segundos

    private LinearLayout ll_pontos;
    private TextView tv_quant_fotos;
    private TextView[] pontos;
    private ViewPager viewPager;
    private int Tamanho_Array;
    private Handler Contador = new Handler();

    public ToolBox_Carossel(Activity activity, LinearLayout linearLayout, final ViewPager viewPager, TextView textView, int lenght) {
        this.activity = activity;
        this.ll_pontos = linearLayout;
        this.viewPager = viewPager;
        this.tv_quant_fotos = textView;
        this.Tamanho_Array = lenght;

        Quantidade_Fotos();

    }

    private void Quantidade_Fotos() {

        if (Tamanho_Array == 1) {
            tv_quant_fotos.setVisibility(View.GONE);
            ll_pontos.setVisibility(View.GONE);
        } else {
            tv_quant_fotos.setText(Tamanho_Array + " FOTOS");



            StartCarossel();

            Indicador_de_Pontos(0);

            viewPager.setCurrentItem(0);

            viewPager.setOnTouchListener(onTouchListener);

            viewPager.addOnPageChangeListener(onPageChangeListener);
        }

    }

    private void StartCarossel() {
        Contador.post(TimeTask_TrocaDeImagem);
    }

    private Runnable TimeTask_TrocaDeImagem = new Runnable() {

        @Override
        public void run() {
            viewPager.setCurrentItem(PaginaAtual);
            Contador.postDelayed(TimeTask_TrocaDeImagem, TempoDeTrocaDeImagem * 1000);
        }
    };

    private Runnable Restart_TimeTask_TrocaDeImagem = new Runnable() {
        @Override
        public void run() {
            Contador.postDelayed(TimeTask_TrocaDeImagem, TempoDeTrocaDeImagem * 1000);
        }
    };

    private void Indicador_de_Pontos(int pos) {

        pontos = new TextView[Tamanho_Array];

        ll_pontos.removeAllViews();

        for (int i = 0; i < pontos.length; i++) {

            pontos[i] = new TextView(activity);
            pontos[i].setText(Html.fromHtml("&#8226;"));
            pontos[i].setTextSize(35);
            pontos[i].setTextColor(activity.getResources().getColor(R.color.ColorSupremeGrayTransparent));

            ll_pontos.addView(pontos[i]);

        }

        if (pontos.length > 0) {
            pontos[pos].setTextColor(activity.getResources().getColor(R.color.white));
        }


        PaginaAtual = pos + 1;

        if (PaginaAtual >= Tamanho_Array) {
            PaginaAtual = 0;
        }

    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            Indicador_de_Pontos(position);

        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    private String TAG = "Moviment";
    float initialX, initialY;

    ViewPager.OnTouchListener onTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_UP) {
                Contador.removeCallbacks(TimeTask_TrocaDeImagem);
                Contador.removeCallbacks(Restart_TimeTask_TrocaDeImagem);
                Contador.postDelayed(Restart_TimeTask_TrocaDeImagem, TempoDeRestartDeTrocaDeImagem * 1000);
            }

            int action = event.getActionMasked();

            switch (action) {

                case MotionEvent.ACTION_DOWN:
                    initialX = event.getX();
                    initialY = event.getY();

                    Log.d(TAG, "Action was DOWN");
                    break;

                case MotionEvent.ACTION_MOVE:
                    Log.d(TAG, "Action was MOVE");
                    break;

                case MotionEvent.ACTION_UP:
                    float finalX = event.getX();
                    float finalY = event.getY();

                    Log.d(TAG, "Action was UP");

                    if (initialX < finalX) {
                        Log.d(TAG, "Left to Right swipe performed");
                    }

                    if (initialX > finalX) {
                        Log.d(TAG, "Right to Left swipe performed");
                    }

                    if (initialY < finalY) {
                        Log.d(TAG, "Up to Down swipe performed");
                    }

                    if (initialY > finalY) {
                        Log.d(TAG, "Down to Up swipe performed");
                    }

                    break;

                case MotionEvent.ACTION_CANCEL:
                    Log.d(TAG, "Action was CANCEL");
                    break;

                case MotionEvent.ACTION_OUTSIDE:
                    Log.d(TAG, "Movement occurred outside bounds of current screen element");
                    break;
            }

            return false;
        }
    };

}