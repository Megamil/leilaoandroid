package net.megamil.leilao24h.Utils.ToolBox;

import android.content.Context;
import android.content.SharedPreferences;

public class ToolBox_SharedPrefs {
    public static final String CONSTANTE_SHAREDPREFS_DO_PROJETO = "splash24h";
    private final SharedPreferences Prefs;

    public static final String CONSTANTE_KEY_SHAREDPREFS_SPLASH = "Splash";



    public ToolBox_SharedPrefs(Context context) {
        Prefs = context.getSharedPreferences(CONSTANTE_SHAREDPREFS_DO_PROJETO, Context.MODE_PRIVATE);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    private static final String CONSTANTE_KEY_SHAREDPREFS_CREDITS = "credits";


    public void setCredits(String s) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_CREDITS, s);
        editor.commit();
    }

    public void addCredits(String s) {
        int cred = Integer.parseInt(Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_CREDITS, "0"));
        int i = Integer.parseInt(s);
        String nv = String.valueOf(cred + i);
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_CREDITS, nv);
        editor.commit();
    }

    public void setCancelCredits() {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_CREDITS, "");
        editor.commit();
    }

    public String getCredits() {
        String cred = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_CREDITS, "0");
        int n = Integer.parseInt(cred);
        String msg = "";
        if (n <=0){
            msg = "0 ingressos";
        } else if (n == 1){
            msg = "1 ingresso";
        } else {
            msg = cred+" ingressos";
        }
        return msg;
    }

    public int getCreditsNum() {
        String cred = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_CREDITS, "0");
        int n = Integer.parseInt(cred);

        return n;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    private static final String CONSTANTE_KEY_SHAREDPREFS_DEBUG = "debug";

    public void setDegug() {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putBoolean(CONSTANTE_KEY_SHAREDPREFS_DEBUG, true);
        editor.commit();
    }

    public void setCancelDegug() {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putBoolean(CONSTANTE_KEY_SHAREDPREFS_DEBUG, false);
        editor.commit();
    }

    public Boolean getDebug() {
        Boolean id = Prefs.getBoolean(CONSTANTE_KEY_SHAREDPREFS_DEBUG, false);
        return id;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    private static final String CONSTANTE_KEY_SHAREDPREFS_TMP_ID = "tmp_id";

    public void setTMP_ID(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_TMP_ID, string);
        editor.commit();
    }

    public String getTMP_ID() {
        String id = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_TMP_ID, "");
        return id;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public static final String CONSTANTE_KEY_SHAREDPREFS_TMP_PASS = "tmp_hash";

    public void setTMP_HASH(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_TMP_PASS, string);
        editor.commit();
    }

    public String getTMP_HASH() {
        String id = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_TMP_PASS, "");
        return id;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public static final String CONSTANTE_KEY_SHAREDPREFS_PRIMEIRA_PERMISSOES = "permissoes";

    public void setPermissoesAPP(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_PRIMEIRA_PERMISSOES, string);
        editor.commit();
    }

    public String getPermissoesAPP() {
        String id = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_PRIMEIRA_PERMISSOES, "");
        return id;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public static final String CONSTANTE_KEY_SHAREDPREFS_PHOTO_PATH = "photo_path";

    public void setPhoto_path(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_PHOTO_PATH, string);
        editor.commit();
    }

    public String getPhoto_path() {
        String id = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_PHOTO_PATH, "");
        return id;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public static final String CONSTANTE_KEY_SHAREDPREFS_IMG_USER = "img_user";

    public void setImgUser(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_IMG_USER, string);
        editor.commit();
    }

    public String getImgUser() {
        String String = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_IMG_USER, "");
        return String;
    }

}