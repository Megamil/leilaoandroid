package net.megamil.leilao24h.Utils.ToolBox.Carossel;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import net.megamil.leilao24h.R;

/**
 * Created by John on 25/03/2018.
 */

public class ToolBox_Modal_Carossel_IMG {

    private static Dialog dialog;

    public static void ChamaModal(final Activity activity, String[] resources, int position) {

        dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(R.layout.dialog_carossel);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogNoAnimation;

        ViewPager viewPager = dialog.findViewById(R.id.imgCarossel);
        ImageView btn_close = dialog.findViewById(R.id.btnClose);


        TextView currentImage = dialog.findViewById(R.id.currentImage);
        TextView imageSizeArray = dialog.findViewById(R.id.imageSizeArray);

        imageSizeArray.setText(String.valueOf(resources.length));

        ToolBox_Modal_Carossel_ViewPager tc = new ToolBox_Modal_Carossel_ViewPager(
                activity,
                viewPager,
                resources,
                currentImage,
                position
        );


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}
