package net.megamil.leilao24h.Utils.ToolBox.Carossel;


import android.app.Activity;
import android.support.v4.view.ViewPager;
import android.widget.TextView;


public class ToolBox_Modal_Carossel_ViewPager {

    private TextView tvImage;

    public ToolBox_Modal_Carossel_ViewPager(Activity activity, final ViewPager viewPager, String[] resources, TextView textView, int PaginaAtual) {
        this.tvImage = textView;

        Adapter_ViewPager_Modal mCustomPagerAdapter = new Adapter_ViewPager_Modal(activity, resources);

        viewPager.setAdapter(mCustomPagerAdapter);

        viewPager.setCurrentItem(PaginaAtual);

        viewPager.addOnPageChangeListener(onPageChangeListener);

        textView.setText(String.valueOf(PaginaAtual+1));

    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            tvImage.setText(String.valueOf(position+1));

        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

}