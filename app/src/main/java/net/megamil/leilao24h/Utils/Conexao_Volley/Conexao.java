package net.megamil.leilao24h.Utils.Conexao_Volley;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Editar_Usuario;

import maripoppis.com.socialnetwork.FaceBook.FacebookAcountKitUtils;

import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;
import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Funcao_verifica_Status_HTTP;

/**
 * Criado por Eduardo dos santos em 19/11/2017.
 * Megamil.net
 */

public class Conexao {

    //    private Context context;
    private static Activity activity;
    private RequestQueue requisicao;
    private JSONObject objeto = null;

    private String url;                     /*http://192.168.0.106/ws*/
    private String funcao;                  /*Caso seja GET enviar os parametros concatenados FUNÇÃO?CAMPO=VALOR&CAMPO=VALOR&...*/
    private String tipo;                    /*DELETE,GET,POST ou PUT*/
    private String credenciais;             /*USUARIO:SENHA*/
    private Map<String, String> parametros;
    private Boolean showDialog;

    private static int HTTP_ERROR = 0;

    private static Dialog dialog;

    //Instancia com valores.
    public Conexao(Activity activity, String url, String funcao, String tipo, Map<String, String> parametros, String credenciais, Boolean showDialog) {

        this.credenciais = credenciais;
        this.parametros = parametros;
        this.url = url;
        this.funcao = funcao;
        this.tipo = tipo;
        this.activity = activity;
        this.showDialog = showDialog;
//        this.context = context;
        this.requisicao = Volley.newRequestQueue(this.activity);

        if (showDialog) {
            Mostra_Pagina_Loading();
        }

    }

    //Instancia Em Branco.
    public Conexao(Activity activity) {

        this.credenciais = null;
        this.parametros = null;
        this.url = "";
        this.funcao = "";
        this.tipo = "";
//        this.context = context;
        this.activity = activity;
        this.requisicao = Volley.newRequestQueue(this.activity);

        System.out.println("WS Instanciado sem valores iniciais.");

    }

    /*
     * Gets e Sets
     * */
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCredenciais() {
        return credenciais;
    }

    public void setCredenciais(String credenciais) {
        this.credenciais = credenciais;
    }

    public Map<String, String> getParametros() {
        return parametros;
    }

    public void setParametros(Map<String, String> parametros) {
        this.parametros = parametros;
    }

    /*
     *  Funções específicas do Conexao.
     * */

    public interface RetornoAssincrono {
        JSONObject onSuccess(JSONObject objeto);
    }

    public void getData(final RetornoAssincrono retorno) {

        int tipoRequisicao = 0;

        switch (tipo) {
            case "POST":
                tipoRequisicao = Request.Method.POST;
                break;
            case "GET":
                tipoRequisicao = Request.Method.GET;
                break;
            case "PUT":
                tipoRequisicao = Request.Method.PUT;
                break;
            case "DELETE":
                tipoRequisicao = Request.Method.DELETE;
                break;
            default:
                tipoRequisicao = Request.Method.POST;
                break;

        }

        StringRequest stringRequisicao = new StringRequest(tipoRequisicao, url + funcao, new Response.Listener<String>() {

            @Override
            public void onResponse(String respostaWs) {


                if (Verifica_Resposta(respostaWs)) {
//                    MsgUtil.Funcao_MSG(activity, "Resposta WS Null");
                    Log.e("Conexão com WS ", "FALHOU , Resposta NULL");
                } else {
                    Log.w("Conexão com WS ", "OK");
                    try {
                        JSONObject objeto = new JSONObject(respostaWs);
                        retorno.onSuccess(objeto);
                    } catch (JSONException e) {
                        Log.e("Conexão com WS ", "FALHOU , erro = " + e.getMessage());
                        Log.e("Resposta do WS", respostaWs);

                        Funcao_VolleyErrorListener(funcao, e.getMessage(), -1, -1);
                    }

                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int HTTP_Codigo = 0;
//                int HTTP_Codigo =  error.networkResponse.statusCode;
                int HTTPSTATUS = 0;
//                int HTTPSTATUS = Funcao_verifica_Status_HTTP(activity, error.networkResponse.statusCode, tv_erro);


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    HTTP_Codigo = 0;
                } else {
//                     HTTP_Codigo =  error.networkResponse.statusCode;
//                    if (raw_HTTP_Codigo != null) {
//                        HTTP_Codigo = Integer.parseInt(raw_HTTP_Codigo);
//                    }
                }

                Log.e("Volley Error HTTP", String.valueOf(HTTP_Codigo));
                Log.e("Volley Error MSG", String.valueOf(error));

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    int Codigo_Do_Error = 1;
                    String MSG = "Sem Conexão Com a Internet";
                    Funcao_VolleyErrorListener(funcao, MSG, Codigo_Do_Error, HTTP_Codigo);

                } else if (error instanceof AuthFailureError) {
                    int Codigo_Do_Error = 2;
                    String MSG = "Falha na autenticação de Usuário";
                    Funcao_VolleyErrorListener(funcao, MSG, Codigo_Do_Error, -2);

                } else if (error instanceof ServerError) {

                    String raw_HTTP_Codigo = String.valueOf(error.networkResponse.statusCode);

                    if (!raw_HTTP_Codigo.equals("null")) {
                        HTTPSTATUS = Funcao_verifica_Status_HTTP(activity, error.networkResponse.statusCode, tv_erro);
                    }

//                    int HTTPSTATUS = error.networkResponse.statusCode;

                    int Codigo_Do_Error = 3;

                    String MSG = "";
                    if (HTTPSTATUS == 404) {
                        MSG = "Usuario ou senha Invalida";
                    } else {
                        MSG = "Falha no Servidor";
                    }
                    Funcao_VolleyErrorListener(funcao, MSG, Codigo_Do_Error, HTTP_Codigo);

                } else if (error instanceof NetworkError) {
                    int Codigo_Do_Error = 4;
                    String MSG = "Falha na Conexão";
                    Funcao_VolleyErrorListener(funcao, MSG, Codigo_Do_Error, HTTP_Codigo);

                } else if (error instanceof ParseError) {
                    int Codigo_Do_Error = 5;
                    String MSG = "[WSF05] Falha Parse Error";
                    Funcao_VolleyErrorListener(funcao, MSG, Codigo_Do_Error, HTTP_Codigo);

//                } else if (error.networkResponse == null) {
//                    int Codigo_Do_Error = 6;
//                    String MSG = "[WSF] Status: " + error.networkResponse.statusCode;
//                    Funcao_VolleyErrorListener(funcao, MSG, Codigo_Do_Error, HTTP_Codigo);

                } else {
                    int Codigo_Do_Error = 7;
                    String MSG = "Erro Não Indentificado! " + error.getMessage().trim();
                    Funcao_VolleyErrorListener(funcao, MSG, Codigo_Do_Error, HTTP_Codigo);
                }

            }
        })

        {

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int statusCode = response.statusCode;
                Log.w("Status do HTTP", String.valueOf(statusCode));
                return super.parseNetworkResponse(response);
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                ToolBox_SharedPrefs_Usuario pref = new ToolBox_SharedPrefs_Usuario(activity);
                String token = pref.getTOKEN();

                Log.w("Token", token);
//                ToolBox_SQlite db = new ToolBox_SQlite(activity);
//                String token = db.getDBToken();

                Map<String, String> headers = new HashMap<>();
                String auth = "";

                if (token.equals("null")) {
                    auth = "Basic " + Base64.encodeToString(getCredenciais().getBytes(), Base64.NO_WRAP);
                    Log.w("Authorization", "Basic");
                } else {
                    auth = "Bearer " + token;
                    Log.w("Authorization", "Token");
                }

                headers.put("Accept", "application/json");
                headers.put("Authorization", auth);
                headers.put("User-Agent", "Megamil");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return getParametros();
            }


        };

        exibirParametros();
        requisicao.add(stringRequisicao);

    }

    private void exibirParametros() {

        System.out.println("Campos preenchidos: " + "\n" +
                "   URL: '" + getUrl() + "'\n" +
                "   Função: '" + getFuncao() + "'\n" +
                "   Tipo de requisição: '" + getTipo() + "'\n" +
                "   Credenciais: '" + credenciais + "'\n" +
                "   Parametros: '" + parametros + "'");

    }

    private void Funcao_VolleyErrorListener(String funcao, String msg, int codigo_do_error, int http_codigo) {

        //codigo do erro pode ser utilizado para tratar uma determinadafunção para casos esecificos
        switch (codigo_do_error) {
            case 1: // tempo exedido de conexão , tempo limite de conexão atingido
                break;
            case 2: // falha de autenticação com o servidor
                break;
            case 3: // falha no servido , servidor não existe ou fora do ar
                break;
            case 4: // falha na conexão com a internet
                break;
            case 5: // falha parse
                break;
            case 6: // a resposta do servidor retornou Null (Vazio)
                break;
            case 7://  erro nao indentificado
                break;
        }

        HTTP_ERROR = http_codigo;

        //switch para retornar determinadas funçoes caso algo de errado
        //switch é relacionado a função chamada no webservise
        switch (funcao) {
            case "teste":
                break;
        }

//        MsgUtil.Funcao_MSG(activity, msg);
        String msg1 = msg;
        Fecha_Pagina_Loading(msg1, codigo_do_error, -1);

    }

    private boolean Verifica_Resposta(String respostaWs) {

        if (respostaWs.isEmpty()
                || respostaWs.equals("")
                || respostaWs.equals("null")
                || respostaWs.equals("Null")
                || respostaWs.equals("NULL")) {

            return true;
        }

        return false;
    }

    private static LinearLayout ll_load;
    private static LinearLayout ll_erro;
    private static TextView tv_erro;
    private static Button btn_erro;
    private static ImageView btn_close;

    private static void Mostra_Pagina_Loading() {
        dialog = null;
        dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(R.layout.activity_loading);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogNoAnimation;
        dialog.setCancelable(false);

        ll_load = dialog.findViewById(R.id.ll_load);
        ll_erro = dialog.findViewById(R.id.ll_error);
        tv_erro = dialog.findViewById(R.id.tv_error);
        btn_erro = dialog.findViewById(R.id.btn_error);
        btn_close = dialog.findViewById(R.id.btn_close);

        dialog.show();

        ll_load.setVisibility(View.VISIBLE);

    }

    private static void Mostra_Pagina_Loading(Activity activity) {
        dialog = null;
        dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(R.layout.activity_loading);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogNoAnimation;
        dialog.setCancelable(false);

        ll_load = dialog.findViewById(R.id.ll_load);
        ll_erro = dialog.findViewById(R.id.ll_error);
        tv_erro = dialog.findViewById(R.id.tv_error);
        btn_erro = dialog.findViewById(R.id.btn_error);
        btn_close = dialog.findViewById(R.id.btn_close);

        dialog.show();

        ll_load.setVisibility(View.VISIBLE);
    }

    public static void Fecha_Pagina_Loading(String msg, final int codigo_do_error, final int ID_Conexao) {

//        if (ID_Conexao == -1) {
//            if (!dialog.isShowing()) {
//                dialog.show();
//            }
//        }
//        Mostra_Pagina_Loading();

        if (HTTP_ERROR != 401) {
            checkDialogShowing();
            if (!msg.equals("null") || !msg.equals(""))
                tv_erro.setText(msg.replace(",", "\n"));
        }

        ll_erro.setVisibility(View.VISIBLE);
        ll_load.setVisibility(View.GONE);

        if (ID_Conexao == -1 || ID_Conexao == 7) {
            btn_erro.setVisibility(View.GONE);
        }

        // Funções de retorno da tela de Load
        switch (ID_Conexao) {
            case -2:
                btn_erro.setVisibility(View.GONE);
                break;
            case 11:
                checkDialogShowing();
                btn_erro.setVisibility(View.GONE);
                break;
            case 16:
                btn_erro.setVisibility(View.GONE);
                break;
        }

        //codigo do httptambem pode ser usado para tratar determinado error
        switch (HTTP_ERROR) {
            case -2:
                checkDialogShowing();
                btn_close.setVisibility(View.GONE);
                Conexao_WS.Funcao_Handler(activity, tv_erro);
                break;
            case HttpURLConnection.HTTP_OK:
                //do stuff
                break;
            case HttpURLConnection.HTTP_NOT_FOUND:
                //do stuff
                break;
            case HttpURLConnection.HTTP_INTERNAL_ERROR:
                //do stuff
                break;
            case 401:
                btn_close.setVisibility(View.GONE);
                break;
        }

        switch (codigo_do_error) {
            case -5:
                btn_erro.setVisibility(View.VISIBLE);
                btn_erro.setText("Completar Perfíl");
                break;
            case 0: // tempo exedido de conexão , tempo limite de conexão atingido
                break;
            case 1: // tempo exedido de conexão , tempo limite de conexão atingido
                break;
            case 2: // falha de autenticação com o servidor
                break;
            case 3: // falha no servido , servidor não existe ou fora do ar
                break;
            case 4: // falha na conexão com a internet
                break;
            case 5: // falha parse
                break;
            case 6: // a resposta do servidor retornou Null (Vazio)
                break;
            case 7://  erro nao indentificado
                break;
        }

//        if (dialog == null) {
//            Mostra_Pagina_Loading();
//        } else if (dialog != null) {
//            Mostra_Pagina_Loading();
//        }//


        switch (ID_Conexao) {
            case -3:
                btn_erro.setText("Validar Telefone");
                break;
            case 5:
                btn_erro.setText("Voltar");
                btn_close.setVisibility(View.GONE);
                break;
            case 9:
                btn_erro.setVisibility(View.GONE);
                break;
            case 201:
                btn_erro.setText("Fazer Login");
                break;
        }

        btn_erro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//inicia função de tentar novamente
                switch (ID_Conexao) {
                    case -3:
                        if (dialog != null)
                            FacebookAcountKitUtils.smsAtivation(activity);
                        dialog.dismiss();
                        break;
                    case 1:
                        if (dialog != null)
                            dialog.dismiss();
//                        Fragment_Favoritos.initWSConection(pageNumber);
                        break;
                    case 5:
                        finishActivity();
                        break;
                    case 201:
//                        if (dialog != null)
//                            dialog.dismiss();
                        Dados_Ws(2, "", null, activity);
                        break;
                }

                switch (codigo_do_error) {
                    case -5:
                        Intent intent = new Intent(activity, Editar_Usuario.class);
//                        intent.putExtra("comp", LoginActivity.dataHMAux.toString());
                        intent.putExtra("comp", "");
                        ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");
                        closeDialog();
                        break;
                }

            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// inicia função do backpresed
                switch (ID_Conexao) {
                    case -2:
                        Fecha_Pagina_Loading();
                        break;
                    case 7:
                        finishActivity();
                        break;
                    case 9:
                        finishActivity();
                        break;
                    case 11:
                        finishActivity();
                        break;
                    case 16:
                        finishActivity();
                        break;
                }


                if (dialog != null)
                    dialog.dismiss();
            }
        });

    }

    public static void Fecha_Pagina_Loading_Com_Login(String msg, final int codigo_do_error, final int ID_Conexao, final Map<String, String> parametros) {

//
        if (HTTP_ERROR != 401) {
            checkDialogShowing();
            if (!msg.equals("null") || !msg.equals(""))
                tv_erro.setText(msg.replace(",", "\n"));
        }

        ll_erro.setVisibility(View.VISIBLE);
        ll_load.setVisibility(View.GONE);

        if (ID_Conexao == -1 || ID_Conexao == 7) {
            btn_erro.setVisibility(View.GONE);
        }

        //codigo do httptambem pode ser usado para tratar determinado error
        switch (HTTP_ERROR) {
            case -2:
                checkDialogShowing();
                btn_close.setVisibility(View.GONE);
                Conexao_WS.Funcao_Handler(activity, tv_erro);
                break;
            case HttpURLConnection.HTTP_OK:
                //do stuff
                break;
            case HttpURLConnection.HTTP_NOT_FOUND:
                //do stuff
                break;
            case HttpURLConnection.HTTP_INTERNAL_ERROR:
                //do stuff
                break;
            case 401:
                btn_close.setVisibility(View.GONE);
                break;
        }

        switch (codigo_do_error) {
            case -5:
                btn_erro.setVisibility(View.VISIBLE);
                btn_erro.setText("Completar Perfíl");
                break;
            case 0: // tempo exedido de conexão , tempo limite de conexão atingido
                break;
            case 1: // tempo exedido de conexão , tempo limite de conexão atingido
                break;
            case 2: // falha de autenticação com o servidor
                break;
            case 3: // falha no servido , servidor não existe ou fora do ar
                break;
            case 4: // falha na conexão com a internet
                break;
            case 5: // falha parse
                break;
            case 6: // a resposta do servidor retornou Null (Vazio)
                break;
            case 7://  erro nao indentificado
                break;
        }

//        if (dialog == null) {
//            Mostra_Pagina_Loading();
//        } else if (dialog != null) {
//            Mostra_Pagina_Loading();
//        }//


        switch (ID_Conexao) {
            case -3:
                btn_erro.setText("Validar Telefone");
                break;
            case 5:
                btn_erro.setText("Voltar");
                btn_close.setVisibility(View.GONE);
                break;
            case 9:
                btn_erro.setVisibility(View.GONE);
                break;
            case 201:
                btn_erro.setText("Fazer Login");
                break;
        }

        btn_erro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//inicia função de tentar novamente
                switch (ID_Conexao) {
                    case -3:
                        if (dialog != null)
                            FacebookAcountKitUtils.smsAtivation(activity);
                        dialog.dismiss();
                        break;
                    case 1:
                        if (dialog != null)
                            dialog.dismiss();
//                        Fragment_Favoritos.initWSConection(pageNumber);
                        break;
                    case 5:
                        finishActivity();
                        break;
                    case 201:
//                        if (dialog != null)
//                            dialog.dismiss();
                        Dados_Ws(2, "", parametros, activity);
                        break;
                }

                switch (codigo_do_error) {
                    case -5:
                        Intent intent = new Intent(activity, Editar_Usuario.class);
//                        intent.putExtra("comp", LoginActivity.dataHMAux.toString());
                        intent.putExtra("comp", "");
                        ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");
                        closeDialog();
                        break;
                }

            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// inicia função do backpresed
                switch (ID_Conexao) {
                    case -2:
                        Fecha_Pagina_Loading();
                        break;
                    case 7:
                        finishActivity();
                        break;
                    case 9:
                        finishActivity();
                        break;
                    case 11:
                        finishActivity();
                        break;
                    case 16:
                        finishActivity();
                        break;
                }


                if (dialog != null)
                    dialog.dismiss();
            }
        });

    }

    public static void openDialogWhitePerfil(final Activity activity, String resultado, final JSONObject objeto, int i, int i1) {
        Mostra_Pagina_Loading(activity);
        showErrors();
        tv_erro.setText(resultado.replace(",", "\n"));

        btn_close.setOnClickListener(clickFinish);

        btn_erro.setVisibility(View.VISIBLE);
        btn_erro.setText("Completar Perfíl");
        btn_erro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, Editar_Usuario.class);

                try {
                    JSONObject usuario = objeto.getJSONObject("usuarios");
                    intent.putExtra("comp", usuario.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");
                closeDialog();
            }
        });
    }

    public static void Fecha_Pagina_Loading(String msg, final JSONObject objeto, final int status, final int ID_Conexao) {

        switch (status) {
            case -5:
                checkDialogShowing();
                showErrors();
                tv_erro.setText(msg.replace(",", "\n"));

                btn_close.setOnClickListener(clickFinish);

                btn_erro.setVisibility(View.VISIBLE);
                btn_erro.setText("Completar Perfíl");
                btn_erro.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(activity, Editar_Usuario.class);

                        try {
                            JSONObject usuario = objeto.getJSONObject("usuarios");
                            intent.putExtra("comp", usuario.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");
                        closeDialog();
                    }
                });
                break;
        }

    }

    private static void showErrors() {
        ll_erro.setVisibility(View.VISIBLE);
        ll_load.setVisibility(View.GONE);
    }

    private static void showLoad() {
        ll_erro.setVisibility(View.GONE);
        ll_load.setVisibility(View.VISIBLE);
    }

    public static void Fecha_Pagina_Loading() {
        if (dialog != null)
            dialog.dismiss();
    }

    private static View.OnClickListener clickFinish = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (dialog != null || dialog.isShowing()) {
                dialog.dismiss();
                dialog = null;
            }
        }
    };

    private static void finishActivity() {
        if (dialog != null)
            dialog.dismiss();
        activity.finish();
    }

    private static void checkDialogShowing() {
        if (dialog == null) {
            Mostra_Pagina_Loading();
        } else if (dialog.isShowing()) {

        } else {
            dialog = null;
            Mostra_Pagina_Loading();
        }
    }

    private static void closeDialog() {
        if (dialog != null || dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

}
