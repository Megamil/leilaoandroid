package net.megamil.leilao24h.Utils.Conexao_Volley;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;

import net.megamil.leilao24h.Login.SigninActivity;
import net.megamil.leilao24h.Login.LoginActivity;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_Dialog_Categoria;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Categorias;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Favoritos;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Inicio;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Leiloes;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.LeilaoNortification;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.Planos;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Afiliados;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Ativar_Leilao;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Cadastra_Leilao;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Editar_Senha;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Editar_Usuario;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Ativar_Leilao.Activity_Ativar_Leilao;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Arremates;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Lances;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Pagamento;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Recompensas;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Recompensa;
import net.megamil.leilao24h.Usuario.Pagina_Busca;
import net.megamil.leilao24h.Usuario.Pagina_Inicio;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SQlite;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;
import net.megamil.library.Utils.ActivityUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by John on 24/01/2018.
 */

public class Conexao_WS {

    private static String Oque_A_Pagina_Faz; // simplismente para facilitar a manutenção do codigo

    private static String FUNCAO_DA_PAGINA;
    private static String FUNCAO_DA_PAGINA_FINAL;
    private static String TIPO;
    private static Boolean TIPO_COM_PARAMETRO;
    private static Map<String, String> PARAMETROS;
    private static String CREDENCIAIS;
    private static int final_status = -1;
    private static Boolean SHOWDIALOG = true;


    //Funçoes para tratativas de erros

    @SuppressLint("LongLogTag")
    public static int Funcao_verifica_Status_HTTP(Activity activity, int status, TextView tv_erro) {
        int i = status;

        Log.w("HTTP Web", String.valueOf(status));

        if (i == 401) {

            Funcao_Handler(activity, tv_erro);

//            Log.e("Logado em outro Aparelho", resultado);
        }

        return i;

    }

    public static void Funcao_Handler(final Activity activity, final TextView tv_erro) {

        final int raw_tm = Integer.parseInt(10 + "000");

//        new Handler().postDelayed(
//                new Runnable() {
//                    @Override
//                    public void run() {

        new CountDownTimer(raw_tm, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.w("Timer", String.valueOf(millisUntilFinished));
                tv_erro.setText(" Falha na autenticação de Usuário \nRedirecionando para o loginModel em " + (millisUntilFinished / 1000));
            }

            public void onFinish() {
//                                tv_erro.setVisibility(View.GONE);
//                                tv_erro.setText("00");
//                if (Conexao.dialog != null)
//                    Conexao.dialog.dismiss();

                Conexao.Fecha_Pagina_Loading();

                ToolBox_Chama_Activity.Funcao_RAW_Logout(activity);

            }
        }.start();

//                    }
//                },
//                raw_tm
//        );
    }

    @SuppressLint("LongLogTag")
    public static int Funcao_verifica_Status(Activity activity, JSONObject objeto, int status, String resultado) {
        int i = status;

        Log.w("STATUS Web", String.valueOf(status));

        if (i == 6) {
            ToolBox_Chama_Activity.Funcao_RAW_Logout(activity);
            Log.e("Logado em outro Aparelho", resultado);
        } else if (i == 5) {
            ToolBox_Chama_Activity.Funcao_RAW_Logout(activity);
            Log.e("Resultado", resultado);
        } else if (i == -5) {
            Log.e("Cadastro incompleto WS", resultado);
            Conexao.Fecha_Pagina_Loading(resultado, objeto, -5, -1);
        }
        return i;

    }

    public static void whitePerfil(Activity activity, String resultado, JSONObject objeto) {
        Log.e("Cadastro incompleto WS", resultado);
        Conexao.openDialogWhitePerfil(activity, resultado, objeto, -5, -1);
    }

    private static void Funcao_MostraResultado(Activity activity, String resultado) {
        MsgUtil.Funcao_MSG(activity, resultado);
    }

    private static void Funcao_STATUSException(int status) {
        Log.e("Error ao recuperar", " Status do servidor! status = " + status);
    }

    private static void Funcao_JSONException(int i, JSONException e) {
        switch (i) {
            case 1:
                Log.e("Conexao com a Pagina " + Oque_A_Pagina_Faz, "FALHOU!");
                break;
            case 2:
                Log.e(Oque_A_Pagina_Faz + ": Erro ao pegar", "Dados do JSON!");
                break;
        }
        Conexao.Fecha_Pagina_Loading(e.getMessage(), 0, -1);
        Log.e("Erro nos Dados: ", e.getMessage());
    }

    private static void ChamaLogin(JSONObject objeto, Activity context, String usuario_) throws JSONException {

        JSONObject dados = null;
        try {
            dados = objeto.getJSONObject("dados");
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        System.out.println("Dados = " + dados);

        ToolBox_SharedPrefs_Usuario prefs = new ToolBox_SharedPrefs_Usuario(context);
        prefs.setLOGADO(true);
        prefs.setID_USUARIO(dados.getString("id_usuario"));
        prefs.setNOME_USUARIO(dados.getString("nome_usuario"));
        prefs.setEMAIL_USUARIO(dados.getString("email_usuario"));
        prefs.setFK_GRUPO_USUARIO(dados.getString("fk_grupo_usuario"));
        prefs.setATIVO_USUARIO(dados.getString("ativo_usuario"));
        prefs.setTOKEN(dados.getString("token_acesso"));
        prefs.setCODE_SHARE(dados.getString("code"));

        if (Parametros.containsKey("facebook_usuario")) {
            prefs.setURL_IMAGE_FACEBOOK(Parametros.get("facebook_url_image"));
        }

        ToolBox_SQlite db = new ToolBox_SQlite(context);
        db.setDBToken(dados.getString("token_acesso"));

//        MsgUtil.Funcao_TOAST(context , db.getDBToken());

//        if (objeto.has("assinaturas")) {
//            JSONObject dados_assinatura = objeto.getJSONObject("assinaturas");
//            System.out.println("Dados assinaturas= " + dados_assinatura);
//
//            String nome_plano = dados_assinatura.getString("nome_plano");
//            String detalhes_plano = dados_assinatura.getString("detalhes_plano");
//            String id_assinatura = dados_assinatura.getString("id_assinatura");
//            String status_assinatura = dados_assinatura.getString("status_assinatura");
//
//            String data_assinatura = dados_assinatura.getString("data_assinatura");
//            String data_status_assinatura = dados_assinatura.getString("data_status_assinatura");
//            String valor_plano = dados_assinatura.getString("valor_plano");
//            String proxima_cobranca = dados_assinatura.getString("proxima_cobranca");
//
//
//            editor.putString("nome_plano", nome_plano);
//            editor.putString("detalhes_plano", detalhes_plano);
//            editor.putString("status_assinatura", status_assinatura);
//            editor.putString("id_assinatura", id_assinatura);
//            editor.putString("data_assinatura", data_assinatura);
//            editor.putString("data_status_assinatura", data_status_assinatura);
//            editor.putString("valor_plano", valor_plano);
//            editor.putString("proxima_cobranca", proxima_cobranca);
//
//        }

//        editor.commit();

//
//        SharedPreferences prefs2 = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor2 = prefs2.edit();
//        editor2.putBoolean("Logado", true);
//        editor2.commit();

        Boolean prefs_ok = prefs.getLOGADO();

        if (prefs_ok) {
            Intent pagina_usuario = new Intent(context, Pagina_Inicio.class);
            ToolBox_Chama_Activity.Chama_Tela(context, pagina_usuario);
        }
//
//        context.startActivity(pagina_usuario);
//        context.finish();

    }


//
//    String Dados_Para_URL_GET = null; // padrao
//    Map<String, String> Dados_Para_Parametros = new HashMap<>(); // padrao

    public static int Dados_Ws(
            final int ID_Conexao,
            final String Dados_Para_URL_GET,
            final Map<String, String> Dados_Para_Parametros,
            final Context context) {

        Parametros = Dados_Para_Parametros; // paramentros static para caso precisar utilizar em alguma Funcão!

        /**
         * Switch para receber o Context da pagina e outras funçoes de cada pagina
         * especifica , aqui recebe o API do WS, funçoes e outras coisas
         */

        switch (ID_Conexao) {
            case 101:
                Oque_A_Pagina_Faz = "listar_leiloes_ativos filtro id";
                FUNCAO_DA_PAGINA = "listar_leiloes_ativos";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
        }

        if (TIPO == "GET") {
            FUNCAO_DA_PAGINA_FINAL = FUNCAO_DA_PAGINA + "?" + Dados_Para_URL_GET;
        } else {
            FUNCAO_DA_PAGINA_FINAL = FUNCAO_DA_PAGINA;
        }


        Conexao ws = new Conexao(
                activity,
                Conexao_Constantes.URL_WS_PADRAO,
                FUNCAO_DA_PAGINA_FINAL,
                TIPO,
                PARAMETROS,
                CREDENCIAIS,
                false
        );

        ws.getData(new Conexao.RetornoAssincrono() {

            @Override
            public JSONObject onSuccess(JSONObject objeto) {

                Log.w("Dados do WS = ", objeto.toString());

                try {

                    String resultado = objeto.getString("resultado");
                    int status = Funcao_verifica_Status(activity, objeto, objeto.getInt("status"), resultado);
                    Resultado_Do_WS(activity, ID_Conexao, status, resultado, objeto);
                    Log.w("Conexao com a Pagina " + Oque_A_Pagina_Faz, "OK!");

                } catch (JSONException e) {

                    int status = 0;
                    String resultado = "Erro no Json";
                    Resultado_Do_WS(activity, ID_Conexao, status, resultado, objeto);
                    Funcao_JSONException(1, e);

                }

                return objeto;
            }
        });


        return final_status;
    }

    private int ID_Conexao;
    private String Dados_Para_URL_GET;
    private static Map<String, String> Parametros = new HashMap<>();
    private static Context context;
    private static Activity activity;

    public static int Dados_Ws(
            final int ID_Conexao,
            final String Dados_Para_URL_GET,
            final Map<String, String> Dados_Para_Parametros,
            final Activity activity) {

//        if (Parametros.size() > 0) {
//            Parametros.clear();
//        }
        Parametros = Dados_Para_Parametros; // paramentros static para caso precisar utilizar em alguma Funcão!

        ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
        String usuario_ = prefs.getTMP_ID();
        String senha_ = prefs.getTMP_HASH();

        /**
         * Switch para receber o Context da pagina e outras funçoes de cada pagina
         * especifica , aqui recebe o API do WS, funçoes e outras coisas
         */

        if (ID_Conexao == 1 || ID_Conexao == 3) {
            CREDENCIAIS = null + ":" + null;
        } else {
            CREDENCIAIS = usuario_ + ":" + senha_;
        }

//        CREDENCIAIS = null;

        switch (ID_Conexao) {
            //Funçoes Auxiliares
            case 1001:
                Oque_A_Pagina_Faz = "Buscar Cep";
                FUNCAO_DA_PAGINA = "buscar_cep";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;

            // Funçoes do projeto
            case 1:
                Oque_A_Pagina_Faz = "Recuperação de Senha";
                FUNCAO_DA_PAGINA = "esqueci_senha";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 2:
                Oque_A_Pagina_Faz = "Login";
                FUNCAO_DA_PAGINA = "login_usuario";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 201:
                Oque_A_Pagina_Faz = "Ativar via SMS";
                FUNCAO_DA_PAGINA = "ativar_sms";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 3:
                Oque_A_Pagina_Faz = "Criar Usuário";
                FUNCAO_DA_PAGINA = "criar_usuario";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 4:
                Oque_A_Pagina_Faz = "Pré editar Usuário";
                FUNCAO_DA_PAGINA = "pre_editar_usuario";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 401:
                Oque_A_Pagina_Faz = "Pré editar Senha do Usuário";
                FUNCAO_DA_PAGINA = "pre_editar_usuario";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 5:
                Oque_A_Pagina_Faz = "Editar Usuário";
                FUNCAO_DA_PAGINA = "editar_usuario";
                TIPO = "PUT";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 6:
                Oque_A_Pagina_Faz = "Pré criar produto";
                FUNCAO_DA_PAGINA = "pre_criar_produto";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 601:
                Oque_A_Pagina_Faz = "Listar Categorias";
                FUNCAO_DA_PAGINA = "pre_criar_produto";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                SHOWDIALOG = false;
                break;
            case 7:
                Oque_A_Pagina_Faz = "Criar Produto";
                FUNCAO_DA_PAGINA = "criar_produto";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 8:
                Oque_A_Pagina_Faz = "listar_produtos";
                FUNCAO_DA_PAGINA = "listar_produtos";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 9:
                Oque_A_Pagina_Faz = "criar_leilao";
                FUNCAO_DA_PAGINA = "criar_leilao";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 10:
                Oque_A_Pagina_Faz = "listar_leiloes_ativos";
                FUNCAO_DA_PAGINA = "listar_leiloes_ativos";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                SHOWDIALOG = false;
                break;
            case 102:
                Oque_A_Pagina_Faz = "listar_leiloes_ativos";
                FUNCAO_DA_PAGINA = "listar_leiloes_ativos";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                SHOWDIALOG = true;
                break;
            case 11:
                Oque_A_Pagina_Faz = "entrar_leilao";
                FUNCAO_DA_PAGINA = "entrar_leilao";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                SHOWDIALOG = true;
                break;
            case 12:
                Oque_A_Pagina_Faz = "listar_pacotes";
                FUNCAO_DA_PAGINA = "listar_pacotes";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 13:
                Oque_A_Pagina_Faz = "favorito";
                FUNCAO_DA_PAGINA = "favorito";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                SHOWDIALOG = false;
                break;
            case 14:
                Oque_A_Pagina_Faz = "listar_favoritos";
                FUNCAO_DA_PAGINA = "listar_favoritos";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 15:
                Oque_A_Pagina_Faz = "meus_leiloes";
                FUNCAO_DA_PAGINA = "meus_leiloes";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 16:
                Oque_A_Pagina_Faz = "upload_foto";
                FUNCAO_DA_PAGINA = "upload_foto";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                SHOWDIALOG = false;
                break;
            case 17:
                Oque_A_Pagina_Faz = "leiloes_categorias";
                FUNCAO_DA_PAGINA = "leiloes_categorias";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 18:
                Oque_A_Pagina_Faz = "buscar_leiloes";
                FUNCAO_DA_PAGINA = "buscar_leiloes";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 19:
                Oque_A_Pagina_Faz = "Histórico lances";
                FUNCAO_DA_PAGINA = "historico_lances";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 20:
                Oque_A_Pagina_Faz = "Avaliar";
                FUNCAO_DA_PAGINA = "avaliar";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 21:
                Oque_A_Pagina_Faz = "Listar Arrematados";
                FUNCAO_DA_PAGINA = "listar_arrematados";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 22:
                Oque_A_Pagina_Faz = "Listar Afiliados";
                FUNCAO_DA_PAGINA = "listar_afiliados";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 23:
                Oque_A_Pagina_Faz = "Listar Recompensas";
                FUNCAO_DA_PAGINA = "listar_recompensas";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 24:
                Oque_A_Pagina_Faz = "Comprar Recompensas";
                FUNCAO_DA_PAGINA = "comprar_recompensas";
                TIPO = "POST";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 25:
                Oque_A_Pagina_Faz = "meus_produtos_arrematados";
                FUNCAO_DA_PAGINA = "meus_produtos_arrematados";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 26:
                Oque_A_Pagina_Faz = "listar_minhas_recompensas";
                FUNCAO_DA_PAGINA = "listar_minhas_recompensas";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
            case 27:
                Oque_A_Pagina_Faz = "listar_meus_pagamentos";
                FUNCAO_DA_PAGINA = "listar_meus_pagamentos";
                TIPO = "GET";
                PARAMETROS = Dados_Para_Parametros;
                break;
        }

        if (TIPO == "GET") {
            FUNCAO_DA_PAGINA_FINAL = FUNCAO_DA_PAGINA + "?" + Dados_Para_URL_GET;
        } else {
            FUNCAO_DA_PAGINA_FINAL = FUNCAO_DA_PAGINA;
        }

        Conexao ws = new Conexao(
                activity,
                Conexao_Constantes.URL_WS_PADRAO,
                FUNCAO_DA_PAGINA_FINAL,
                TIPO,
                PARAMETROS,
                CREDENCIAIS,
                SHOWDIALOG

        );

        ws.getData(new Conexao.RetornoAssincrono() {

            @Override
            public JSONObject onSuccess(JSONObject objeto) {

                Log.w("Dados do WS = ", objeto.toString());

                try {

                    String resultado = objeto.getString("resultado");
                    int status = Funcao_verifica_Status(activity, objeto, objeto.getInt("status"), resultado);
                    Resultado_Do_WS(activity, ID_Conexao, status, resultado, objeto);
                    Log.w("Conexao com a Pagina " + Oque_A_Pagina_Faz, "OK!");

                } catch (JSONException e) {

                    int status = 0;
                    String resultado = "Erro no Json";
                    Resultado_Do_WS(activity, ID_Conexao, status, resultado, objeto);
                    Funcao_JSONException(1, e);

                }

                return objeto;
            }
        });


        return final_status;
    }


    private static void Resultado_Do_WS(Activity activity, int ID_Conexao, int status, String resultado, JSONObject objeto) {

        if (objeto.has("creditos")) {
            try {
                ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
                prefs.setCredits(objeto.getString("creditos"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //case padrao

//        case 1:
//
//        if (status == 1) {
//            JSONObject dados = null;
//            try {
//                dados = objeto.getJSONObject("dados");
//                String id_usuario = dados.getString("id_usuario");
//                        Conexao.Fecha_Pagina_Loading();

//            } catch (JSONException e) {
//                e.printStackTrace();
//                Funcao_JSONException(2, e);
//            }
//
//
//        } else if (status == 0) {
//  // -1 significa que nao faz nada , somente mostra o load e o erro
//        Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
//        } else {
//            Funcao_STATUSException(status);
//        }
//
//        Funcao_MostraResultado(activity , resultado);
//
//        break;

        /**
         * Switch para funçoes diferenciadas das paginas
         */
        switch (ID_Conexao) {

            case 1001:

                if (status == 1) {
                    JSONObject dados = null;
                    try {
                        dados = objeto.getJSONObject("endereco");

                        Editar_Usuario.et_endereco_usuario.setText(dados.getString("logradouro"));
                        Editar_Usuario.et_complemento_usuario.setText(dados.getString("complemento"));
                        Editar_Usuario.et_bairro_usuario.setText(dados.getString("bairro"));
                        Editar_Usuario.et_cidade_usuario.setText(dados.getString("localidade"));
                        Editar_Usuario.sp_estado.setSelection(ToolBox_Verivifacao_Campos.Funcao_setSelectionSpinnerByText(Editar_Usuario.sp_estado, dados.getString("uf")));
                        Editar_Usuario.et_numero_usuario.requestFocus();

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 1:

                if (status == 1) {

                    MsgUtil.Funcao_TOAST(activity, resultado);
                    Conexao.Fecha_Pagina_Loading();
//                    RecoverPassActivity.LimpaCampos();
                    ActivityUtil.callActivity(activity, LoginActivity.class, true);

                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

                break;

            case 2:

                if (status == 1) {

                    try {

//                        String creditos_usuario = objeto.getString("creditos");
//                        JSONObject dados_clientes = objeto.getJSONObject("dados");
//
//                        ArrayList<HMAux> raw_arrayList_dados = new ArrayList<>();
//                        raw_arrayList_dados.clear();
//
//                        String id_usuario = dados_clientes.getString("id_usuario");
//                        String nome_usuario = dados_clientes.getString("nome_usuario");
//                        String email_usuario = dados_clientes.getString("email_usuario");
//                        String fk_grupo_usuario = dados_clientes.getString("fk_grupo_usuario");
//                        String ativo_usuario = dados_clientes.getString("ativo_usuario");

                        ChamaLogin(objeto, activity, "1");
                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {

                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    if (Parametros.containsKey("facebook_usuario")) {
//                        Conexao.Fecha_Pagina_Loading("Logando com faceBOOOOK", -1, -1);
                        Conexao.Fecha_Pagina_Loading();
                        LoginActivity.sendDataFacebook();
                    } else {
                        Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                    }

                } else if (status == -2) {
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -2);
                } else if (status == -3) {
                    try {
                        LoginActivity.idUser = objeto.getString("id_usuario");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -3);
                }

//                else if (status == -5) {
//                    try {
//
//                        JSONObject usuarios = objeto.getJSONObject("usuarios");
//                        LoginActivity.dataHMAux = usuarios;
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
////                    Log.e("Cadastro incompleto WS", resultado);
////                    Conexao.Fecha_Pagina_Loading(resultado, -5, -1);
//                    // }
//
//                }

                else {
                    Funcao_STATUSException(status);
                }

                break;

            case 201:
                Conexao.Fecha_Pagina_Loading_Com_Login(resultado, -1, 201, Parametros);
                break;
            case 3:

                if (status == 1) {

//                    Funcao_MostraResultado(activity, resultado);
                    MsgUtil.Funcao_TOAST(activity, resultado);
                    SigninActivity.LimpaCampos();
                    Conexao.Fecha_Pagina_Loading();

                    Intent login = new Intent(activity, LoginActivity.class);

                    if (!Parametros.containsKey("facebook_usuario")) {
                        login.putExtra(SigninActivity.keyName, Parametros.get("email_usuario"));
                        login.putExtra(SigninActivity.keyPass, Parametros.get("kfil"));
                    }

                    ActivityUtil.callActivity(activity, login, true);

                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }


                break;
            case 4:

                if (status == 1) {
                    JSONObject dados = null;
                    try {
                        JSONObject jsonObject = objeto.getJSONObject("usuarios");

                        Editar_Usuario.setUserData(jsonObject);
//                        String name = jsonObject.getString("nome_usuario");
//
//                        Editar_Usuario.et_nome_usuario.setText(name);
//                        Editar_Usuario.et_email_usuario.setText(jsonObject.getString("email_usuario"));
//                        Editar_Usuario.et_rg_usuario.setText(jsonObject.getString("rg_usuario"));
//                        Editar_Usuario.et_cpf_usuario.setText(jsonObject.getString("cpf_usuario"));
//                        Editar_Usuario.et_celular_usuario.setText(jsonObject.getString("celular_usuario"));
//                        Editar_Usuario.et_telefone_usuario.setText(jsonObject.getString("telefone_usuario"));
//                        Editar_Usuario.et_endereco_usuario.setText(jsonObject.getString("logradouro_usuario"));
//                        Editar_Usuario.et_cep_usuario.setText(jsonObject.getString("cep_usuario"));
//                        Editar_Usuario.et_numero_usuario.setText(jsonObject.getString("numero_usuario"));
//                        Editar_Usuario.et_complemento_usuario.setText(jsonObject.getString("complemento_usuario"));
//                        Editar_Usuario.et_bairro_usuario.setText(jsonObject.getString("bairro_usuario"));
//                        Editar_Usuario.et_cidade_usuario.setText(jsonObject.getString("cidade_usuario"));
//
//                        Editar_Usuario.sp_estado.setSelection(ToolBox_Verivifacao_Campos.Funcao_verificaSpinnerVazio(jsonObject.getString("estado_usuario")));
//                        Editar_Usuario.sp_sexo.setSelection(ToolBox_Verivifacao_Campos.Funcao_verificaSpinnerVazioSex(jsonObject.getString("sexo_usuario")));

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;
            case 401:

                if (status == 1) {
                    JSONObject dados = null;
                    try {
                        JSONObject jsonObject = objeto.getJSONObject("usuarios");

                        String name = jsonObject.getString("nome_usuario");

                        Editar_Senha.et_nome_usuario.setText(name);

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;
            case 5:

                if (status == 1) {
//                    JSONObject dados = null;
//                    try {
//                        dados = objeto.getJSONObject("dados");
//                        String id_usuario = dados.getString("id_usuario");
                    Conexao.Fecha_Pagina_Loading(resultado, -1, 5);

//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Funcao_JSONException(2, e);
//                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 6:
                if (status == 1) {

                    try {

                        if (objeto.getJSONArray("categorias").length() == 0) {
                            Cadastra_Leilao.Funcao_FinalizaCategoria();
                        } else {
                            Cadastra_Leilao.categoria_json = objeto.getJSONArray("categorias");
                            Cadastra_Leilao.Funcao_ListaCategoria();
                        }

                        if (Parametros != null) {
                            Adapter_Dialog_Categoria adapter_dialog = new Adapter_Dialog_Categoria(activity,
                                    R.layout.celula_categorias,
                                    Cadastra_Leilao.dados_categorias
                            );
                            Cadastra_Leilao.listView_catgorias.setAdapter(adapter_dialog);
                        }
//                        Cadastra_Leilao.creditos = objeto.getString("creditos");
                        Conexao.Fecha_Pagina_Loading();

//                        Log.w("Categorias", Cadastra_Leilao.categoria_json.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 601:
                if (status == 1) {

                    try {

                        if (objeto.getJSONArray("categorias").length() <= 0) {
                            Fragment_Categorias.Funcao_FinalizaCategoria();
                            Fragment_Categorias.removeArray();
                        } else {
                            Fragment_Categorias.categoria_json = objeto.getJSONArray("categorias");
                            Fragment_Categorias.Funcao_ListaCategoria();

                            if (Parametros != null && Parametros.containsKey("add")) {
                                Fragment_Categorias.addBackButton();
                            }
                        }

                        if (Parametros != null) {

                            if (Parametros.containsKey("back")) {
                                Fragment_Categorias.removeArray();
                            }

                            Adapter_Dialog_Categoria adapter_dialog = new Adapter_Dialog_Categoria(activity,
                                    R.layout.celula_categorias,
                                    Fragment_Categorias.dados_categorias
                            );
                            Fragment_Categorias.listView_catgorias.setAdapter(adapter_dialog);

                            Dados_Ws(17, "quantidade_por_pagina=" + Parametros.get("quantidade_por_pagina") + "&pagina=" + Parametros.get("pagina") + "&categoria=" + Parametros.get("id"), null, activity);
                        }
//                        Cadastra_Leilao.creditos = objeto.getString("creditos");
                        Conexao.Fecha_Pagina_Loading();


//                        Log.w("Categorias", Cadastra_Leilao.categoria_json.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 7:
//
                if (status == 1) {
                    JSONObject dados = null;
                    try {
                        String id_produto = objeto.getString("id_produto");
//                        String id_usuario = dados.getString("id_usuario");
//                        Conexao.Fecha_Pagina_Loading();
//                    Conexao.Fecha_Pagina_Loading(resultado, -1, 7);
                        Intent intent = new Intent(activity, Activity_Ativar_Leilao.class);
                        intent.putExtra(HMAux.INTENT_TYPE, 3);
                        intent.putExtra(HMAux.ID_PROCUTO, id_produto);
                        intent.putExtra(HMAux.NOME_PRODUTO, Parametros.get("nome_produto"));
                        ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, true, false, "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }
//

                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

                Funcao_MostraResultado(activity, resultado);

                break;

            case 8:

                if (status == 1) {
                    JSONArray dados = null;
                    try {
                        dados = objeto.getJSONArray("produtos");

                        Ativar_Leilao.arrayList_dados.clear();

                        for (int i = 0; i < dados.length(); i++) {

                            JSONObject arrayInterno = dados.getJSONObject(i);
//
//                            String nome_produto = arrayInterno.getString("nome_produto");
//                            String descricao_produto = arrayInterno.getString("descricao_produto");
//                            String nome_categoria = arrayInterno.getString("nome_categoria");

                            JSONArray imagems = arrayInterno.getJSONArray("imagens");

                            HMAux hmAux = new HMAux();
                            //
                            // Imita mini-registro
                            hmAux.put(HMAux.ID, String.valueOf(i));
                            hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto")); // Primary Key
                            hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                            hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                            hmAux.put(HMAux.NOME_CATEGORIA, arrayInterno.getString("nome_categoria"));
                            hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                            if (imagems.length() > 0) {
                                hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                            } else {
                                hmAux.put(HMAux.IMAGENS, "");
                            }

                            //
                            Ativar_Leilao.arrayList_dados.add(hmAux);

//
                        }

                        Ativar_Leilao.Set_Dados_Array();

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;
            case 9:

                if (status == 1) {

                    Conexao.Fecha_Pagina_Loading(resultado, -1, 9);


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

                Funcao_MostraResultado(activity, resultado);

                break;

            case 10:
                if (status == 1) {
                    JSONArray dados = null;
                    try {
                        dados = objeto.getJSONArray("leiloes");

                        int pageNumber = Fragment_Inicio.pageNumber;
                        if (pageNumber <= 1) {
                            Fragment_Inicio.arrayList_dados.clear();
                        } else {
                            Fragment_Inicio.arrayList_dados_NextPage.clear();
                        }

                        if (dados.length() > 0) {
                            Fragment_Inicio.pageNumber++;
                        }
                        for (int i = 0; i < dados.length(); i++) {

                            JSONObject arrayInterno = dados.getJSONObject(i);
//
//                            String nome_produto = arrayInterno.getString("nome_produto");
//                            String descricao_produto = arrayInterno.getString("descricao_produto");
//                            String nome_categoria = arrayInterno.getString("nome_categoria");

                            JSONArray imagems = arrayInterno.getJSONArray("imagens");

                            HMAux hmAux = new HMAux();

                            // Imita mini-registro
                            hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                            hmAux.put(HMAux.ID_LEILAO, arrayInterno.getString("id_leilao"));
                            hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
                            hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                            hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                            hmAux.put(HMAux.VALOR_MINIMO, arrayInterno.getString("valor_minimo"));
                            hmAux.put(HMAux.LANCE_MINIMO, arrayInterno.getString("lance_minimo"));
                            hmAux.put(HMAux.INGRESSOS, arrayInterno.getString("ingressos"));
                            hmAux.put(HMAux.DATA_INICIO, arrayInterno.getString("data_inicio"));
                            hmAux.put(HMAux.DATA_FIM_PREVISTO, arrayInterno.getString("data_fim_previsto"));
                            hmAux.put(HMAux.NOME_USUARIO, arrayInterno.getString("nome_usuario"));
                            hmAux.put(HMAux.ESTADO_USUARIO, arrayInterno.getString("estado_usuario"));
                            hmAux.put(HMAux.AVALIACAO, arrayInterno.getString("avaliacao"));
                            hmAux.put(HMAux.PRODUTO_USADO, arrayInterno.getString("produto_usado"));
                            hmAux.put(HMAux.FAVORITO, arrayInterno.getString("favorito"));
                            hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                            if (imagems.length() > 0) {
                                hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                                Log.w("Array IMG", imagems.toString());
                            } else {
                                hmAux.put(HMAux.IMAGENS, "");
                            }

                            if (pageNumber <= 1) {
                                Fragment_Inicio.arrayList_dados.add(hmAux);
                            } else {
                                Fragment_Inicio.arrayList_dados_NextPage.add(hmAux);
                            }

                        }

                        if (pageNumber <= 1) {
                            Fragment_Inicio.Set_Dados_Array();
                        } else {
                            Fragment_Inicio.addDataArray();
                        }

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

//            case 10:
//                if (status == 1) {
//                    JSONArray dados = null;
//                    try {
//                        dados = objeto.getJSONArray("leiloes");
//
//                        Fragment_Inicio.arrayList_dados.clear();
//
//                        for (int i = 0; i < dados.length(); i++) {
//
//                            JSONObject arrayInterno = dados.getJSONObject(i);
////
////                            String nome_produto = arrayInterno.getString("nome_produto");
////                            String descricao_produto = arrayInterno.getString("descricao_produto");
////                            String nome_categoria = arrayInterno.getString("nome_categoria");
//
//                            JSONArray imagems = arrayInterno.getJSONArray("imagens");
//
//                            HMAux hmAux = new HMAux();
//                            //
//                            // Imita mini-registro
//                            hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
//                            hmAux.put(HMAux.ID_LEILAO, arrayInterno.getString("id_leilao"));
//                            hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
//                            hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
//                            hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
//                            hmAux.put(HMAux.VALOR_MINIMO, arrayInterno.getString("valor_minimo"));
//                            hmAux.put(HMAux.LANCE_MINIMO, arrayInterno.getString("lance_minimo"));
//                            hmAux.put(HMAux.INGRESSOS, arrayInterno.getString("ingressos"));
//                            hmAux.put(HMAux.DATA_INICIO, arrayInterno.getString("data_inicio"));
//                            hmAux.put(HMAux.DATA_FIM_PREVISTO, arrayInterno.getString("data_fim_previsto"));
//                            hmAux.put(HMAux.FAVORITO, arrayInterno.getString("favorito"));
//                            hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());
//
//                            if (imagems.length() > 0) {
//                                hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
//                                Log.w("Array IMG", imagems.toString());
//                            } else {
//                                hmAux.put(HMAux.IMAGENS, "");
//                            }
//
//                            Fragment_Inicio.arrayList_dados.add(hmAux);
//
//                        }
//
//                        Fragment_Inicio.Set_Dados_Array();
//
//                        Conexao.Fecha_Pagina_Loading();
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Funcao_JSONException(2, e);
//                    }
//
//
//                } else if (status == 0) {
//                    // -1 significa que nao faz nada , somente mostra o load e o erro
//                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
//                } else {
//                    Funcao_STATUSException(status);
//                }
//
////                Funcao_MostraResultado(activity, resultado);
//
//                break;

            case 101:
                if (status == 1) {
                    JSONArray dados = null;
                    try {
                        dados = objeto.getJSONArray("leiloes");

                        Fragment_Inicio.arrayList_dados.clear();

                        for (int i = 0; i < dados.length(); i++) {

                            JSONObject arrayInterno = dados.getJSONObject(i);
//
//                            String nome_produto = arrayInterno.getString("nome_produto");
//                            String descricao_produto = arrayInterno.getString("descricao_produto");
//                            String nome_categoria = arrayInterno.getString("nome_categoria");

                            JSONArray imagems = arrayInterno.getJSONArray("imagens");

                            HMAux hmAux = new HMAux();
                            //
                            // Imita mini-registro
                            hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                            hmAux.put(HMAux.ID_LEILAO, arrayInterno.getString("id_leilao"));
                            hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
                            hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                            hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                            hmAux.put(HMAux.VALOR_MINIMO, arrayInterno.getString("valor_minimo"));
                            hmAux.put(HMAux.LANCE_MINIMO, arrayInterno.getString("lance_minimo"));
                            hmAux.put(HMAux.INGRESSOS, arrayInterno.getString("ingressos"));
                            hmAux.put(HMAux.DATA_INICIO, arrayInterno.getString("data_inicio"));
                            hmAux.put(HMAux.DATA_FIM_PREVISTO, arrayInterno.getString("data_fim_previsto"));
                            hmAux.put(HMAux.FAVORITO, arrayInterno.getString("favorito"));
                            hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                            if (imagems.length() > 0) {
                                hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                            } else {
                                hmAux.put(HMAux.IMAGENS, "");
                            }

                            Fragment_Inicio.arrayList_dados.add(hmAux);

//                            NotificationUtils.Funcao_Nortificacao(context, Parametros.get("titulo"), Parametros.get("mensagem"), hmAux);
                        }

                        Fragment_Inicio.Set_Dados_Array();

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);


                break;

            case 102:
                if (status == 1) {
                    JSONArray dados = null;
                    try {
                        dados = objeto.getJSONArray("leiloes");
                        HMAux hmAux = new HMAux();

                        for (int i = 0; i < dados.length(); i++) {

                            JSONObject arrayInterno = dados.getJSONObject(0);
//
//                            String nome_produto = arrayInterno.getString("nome_produto");
//                            String descricao_produto = arrayInterno.getString("descricao_produto");
//                            String nome_categoria = arrayInterno.getString("nome_categoria");

                            JSONArray imagems = arrayInterno.getJSONArray("imagens");

                            // Imita mini-registro
                            hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                            hmAux.put(HMAux.ID_LEILAO, arrayInterno.getString("id_leilao"));
                            hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
                            hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                            hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                            hmAux.put(HMAux.VALOR_MINIMO, arrayInterno.getString("valor_minimo"));
                            hmAux.put(HMAux.LANCE_MINIMO, arrayInterno.getString("lance_minimo"));
                            hmAux.put(HMAux.INGRESSOS, arrayInterno.getString("ingressos"));
                            hmAux.put(HMAux.DATA_INICIO, arrayInterno.getString("data_inicio"));
                            hmAux.put(HMAux.DATA_FIM_PREVISTO, arrayInterno.getString("data_fim_previsto"));
                            hmAux.put(HMAux.NOME_USUARIO, arrayInterno.getString("nome_usuario"));
                            hmAux.put(HMAux.ESTADO_USUARIO, arrayInterno.getString("estado_usuario"));
                            hmAux.put(HMAux.AVALIACAO, arrayInterno.getString("avaliacao"));
                            hmAux.put(HMAux.PRODUTO_USADO, arrayInterno.getString("produto_usado"));
                            hmAux.put(HMAux.FAVORITO, arrayInterno.getString("favorito"));
                            hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                            if (imagems.length() > 0) {
                                hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                                Log.w("Array IMG", imagems.toString());
                            } else {
                                hmAux.put(HMAux.IMAGENS, "");
                            }

                        }

                        LeilaoNortification.iniDados(hmAux);

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 11:

                if (status == 1) {
//                    JSONObject dados = null;
//                    try {
//                        dados = objeto.getJSONObject("dados");
//                        String id_usuario = dados.getString("id_usuario");
                    Conexao.Fecha_Pagina_Loading();

//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Funcao_JSONException(2, e);
//                    }

                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, 11);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, "Usuario na sala de teste");

                break;

            case 12:
                if (status == 1) {
                    JSONArray dados = null;
                    try {
                        dados = objeto.getJSONArray("pacotes");

                        Planos.arrayList_dados.clear();

                        for (int i = 0; i < dados.length(); i++) {

                            JSONObject arrayInterno = dados.getJSONObject(i);

                            HMAux hmAux = new HMAux();
                            //
                            // Imita mini-registro
                            hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                            hmAux.put(HMAux.ID_PACOTE_INGRESSO, arrayInterno.getString(HMAux.ID_PACOTE_INGRESSO));
                            hmAux.put(HMAux.DESCRICAO_PACOTE, arrayInterno.getString(HMAux.DESCRICAO_PACOTE));
                            hmAux.put(HMAux.QUANTIDADE_INGRESSO, arrayInterno.getString(HMAux.QUANTIDADE_INGRESSO));
                            hmAux.put(HMAux.VALOR_PACOTE, arrayInterno.getString(HMAux.VALOR_PACOTE));

                            Planos.arrayList_dados.add(hmAux);
                        }

                        Planos.Set_Dados_Array();

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;


            case 13:

                if (status == 1) {
//                    JSONObject dados = null;
//                    try {
//                        dados = objeto.getJSONObject("dados");
//                        String id_usuario = dados.getString("id_usuario");
                    Conexao.Fecha_Pagina_Loading();

//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Funcao_JSONException(2, e);
//                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 14:
                if (status == 1) {
                    JSONArray dados = null;

                    try {
                        dados = objeto.getJSONArray("leiloes");

                        int pageNumber = Fragment_Favoritos.pageNumber;
                        if (pageNumber <= 1) {
                            Fragment_Favoritos.arrayList_dados.clear();
                        } else {
                            Fragment_Favoritos.arrayList_dados_NextPage.clear();
                        }

                        if (dados.length() > 0) {
                            Fragment_Favoritos.pageNumber++;

                            for (int i = 0; i < dados.length(); i++) {

                                JSONObject arrayInterno = dados.getJSONObject(i);

                                JSONArray imagems = arrayInterno.getJSONArray("imagens");

                                HMAux hmAux = new HMAux();
                                //
                                // Imita mini-registro
                                hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                                hmAux.put(HMAux.ID_LEILAO, arrayInterno.getString("id_leilao"));
                                hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
                                hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                                hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                                hmAux.put(HMAux.VALOR_MINIMO, arrayInterno.getString("valor_minimo"));
                                hmAux.put(HMAux.LANCE_MINIMO, arrayInterno.getString("lance_minimo"));
                                hmAux.put(HMAux.INGRESSOS, arrayInterno.getString("ingressos"));
                                hmAux.put(HMAux.DATA_INICIO, arrayInterno.getString("data_inicio"));
                                hmAux.put(HMAux.DATA_FIM_PREVISTO, arrayInterno.getString("data_fim_previsto"));
                                hmAux.put(HMAux.NOME_USUARIO, arrayInterno.getString("nome_usuario"));
                                hmAux.put(HMAux.AVALIACAO, arrayInterno.getString("avaliacao"));
                                hmAux.put(HMAux.PRODUTO_USADO, arrayInterno.getString("produto_usado"));
                                hmAux.put(HMAux.FAVORITO, arrayInterno.getString("favorito"));
                                hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                                if (imagems.length() > 0) {
                                    hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                                    Log.w("Array IMG", imagems.toString());
                                } else {
                                    hmAux.put(HMAux.IMAGENS, "");
                                }

                                if (pageNumber <= 1) {
                                    Fragment_Favoritos.arrayList_dados.add(hmAux);
                                } else {
                                    Fragment_Favoritos.arrayList_dados_NextPage.add(hmAux);
                                }
                            }

                            if (pageNumber <= 1) {
                                Fragment_Favoritos.Set_Dados_Array();
                            } else {
                                Fragment_Favoritos.addDataArray();
                            }
                        } else if (pageNumber <= 1) {
//                            MsgUtil.Funcao_MSG(activity , "Sem Favoritos");
//                            Conexao.Fecha_Pagina_Loading("Sem Arremates até o momento", -1, 9);
                        } else {
                            Fragment_Favoritos.addDataArray();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);
                break;

            case 15:
                if (status == 1) {
                    JSONArray dados = null;
                    try {
                        dados = objeto.getJSONArray("leiloes");

                        Fragment_Leiloes.arrayList_dados.clear();

                        for (int i = 0; i < dados.length(); i++) {

                            JSONObject arrayInterno = dados.getJSONObject(i);
//
//                            String nome_produto = arrayInterno.getString("nome_produto");
//                            String descricao_produto = arrayInterno.getString("descricao_produto");
//                            String nome_categoria = arrayInterno.getString("nome_categoria");

                            JSONArray imagems = arrayInterno.getJSONArray("imagens");

                            HMAux hmAux = new HMAux();
                            //
                            // Imita mini-registro
                            hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                            hmAux.put(HMAux.ID_LEILAO, arrayInterno.getString("id_leilao"));
                            hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
                            hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                            hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                            hmAux.put(HMAux.VALOR_MINIMO, arrayInterno.getString("valor_minimo"));
                            hmAux.put(HMAux.LANCE_MINIMO, arrayInterno.getString("lance_minimo"));
                            hmAux.put(HMAux.INGRESSOS, arrayInterno.getString("ingressos"));
                            hmAux.put(HMAux.DATA_INICIO, arrayInterno.getString("data_inicio"));
                            hmAux.put(HMAux.DATA_FIM_PREVISTO, arrayInterno.getString("data_fim_previsto"));
                            hmAux.put(HMAux.STATUS_LEILAO, arrayInterno.getString("status_leilao"));
                            hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                            if (imagems.length() > 0) {
                                hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                            } else {
                                hmAux.put(HMAux.IMAGENS, "");
                            }

                            Fragment_Leiloes.arrayList_dados.add(hmAux);
//
                        }

                        Fragment_Leiloes.Set_Dados_Array();

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);


                break;

            case 16:

                if (status == 1) {
                    Conexao.Fecha_Pagina_Loading(resultado, -1, 16);

                    ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
                    prefs.setImgUser(prefs.getImgUser());

                    ToolBox_SharedPrefs_Usuario prefs2 = new ToolBox_SharedPrefs_Usuario(activity);
                    prefs2.setURL_IMAGE_FACEBOOK("");

                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }
                Funcao_MostraResultado(activity, resultado);
                break;

            case 17:

                if (status == 1) {
                    JSONArray dados = null;

                    try {
                        dados = objeto.getJSONArray("leiloes");

                        int pageNumber = Fragment_Categorias.pageNumber;
                        if (pageNumber <= 1) {
                            Fragment_Categorias.arrayList_dados.clear();
                        } else {
                            Fragment_Categorias.arrayList_dados_NextPage.clear();
                        }

                        if (dados.length() > 0) {
                            Fragment_Categorias.pageNumber++;

                            if (Fragment_Categorias.modalCategShow) {
                                Fragment_Categorias.animationHide();
                            }


                            for (int i = 0; i < dados.length(); i++) {

                                JSONObject arrayInterno = dados.getJSONObject(i);

                                JSONArray imagems = arrayInterno.getJSONArray("imagens");

                                HMAux hmAux = new HMAux();
                                //
                                // Imita mini-registro
                                hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                                hmAux.put(HMAux.ID_LEILAO, arrayInterno.getString("id_leilao"));
                                hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
                                hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                                hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                                hmAux.put(HMAux.VALOR_MINIMO, arrayInterno.getString("valor_minimo"));
                                hmAux.put(HMAux.LANCE_MINIMO, arrayInterno.getString("lance_minimo"));
                                hmAux.put(HMAux.INGRESSOS, arrayInterno.getString("ingressos"));
                                hmAux.put(HMAux.DATA_INICIO, arrayInterno.getString("data_inicio"));
                                hmAux.put(HMAux.DATA_FIM_PREVISTO, arrayInterno.getString("data_fim_previsto"));
                                hmAux.put(HMAux.NOME_USUARIO, arrayInterno.getString("nome_usuario"));
                                hmAux.put(HMAux.AVALIACAO, arrayInterno.getString("avaliacao"));
                                hmAux.put(HMAux.PRODUTO_USADO, arrayInterno.getString("produto_usado"));
                                hmAux.put(HMAux.FAVORITO, arrayInterno.getString("favorito"));
                                hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                                if (imagems.length() > 0) {
                                    hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                                    Log.w("Array IMG", imagems.toString());
                                } else {
                                    hmAux.put(HMAux.IMAGENS, "");
                                }

                                if (pageNumber <= 1) {
                                    Fragment_Categorias.arrayList_dados.add(hmAux);
                                } else {
                                    Fragment_Categorias.arrayList_dados_NextPage.add(hmAux);
                                }
                            }

                            if (pageNumber <= 1) {
                                Fragment_Categorias.Set_Dados_Array();
                            } else {
                                Fragment_Categorias.addDataArray();
                            }
                        } else if (pageNumber <= 1) {
//                                MsgUtil.Funcao_MSG(activity , "Sem Favoritos");
//                            Conexao.Fecha_Pagina_Loading("Sem Arremates até o momento", -1, 9);
                        } else {
                            Fragment_Categorias.addDataArray();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 18:
                if (status == 1) {
                    JSONArray dados = null;

                    try {
                        dados = objeto.getJSONArray("leiloes");

                        int pageNumber = Pagina_Busca.pageNumber;
                        if (pageNumber <= 1) {
                            Pagina_Busca.arrayList_dados.clear();
                        } else {
                            Pagina_Busca.arrayList_dados_NextPage.clear();
                        }

                        if (dados.length() > 0) {
                            Pagina_Busca.pageNumber++;

                            for (int i = 0; i < dados.length(); i++) {

                                JSONObject arrayInterno = dados.getJSONObject(i);

                                JSONArray imagems = arrayInterno.getJSONArray("imagens");

                                HMAux hmAux = new HMAux();
                                //
                                // Imita mini-registro
                                hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                                hmAux.put(HMAux.ID_LEILAO, arrayInterno.getString("id_leilao"));
                                hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
                                hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                                hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                                hmAux.put(HMAux.VALOR_MINIMO, arrayInterno.getString("valor_minimo"));
                                hmAux.put(HMAux.LANCE_MINIMO, arrayInterno.getString("lance_minimo"));
                                hmAux.put(HMAux.INGRESSOS, arrayInterno.getString("ingressos"));
                                hmAux.put(HMAux.DATA_INICIO, arrayInterno.getString("data_inicio"));
                                hmAux.put(HMAux.DATA_FIM_PREVISTO, arrayInterno.getString("data_fim_previsto"));
                                hmAux.put(HMAux.NOME_USUARIO, arrayInterno.getString("nome_usuario"));
                                hmAux.put(HMAux.AVALIACAO, arrayInterno.getString("avaliacao"));
                                hmAux.put(HMAux.PRODUTO_USADO, arrayInterno.getString("produto_usado"));
                                hmAux.put(HMAux.FAVORITO, arrayInterno.getString("favorito"));
                                hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                                if (imagems.length() > 0) {
                                    hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                                    Log.w("Array IMG", imagems.toString());
                                } else {
                                    hmAux.put(HMAux.IMAGENS, "");
                                }

                                if (pageNumber <= 1) {
                                    Pagina_Busca.arrayList_dados.add(hmAux);
                                } else {
                                    Pagina_Busca.arrayList_dados_NextPage.add(hmAux);
                                }
                            }

                            if (pageNumber <= 1) {
                                Pagina_Busca.Set_Dados_Array();
                            } else {
                                Pagina_Busca.addDataArray();
                            }
                        } else if (pageNumber <= 1) {
//                                MsgUtil.Funcao_MSG(activity , "Sem Favoritos");
//                            Conexao.Fecha_Pagina_Loading("Sem Arremates até o momento", -1, 9);
                        } else {
                            Pagina_Busca.addDataArray();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);


                break;

            case 19:
                if (status == 1) {
                    JSONArray dados = null;

                    try {
                        dados = objeto.getJSONArray("lances");

                        if (dados.length() > 0) {
                            Activity_Lances.arrayList_dados.clear();

                            for (int i = 0; i < dados.length(); i++) {

                                JSONObject arrayInterno = dados.getJSONObject(i);

//                            String nome_produto = arrayInterno.getString("nome_produto");
//                            String descricao_produto = arrayInterno.getString("descricao_produto");
//                            String nome_categoria = arrayInterno.getString("nome_categoria");

                                HMAux hmAux = new HMAux();
                                //
                                // Imita mini-registro
                                hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                                hmAux.put(HMAux.ID_LANCE, arrayInterno.getString("id_leilao"));
                                hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                                hmAux.put(HMAux.VALOR, arrayInterno.getString("valor"));
                                hmAux.put(HMAux.DATA, arrayInterno.getString("data"));

                                Activity_Lances.arrayList_dados.add(hmAux);
//
                            }

                            Activity_Lances.Set_Dados_Array();

                            Conexao.Fecha_Pagina_Loading();
                        } else {
                            Conexao.Fecha_Pagina_Loading("Sem lances até o momento", -1, 9);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;


            case 20:

                if (status == 1) {
//                    JSONObject dados = null;
//                    try {
                    HMAux hmAux = Activity_Arremates.arrayList_dados.get(Integer.parseInt(Parametros.get("position")));
                    hmAux.put(HMAux.AVALIADO, "5");
                    Conexao.Fecha_Pagina_Loading();
                    MsgUtil.Funcao_MSG(activity, resultado);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Funcao_JSONException(2, e);
//                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

                Funcao_MostraResultado(activity, resultado);

                break;

            case 21:
                if (status == 1) {
                    JSONArray dados = null;

                    try {
                        dados = objeto.getJSONArray("leiloes");

                        int pageNumber = Activity_Arremates.pageNumber;
                        if (pageNumber <= 1) {
                            Activity_Arremates.arrayList_dados.clear();
                        } else {
                            Activity_Arremates.arrayList_dados_NextPage.clear();
                        }

                        if (dados.length() > 0) {
                            Activity_Arremates.pageNumber++;

                            for (int i = 0; i < dados.length(); i++) {

                                JSONObject arrayInterno = dados.getJSONObject(i);

                                JSONArray imagems = arrayInterno.getJSONArray("imagens");

                                HMAux hmAux = new HMAux();
                                //
                                // Imita mini-registro
                                hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                                hmAux.put(HMAux.ID_LEILAO, arrayInterno.getString("id_leilao"));
                                hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
                                hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                                hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                                hmAux.put(HMAux.VALOR_MINIMO, arrayInterno.getString("valor_minimo"));
                                hmAux.put(HMAux.DATA_INICIO, arrayInterno.getString("data_inicio"));
                                hmAux.put(HMAux.DATA_FIM_PREVISTO, arrayInterno.getString("data_fim_previsto"));
                                hmAux.put(HMAux.AVALIADO, arrayInterno.getString("avaliado"));
                                hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                                if (imagems.length() > 0) {
                                    hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                                    Log.w("Array IMG", imagems.toString());
                                } else {
                                    hmAux.put(HMAux.IMAGENS, "");
                                }

                                if (pageNumber <= 1) {
                                    Activity_Arremates.arrayList_dados.add(hmAux);
                                } else {
                                    Activity_Arremates.arrayList_dados_NextPage.add(hmAux);
                                }
                            }

                            if (pageNumber <= 1) {
                                Activity_Arremates.Set_Dados_Array();
                            } else {
                                Activity_Arremates.addDataArray();
                            }
                        } else if (pageNumber <= 1) {
                            Conexao.Fecha_Pagina_Loading("Sem Arremates até o momento", -1, 9);
                        } else {
                            Activity_Arremates.addDataArray();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 22:
                if (status == 1) {
                    JSONArray dados = null;
                    try {

                        if (objeto.has("pontos_afiliados")) {
                            int num = objeto.getInt("pontos_afiliados");
                            Afiliados.tvCredits.setText(ToolBox_Verivifacao_Campos.setTextWithQuanty(num, "ponto", "pontos"));
                        }

                        dados = objeto.getJSONArray("afiliados");

                        int pageNumber = Afiliados.pageNumber;
                        if (pageNumber <= 1) {
                            Afiliados.arrayList_dados.clear();
                        } else {
                            Afiliados.arrayList_dados_NextPage.clear();
                        }

                        if (dados.length() > 0) {
                            Afiliados.pageNumber++;

                            for (int i = 0; i < dados.length(); i++) {

                                JSONObject arrayInterno = dados.getJSONObject(i);

                                HMAux hmAux = new HMAux();
                                //
                                // Imita mini-registro
                                hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                                hmAux.put(HMAux.NOME_USUARIO, arrayInterno.getString("nome_usuario"));
                                hmAux.put(HMAux.CRIADO_EM, arrayInterno.getString("criado_em"));

                                if (pageNumber <= 1) {
                                    Afiliados.arrayList_dados.add(hmAux);
                                } else {
                                    Afiliados.arrayList_dados_NextPage.add(hmAux);
                                }
                            }

                            if (pageNumber <= 1) {
                                Afiliados.Set_Dados_Array();
                            } else {
                                Afiliados.addDataArray();
                            }
                        } else if (pageNumber <= 1) {
                            Afiliados.noData();
                        } else {
                            Afiliados.addDataArray();
                        }

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);
                break;


            case 23:
                if (status == 1) {

                    JSONObject dados = null;
                    try {
                        if (objeto.has("pontos_afiliados")) {
                            int num = objeto.getInt("pontos_afiliados");
                            Recompensa.points = num;
                            Recompensa.tvCredits.setText(ToolBox_Verivifacao_Campos.setTextWithQuanty(num, "ponto", "pontos"));
                        }

                        dados = objeto.getJSONObject("recompensas");

                        JSONArray dataPacotes = null;
                        JSONArray dataProdutos = null;

                        if (dados.has("pacotes")) {
                            dataPacotes = dados.getJSONArray("pacotes");
                        }

                        if (dados.has("produtos")) {
                            dataProdutos = dados.getJSONArray("produtos");
                        }

                        int pageNumber = Recompensa.pageNumber;
                        if (pageNumber <= 1) {
                            Recompensa.arrayList_dados.clear();
                        } else {
                            Recompensa.arrayList_dados_NextPage.clear();
                        }

                        if (dados.length() > 0) {
                            Recompensa.pageNumber++;
                        }

//                        if (dados.length() > 0) {
//                            Recompensa.pageNumber++;

                        for (int i = 0; i < dataPacotes.length(); i++) {

                            JSONObject arrayInterno = dataPacotes.getJSONObject(i);

                            HMAux hmAux = new HMAux();
                            //
                            // Imita mini-registro
                            hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                            hmAux.put(HMAux.INTENT_TYPE, String.valueOf(1));// Primary Key
                            hmAux.put(HMAux.ID_PACOTE_INGRESSO, arrayInterno.getString("id_pacote_ingresso"));
                            hmAux.put(HMAux.DESCRICAO_PACOTE, arrayInterno.getString("descricao_pacote"));
                            hmAux.put(HMAux.QUANTIDADE_INGRESSO, arrayInterno.getString("quantidade_ingresso"));
                            hmAux.put(HMAux.VALOR_PACOTE, arrayInterno.getString("valor_pacote"));

                            if (pageNumber <= 1) {
                                Recompensa.arrayList_dados.add(hmAux);
                            } else {
                                Recompensa.arrayList_dados_NextPage.add(hmAux);
                            }
                        }

                        for (int i = 0; i < dataProdutos.length(); i++) {

                            JSONObject arrayInterno = dataProdutos.getJSONObject(i);

                            JSONArray imagems = arrayInterno.getJSONArray("imagens");

                            HMAux hmAux = new HMAux();
                            //
                            // Imita mini-registro
                            hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                            hmAux.put(HMAux.INTENT_TYPE, String.valueOf(2));// Primary Key
                            hmAux.put(HMAux.ID_PRODUTO, arrayInterno.getString("id_produto"));
                            hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                            hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                            hmAux.put(HMAux.NOME_CATEGORIA, arrayInterno.getString("nome_categoria"));
                            hmAux.put(HMAux.PRODUTO_USADO, arrayInterno.getString("produto_usado"));
                            hmAux.put(HMAux.PRODUTO_RECOMPENSA_CUSTO, arrayInterno.getString("produto_recompensa_custo"));

                            hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                            if (imagems.length() > 0) {
                                hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                            } else {
                                hmAux.put(HMAux.IMAGENS, "");
                            }

                            if (pageNumber <= 1) {
                                Recompensa.arrayList_dados.add(hmAux);
                            } else {
                                Recompensa.arrayList_dados_NextPage.add(hmAux);
                            }
                        }

                        if (pageNumber <= 1) {
                            Recompensa.Set_Dados_Array();
                        } else {
                            Recompensa.addDataArray();
                        }


//                        } else if (pageNumber <= 1) {
//                            Recompensa.noData();
//                        } else {
//                            Recompensa.addDataArray();
//                        }

                        Conexao.Fecha_Pagina_Loading();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 24:
                if (status == 1) {
                    try {

                        if (objeto.has("pontos_afiliados")) {
                            int num = objeto.getInt("pontos_afiliados");
                            Recompensa.points = num;
                            Recompensa.tvCredits.setText(ToolBox_Verivifacao_Campos.setTextWithQuanty(num, "ponto", "pontos"));
                        }

                        if (Parametros.containsKey(HMAux.QUANTIDADE_INGRESSO)) {
                            String num = Parametros.get(HMAux.QUANTIDADE_INGRESSO);
                            ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
                            prefs.addCredits(num);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }

                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

                MsgUtil.Funcao_MSG(activity, resultado);

                break;

            case 26:
                if (status == 1) {
                    JSONArray dados = null;

                    try {
                        dados = objeto.getJSONArray("produtos");

                        if (dados.length() > 0) {

                            for (int i = 0; i < dados.length(); i++) {

                                JSONObject arrayInterno = dados.getJSONObject(i);

                                JSONArray imagems = arrayInterno.getJSONArray("imagens");

                                HMAux hmAux = new HMAux();
                                //
                                // Imita mini-registro
                                hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                                hmAux.put(HMAux.ID_PROCUTO, arrayInterno.getString("id_produto"));
                                hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
                                hmAux.put(HMAux.DESCRICAO_PRODUTO, arrayInterno.getString("descricao_produto"));
                                hmAux.put(HMAux.NOME_CATEGORIA, arrayInterno.getString("nome_categoria"));
                                hmAux.put(HMAux.PRODUTO_USADO, arrayInterno.getString("produto_usado"));
                                hmAux.put(HMAux.PRODUTO_RECOMPENSA_CUSTO, arrayInterno.getString("produto_recompensa_custo"));
                                hmAux.put(HMAux.IMAGENS_ARRAY, imagems.toString());

                                if (imagems.length() > 0) {
                                    hmAux.put(HMAux.IMAGENS, imagems.get(0).toString());
                                } else {
                                    hmAux.put(HMAux.IMAGENS, "");
                                }

                                Activity_Recompensas.arrayList_dados.add(hmAux);

                            }

                            Activity_Recompensas.Set_Dados_Array();

                        } else if (dados.length() <= 1) {
                            Conexao.Fecha_Pagina_Loading("Sem recompensas até o momento", -1, 9);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

            case 27:
                if (status == 1) {
                    JSONArray dados = null;

                    try {
                        dados = objeto.getJSONArray("vendas");

                        if (dados.length() > 0) {

                            for (int i = 0; i < dados.length(); i++) {

                                JSONObject arrayInterno = dados.getJSONObject(i);

                                HMAux hmAux = new HMAux();

                                // Imita mini-registro
                                hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
                                hmAux.put(HMAux.PREFERENCE, arrayInterno.getString("preference"));
                                hmAux.put(HMAux.DESCRICAO_PACOTE, arrayInterno.getString("descricao_pacote"));
                                hmAux.put(HMAux.UNIT_PRICE, arrayInterno.getString("unit_price"));
                                hmAux.put(HMAux.QUANTIDADE_INGRESSO, arrayInterno.getString("quantidade_ingresso"));
                                hmAux.put(HMAux.DATE_BUY, arrayInterno.getString("date_buy"));
                                hmAux.put(HMAux.STATUS, arrayInterno.getString("status"));
                                hmAux.put(HMAux.FINALIZADO, arrayInterno.getString("finalizado"));

                                Activity_Pagamento.arrayList_dados.add(hmAux);

                            }

                            Activity_Pagamento.Set_Dados_Array();

                        } else if (dados.length() <= 1) {
                            Conexao.Fecha_Pagina_Loading("Sem Pagamentos até o momento", -1, 9);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Funcao_JSONException(2, e);
                    }


                } else if (status == 0) {
                    // -1 significa que nao faz nada , somente mostra o load e o erro
                    Conexao.Fecha_Pagina_Loading(resultado, -1, -1);
                } else {
                    Funcao_STATUSException(status);
                }

//                Funcao_MostraResultado(activity, resultado);

                break;

        }
    }


}



