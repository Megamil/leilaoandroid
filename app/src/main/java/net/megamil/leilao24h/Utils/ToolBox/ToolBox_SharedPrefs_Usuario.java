package net.megamil.leilao24h.Utils.ToolBox;

import android.content.Context;
import android.content.SharedPreferences;

public class ToolBox_SharedPrefs_Usuario {

    public static final String CONSTANTE_SHAREDPREFS_DO_USUARIO = "splash24h_us";
    private final SharedPreferences Prefs;

    public ToolBox_SharedPrefs_Usuario(Context context) {
        Prefs = context.getSharedPreferences(CONSTANTE_SHAREDPREFS_DO_USUARIO, Context.MODE_PRIVATE);
    }

    //Função para Limpar Os dados do usuarios completamente

    public void clear() {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.clear();
        editor.commit();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public static final String CONSTANTE_KEY_SHAREDPREFS_LOGADO = "Logado";
    private static final String CONSTANTE_KEY_SHAREDPREFS_ID_USUARIO = "id_usuario";
    private static final String CONSTANTE_KEY_SHAREDPREFS_NOME_USUARIO = "nome_usuario";
    private static final String CONSTANTE_KEY_SHAREDPREFS_EMAIL_USUARIO = "email_usuario";
    private static final String CONSTANTE_KEY_SHAREDPREFS_FK_GRUPO_USUARIO = "fk_grupo_usuario";
    private static final String CONSTANTE_KEY_SHAREDPREFS_ATIVO_USUARIO = "ativo_usuario";
    private static final String CONSTANTE_KEY_SHAREDPREFS_TOKEN = "token";
    private static final String CONSTANTE_KEY_SHAREDPREFS_CODESHARE = "code";
    private static final String CONSTANTE_KEY_SHAREDPREFS_FACEBOOK_IMAGE_URL = "facebook_url_image";




    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public void setURL_IMAGE_FACEBOOK(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_FACEBOOK_IMAGE_URL, string);
        editor.commit();
    }

    public void setLOGADO(Boolean string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putBoolean(CONSTANTE_KEY_SHAREDPREFS_LOGADO, string);
        editor.commit();
    }

    public void setID_USUARIO(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_ID_USUARIO, string);
        editor.commit();
    }

    public void setNOME_USUARIO(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_NOME_USUARIO, string);
        editor.commit();
    }

    public void setEMAIL_USUARIO(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_EMAIL_USUARIO, string);
        editor.commit();
    }

    public void setFK_GRUPO_USUARIO(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_FK_GRUPO_USUARIO, string);
        editor.commit();
    }

    public void setATIVO_USUARIO(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_ATIVO_USUARIO, string);
        editor.commit();
    }

    public void setTOKEN(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_TOKEN, string);
        editor.commit();
    }
    public void setCODE_SHARE(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(CONSTANTE_KEY_SHAREDPREFS_CODESHARE, string);
        editor.commit();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public String getURL_IMAGE_FACEBOOK() {
        String raw_string = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_FACEBOOK_IMAGE_URL, "NULL");
        return raw_string;
    }

    public Boolean getLOGADO() {
        Boolean raw_string = Prefs.getBoolean(CONSTANTE_KEY_SHAREDPREFS_LOGADO, false);
        return raw_string;
    }

    public String getID_USUARIO() {
        String raw_string = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_ID_USUARIO, "NULL");
        return raw_string;
    }

    public String getNOME_USUARIO() {
        String raw_string = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_NOME_USUARIO, "NULL");
        return raw_string;
    }

    public String getEMAIL_USUARIO() {
        String raw_string = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_EMAIL_USUARIO, "NULL");
        return raw_string;
    }

    public String getFK_GRUPO_USUARIO() {
        String raw_string = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_FK_GRUPO_USUARIO, "NULL");
        return raw_string;
    }

    public String getATIVO_USUARIO() {
        String raw_string = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_ATIVO_USUARIO, "NULL");
        return raw_string;
    }

    public String getTOKEN() {
        String raw_string = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_TOKEN, "null");
        return raw_string;
    }

    public String getCODE_SHARE() {
        String raw_string = Prefs.getString(CONSTANTE_KEY_SHAREDPREFS_CODESHARE, "null");
        return raw_string;
    }
}