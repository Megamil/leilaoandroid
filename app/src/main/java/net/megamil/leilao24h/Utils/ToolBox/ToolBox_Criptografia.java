package net.megamil.leilao24h.Utils.ToolBox;

import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by John on 21/02/2018.
 */

public class ToolBox_Criptografia {

    private String key = "APPKEY";
    private String split = "::";

    private String md5Key(String user, String pass) {
        String base64 = "";

        String md5 = pass + split + key + split + user;

        try {
            byte[] data1 = md5.getBytes("UTF-8");
            base64 = Base64.encodeToString(data1, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.w("Encode", base64);
        return base64;
    }

    private String md5DecodeKey(String base64) {
        String rawDecoded = "";
        try {
            byte[] data2 = Base64.decode(base64, Base64.DEFAULT);
            rawDecoded = new String(data2, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.w("Raw Decode", rawDecoded);

        String array[] = rawDecoded.split(split);

        Log.w("Decode", array[0]);
        return array[0];
    }

    public static String md5(String teste) {
        String base64 = "";
        try {
            byte[] data1 = teste.getBytes("UTF-8");
            base64 = Base64.encodeToString(data1, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.w("Encode", base64);
        return base64;
    }

    public static String md5Decode(String base64) {
        String decoded = "";
        try {
            byte[] data2 = Base64.decode(base64, Base64.DEFAULT);
            decoded = new String(data2, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.w("Decode", decoded);
        return decoded;
    }

    public static String Funcao_CriptoPASS(String senha) {

        String raw_pass = "";

            raw_pass = Funcao_Converter_String_To_MD5(Funcao_Converter_String_To_SHA1(senha));

        return raw_pass;
    }

    private static final String Funcao_Converter_String_To_MD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            return Funcao_Converter_Bites_To_HEX(messageDigest);

//            // Create Hex String
//            StringBuilder hexString = new StringBuilder();
//            for (byte aMessageDigest : messageDigest) {
//                String h = Integer.toHexString(0xFF & aMessageDigest);
//                while (h.length() < 2)
//                    h = "0" + h;
//                hexString.append(h);
//            }
//            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String convertPassMd5(String pass) {
        String password = null;
        MessageDigest mdEnc;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(pass.getBytes(), 0, pass.length());
            pass = new BigInteger(1, mdEnc.digest()).toString(16);
            while (pass.length() < 32) {
                pass = "0" + pass;
            }
            password = pass;
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        return password;
    }

    /**
     * Este método converte String para SHA1 - Utilizado para criar HASH de senhas para banco de dados
     * Só funciona com a função "Converter_Bites_To_HEX" a cima
     */
    public static String Funcao_Converter_String_To_SHA1(String text)  {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
            try {
                md.update(text.getBytes("iso-8859-1"), 0, text.length());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] sha1hash = md.digest();
        return Funcao_Converter_Bites_To_HEX(sha1hash);
    }

    ///////////// complemento para a primeira função
    //////////// Este método converte Bits para Hexadecimal
    ////////////      
    /////////////
    private static String Funcao_Converter_Bites_To_HEX(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;

            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

}