package net.megamil.leilao24h.Utils.Conexao_Volley;

/**
 * Created by nalmir on 30/11/2017.
 */

public class Conexao_Constantes {

//    public static String Base_URL_WS = "http://167.99.151.21";
//    public static String Base_URL_WS = "http://31.220.57.245/leilao";
    public static String Base_URL_WS = "http://api.leilao24h.com.br";

    //private static String URL_WS = Base_URL_WS + "/leilao";
    private static String URL_WS = Base_URL_WS;

    public static final String SOCKET_WS = Base_URL_WS + ":3005";
    public static final String URL_WS_PADRAO = URL_WS + "/controller_webservice/";
    public static final String URL_WS_PACOTES = URL_WS + "/comprar_ingressos/";
    public static final String URL_WS_FOTO_PERFIL = URL_WS + "/upload/perfil/foto/";

}
