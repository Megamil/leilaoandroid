package net.megamil.leilao24h.Utils.ToolBox.Carossel;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;


public class Adapter_ViewPager_Modal extends PagerAdapter {

    private Activity activity;
    private LayoutInflater mLayoutInflater;
    private String[] mResources;

    public Adapter_ViewPager_Modal(Activity activity, String[] resources) {
        this.activity = activity;
        mResources = resources;
        mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.celula_viewpager_modal, container, false);

        PhotoView imageView = (PhotoView) itemView.findViewById(R.id.imageView);

//        imageView.setImageResource(mResources[position]);
        Glide.with(activity).asBitmap()
                .load(Conexao_Constantes.Base_URL_WS + mResources[position].replace("\"" , "").replace("\\" , "").replace("]", "").replace("[" , "").trim())
                .apply(new RequestOptions()
                        .fitCenter()
                )
                .into(imageView);

//        PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(imageView);
//        photoViewAttacher.update();

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}
