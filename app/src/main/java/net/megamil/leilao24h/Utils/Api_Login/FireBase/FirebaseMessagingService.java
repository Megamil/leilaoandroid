package net.megamil.leilao24h.Utils.Api_Login.FireBase;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.ArrayMap;

import com.google.firebase.messaging.RemoteMessage;
import net.megamil.leilao24h.Utils.ToolBox.NotificationUtils;

import java.util.Map;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;

///**
// * Created by John on 23/02/2018.
// */
//
public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    //

    public static String FB_NORTIFICATION_ID = "id_leilao";
    public static String FB_NORTIFICATION_TITULO = "titulo";
    public static String FB_NORTIFICATION_MENSAGEM = "mensagem";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

//      System.out.println("onMessageReceived");

        System.out.println("O ID É = " + remoteMessage.getData().get("id_leilao"));
        System.out.println("O TITULO DA NOTIFICACAO É = " + remoteMessage.getData().get("titulo"));
        System.out.println("A MENSAGEM DA NOTIFICACAO É = " + remoteMessage.getData().get("mensagem"));
        System.out.println("A MENSAGEM BRUTA É = " + remoteMessage.getData());
//        showNotification(remoteMessage.getDados().get("mensagem"), remoteMessage.getDados().get("titulo"));
        String id = "";
        if (remoteMessage.getData().containsKey(FB_NORTIFICATION_ID)) {
            id = remoteMessage.getData().get(FB_NORTIFICATION_ID);
        }
        String msg = remoteMessage.getData().get(FB_NORTIFICATION_MENSAGEM);
        String titulo = remoteMessage.getData().get(FB_NORTIFICATION_TITULO);

        Map<String, String> map = new ArrayMap<>();
//        map.put(FB_NORTIFICATION_ID, "8"); // DEBUG
        map.put(FB_NORTIFICATION_ID, id);
        map.put(FB_NORTIFICATION_MENSAGEM, msg);
        map.put(FB_NORTIFICATION_TITULO, titulo);

        NotificationUtils.initNotification(this, titulo, msg, map);

        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }

    }
//
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//        // ...
//        String TAG = "MSG";
//        // TODO(developer): Handle FCM messages here.
//        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
////        Log.w("MGS", "From: " + remoteMessage.getFrom());
////        Log.w("MGS", "From: " + remoteMessage.getDados());
//
//
////        // Check if message contains a dados payload.
////        if (remoteMessage.getDados().size() > 0) {
//////            Log.w(TAG, "Message dados payload: " + remoteMessage.getDados());
////
////
////
////            if (/* Check if dados needs to be processed by long running job */ true) {
////                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//////                showNotification(remoteMessage.getDados().get("mensagem"), remoteMessage.getDados().get("titulo"));
//////                NotificationUtils.Funcao_Nortificacao(this , remoteMessage.getDados().get("titulo") , remoteMessage.getDados().get("mensagem"));
//////                scheduleJob();
////                Map<String , String> map = null;
////
//////                map.put("id_leilao", remoteMessage.getDados().get("id_leilao"));
////                map.put("mensagem", remoteMessage.getDados().get("mensagem"));
////                map.put("titulo", remoteMessage.getDados().get("titulo"));
////
////                Dados_Ws(101, "id_leilao="+remoteMessage.getDados().get("id_leilao"), map, this);
////            } else {
////                // Handle message within 10 seconds
//////                handleNow();
////            }
////
////        }
////
////        // Check if message contains a notification payload.
////        if (remoteMessage.getNotification() != null) {
////            Log.w(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
////        }
//
////        showNotification(remoteMessage.getDados().get("mensagem"), remoteMessage.getDados().get("titulo"));
//
//        // Also if you intend on generating your own notifications as a result of a received FCM
//        // message, here is where that should be initiated. See sendNotification method below.
//    }
//
//    private void showNotification(String mensagem, String titulo) {
//
//        Log.w("Receiver" , "Nortification FireBase");
//
//        Intent i = new Intent(this, Pagina_Inicio.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        PendingIntent p = PendingIntent.getActivity(this, 0, new Intent(this, Pagina_Inicio.class), 0);
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
//        builder.setTicker(titulo);
//        builder.setContentTitle(titulo);
//        builder.setContentText(mensagem);
////        builder.setSmallIcon(R.drawable.notification1);
////        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notification1));
//        builder.setContentIntent(p);
//
//        System.out.println("Título: " + titulo);
//
//        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
//        String[] descs = new String[]{titulo, mensagem};
//
//        for (int num = 0; num < descs.length; num++) {
//            style.addLine(descs[num]);
//        }
//        builder.setStyle(style);
//
//        Notification n = builder.build();
//        n.vibrate = new long[]{150, 300, 150, 600};
//        n.flags = Notification.FLAG_AUTO_CANCEL;
////        nm.notify(R.drawable.notification1, n);
//
//        try {
//            Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Ringtone toque = RingtoneManager.getRingtone(this, som);
//            toque.play();
//        } catch (Exception e) {
//        }
//    }
//
//
}
