package net.megamil.leilao24h.Utils.ToolBox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.megamil.leilao24h.BuildConfig;
import net.megamil.leilao24h.R;

/**
 * Created by John on 09/12/2017.
 */

public class MsgUtil {

    //==============================================================================================
    //
    //
    //
    //==============================================================================================
    public static void logW(String msg) {
        if (statusRealise()) {
            Log.w("MSG", msg);
        }
    }

    private static boolean statusRealise() {
        return BuildConfig.DEBUG;
    }

    //==============================================================================================
    //
    //
    //
    //==============================================================================================
    //
    // 1° Context
    // 2° MSG para o Toast
    //

    public static void Funcao_MSG(Context context, String texto) {
        Funcao_SNACKBAR(context, texto);
    }

    public static void Funcao_TOAST(Context context, String texto) {
        Toast.makeText(
                context,
                texto,
                Toast.LENGTH_LONG
        ).show();
    }

    public static void Funcao_SNACKBAR(Context context, String texto) {

        View rootView = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);

        Snackbar mSnackBar = Snackbar.make(rootView, texto, Snackbar.LENGTH_LONG);
        TextView mainTextView = (TextView) (mSnackBar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        else
            mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mSnackBar.show();

    }

    public static void Funcao_SNACKBAR_Personalizado(View view, Context context, String msg, String retry_msg, View.OnClickListener clickListener) {

        Snackbar snackbar = Snackbar
                .make(view, msg, Snackbar.LENGTH_LONG)
                .addCallback(new Snackbar.Callback() {
                    @Override
                    public void onShown(Snackbar sb) {
                        super.onShown(sb);
                    }

                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        super.onDismissed(transientBottomBar, event);
                    }
                })
                .setAction(retry_msg, clickListener);


// Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();

    }


    //
    // 1° Activity a receber o Alert
    // 2° MSG para o Alert
    // 3° Evento de Click para receber no evento ok
    // 4° Evento de Click para receber no evento Cancelar
    //
    //Evento Exenplo
    //    DialogInterface.OnClickListener acao = new DialogInterface.OnClickListener() {
    //        @Override
    //        public void onClick(DialogInterface dialogInterface, int i) {
    //            Açoes do evento
    //        }
    //    };

    public static void Funcao_Dialog_OK_CANCEL(Activity activity, String message, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("Permitir", okListener)
                .setNegativeButton("Cancelar", cancelListener)
                .create()
                .show();
    }

    public static void Funcao_Dialog_OK(Activity activity, String message, String textBtnOk, String textBtnCancel, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(textBtnOk, okListener)
                .setNegativeButton(textBtnCancel, null)
                .create()
                .show();
    }

    public static void Funcao_Dialog_OK(Activity activity, String message, String btn_text, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(btn_text, okListener)
                .create()
                .show();
    }

    public static void Funcao_BottomSheetDialog(Activity activity, String msg, View.OnClickListener clickListener_ok, View.OnClickListener clickListener_cancelar) {
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_bottomsheet, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.show();

        TextView tv_msg = view.findViewById(R.id.msg);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancelar = view.findViewById(R.id.btn_cancelar);

        if (!msg.equals("")) {
            tv_msg.setText(msg);
        } else {
            tv_msg.setText("ok");
        }

        if (clickListener_ok != null) {
            btn_ok.setOnClickListener(clickListener_ok);
        } else {
            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }

        if (clickListener_cancelar != null) {
            btn_cancelar.setOnClickListener(clickListener_cancelar);
        } else {
            btn_cancelar.setVisibility(View.GONE);
        }

    }

}
