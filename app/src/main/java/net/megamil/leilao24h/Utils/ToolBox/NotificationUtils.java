package net.megamil.leilao24h.Utils.ToolBox;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;


import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Html;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Patterns;
import android.widget.CompoundButton;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Splash.Splash;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.LeilaoNortification;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static net.megamil.leilao24h.Utils.Api_Login.FireBase.FirebaseMessagingService.FB_NORTIFICATION_ID;
import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


/**
 * Created by John on 28/07/18.
 */
public class NotificationUtils {

//    {
//        "to":"push-token",
//            "priority": "high",
//            "notification": {
//        "title": "Test",
//                "body": "Mary sent you a message!",
//                "sound": "default",
//                "icon": "youriconname"
//    }
//    }

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";


    public static final String SHARED_PREF = "ah_firebase";

    private static String TAG = NotificationUtils.class.getSimpleName();

    private static Context mContext;
    private static String mTitle;
    private static String mMessage;
    private static Bitmap mBitmap;
    private static Map<String, String> mMap = new ArrayMap<>();

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    //========================================================================================================================

    //Playing notification sound
    private static Uri getSound() {
        Uri alarmSound = null;
        try {
            alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/raw/judgegavel");
//            Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
//            r.play();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return alarmSound;
    }

    private enum intentState {
        MAIN_ACTIVITY, ITEM_ACTYVITY
    }

    //NortificationChannel Setings
    private static String CHANNEL_ID = "";
    private static String CHANNEL_NAME = "";
    private static String CHANNEL_DESCRIPTION = "";

    //init PendingIntent For Nortification
    private static PendingIntent getResultPendingIntent(intentState intentState) {

        Intent intent = null;
        switch (intentState) {
            case MAIN_ACTIVITY:
                intent = new Intent(mContext, Splash.class);
                intent.putExtra(FB_NORTIFICATION_ID, mMap.get(FB_NORTIFICATION_ID));

                CHANNEL_ID = "10012";
                CHANNEL_NAME = "Informativo";
                CHANNEL_DESCRIPTION = "";
                break;
            case ITEM_ACTYVITY:

                intent = new Intent(mContext, LeilaoNortification.class);
                intent.putExtra(FB_NORTIFICATION_ID, mMap.get(FB_NORTIFICATION_ID));

                CHANNEL_ID = "10015";
                CHANNEL_NAME = "Leilões";
                CHANNEL_DESCRIPTION = "";
                break;
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // inicia a PendingIntent e coloca o valor da intent criada a cima
        final PendingIntent AtividadeResultado =
                PendingIntent.getActivity(
                        mContext,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        return AtividadeResultado;
    }

    //get Vibration
    private static long[] getVibration() {
        return new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400};
    }

    //getIcon
    private static int getIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.leilao_logo : R.mipmap.ic_launcher;
    }

    private static Bitmap getBitmapIcon() {
        return BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher);
    }

    //getColor
    private static int getColor() {
        return mContext.getResources().getColor(R.color.colorPrimary);
    }

    //getGroupName
    private static String getGroupName() {
        return "com.nortification.leilao";
    }

    //get Nortification Priority
    private static int getPriority() {
        return Notification.PRIORITY_MAX;
    }

    //verifie ID
    private static int verifieID(String s) {
        if (s.equals("") || s.equals("null")) {
            return 12;
        } else {
            return Integer.parseInt(s);
        }
    }

    //getStyle BigPictureStyle
    private static NotificationCompat.BigPictureStyle getBigPictureStyle() {
        NotificationCompat.BigPictureStyle inboxStyle = new NotificationCompat.BigPictureStyle();
        inboxStyle.bigPicture(mBitmap);
        inboxStyle.bigLargeIcon(null);

        return inboxStyle;
    }

    //getStyle BigText
    private static NotificationCompat.BigTextStyle getBigTextStyle() {
        return new NotificationCompat.BigTextStyle()
                .bigText(mMessage);
    }

    //getStyle MensagingStyle
    private static NotificationCompat.MessagingStyle getMessagingStyle() {

        NotificationCompat.MessagingStyle.Message message1 =
                new NotificationCompat.MessagingStyle.Message("Title 1",
                        00,
                        "");
        NotificationCompat.MessagingStyle.Message message2 =
                new NotificationCompat.MessagingStyle.Message("Title 2",
                        00,
                        "");

        return new NotificationCompat.MessagingStyle("Reply")
                .addMessage(message1)
                .addMessage(message2);
    }

    //getStyle InboxStyle
    private static NotificationCompat.InboxStyle getInboxStyle() {
        return new NotificationCompat.InboxStyle()
                .addLine("Text 1")
                .addLine("Text 2");
    }

    //getStyle MediaStyle
//    private static NotificationCompat.InboxStyle getMediaStyle(){
//
////                .addAction(R.drawable.ic_prev, "Previous", prevPendingIntent) // #0
////                .addAction(R.drawable.ic_pause, "Pause", pausePendingIntent)  // #1
////                .addAction(R.drawable.ic_next, "Next", nextPendingIntent)     // #2
//
//        return new NotificationCompat.MediaStyle()
//                .setShowActionsInCompactView(1 /* #1: pause button */)
//                .setMediaSession(mMediaSession.getSessionToken());
//    }

    //========================================================================================================================

    //constructNortification

    private static void constructNortification(NotificationCompat.Builder mBuilder, Notification notification) {

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance);
            notificationChannel.setDescription(CHANNEL_DESCRIPTION);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(getVibration());
            assert notificationManager != null;
            mBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        assert notificationManager != null;
        notificationManager.notify(verifieID(mMap.get(FB_NORTIFICATION_ID)), notification);

    }

//    @RequiresApi(api = Build.VERSION_CODES.O)
//    private void setupChannels(){
//        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
//        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);
//
//        NotificationChannel adminChannel;
//        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
//        adminChannel.setDescription(adminChannelDescription);
//        adminChannel.enableLights(true);
//        adminChannel.setLightColor(Color.RED);
//        adminChannel.enableVibration(true);
//        if (notificationManager != null) {
//            notificationManager.createNotificationChannel(adminChannel);
//        }
//    }

    //========================================================================================================================

    // id to handle the notification in the notification tray
    public static final int NORTIFICACAO_NORMAL = 100;
    public static final int NORTIFICACAO_COM_IMAGEM = 101;
    public static final String KEY_NORTIFICACAO_RESPOSTA = "RESPOSTA";

    public static void initNotification(Context context, final String title, final String message, Map<String, String> map) {
        mContext = context;
        mTitle = "";
        mMessage = "";
        mMap = new ArrayMap<>();

        mTitle = title;
        mMessage = message;
        mMap = map;

//        Dados_Ws(10, "id_leilao=32", null, context);

        if (map.get(FB_NORTIFICATION_ID).equals("") || map.get(FB_NORTIFICATION_ID).equals("null") || map.get(FB_NORTIFICATION_ID).equals("0")) {
            showNortificationBasic();
        } else {
//            String id = mMap.get(FB_NORTIFICATION_ID);
//            String url = Conexao_Constantes.Base_URL_WS + "/leilao/upload/produtos/" + id + "/" + id + "_0.png";
//            Log.w("URL IMG NORTIFICATION", url);
//            new iniTaskLoadImgFromURL().execute(url);
            showNortificationWithIMG();
        }
    }

    private static class iniTaskLoadImgFromURL extends AsyncTask<String, Void, Bitmap> {

        private Exception exception;

        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Bitmap bitmap) {

            mBitmap = bitmap;

            if (mBitmap != null) {
                showNortificationWithIMG();
            } else {
                showNortificationBasic();
            }
        }
    }

    //========================================================================================================================

    private static void showNortificationWithIMG() {

        //inicia a nortificaçao
        Notification notification;
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, "");

        //icone da nortificação

        notification = mBuilder
                //header da dortificação
                .setSmallIcon(getIcon())//icone do header
                .setTicker(mTitle)
                .setWhen(getTimeMilliSec(ToolBox_Data_e_Hora.Funcao_DataCompleta_TimeStamp()))// a hora que foi chamada
                .setColor(getColor())//define a cor do icone e do nome do app na nortificação

                // mostra que a nortificação pode ser mostradas varias vezes com diferentes conteudos, reutilizando a mesma nortificação
                .setGroupSummary(true)
                .setGroup(getGroupName()) // nome do grupo da nortificação

                //corpo da nortificação
                .setContentTitle(mTitle)
                .setContentText(mMessage)
                .setLargeIcon(getBitmapIcon())

                //style personalizado da nortifiação
//                .setStyle(getBigPictureStyle())

                //ação ao clicar na nortificação
                .setContentIntent(getResultPendingIntent(intentState.ITEM_ACTYVITY))

                //ação personalizada de resposta na nortificação
//                .addAction(AcaoResposta)

                //adiciona açoes ao button da dortificação
//                .addAction(android.R.drawable.ic_menu_compass, "Ação 1", AtividadeResultado)
//                .addAction(android.R.drawable.ic_menu_directions, "Ação 2", AtividadeResultado)

                //mosrtra a nortificação na tela sem ter que abrir a aba superior ( Igual WhatsApp )
                .setPriority(getPriority())

                //som personalizado da nortificação
                .setSound(getSound())

                //Vibração
                .setVibrate(getVibration())

                .setAutoCancel(true)
                .build();

        if (Build.VERSION.SDK_INT >= 21) mBuilder.setVibrate(getVibration());

        constructNortification(mBuilder, notification);

    }

    private static void showNortificationBasic() {
        //inicia a nortificaçao
        Notification notification;
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);

        //icone da nortificação

        notification = mBuilder
                //header da dortificação
                .setSmallIcon(getIcon())//icone do header
                .setTicker(mTitle)
                .setWhen(getTimeMilliSec(ToolBox_Data_e_Hora.Funcao_DataCompleta_TimeStamp()))// a hora que foi chamada
                .setColor(getColor())//define a cor do icone e do nome do app na nortificação
                .setLights(Color.RED, 3000, 3000)
                .setSound(Uri.parse("android.resource://"
                        + mContext.getPackageName() + "/" + R.raw.judgegavel))
                // mostra que a nortificação pode ser mostradas varias vezes com diferentes conteudos, reutilizando a mesma nortificação
                .setGroupSummary(true)
                .setGroup(getGroupName()) // nome do grupo da nortificação

                //corpo da nortificação
                .setContentTitle(mTitle)
                .setContentText(mMessage)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), getIcon()))
//                .setLargeIcon(mBitmap)

                //style personalizado da nortifiação
                .setStyle(getBigPictureStyle())

                //ação ao clicar na nortificação
                .setContentIntent(getResultPendingIntent(intentState.MAIN_ACTIVITY))

                //ação personalizada de resposta na nortificação
//                .addAction(AcaoResposta)

                //adiciona açoes ao button da dortificação
//                .addAction(android.R.drawable.ic_menu_compass, "Ação 1", AtividadeResultado)
//                .addAction(android.R.drawable.ic_menu_directions, "Ação 2", AtividadeResultado)

                //mosrtra a nortificação na tela sem ter que abrir a aba superior ( Igual WhatsApp )
                .setPriority(getPriority())

                //som personalizado da nortificação
                .setSound(getSound())

                //Vibração
//                .setVibrate(getVibration())

                .setAutoCancel(true)
                .build();

//        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        if (Build.VERSION.SDK_INT >= 21) mBuilder.setVibrate(getVibration());

        constructNortification(mBuilder, notification);
    }

    public void showNotificationMessage(final String title, final String message, final String timeStamp, Intent intent, String imageUrl) {
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;

        // notification icon
        final int icon = R.mipmap.ic_launcher;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mContext,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext);

        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/raw/zelda_treasure");

        if (!TextUtils.isEmpty(imageUrl)) {

            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

                Bitmap bitmap = null;
                if (bitmap != null) {
                    showBigNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                } else {
                    showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                    bitmap = getBitmapFromURL(imageUrl);

                }
            }
        } else {
            showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
//            playNotificationSound();
        }
    }

    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        inboxStyle.addLine(message);

        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(inboxStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NORTIFICACAO_NORMAL, notification);
    }

    private void showBigNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(bigPictureStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NORTIFICACAO_COM_IMAGEM, notification);
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

//    private void ifHuaweiAlert() {
//        final SharedPreferences settings = getSharedPreferences("ProtectedApps", MODE_PRIVATE);
//        final String saveIfSkip = "skipProtectedAppsMessage";
//        boolean skipMessage = settings.getBoolean(saveIfSkip, false);
//        if (!skipMessage) {
//            final SharedPreferences.Editor editor = settings.edit();
//            Intent intent = new Intent();
//            intent.setClassName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity");
//            if (isCallable(intent)) {
//                final AppCompatCheckBox dontShowAgain = new AppCompatCheckBox(this);
//                dontShowAgain.setText("Do not show again");
//                dontShowAgain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                        editor.putBoolean(saveIfSkip, isChecked);
//                        editor.apply();
//                    }
//                });
//
//                new AlertDialog.Builder(this)
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .setTitle("Huawei Protected Apps")
//                        .setMessage(String.format("%s requires to be enabled in 'Protected Apps' to function properly.%n", getString(R.string.app_name)))
//                        .setView(dontShowAgain)
//                        .setPositiveButton("Protected Apps", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                huaweiProtectedApps();
//                            }
//                        })
//                        .setNegativeButton(android.R.string.cancel, null)
//                        .show();
//            } else {
//                editor.putBoolean(saveIfSkip, true);
//                editor.apply();
//            }
//        }
}
