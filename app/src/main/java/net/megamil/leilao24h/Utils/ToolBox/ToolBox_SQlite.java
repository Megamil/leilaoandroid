package net.megamil.leilao24h.Utils.ToolBox;

import android.content.Context;

import net.megamil.leilao24h.Usuario.Busca.DB_Busca;

public class ToolBox_SQlite {
    private DB_Busca db = null;

    public ToolBox_SQlite(Context context) {
        db = new DB_Busca(context);
    }

    private static final String CONSTANTE_KEY_DB_TOKEN = "token";

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public void clear() {

        String[] tabelas = {
                CONSTANTE_KEY_DB_TOKEN
        };

        for (int i = 0; i < tabelas.length; i++) {
            String raw_name = tabelas[i];
            db.Funcao_DeletaSQLITE(raw_name);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setDBToken(String string) {
        db.Funcao_CriaSQLITE(CONSTANTE_KEY_DB_TOKEN, string);
    }

    public String getDBToken() {
        String dados = db.Funcao_getSQlite(CONSTANTE_KEY_DB_TOKEN);
        return dados;
    }

    public void deleteDBToken() {
        db.Funcao_DeletaSQLITE(CONSTANTE_KEY_DB_TOKEN);
    }

}