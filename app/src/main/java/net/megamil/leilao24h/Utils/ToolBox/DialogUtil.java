/*
 * Copyright (c) Developed by John Alves at 2018/11/1.
 */

package net.megamil.leilao24h.Utils.ToolBox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.util.ArrayMap;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Recompensa;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;
import net.megamil.library.Utils.ActivityUtil;

import java.util.Map;
import java.util.Objects;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;
import static net.megamil.leilao24h.Utils.ToolBox.GlideUtil.initGlide;

public class DialogUtil {

    public static void showDetails(final Activity activity, final HMAux magazine, final int position) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_magazine);

        //==========================================================================================

        LinearLayout labelNew = dialog.findViewById(R.id.labelNew);
        if (magazine.get(HMAux.PRODUTO_USADO).equals("0")) {
            labelNew.setVisibility(View.VISIBLE);
        } else {
            labelNew.setVisibility(View.GONE);
        }
        //==========================================================================================

        LinearLayout labelCategory = dialog.findViewById(R.id.labelCategory);
        if (!magazine.get(HMAux.NOME_CATEGORIA).equals("")) {
            labelCategory.setVisibility(View.VISIBLE);
            ((TextView) dialog.findViewById(R.id.numberPrice)).setText(magazine.get(HMAux.NOME_CATEGORIA));
        } else {
            labelCategory.setVisibility(View.GONE);
            ((TextView) dialog.findViewById(R.id.numberPrice)).setText("");
        }

        //==========================================================================================

        initGlide(activity, Conexao_Constantes.Base_URL_WS + magazine.get(HMAux.IMAGENS), ((ImageView) dialog.findViewById(R.id.magazineCover)));

        //==========================================================================================

        ((TextView) dialog.findViewById(R.id.magazineName)).setText(magazine.get(HMAux.NOME_PRODUTO));
        ((TextView) dialog.findViewById(R.id.description)).setText(magazine.get(HMAux.DESCRICAO_PRODUTO));
        ((TextView) dialog.findViewById(R.id.ingressos)).setText(magazine.get(HMAux.PRODUTO_RECOMPENSA_CUSTO));

        //==========================================================================================

        dialog.findViewById(R.id.btnMoreDetails).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                int value = Integer.parseInt(magazine.get(HMAux.PRODUTO_RECOMPENSA_CUSTO));
                if (Recompensa.points < value) {
                    MsgUtil.Funcao_MSG(activity, "Pontos insuficientes");
                } else {
                    new AlertDialog.Builder(activity)
                            .setMessage("Adquirir?")
                            .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Map<String, String> map = new ArrayMap<>();
                                    map.put("tipo_recompensa", "2");
                                    map.put("id_recompensa", magazine.get(HMAux.ID_PRODUTO));
                                    Dados_Ws(24, "", map, activity);
                                }
                            })
                            .setNegativeButton("Não", null)
                            .create()
                            .show();
                }
            }
        });

        //==========================================================================================

        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();
    }


}
