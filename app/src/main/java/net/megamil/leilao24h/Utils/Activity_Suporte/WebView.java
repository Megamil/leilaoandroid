package net.megamil.leilao24h.Utils.Activity_Suporte;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Inicio;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.Leilao;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.Planos;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;

public class WebView extends AppCompatActivity implements View.OnClickListener {

    private Activity activity;
    private ImageView btn_voltar;
    private LinearLayout ll_toolbar;
    private android.webkit.WebView webView;
    private String url;
    private Boolean show_toolbar;

    public static void startWebView(Activity activity, String url) {
        Intent intent = new Intent(activity, WebView.class);
        intent.putExtra("url", url);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        activity = WebView.this;
        url = getIntent().getStringExtra("url");
        show_toolbar = getIntent().getBooleanExtra("toolbar", false);
        ll_toolbar = findViewById(R.id.toolbar);

        if (!show_toolbar) {
            ll_toolbar.setVisibility(View.GONE);
        }

        btn_voltar = findViewById(R.id.imgVoltarTermos);
        webView = findViewById(R.id.webViewTermos);
        webView.addJavascriptInterface(new WebAppInterface(this), "Android");
        loadWebViewLoad(webView);

        btn_voltar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btn_voltar) {
            finish();
        }

    }

    private void loadWebViewLoad(android.webkit.WebView webview) {

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setSupportMultipleWindows(true);
        webview.setWebViewClient(new WebViewClient());
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadUrl(url);
        Log.w("URL", url);
    }

    //Usado quando chamado pelo WEBVIEW
    public class WebAppInterface {
        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void exibirAvisoAndroid(String msg) {
            Log.w("Função WS", "OK");
//            MainActivity.Funcao_Avaliar_Passageiro(id);
//
//            DialogInterface.OnClickListener acao = new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    finish();
//                }
//            };
//
//            MsgUtil.Funcao_Dialog_OK(activity, msg, "OK", acao);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
                    int credt = prefs.getCreditsNum();
                    prefs.setCredits(String.valueOf(credt + Integer.parseInt(getIntent().getStringExtra("credited"))));
                    Log.w("Cred math", String.valueOf(credt + Integer.parseInt(getIntent().getStringExtra("credited"))));
                    Fragment_Inicio.tv_credits.setText(prefs.getCredits());
                    try {
                        Leilao.credits.setText(prefs.getCredits());
                    } catch (RuntimeException e) {

                    }
                }
            });


            Planos.planosFinish();

            View.OnClickListener acao = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            };

            MsgUtil.Funcao_BottomSheetDialog(activity, msg, acao, null);

        }
    }

}
