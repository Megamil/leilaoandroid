package net.megamil.leilao24h.Utils.ToolBox;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.megamil.leilao24h.BuildConfig;
import net.megamil.leilao24h.R;

import java.util.Random;

/**
 * Created by John on 21/02/2018.
 */

public class ToolBox_VersaoApp {

    private static int fim = 20;
    private static int inicio_mgs = 15;
    private static int inicio = 0;

    public static void setVersion(final Activity activity, LinearLayout linearLayout, final LinearLayout ll_btn_debug) {

        String app_name = activity.getResources().getString(R.string.app_name);
        int color = activity.getResources().getColor(R.color.Color_Palet_Flat_SILVER);

        TextView textView = new TextView(activity);
        textView.setTextColor(color);
        textView.setText(app_name + " V" + BuildConfig.VERSION_NAME);
        linearLayout.addView(textView);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                LinearLayout.LayoutParams.WRAP_CONTENT ); // Height of TextView

        lp.gravity = Gravity.CENTER | Gravity.BOTTOM;

        // Apply the layout parameters to TextView widget
        linearLayout.setLayoutParams(lp);

        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(1);

//

        final ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
        if (prefs.getDebug()) {
            inicio = fim + 1;
            ll_btn_debug.setVisibility(View.VISIBLE);
        }

        Funcao_Liberar_Debug(activity, linearLayout, ll_btn_debug);

    }

    public static void Funcao_Liberar_Debug(final Activity activity, LinearLayout linearLayout, final LinearLayout ll_btn_debug) {

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);

                if (prefs.getDebug()) {
                    Funcao_Chama_Frases(activity);
                } else {

                    if (inicio < inicio_mgs) {
                        inicio++;
                    } else if (inicio >= inicio_mgs) {

                        int falta = fim - inicio;
                        String msg = "";
                        if (falta == 1) {
                            msg = falta + " vez";
                        } else {
                            msg = falta + " vezes";
                        }

                        if (inicio >= fim) {
                            prefs.setDegug();
                            ll_btn_debug.setVisibility(View.VISIBLE);
                            MsgUtil.Funcao_MSG(activity, "Menu de Cheats Liberado!");
                            ToolBox_RingTone.Funcao_Play(activity);
                            inicio++;
                        } else {
                            MsgUtil.Funcao_MSG(activity, "Toque mais " + msg + " Para Liberar os Cheats!");
                            inicio++;
                        }


                    }

                }


            }
        });

    }

    private static void Funcao_Chama_Frases(Activity activity) {
        final Random n = new Random();
        String msg = "";
        int sw = n.nextInt(9) + 1;

        switch (sw) {
            case 1:
                msg = "Vai procurar oque fazer!";
                break;
            case 2:
                msg = "Você quer mais oque?";
                break;
            case 3:
                msg = "BURRO! Já está habilitado o menu!";
                break;
            case 4:
                msg = "Cara, sai dessa!";
                break;
            case 5:
                msg = "Porque ainda está clicando aqui?";
                break;
            case 6:
                msg = "Man, nem falo nada!";
                break;
            case 7:
                msg = "Debug habilitado!";
                break;
            case 8:
                msg = "Não sei você já percebeu, mas...";
                break;
            case 9:
                msg = "Porque clicas aqui?";
                break;
            case 10:
                msg = "Velho sai fora!";
                break;
        }

        MsgUtil.Funcao_MSG(activity, msg);


    }



}