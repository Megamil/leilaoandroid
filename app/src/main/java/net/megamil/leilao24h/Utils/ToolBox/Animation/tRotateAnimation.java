package net.megamil.leilao24h.Utils.ToolBox.Animation;

import android.view.View;
import android.view.animation.Animation;

public class tRotateAnimation {

    View view;

    public tRotateAnimation(View view) {
        this.view = view;
    }

    public void topArrow() {

        android.view.animation.RotateAnimation rotate = new android.view.animation.RotateAnimation(180.0f, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        rotate.setDuration(100);
//            rotate.setRepeatCount(1);
        rotate.setFillAfter(true);

        view.startAnimation(rotate);

    }

    public void bottonArrow() {

        android.view.animation.RotateAnimation rotate = new android.view.animation.RotateAnimation(0.0f, 180.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        rotate.setDuration(100);
//            rotate.setRepeatCount(1);
        rotate.setFillAfter(true);

        view.startAnimation(rotate);

    }
}
