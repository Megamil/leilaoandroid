package net.megamil.leilao24h.Utils.ToolBox;


import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import net.megamil.leilao24h.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;

public abstract class ToolBox_Verivifacao_Campos {

    public static boolean isEmailValide(Activity activity, EditText etField) {
        String emailPattern = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        if (!etField.getText().toString().trim().matches(emailPattern)) {
            etField.setError("Digite um e-mail válido");
            etField.requestFocus();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etField, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }
        return true;
    }

    public static boolean isFieldObrigatory(EditText editText, String msgErro) {
        if (editText.getText().toString().trim().equals("")) {
            editText.setError(msgErro);
            return false;
        } else {
            return true;
        }
    }

    public static boolean isFieldObrigatory(Activity activity, EditText etField) {

        if (etField.getText().toString().trim().equals("") || etField.getText().toString().trim().isEmpty()) {
            etField.setError("Preencha este campo");
            etField.requestFocus();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etField, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }

        return true;
    }

    public static String setTextWithQuanty(int num, String unityText, String multiplesText) {

        String msg = "";
        if (num <= 0) {
            msg = "0 " + multiplesText;
        } else if (num == 1) {
            msg = "1 " + unityText;
        } else {
            msg = num + " " + multiplesText;
        }
        return msg;
    }


    public static View.OnClickListener buttonCheckConection(Activity activity, View.OnClickListener onClickListener) {
        boolean conn = ToolBox_Verivifacao_Sistema.Funcao_Status_Conexao(activity); // função para checar a conexao antes
        if (conn == true) {
            return onClickListener;
        } else {
            MsgUtil.Funcao_MSG(activity, activity.getResources().getString(R.string.msg_global_sem_conexao));
            return onClickListener_null;
        }
    }

    private static View.OnClickListener onClickListener_null = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };


    public static String Regex(String string) {

//        .replaceAll("\\s+", " ") // remover mutiso espassos
//        .replaceAll("[^\\d.]", "") // deixar somente numeros
        return null;
    }

    /**
     * Função validar senha digitada
     */
    public static boolean tValidePass(EditText editText1, EditText editText2) {
        if (editText1.getText().toString().trim().equals(editText2.getText().toString().trim())) {
            return true;
        } else {
            editText1.setText("");
            editText2.setText("");
            editText1.requestFocus();
            return false;
        }
    }

    /**
     * Função retorna senha em Sha1
     */
    public static String tGetTextPass(EditText editText1) {
        return ToolBox_Criptografia.Funcao_Converter_String_To_SHA1(tGetText(editText1));
    }

    /**
     * Função para Pegar Valores de Campos EditText
     */
    public static String tGetText(EditText text) {
        return text.getText().toString().trim();
    }

    public static String tGetText(TextView text) {
        return text.getText().toString().trim();
    }

    /**
     * Função para Pegar posistion do Spinner
     */
    public static String tGetSpinnerPosition(Spinner spinner) {
        return String.valueOf(spinner.getSelectedItemPosition());
    }

    /**
     * Função para Pegar posistion do Spinner
     */
    public static String tGetSpinnerPositionSex(Spinner spinner) {

        int sexo_id = 0;

        if (spinner.getSelectedItemPosition() == 0) {
            sexo_id = 28;
        } else {
            sexo_id = 29;
        }

        return String.valueOf(sexo_id);
    }

    public static int Funcao_setSelectionSpinnerByText(Spinner sp_estado, String uf) {

        int posicao = -1;

        for (int i = 0; i < sp_estado.getAdapter().getCount(); i++) {

            if (sp_estado.getAdapter().getItem(i).toString().contains(uf)) {

                posicao = i;
            }
        }

        return posicao;
    }

    public static Boolean Funcao_verificaTamanhoArray(Activity activity, ArrayList list) {

        if (list.size() < 1) {
            MsgUtil.Funcao_MSG(activity, "Selecione uma Imagem");
            return false;
        } else {
            return true;
        }
    }

    public static Boolean Funcao_ImgSelecionada(Activity activity, String string) {

        if (string.equals("") || string.equals("null")) {
            MsgUtil.Funcao_MSG(activity, "Selecione uma Imagem");
            return false;
        } else {
            return true;
        }
    }

    public static Boolean Funcao_verificaIdCategoria(Activity activity, String categ) {

        if (categ.equals("-1")) {
            MsgUtil.Funcao_MSG(activity, "Selecione uma categoria");
            return false;
        } else {
            return true;
        }
    }

    /**
     * Função para verificar se dados do spinner esta vazio
     */
    public static int Funcao_verificaSpinnerVazio(String valor) {
        int i = 0;

        if (valor.equals("")) {
            i = 0;
        } else {
            i = Integer.parseInt(valor);
        }
        return i;
    }

    public static int Funcao_verificaSpinnerVazioSex(String valor) {
        int i = 0;

        if (valor.equals("")) {
            i = 0;
        } else if (valor.equals("28")) {
            i = 0;
        } else if (valor.equals("29")) {
            i = 1;
        }
        return i;
    }

    public static String Funcao_Converter_Dinhero_Ws(String string) {

        return string.replace(".", "").replace(",", ".").replaceAll("[^\\d.]", "");
    }

    //////////////////////////////////////////////////////////////////////////////////////
    /*
     *  Função para verificar se a dados é menor que hoje
     *
     *  1° Recebe um timestamp no formato yyyy-MM-dd HH:mm:ss ou seja 2018/12/31 13:50:50
     *
     *  exemplo de uso:
     *
     *
     */
    public static Boolean Funcao_Verifica_Data_Menor_que_Hoje(String data) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date data_atual_nova = null;
        try {
            data_atual_nova = df.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(data_atual_nova);
        cal.add(Calendar.MINUTE, 5);

        String new_data_raw = df.format(cal.getTime());

        Boolean data_valida;
//        Log.w("Data Atual", String.valueOf(data_atual_nova.getTime()));
//        Log.w("Data Selecionada", String.valueOf(System.currentTimeMillis()));

        if (data_atual_nova.getTime() <= System.currentTimeMillis()) {
            return false;
        } else {
            return true;
        }
    }


    public static Boolean Funcao_Verifica_Data_Menor_que_Agora(String data) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date data_atual_nova = null;
        try {
            data_atual_nova = df.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(data_atual_nova);
        cal.add(Calendar.MINUTE, 5);

        String new_data_raw = df.format(cal.getTime());

        Boolean data_valida;
//        Log.w("Data Atual", String.valueOf(data_atual_nova.getTime()));
//        Log.w("Data Selecionada", String.valueOf(System.currentTimeMillis()));

        if (data_atual_nova.getTime() <= System.currentTimeMillis()) {
            return false;
        } else {
            return true;
        }
    }

    public static Boolean Funcao_Verifica_Data_Final(Activity activity, String raw_data_inicial, String raw_data_final) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date data_inicial = null;
        Date data_final = null;
        try {
            data_inicial = df.parse(raw_data_inicial);
            data_final = df.parse(raw_data_final);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String raw_timestamp = "0000-00-00 00:00:00";

        if (raw_data_inicial.equals(raw_timestamp)) {
            MsgUtil.Funcao_MSG(activity, "Selecione uma dados de inicio");
            return false;
        } else if (raw_data_final.equals(raw_timestamp)) {
            MsgUtil.Funcao_MSG(activity, "Selecione uma dados de fim");
            return false;
        } else if (data_final.getTime() <= data_inicial.getTime()) {
            MsgUtil.Funcao_MSG(activity, "A dados de fim dever maior que a de inicio");
            return false;
        } else if (data_inicial.getTime() <= System.currentTimeMillis()) {
            MsgUtil.Funcao_MSG(activity, "A dados de inicio dever maior que agora");
            return false;
        } else if (data_final.getTime() <= System.currentTimeMillis()) {
            MsgUtil.Funcao_MSG(activity, "A dados de fim dever maior que agora");
            return false;
        } else {
            return true;
        }
    }


    /**
     * Função para verificar se a variavel esta vazia , neste caso, todos que chamem esta função são obrigatorios , devem ser preeenchidos
     */
    public static boolean Funcao_ValidarSeEstaVazio(Context context, EditText
            editText_para_ser_selecionado, String valor) {

        if (valor.isEmpty() || valor.trim().isEmpty()) {
            editText_para_ser_selecionado.requestFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText_para_ser_selecionado, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }

        return true;
    }


    /**
     * Função para verificar se a variavel esta vazia , neste caso, todos que chamem esta função são obrigatorios , devem ser preeenchidos
     * <p>
     * exeemplo de uso
     * ToolBox_Verivifacao_Campos.Campo_Obrigatorio(context, et_cidade, et_cidade.getText().toString().trim());
     */
    public static boolean Funcao_Campo_Obrigatorio(Context context, EditText editText_para_ser_selecionado, String valor) {

        String MsgDeERROR = "Este Campo Precisa ser Preenchido";
        if (valor.isEmpty() || valor.trim().isEmpty() || valor.equals("")) {
            editText_para_ser_selecionado.requestFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText_para_ser_selecionado, InputMethodManager.SHOW_IMPLICIT);
//            MsgUtil.ExibeMSG(MsgDeERROR, context);
            return false;
        }

        return true;
    }


    public static boolean Funcao_Campo_MinMax_Caracteres(EditText editText, int min, int max) {
        int count = tGetText(editText).length();
        if (count < min || count > max) {
            editText.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    public static boolean Funcao_Campo_Min_Caracteres(EditText editText, int min) {
        int count = tGetText(editText).length();
        if (count < min) {
            editText.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    public static boolean Funcao_VerificaSenhas(EditText editTextPrimario, EditText editTextSecundario) {
        String text1 = tGetText(editTextPrimario);
        String text2 = tGetText(editTextSecundario);
        if (text1.equals(text2)) {
            return true;
        } else {
            editTextPrimario.setText("");
            editTextSecundario.setText("");
            editTextPrimario.requestFocus();
            return false;
        }
    }


    /**
     * Função para verificar se a variavel numerica é numero , e se ela tem o valor minimo para ser guardada no banco
     * <p>
     * exemplo de uso
     * ToolBox_Verivifacao_Campos.Campo_Obrigatorio_Numero(context, et_cpf, et_cpf.getText().toString().trim() , 11);
     */
    public static boolean Funcao_Campo_Obrigatorio_Numero(Context context, EditText
            editText_para_ser_selecionado, String valor_original, int numero_de_caracteres_minimos) {

        String valor_limpo = Funcao_Retirar_Mascara(valor_original);
        String valor_limpo_2 = valor_limpo.replaceAll("[^\\d.]", "");


        if (valor_limpo_2.isEmpty() || valor_limpo_2.trim().isEmpty() || valor_limpo_2.equals("")) {
            Log.w("Validação de numero", "Numero vazio ou invalido = " + valor_limpo_2);
            editText_para_ser_selecionado.requestFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText_para_ser_selecionado, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (valor_limpo_2.length() < numero_de_caracteres_minimos) {
            Log.w("Validação de numero", "a String " + valor_limpo_2 + " é menor que " + numero_de_caracteres_minimos);
            editText_para_ser_selecionado.requestFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText_para_ser_selecionado, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else {
            return true;
        }

    }


    /**
     * Função para verificar se a variavel e um email valido
     * <p>
     * exemplo de uso
     * ToolBox_Verivifacao_Campos.Campo_Obrigatorio_Email(context, et_email, et_email.getText().toString().trim());
     */
    public final static boolean Funcao_Campo_Obrigatorio_Email(Context context, EditText
            editText_para_ser_selecionado, String txtEmail) {


        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = txtEmail;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            return true;
        } else {

            editText_para_ser_selecionado.requestFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText_para_ser_selecionado, InputMethodManager.SHOW_IMPLICIT);

            return false;
        }


    }

    /**
     * Função para verificar se um array com a função Campo_Obrigatorio retornaram todos ok
     * <p>
     * Exemplo funcional
     * <p>
     * boolean[] Array_De_Obrigatorios = new boolean[2];
     * // no Array_De_Obrigatorios tem que ter o numero de campos a serem validados, o 0 (ZERO) não conta
     * // mas nos Array_De_Obrigatorios[0] e contado como uma possição , abaixo sempre tem que iniciar do 0 (ZERO)
     * Array_De_Obrigatorios[0] = ToolBox_Verivifacao_Campos.Campo_Obrigatorio(context, Senha, Senha.getText().toString().trim());
     * Array_De_Obrigatorios[1] = ToolBox_Verivifacao_Campos.Campo_Obrigatorio(context, Usuario, usuarios);
     */
    public static boolean Funcao_Verifica_Se_Todos_Foram_Preenchidos(boolean[] array) {
//        Log.w("Array de Obrigatorios", Arrays.toString(array));
        for (boolean b : array) if (!b) return false;
        return true;
    }


    /**
     * Função para validação de Email digitado
     */

    public final static boolean Funcao_ValidarEmail(String txtEmail) {

        String MsgDeERROR = "E-mail Invalido";
        Log.w("Validando o Email = ", txtEmail + "");
        if (TextUtils.isEmpty(txtEmail) || txtEmail.trim().isEmpty()) {
            Log.w("Email Invalido = ", txtEmail + "");
            return false;

        } else {
            Log.w("Email valido = ", txtEmail + "");
            Log.w("Email valido = ", android.util.Patterns.EMAIL_ADDRESS.matcher(txtEmail).matches() + "");
            return android.util.Patterns.EMAIL_ADDRESS.matcher(txtEmail).matches();
        }
    }

    /**
     * Função para validação de CPF
     */
    public static boolean Funcao_ValidarCPF(Context context, String CPF) {

        String MsgDeERROR = "CPF Invalido";

        CPF = Funcao_Retirar_Mascara(CPF);
        if (CPF.isEmpty() || CPF.equals("00000000000") || CPF.equals("11111111111")
                || CPF.equals("22222222222") || CPF.equals("33333333333")
                || CPF.equals("44444444444") || CPF.equals("55555555555")
                || CPF.equals("66666666666") || CPF.equals("77777777777")
                || CPF.equals("88888888888") || CPF.equals("99999999999")) {

            return false;
        }
        char dig10, dig11;
        int sm, i, r, num, peso;
        try {
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else
                dig10 = (char) (r + 48);
            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig11 = '0';
            else
                dig11 = (char) (r + 48);
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10))) {
                return (true);
            } else {
                return (false);
            }
        } catch (Exception erro) {
            return (false);
        }
    }


    /**
     * Função para etirar qualquer caractere e deixar string limpa para teste
     */
    public static String Funcao_Retirar_Mascara(String s) {
        return s.replaceAll("[.]", "").replaceAll("[-]", "")
                .replaceAll("[/]", "").replaceAll("[(]", "")
                .replaceAll("[)]", "");
    }

}
