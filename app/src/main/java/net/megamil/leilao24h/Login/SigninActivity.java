/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package net.megamil.leilao24h.Login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.isseiaoki.simplecropview.util.Utils;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Criptografia;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Imagem;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Mascaras;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Permissao;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Sistema;
import net.megamil.leilao24h.Utils.ToolBox.toolbox_Modal;
import net.megamil.library.ActivitySuport.WebViewActivity;
import net.megamil.library.BaseActivity.BaseActivity;
import net.megamil.library.Utils.ActivityUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import maripoppis.com.connection.ConstantsUrls.ConstantsConnect;
import maripoppis.com.socialnetwork.FaceBook.FacebookData;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;
import static net.megamil.leilao24h.Utils.ToolBox.A_ToolBox_Gerador_De_Dados.Spinner_Estados_do_Brasil_white;
import static net.megamil.leilao24h.Utils.ToolBox.toolbox_Modal.OPEN_CROPVIEW;

public class SigninActivity extends BaseActivity implements WebViewActivity.TermsOfUseCallback {

    private static FacebookData facebookData;
    @BindView(R.id.btnLogin)
    TextView btnBack;

    private static CircleImageView img_usuario;
    private static CircleImageView img_plus;

    private static EditText et_nome;
    private static EditText et_telefone;
    private static EditText et_email;
    private static EditText et_senha;
    private static EditText et_senha_confirm;
    private static EditText et_code;

    private Spinner sp_estados;
    private int spIndice = 0;
    private CheckBox chbTermos;
    private TextView txtTermosDeUso;

    private static String fotoUser;

    private Button btn_cadastra;


    public interface SetLogin {
        void DadosLogin(String nome, String senha);
    }

    private static SetLogin delegate;

    public void setOnMudarUltimaTelaListener(SigninActivity.SetLogin delegate) {
        this.delegate = delegate;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFirstContainerView(R.layout.activity_signin);

        ButterKnife.bind(this);

        WebViewActivity.setTermsCallback(this);


        initVars();
        initActions();
        verifyBundleData();
    }

    //==============================================================================================
    //
    //  Facebook Bundle Data
    //
    //==============================================================================================

    private void verifyBundleData() {

        if (getIntent().getExtras().size() > 0 && getIntent().getExtras().containsKey(FacebookData.getKeyIntent())) {

            facebookData = (FacebookData) getIntent().getExtras().getSerializable(FacebookData.getKeyIntent());
            et_nome.setText(facebookData.getName());
            et_email.setText(facebookData.getEmail());

            et_senha.setVisibility(View.GONE);
            et_senha_confirm.setVisibility(View.GONE);
        }

    }
//
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//
//        view = inflater.inflate(R.layout.fragment_activity_login_cadastro, container, false);
//
//        initVars(view);
//        initActions();
//
////        ToolBox_Views.setupParent(activity ,view);
//
//        return view;
//    }

    private void initVars() {

        img_usuario = findViewById(R.id.login_frag_cadastro_et_usuario_img_principal);
        img_plus = findViewById(R.id.iv_pupila_foto_icone_foto_olho_viewpager_pronturio_esquerdo_f02);

        et_nome = findViewById(R.id.login_frag_cadastro_et_usuario);
        et_telefone = findViewById(R.id.login_frag_login_et_usuario_telefone);
        et_email = findViewById(R.id.login_frag_cadastro_et_usuario_email);
        et_senha = findViewById(R.id.login_frag_login_et_senha);
        et_senha_confirm = findViewById(R.id.login_frag_login_et_senha_confirm);
        et_code = findViewById(R.id.login_frag_login_et_usuario_code);

        sp_estados = findViewById(R.id.sp_estados);
        Spinner_Estados_do_Brasil_white(activity, sp_estados);

        chbTermos = findViewById(R.id.chbTermos);
        txtTermosDeUso = findViewById(R.id.txtTermosDeUso);

        btn_cadastra = findViewById(R.id.login_frag_login_btn_cadastro);

    }

    private void initActions() {

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        sp_estados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spIndice = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        txtTermosDeUso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AbrirTermos();
            }
        });

        img_usuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean permissao = ToolBox_Permissao.Funcao_Verifica_Permissao(activity, ToolBox_Permissao.VERIFICA_CAMERA);

                if (permissao) {
                    toolbox_Modal.ChamaModal(activity, toolbox_Modal.Uri_Camera);
                }
            }
        });

        et_telefone.addTextChangedListener(ToolBox_Mascaras.Funcao_MascaraParaTelefone(et_telefone));

        btn_cadastra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chbTermos.isChecked()) {
                    boolean conn = ToolBox_Verivifacao_Sistema.Funcao_Status_Conexao(activity); // função para checar a conexao antes
                    if (conn) {

                        final String nome_usuario = et_nome.getText().toString().trim();
                        final String telefone = ToolBox_Verivifacao_Campos.Funcao_Retirar_Mascara(et_telefone.getText().toString().trim());
                        final String email = et_email.getText().toString().trim();
                        final String senha = et_senha.getText().toString().trim();

                     /*
                     Aqui inicia a verificação de campos obrigatorios
                     array boolean armazena true ou false para ser verificado
                     se tudo dentro do array estir em true , o dados sao enviados para o webservice
                     do contrario , uma msg é exibida para o usuarios
                      */
                        boolean validação_final;
                        if (facebookData != null) {
                            boolean[] Array_De_Obrigatorios = {
                                    // no Array_De_Obrigatorios tem que ter o numero de campos a serem validados, o 0 (ZERO) não conta
                                    // mas nos Array_De_Obrigatorios[0] e contado como uma possição , abaixo sempre tem que iniciar do 0 (ZERO)

                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_telefone, telefone),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_email, email),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_nome, nome_usuario),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio_Numero(activity, et_telefone, et_telefone.getText().toString().trim(), 8),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio_Email(activity, et_email, et_email.getText().toString().trim())
//                            ToolBox_Verivifacao_Campos.Funcao_ImgSelecionada(activity, fotoUser),

                            };
                            validação_final = ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios);
                        } else {
                            boolean[] Array_De_Obrigatorios = {
                                    // no Array_De_Obrigatorios tem que ter o numero de campos a serem validados, o 0 (ZERO) não conta
                                    // mas nos Array_De_Obrigatorios[0] e contado como uma possição , abaixo sempre tem que iniciar do 0 (ZERO)

                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_telefone, telefone),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_email, email),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_senha, senha),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Min_Caracteres(et_senha, 6),
                                    ToolBox_Verivifacao_Campos.Funcao_VerificaSenhas(et_senha, et_senha_confirm),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_nome, nome_usuario),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio_Numero(activity, et_telefone, et_telefone.getText().toString().trim(), 8),
                                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio_Email(activity, et_email, et_email.getText().toString().trim())
//                            ToolBox_Verivifacao_Campos.Funcao_ImgSelecionada(activity, fotoUser),

                            };
                            validação_final = ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios);
                        }


//                    Log.w("Array de obrigatorios", Arrays.toString(Array_De_Obrigatorios));

                        // esta variavel armazena apenas 1 true ou um false , apos verificar se o array esta completo com true
                        // ela retornara em true , se estiver apenas 1 false, retornara false e o codigo não segue


//                    Log.w("array de obrigatorios", validação_final + "");

                        if (validação_final) {

//                        String senhaSha1 = ToolBox_Criptografia.Funcao_CriptoPASS(senha);
                            String senhaSha1 = ToolBox_Criptografia.Funcao_Converter_String_To_SHA1(senha);
                            String senhaMd5 = ToolBox_Criptografia.md5(senha);

                            String Dados_Para_URL_GET = null;
                            Map<String, String> Dados_Para_Parametros = new HashMap<>();

                            Dados_Para_URL_GET = "";
                            Dados_Para_Parametros.put("nome_usuario", nome_usuario);
                            Dados_Para_Parametros.put("celular_usuario", telefone);
                            Dados_Para_Parametros.put("email_usuario", email);
                            Dados_Para_Parametros.put("estado_usuario", String.valueOf(spIndice));
                            Dados_Para_Parametros.put("senha_usuario", senhaSha1);
                            Dados_Para_Parametros.put("kfil", senhaMd5);

                            String code = ToolBox_Verivifacao_Campos.tGetText(et_code);
                            if (code != "") {
                                Dados_Para_Parametros.put("code", code);
                            }

                            if (facebookData != null) {
                                Dados_Para_Parametros.put("facebook_usuario", facebookData.getId());
                            }

                            Dados_Ws(3, Dados_Para_URL_GET, Dados_Para_Parametros, activity);
                        } else {
                            MsgUtil.Funcao_MSG(activity, "Revise os Campos");
                        }

                    } else {
                        MsgUtil.Funcao_MSG(activity, activity.getResources().getString(R.string.msg_global_sem_conexao));
                    }

                } else {
                    AbrirTermos();
                }

//                ToolBox.ExibeMSG(senha,context);
//                ToolBox.ExibeMSG(usuarios + " " + senha,context);
//                Intent pagina_usuario = new Intent(context, Pagina_Usuario.class);
//                context.startActivity(pagina_usuario);
//                getActivity().finish();

            }
        });
//        initJsonFacebook();
    }

    public void AbrirTermos() {
//        final Dialog dialog = new Dialog(activity, R.style.DialogFullscreen);
//        dialog.setContentView(R.layout.dialog_termos);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogNoAnimation;
//
//        WebView view = new WebView(activity);
//        view.setVerticalScrollBarEnabled(false);
//
//        ((LinearLayout) dialog.findViewById(R.id.ll)).addView(view);
//
////        String html = activity.getResources().getString(R.string.tela_politica_de_privacidade);
////        view.loadData(html, "text/html; charset=utf-8", "utf-8");
//
////        view.getSettings().setJavaScriptEnabled(true);
////        view.loadDataWithBaseURL(null, "<html><body>" + activity.getResources().getString(R.string.termos) + "</body></html>", "text/html", "utf-8", null);
////        view.loadDataWithBaseURL("x-dados://base", "<html><body>" + activity.getResources().getString(R.string.termos) + "</body></html>", "text/html", "UTF-8", null);
//        Button aceitar = dialog.findViewById(R.id.aceitar);
//        Button rejeitar = dialog.findViewById(R.id.rejeitar);
//
//        aceitar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                chbTermos.setChecked(true);
//                dialog.dismiss();
//            }
//        });
//
//        rejeitar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();

        WebViewActivity.callActivityWebViewForTermOfUser(activity, ConstantsConnect.getUrlTermOfUse(), false);

    }


    public static void LimpaCampos() {

        et_nome.setText("");
        et_telefone.setText("");
        et_email.setText("");
        et_senha.setText("");
        et_senha_confirm.setText("");
        et_code.setText("");

        final String login = et_nome.getText().toString().trim();
        final String senha = et_senha.getText().toString().trim();

        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        if (delegate != null) {
            delegate.DadosLogin(login, senha);
            facebookData = null;
        }

    }

    public void addDataFromFacebook(final FacebookData map) {
        facebookData = map;
    }

//    private void initJsonFacebook() {
//
//        if (facebookData != null) {
//            if (et_nome != null) {
//                et_nome.setText(facebookData.getName());
//                et_email.setText(facebookData.getEmail());
//
//                et_senha.setVisibility(View.GONE);
//                et_senha_confirm.setVisibility(View.GONE);
//            }
//        }
//    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

//        Log.w("Entrei", "ActivityResult");

        if (resultCode == RESULT_OK && reqCode == toolbox_Modal.REQUEST_LOAD_IMG) {
            Log.w("Entrei", "Galeria");

            Uri imageUri = data.getData();
//                Bitmap selectedImage = ToolBox_Imagem.Funcao_decodeUri(activity, imageUri);
            Boolean imagem_valida = ToolBox_Imagem.Funcao_Verifica_Tamanho_IMG(activity, imageUri);
            if (imagem_valida) {

                if (OPEN_CROPVIEW) {
                    toolbox_Modal.ChamaCropView(activity, imageUri);
                } else {
                    toolbox_Modal.Carrega_Imagem_Selecionada(activity, imageUri, 1);
                }

            }


        } else if (resultCode == RESULT_OK && reqCode == toolbox_Modal.REQUEST_IMAGE_CAPTURE) {
            Log.w("Entrei", "Camera");

//            Uri uri = dados.getDados();
//            Bundle extras = dados.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("dados");
//
//            final String nome_foto = "principal.jpg";
//            final String imagem = _ToolBox_Config_Inicial.Constante_Pasta_Projeto_Imagens + nome_foto;
//            Uri imageUri_Cam = Novo_Uri_Imagem(context, Bitmap.CompressFormat.JPEG);
//            Bitmap bitmap = BitmapFactory.decodeFile(imagem);

            if (OPEN_CROPVIEW) {
                toolbox_Modal.ChamaCropView(activity, toolbox_Modal.Uri_Camera);
            } else {
                toolbox_Modal.Carrega_Imagem_Selecionada(activity, toolbox_Modal.Uri_Camera, 1);
            }

        } else {
            MsgUtil.Funcao_MSG(activity, "Foto não Capturada.");
        }
        System.gc();
    }

    //FUNCAO RESPONSAVEL POR REDIMENCIONAR A IMAGEM ANTES DE ADICIONAR A TELA
    //APOS REDIMENCIONAR , ESTA FUNÇÃO ADICIONA A IMAGEM A TELA

    public static class Redimencionar_Imagem implements Runnable {
        private Handler mHandler = new Handler(Looper.getMainLooper());
        Context context;
        Uri uri;
        ImageView imageView;
        int width;

        public Redimencionar_Imagem(Context context, Uri uri, int width) {
            this.context = context;
            this.uri = uri;
            this.width = width;
        }

        @Override
        public void run() {
            final int raw_exifRotation = Utils.getExifOrientation(context, uri);

            final int exifRotation = Utils.getRotateDegreeFromOrientation(raw_exifRotation);

            ToolBox_SharedPrefs toolBox_sharedPrefs = new ToolBox_SharedPrefs(activity);
            String path = toolBox_sharedPrefs.getPhoto_path();
            Log.w("Path", path);

            final int ori = toolbox_Modal.Funcao_Retorna_Orientacao_Imagem(context, uri, path);

            int maxSize = Utils.getMaxSize();
            int requestSize = Math.min(width, maxSize);
            try {
//                final Bitmap raw_sampledBitmap = Utils.decodeSampledBitmapFromUri(context, uri, requestSize);
//
//                final Bitmap sampledBitmap = toolbox_Modal.Funcao_Rotaciona_Imagem(raw_sampledBitmap, ori);

                //AQUI CHAMO A FUNÇÃO PARA GUARDAR A IMAGEM EM UM CHAR64 OU DECODIFICAR ELA
                //Encode_Decode_Imagem64(sampledBitmap);

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {

//                        Bitmap bitmap = null; // nao carrega a imagem - le informacoes da imagem options
//                        try {
//                            bitmap = ToolBox_Imagem.Funcao_decodeUri(activity, uri);
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        }


                        try {
                            FutureTarget<Bitmap> futureTarget =
                                    Glide.with(context)
                                            .asBitmap()
                                            .load(uri)
                                            .submit(400, 400);

                            Bitmap bitmap = futureTarget.get();

                            Glide.with(activity).asBitmap()
                                    .load(bitmap)
                                    .into(img_usuario);
                            fotoUser = ToolBox_Imagem.Funcao_Imagem_encodeToBase64(bitmap);

                        } catch (ExecutionException e) {

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
//                        img_usuario.setImageMatrix(Utils.getMatrixFromExifOrientation(exifRotation));
//                        img_usuario.setImageBitmap(sampledBitmap);

//                        Picasso.get().load(uri)
////                                .placeholder(R.drawable.images).error(R.drawable.ic_launcher)
//                                .into(img_usuario);

//                        Glide
//                                .with(activity)
//                                .load(raw_sampledBitmap)
//                                .apply(
//                                        new RequestOptions().dontAnimate()
//                                )
//                                .into(img_usuario);
//                                .get();

//                        Glide.with(activity).load(sampledBitmap).into(img_usuario);
//
//  Picasso.with(this).load(uri).into(imageView);

                    }
                });
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    public static Bitmap rotateImage(Bitmap source, float angle) {
//        Matrix matrix = new Matrix();
//        matrix.postRotate(angle);
//        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
//    }
//
//    public static int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
//        int rotate = 0;
//        try {
//            context.getContentResolver().notifyChange(imageUri, null);
//            File imageFile = new File(imagePath);
//
//            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//
//            switch (orientation) {
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    rotate = 270;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    rotate = 180;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    rotate = 90;
//                    break;
//            }
//
//            Log.i("RotateImage", "Exif orientation: " + orientation);
//            Log.i("RotateImage", "Rotate value: " + rotate);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return rotate;
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityUtil.callActivity(activity, LoginActivity.class, true);
    }

    //==============================================================================================
    //
    // Terms of user
    //
    //==============================================================================================

    @Override
    public void TermsOfUseAccept() {
        chbTermos.setChecked(true);
    }

    @Override
    public void TermsOfUseRegected() {
        chbTermos.setChecked(false);
    }

    //==============================================================================================
    //
    // Key For Extras
    //
    //==============================================================================================

    public static String keyName = "keyname";
    public static String keyPass = "keypass";
}
