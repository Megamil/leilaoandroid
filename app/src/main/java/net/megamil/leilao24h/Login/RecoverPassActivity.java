/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package net.megamil.leilao24h.Login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Sistema;
import net.megamil.library.BaseActivity.BaseActivity;
import net.megamil.library.Utils.ActivityUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;

/**
 * Created by John on 09/12/2017.
 */

public class RecoverPassActivity extends BaseActivity {

    @BindView(R.id.login_frag_rec_et_email)
    EditText et_email_usuario;

    @BindView(R.id.login_frag_btn_rec_senha)
    Button btn_rec;

    @BindView(R.id.btnLogin)
    TextView btnBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFirstContainerView(R.layout.activity_recover_password);

        ButterKnife.bind(this);

        initVars();
        initActions();
    }

    private void initVars() {

    }

    private void initActions() {

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_rec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean conn = ToolBox_Verivifacao_Sistema.Funcao_Status_Conexao(activity); // função para checar a conexao antes
                if (conn) {

                    String usuario = et_email_usuario.getText().toString().trim();


                    boolean[] Array_De_Obrigatorios = new boolean[1];

                    Array_De_Obrigatorios[0] = ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_email_usuario, usuario);
                    Boolean validação_final = ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios);

                    if (validação_final) {
                        String Dados_Para_URL_GET = "email_usuario=" + usuario;
                        Dados_Ws(1, Dados_Para_URL_GET, null, activity);

                    } else {
                        MsgUtil.Funcao_MSG(activity, activity.getResources().getString(R.string.msg_global_sem_conexao));
                    }


                } else {
                    MsgUtil.Funcao_MSG(activity, activity.getResources().getString(R.string.msg_global_sem_conexao));
                }
            }

        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityUtil.callActivity(activity, LoginActivity.class, true);
    }
}

