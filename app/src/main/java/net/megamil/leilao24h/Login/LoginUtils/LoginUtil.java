/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package net.megamil.leilao24h.Login.LoginUtils;

import android.app.Activity;
import android.content.Intent;

import net.megamil.leilao24h.Login.LoginActivity;
import net.megamil.leilao24h.Usuario.Busca.DB_Busca;
import net.megamil.leilao24h.Usuario.Pagina_Inicio;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SQlite;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;
import net.megamil.library.Utils.ActivityUtil;

import org.json.JSONException;

import maripoppis.com.connection.Model.WSResponse;
import maripoppis.com.socialnetwork.FaceBook.FacebookDataUtils;

public class LoginUtil {

    public static void deleteLogin(Activity activity) {
        final DB_Busca db_busca = new DB_Busca(activity);//conexao
        db_busca.DeletaBanco_Historico_Busca();

        ToolBox_SQlite db = new ToolBox_SQlite(activity);
        db.clear();

        ToolBox_SharedPrefs toolBox_sharedPrefs = new ToolBox_SharedPrefs(activity);
        toolBox_sharedPrefs.setTMP_ID("");
        toolBox_sharedPrefs.setTMP_HASH("");
        toolBox_sharedPrefs.setCancelDegug();

        ToolBox_SharedPrefs_Usuario toolBox_sharedPrefs_usuario = new ToolBox_SharedPrefs_Usuario(activity);
        toolBox_sharedPrefs_usuario.clear();

        FacebookDataUtils.FacebookLogout();

        Intent intent = new Intent(activity, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void whiteLogin(Activity activity, WSResponse response , String idFacebook) {

        WSResponse.dados dados = response.getDados();
        ToolBox_SharedPrefs_Usuario prefs = new ToolBox_SharedPrefs_Usuario(activity);

        if (!idFacebook.equals("")) {
            prefs.setURL_IMAGE_FACEBOOK(idFacebook);
        }

//        ToolBox_SQlite db = new ToolBox_SQlite(activity);
//        db.setDBToken(dados.getTokenAcesso());

        prefs.setLOGADO(true);
        prefs.setID_USUARIO(dados.getIdUsuario());
        prefs.setNOME_USUARIO(dados.getNomeUsuario());
        prefs.setEMAIL_USUARIO(dados.getEmailUsuario());
        prefs.setFK_GRUPO_USUARIO(dados.getFkGrupoUsuario());
        prefs.setATIVO_USUARIO(dados.getAtivoUsuario());
        prefs.setTOKEN(dados.getTokenAcesso());
        prefs.setCODE_SHARE(dados.getCode());

        Boolean prefs_ok = prefs.getLOGADO();

        if (prefs_ok) {
            ActivityUtil.callActivity(activity, Pagina_Inicio.class, true);
        }

    }
}
