/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package net.megamil.leilao24h.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.accountkit.AccountKitLoginResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.megamil.leilao24h.Login.LoginUtils.LoginUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import maripoppis.com.connection.Connect;
import maripoppis.com.connection.ConstantsUrls.ConstantsConnect;
import maripoppis.com.connection.Model.SubModels.Login;
import maripoppis.com.connection.Model.WSResponse;
import maripoppis.com.socialnetwork.FaceBook.FacebookAcountKitUtils;
import maripoppis.com.socialnetwork.FaceBook.FacebookData;
import maripoppis.com.socialnetwork.FaceBook.FacebookDataUtils;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.Api_Login.FireBase.FirebaseUtil;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Criptografia;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Sistema;
import net.megamil.library.ActivitySuport.WebViewActivity;
import net.megamil.library.BaseActivity.BaseActivity;
import net.megamil.library.Utils.ActivityUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends BaseActivity implements FacebookDataUtils.FacebookDataCallback, Connect.ConnectCallback {

    @BindView(R.id.login_frag_login_et_usuario)
    EditText Usuario;

    @BindView(R.id.login_frag_login_et_senha)
    EditText Senha;

    @BindView(R.id.login_frag_login_btn_login)
    Button btnSimpleLogin;

    @BindView(R.id.telalogin_btn_recuperar)
    TextView btnRecoverPass;

    @BindView(R.id.telalogin_btn_cadastro)
    TextView btnSingin;

    @BindView(R.id.tela_login_btn_politica_de_privacidade)
    Button btnPrivacityPolicy;

    private boolean googleServicesNeedsUpdate;

    //Login
    public static String idUser = "";
    public Login loginModel;

    //Botao de Login com FaceBook
    @BindView(R.id.ll_btn_fb)
    LinearLayout btnFacebookLogin;
    private CallbackManager facebookCallbackManager;
    private static String facebookID = "";

    int normalLogin = 1;
    int facebookLogin = 2;

    //Token
    private static String token;

    //conection
    Connect connect;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFirstContainerView(R.layout.activity_login);

        ButterKnife.bind(this);

        googleServicesNeedsUpdate = FirebaseUtil.verifieGoogleServisesVersion(activity);

        initVars();
        initActions();
        //
        initFirebase();
        initFacebook();
        //
        startConectService();
        verifyBundleData();

    }


    private void initVars() {

    }

    private void initActions() {

        btnRecoverPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtil.callActivity(activity, RecoverPassActivity.class, true);
            }
        });

        btnSingin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtil.callActivity(activity, SigninActivity.class, true);
            }
        });

        btnPrivacityPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebViewActivity.callActivityWebView(activity, ConstantsConnect.getUrlPrivaciryPolicy(), false);
            }
        });

        btnSimpleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initLogin(normalLogin);
            }
        });
    }

    //==============================================================================================
    //
    // Login init
    //
    //==============================================================================================
    private void initLogin(int type) {

        if (!googleServicesNeedsUpdate) {
            boolean conn = ToolBox_Verivifacao_Sistema.Funcao_Status_Conexao(activity); // função para checar a conexao antes
            if (conn) {

                switch (type) {
                    case 1:

                        String senhaSha1 = ToolBox_Criptografia.Funcao_Converter_String_To_SHA1(Senha.getText().toString().trim());
                        String usuario = Usuario.getText().toString().trim();

                        ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
                        prefs.setTMP_ID(usuario);
                        prefs.setTMP_HASH(senhaSha1);

                        boolean[] Array_De_Obrigatorios = {
                                ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, Senha, Senha.getText().toString().trim()),
                                ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, Usuario, usuario)
                        };

                        if (ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios)) {
                            loginModel = new Login(usuario, senhaSha1, token);
                            tryConection();
                        } else {
                            MsgUtil.Funcao_MSG(activity, getString(R.string.msg_revise_fields));
                        }

                        break;
                    case 2:
                        loginModel = new Login(token, facebookID);
                        tryConection();
                        break;
                }

            } else {
                MsgUtil.Funcao_MSG(activity, activity.getResources().getString(R.string.msg_global_sem_conexao));
            }

        } else {
            googleServicesNeedsUpdate = FirebaseUtil.verifieGoogleServisesVersion(activity);
        }

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (data != null) {
            facebookCallbackManager.onActivityResult(reqCode, resultCode, data);
        }

        if (reqCode == FacebookAcountKitUtils.APP_REQUEST_CODE) {
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);

            if (loginResult.getError() != null) {

            } else if (loginResult.wasCancelled()) {

            } else {
                if (loginResult.getAccessToken() != null) {

                } else {

//                    Map<String, String> Dados_Para_Parametros = new HashMap<>();
//                    Dados_Para_Parametros.put("token", token);
//                    if (loginWithFaceBook) {
//                        Dados_Para_Parametros.put("facebook_usuario", facebookID);
//                        Dados_Para_Parametros.put("facebook_url_image", FacebookDataUtils.getProfilePicUrl(facebookID));
//                    }
//                    Dados_Para_Parametros.put("id_usuario", idUser);

                    tryConectionSms();
//                    Dados_Ws(201, "", Dados_Para_Parametros, activity);
                }

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...
//                Intent intent = new Intent(this, MainActivity.class);
//                startActivity(intent);
            }

//            if (!toastMessage.equals("")) {
//                MsgUtil.Funcao_MSG(activity, toastMessage);
//            }

//            AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
//                @Override
//                public void onSuccess(final Account account) {
//                    // Get Account Kit ID
//                    String accountKitId = account.getId();
//
//                    // Get phone number
//                    PhoneNumber phoneNumber = account.getPhoneNumber();
//                    if (phoneNumber != null) {
//                        String phoneNumberString = phoneNumber.toString();
//                    }
//
//                    // Get email
//                    String email = account.getEmail();
//                }
//
//                @Override
//                public void onError(final AccountKitError error) {
//                    // Handle Error
//                }
//            });
        }
    }


//    public static void sendDataFacebook() {
//        if (mDelegate != null) {
//            mDelegate.getDados(mDelegate);
//        }
//    }

    //==============================================================================================
    //
    // FireBase
    //
    //==============================================================================================

    private void initFirebase() {
        FirebaseApp.initializeApp(activity);
        token = FirebaseInstanceId.getInstance().getToken();
        MsgUtil.logW("O TOKEN DO FIREBASE É = " + token);
    }

    //==============================================================================================
    //
    // Facebook
    //
    //==============================================================================================

    private static FacebookData mFacebookData;

    public void initFacebook() {
        FacebookDataUtils.setCallback(this);
        facebookCallbackManager = CallbackManager.Factory.create();

        btnFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                facebookID = "";
                FacebookDataUtils.startFacebookLogin(activity, facebookCallbackManager);
            }
        });
    }

    @Override
    public void FacebookDataSuccess(FacebookData facebookData) {
        mFacebookData = facebookData;
//        loginWithFaceBook = true;
        facebookID = facebookData.getId();
        initLogin(facebookLogin);
    }

    //==============================================================================================
    //
    // Signin Callback
    //
    //==============================================================================================

    private void verifyBundleData() {

        Bundle bundle = getIntent().getExtras();

        assert bundle != null;
        if (bundle.size() > 0 && getIntent().hasExtra(SigninActivity.keyName)) {

            Usuario.setText(bundle.getString(SigninActivity.keyName));
            Senha.setText(ToolBox_Criptografia.md5Decode(bundle.getString(SigninActivity.keyPass)));
            btnSimpleLogin.performClick();

        }

    }

    //==============================================================================================
    //
    // send Facebook dados for Signin
    //
    //==============================================================================================

    public static void sendDataFacebook() {
        Intent signin = new Intent(activity, SigninActivity.class);
        signin.putExtra(FacebookData.getKeyIntent(), mFacebookData);
        ActivityUtil.callActivity(activity, signin, true);
    }

    //==============================================================================================
    //
    // Conection Login Callback
    //
    //==============================================================================================

    private void startConectService() {
        Connect.setCallback(this);
        connect = new Connect(activity);
    }

    private void tryConection() {
        WSResponse.dados dadosWS = new WSResponse.dados();
        dadosWS.setLogin(loginModel);
        connect.getDataFrom(Connect.ConnectionType.WS_LOGIN, dadosWS, true);
    }

    private void tryConectionSms() {
        WSResponse.dados dadosWS = new WSResponse.dados();
        loginModel.setId_usuario(idUser);
        dadosWS.setLogin(loginModel);
        connect.getDataFrom(Connect.ConnectionType.WS_SMS_ACTIVATE, dadosWS, true);
    }


    @Override
    public void ConnectionSuccess(WSResponse response, Connect.ConnectionType connectionTypeID) {

        int status = response.getStatus();

        switch (status) {
            case 1:
                if (connectionTypeID == Connect.ConnectionType.WS_SMS_ACTIVATE) {
                    tryConection();
                } else {
                    goLogin(response);
//                    LoginUtil.whiteLogin(activity, response, facebookID);
                }
                break;
            case -5:
                //Create gson instance
                Gson gson = new Gson();
                try {
                    Conexao_WS.whitePerfil(activity, response.getResultado(), new JSONObject(gson.toJson(response)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    @Override
    public void ConnectionError(int statusCallback, WSResponse response, Connect.ConnectionType connectionTypeID) {
        facebookID = "";

        if (statusCallback == Connect.STATUS_NEW_FACEBOOK_USER) {
            sendDataFacebook();
        }

        if (statusCallback == Connect.STATUS_SMS_ACTIVATION) {
            idUser = response.getId_usuario();
        }

    }

    @Override
    public void ConnectionFail(Throwable t) {

    }

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    public static void goLogin(WSResponse response) {
        LoginUtil.whiteLogin(activity, response, facebookID);
    }
}
