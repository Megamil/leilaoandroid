package net.megamil.leilao24h.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import net.megamil.leilao24h.R;


public class Politica_Privacidade extends AppCompatActivity {

    private Context context;
    private Window window;

    ImageButton imgButtonVoltar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_politica_privacidade);

        initVars();
//        initActions();
    }

    private void initVars() {
        imgButtonVoltar = findViewById(R.id.btn_politica_privacidade_voltar_login);
        context = getApplicationContext();
        window = getWindow();

        WebView view = new WebView(this);
        view.setVerticalScrollBarEnabled(false);

        ((LinearLayout)findViewById(R.id.relativeLayout2)).addView(view);
//        view.loadData(getString(R.string.tela_politica_de_privacidade), "text/html; charset=utf-8", "utf-8");
    }

//    private void initActions() {
//        imgButtonVoltar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ChamaPrimeiraTela();
//            }
//        });
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        ChamaPrimeiraTela();
//    }

//    private void ChamaPrimeiraTela() {
//        Intent intent = new Intent(context, Pagina_Login.class);
//        startActivity(intent);
//        finish();
//    }


}


