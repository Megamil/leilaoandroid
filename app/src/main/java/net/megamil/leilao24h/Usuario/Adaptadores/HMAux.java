package net.megamil.leilao24h.Usuario.Adaptadores;

import java.util.HashMap;

/**
 * Created by nalmir on 16/11/2017.
 */

public class HMAux extends HashMap<String, String> {

//    ToLowerCase ou ToUpperCase= ctrl + shif +U

    public static final String ID = "_id";
    public static final String ID_POSITION = "id_position";
    public static final String ID_PRODUTO = "id_produto";
    public static final String INTENT_TYPE = "intent_type";
    public static final String NORTIFICATION = "nortification";

    //busca
    public static final String BUSCA_TEXTO_PESQUISA = "texto_pesquisa";
    public static final String NOME_CATEGORIA = "nome_categoria";

    //favoritos
    public static final String FAVORITO_NOME = "favorito_nome";

    //categorias
    public static final String ID_CATEGORIA = "id_categoria";
    public static final String FK_CATEGORIA_PAI = "fk_categoria_pai";

    //items
    public static final String NOME_ITEM = "nome_item";
    public static final String ITEM_FAVORITO = "item_favorito";

    //Lista de leiloes
    public static final String ID_LEILAO = "id_leilao";
    public static final String ID_PROCUTO = "id_produto";
    public static final String NOME_PRODUTO = "nome_produto";
    public static final String DESCRICAO_PRODUTO = "descricao_produto";
    public static final String VALOR_MINIMO = "valor_minimo";
    public static final String LANCE_MINIMO = "lance_minimo";
    public static final String INGRESSOS = "ingressos";
    public static final String DATA_INICIO = "data_inicio";
    public static final String DATA_FIM_PREVISTO = "data_fim_previsto";
    public static final String AVALIACAO = "avaliacao";
    public static final String FAVORITO = "favorito";
    public static final String IMAGENS = "imagens";
    public static final String PRODUTO_USADO = "produto_usado";
    public static final String IMAGENS_ARRAY = "imagens_array";
    public static final String PRODUTO_RECOMPENSA_CUSTO = "produto_recompensa_custo";
    public static final String AVALIADO = "avaliado";

    //Planos e Recompensas
    public static final String ID_PACOTE_INGRESSO = "id_pacote_ingresso";
    public static final String DESCRICAO_PACOTE = "descricao_pacote";
    public static final String QUANTIDADE_INGRESSO = "quantidade_ingresso";
    public static final String VALOR_PACOTE = "valor_pacote";

    //Meus Leilões
    public static final String STATUS_LEILAO = "status_leilao";

    //Historico
    public static final String ID_LANCE = "id_lance";
    public static final String VALOR = "valor";
    public static final String DATA = "dados";

    //Afiliados
    public static final String NOME_USUARIO = "nome_usuario";
    public static final String CRIADO_EM = "criado_em";

    //completar cadastro
    public static final String ID_USUARIO = "id_usuario";
    public static final String SEXO_USUARIO = "sexo_usuario";
    public static final String RG_USUARIO = "rg_usuario";
    public static final String CPF_USUARIO = "cpf_usuario";
    public static final String CELULAR_USUARIO = "celular_usuario";
    public static final String TELEFONE_USUARIO = "telefone_usuario";
    public static final String CEP_USUARIO = "cep_usuario";
    public static final String LOGRADOURO_USUARIO = "logradouro_usuario";
    public static final String NUMERO_USUARIO = "numero_usuario";
    public static final String BAIRRO_USUARIO = "bairro_usuario";
    public static final String CIDADE_USUARIO = "cidade_usuario";
    public static final String ESTADO_USUARIO = "estado_usuario";
    public static final String COMPLEMENTO_USUARIO = "complemento_usuario";
    public static final String EMAIL_USUARIO = "email_usuario";


    public static final String PREFERENCE = "preference";
    public static final String UNIT_PRICE = "unit_price";
    public static final String DATE_BUY = "date_buy";
    public static final String STATUS = "status" ;
    public static final String FINALIZADO = "finalizado";
}
