package net.megamil.leilao24h.Usuario.Adaptadores;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Arremates;

import java.util.List;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;

public class adapterHistoricoPagamento extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //First
    private static Activity activity;
    private int layout;
    private List<HMAux> dataBase;
    private HMAux hmAux = new HMAux();
    private LayoutInflater layoutInflater;
    private int viewPosition;

    //Second - View's Types
    private int NORMAL_TYPE = 0;
    private int FOOTER_TYPE = 1;
    private int END_TYPE = 2;
    private int ADS_TYPE = 3;

    /**
     * init Constructor with HMAux's List and Resorce
     *
     * @param activity activity pai
     * @param dataBase List de HMAux
     * @param layout   layout chamado de fora do metodo
     */
    public adapterHistoricoPagamento(Activity activity, List<HMAux> dataBase, int layout) {
        this.activity = activity;
        this.dataBase = dataBase;
        this.layout = layout;
        this.layoutInflater = LayoutInflater.from(activity);
    }

    /**
     * init Constructor with HMAux's List
     *
     * @param activity activity pai
     * @param dataBase HMAux's list
     */
    public adapterHistoricoPagamento(Activity activity, List<HMAux> dataBase) {
        this.activity = activity;
        this.dataBase = dataBase;
        this.layoutInflater = LayoutInflater.from(activity);
    }

    /**
     * @param list HMAux's list
     */
    public void addList(List<HMAux> list) {
        dataBase.addAll(list);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * @param hmAux add dados raw hmaux
     */
    public void addItem(HMAux hmAux) {
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * add raw item dados with position
     *
     * @param position int position
     * @param hmAux    raw dados
     */
    public void addItemAtPosition(int position, HMAux hmAux) {
        dataBase.add(position, hmAux);
        notifyItemInserted(position);
    }

    /**
     * remove at first position
     */
    public void removeFirstItem() {
        dataBase.remove(0);
        notifyItemRemoved(0);
    }

    /**
     * remove at last position
     */
    public void removeLastItem() {
        dataBase.remove(dataBase.size() - 1);
        notifyItemRemoved(dataBase.size() - 1);
    }

    /**
     * remove item at positon with parameter
     *
     * @param position
     */
    public void removeItemAtPosition(int position) {
        dataBase.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * add FooterView at RecyclerView
     */
    public void addFooterView() {
        hmAux.put("type", String.valueOf(FOOTER_TYPE));
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * add End at RecyclerView
     */
    public void addEndView() {
        hmAux.put("type", String.valueOf(END_TYPE));
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * add Google ADS at RecyclerView
     */
    public void addAdsView() {
        hmAux.put("type", String.valueOf(ADS_TYPE));
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param position
     * @return ViewType for load Resorce
     */
    @Override
    public int getItemViewType(int position) {
        HMAux s = dataBase.get(position);
        String typeView = s.get("type");

        if (s.containsKey("type")) {

            if (typeView.equals(String.valueOf(FOOTER_TYPE))) {
                return FOOTER_TYPE;
            } else if (typeView.equals(String.valueOf(END_TYPE))) {
                return END_TYPE;
            } else if (typeView.equals(String.valueOf(ADS_TYPE))) {
                return ADS_TYPE;
            } else {
                return NORMAL_TYPE;
            }

        } else {
            return NORMAL_TYPE;
        }
    }

    /**
     * @return HMAux's List size
     */
    @Override
    public int getItemCount() {
        return dataBase.size();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator animatorUP = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    animatorUP.setDuration(200);
                    animatorUP.setInterpolator(new DecelerateInterpolator());
                    animatorUP.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator animatorDOWN = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    animatorDOWN.setDuration(200);
                    animatorDOWN.setInterpolator(new AccelerateInterpolator());
                    animatorDOWN.start();
                    break;
            }
            return false;
        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * in class implements onMoveAndSwipedListener to use methods at down
     */

//    @Override
//    public boolean onItemMove(int fromPosition, int toPosition) {
//        Collections.swap(mItems, fromPosition, toPosition);
//        notifyItemMoved(fromPosition, toPosition);
//        return true;
//    }
//
//    @Override
//    public void onItemDismiss(final int position) {
//        mItems.remove(position);
//        notifyItemRemoved(position);
//
//        Snackbar.make(parentView, "Item Deleted", Snackbar.LENGTH_SHORT)
//                .setAction("Undo", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        addItem(position, mItems.get(position));
//                    }
//                }).show();
//    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == FOOTER_TYPE) {
            View view = layoutInflater.inflate(R.layout.celula_recycler_footer_vertical, parent, false);
            return new FooterViewHolder(view);
        } else if (viewType == END_TYPE) {
            View view = layoutInflater.inflate(R.layout.celula_recycler_end_vertical, parent, false);
            return new EndViewHolder(view);
        } else if (viewType == ADS_TYPE) {
            View view = layoutInflater.inflate(R.layout.celula_historicopagamento, parent, false);
            return new AdsViewHolder(view);
        } else {
            View view = layoutInflater.inflate(R.layout.celula_historicopagamento, parent, false);
            return new RecyclerViewHolder(view);
        }

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        hmAux = dataBase.get(position);
        if (holder instanceof FooterViewHolder) {
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            initActions_FooterView(viewHolder);
        } else if (holder instanceof EndViewHolder) {
            EndViewHolder viewHolder = (EndViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            initActions_EndView(viewHolder);
        } else if (holder instanceof AdsViewHolder) {
            AdsViewHolder viewHolder = (AdsViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            initActions_AdsView(viewHolder);
        } else {
            RecyclerViewHolder viewHolder = (RecyclerViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            initActions_NormalView(viewHolder);
        }
    }

    /**
     * ViewHolder FOOTER_TYPE
     */
    private class FooterViewHolder extends RecyclerView.ViewHolder {
        private View view;
        //init privates instances

        public FooterViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            //init instance's findViewById


            /**
             * Events at View
             */
            view.setOnClickListener(FOOTER_TYPE_ViewClick);
        }
    }

    /**
     * @param viewHolder
     */
    private void initActions_FooterView(FooterViewHolder viewHolder) {

    }

    private View.OnClickListener FOOTER_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * ViewHolder END_VIEW
     */
    private class EndViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private Button buttonReload;

        public EndViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            buttonReload = view.findViewById(R.id.btnRetry);
            buttonReload.setOnClickListener(END_TYPE_ViewClick);
        }
    }

    private void initActions_EndView(EndViewHolder viewHolder) {

    }

    private View.OnClickListener END_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            removeLastItem();
            addFooterView();

            Activity_Arremates.initWSConection(Activity_Arremates.pageNumber);

        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ViewHolder ADS_VIEW
     */
    private class AdsViewHolder extends RecyclerView.ViewHolder {
        private View view;
        //init privates instances

        public AdsViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            //init instance's findViewById


            /**
             * Events at View
             */
            view.setOnClickListener(ADS_TYPE_ViewClick);
        }
    }

    private void initActions_AdsView(AdsViewHolder viewHolder) {

    }

    private View.OnClickListener ADS_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * ViewHolder NORMAL_TYPE
     */
    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private View view;
        //init privates instances

        private TextView status;
        private TextView idPgt;
        private TextView quant;
        private TextView preco;
        private TextView desc;
        private TextView data;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            //init instance's findViewById

            status = itemView.findViewById(R.id.status);
            idPgt = itemView.findViewById(R.id.idPgt);
            quant = itemView.findViewById(R.id.quant);
            preco = itemView.findViewById(R.id.preco);
            desc = itemView.findViewById(R.id.desc);
            data = itemView.findViewById(R.id.data);

            /**
             * Events at View
             */

            view.setOnClickListener(NORMAL_TYPE_ViewClick);

        }
    }

    private void initActions_NormalView(RecyclerViewHolder viewHolder) {

        viewHolder.status.setText(this.hmAux.get(HMAux.FINALIZADO));
        viewHolder.idPgt.setText(this.hmAux.get(HMAux.PREFERENCE));
        viewHolder.quant.setText(this.hmAux.get(HMAux.QUANTIDADE_INGRESSO));
        viewHolder.preco.setText(this.hmAux.get(HMAux.UNIT_PRICE));
        viewHolder.desc.setText(this.hmAux.get(HMAux.DESCRICAO_PACOTE));
        viewHolder.data.setText(this.hmAux.get(HMAux.DATE_BUY));

    }

    private View.OnClickListener NORMAL_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            viewPosition = (int) view.getTag();
            HMAux hmAux_click = dataBase.get(viewPosition);
        }
    };



}
