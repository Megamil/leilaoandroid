package net.megamil.leilao24h.Usuario.Fragmentos.Inicio;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Carossel;

public class Activity_Item extends AppCompatActivity {

    private Context context;
    private Context context_activity;
    private Activity activity;
    private Window window;
    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    //Carossel
    private LinearLayout dotsLinearLayout;
    private TextView imgCount;
    private int[] mResources = null;
    private ViewPager mViewPager;

    private TextView tv_share_view_tip;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leilao);

        //Var padroes
        context = getApplicationContext();
        context_activity = Activity_Item.this;
        activity = Activity_Item.this;
        window = getWindow();

        int color = getIntent().getIntExtra("pos", 0);

        iniWS();
        initVars();
        initActions();

    }


    private void initVars() {
        tv_share_view_tip = findViewById(R.id.tv_share_view_tip);

        dotsLinearLayout = (LinearLayout) findViewById(R.id.dotLinear);
        imgCount = findViewById(R.id.img_count);
        mViewPager = (ViewPager) findViewById(R.id.vp_img_pager);

// Aqui estao suas imagens dentro do drawable
        mResources = new int[]{
                R.drawable.bg_leilao,
                R.drawable.bg_leilao405
        };

    }


    private void initActions() {

//        ViewPager_IMG mCustomPagerAdapter = new ViewPager_IMG(this, mResources);

//        mViewPager.setAdapter(mCustomPagerAdapter);

        ToolBox_Carossel tc = new ToolBox_Carossel(
                activity,
                dotsLinearLayout,
                mViewPager,
                imgCount,
                mResources.length
        );


        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(1500);
        alphaAnimation.setStartOffset(1000);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tv_share_view_tip.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        tv_share_view_tip.startAnimation(alphaAnimation);

    }

    private void iniWS() {

    }



    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator upAnim = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    upAnim.setDuration(200);
                    upAnim.setInterpolator(new DecelerateInterpolator());
                    upAnim.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator downAnim = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    downAnim.setDuration(200);
                    downAnim.setInterpolator(new AccelerateInterpolator());
                    downAnim.start();
                    break;
            }
            return false;
        }
    };

}
