package net.megamil.leilao24h.Usuario.Fragmentos.Perfil;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Sistema;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


public class Editar_Senha extends AppCompatActivity {

    private Context context;
    private Context context_activity;
    private Activity activity;
    private Window window;

    private static ImageButton btn_voltar;
    /////////////////////////////////////////////////////////////////////////////


    /*
     *  _Inicio de Variaveis
     */
    public static JSONObject jsonObject = null;

    public static EditText et_nome_usuario;

    private static Button btn_atualizar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_usuario);

        initVars();
        initActions();

        Funcao_PreEditar_Usuario();

    }


    private void initVars() {

        //Var padroes
        context = getApplicationContext();
        context_activity = Editar_Senha.this;
        activity = Editar_Senha.this;
        window = getWindow();

        btn_voltar = findViewById(R.id.btn_voltar);
        ////////////////////////////////////////////////////////////////////////

        /*
         *  _Inicio de findViewById's
         */

        et_nome_usuario = findViewById(R.id.et_nome_usuario);
        btn_atualizar = findViewById(R.id.btn_atualizar);

    }

    private void initActions() {
        /*
         *  _Inicio de Ações da Pagina
         */

        btn_atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Funcao_Atualizar();
            }
        });

        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    private void Funcao_PreEditar_Usuario() {
        Dados_Ws(401, "", null, activity);

    }

    private void Funcao_Atualizar() {

        boolean conn = ToolBox_Verivifacao_Sistema.Funcao_Status_Conexao(context); // função para checar a conexao antes
        if (conn == true) {


            String usuario = et_nome_usuario.getText().toString().trim();

            boolean[] Array_De_Obrigatorios = new boolean[1];

            Array_De_Obrigatorios[0] = ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(context, et_nome_usuario, usuario);

            //verifica ArrayObrigarotios
            Boolean validação_final = ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios);

            if (validação_final == true) {

                String Dados_Para_URL_GET = null;
                Map<String, String> Dados_Para_Parametros = new HashMap<>();

                Dados_Para_URL_GET = "";
                Dados_Para_Parametros.put("nome_usuario", usuario);

                Dados_Ws(5, Dados_Para_URL_GET, Dados_Para_Parametros, activity);


            } else {
                MsgUtil.Funcao_MSG(context, context.getResources().getString(R.string.msg_global_sem_conexao));
            }


        } else {
            MsgUtil.Funcao_MSG(context, context.getResources().getString(R.string.msg_global_sem_conexao));
        }

    }


    @Override
    public void onBackPressed() {
//        ToolBox_Chama_Activity.Funcao_Chama_TelaPrincipal(activity);
        finish();
        super.onBackPressed();
    }
}
