package net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Adaptadores.adapterHistoricoLances;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RecyclerView;

import java.util.ArrayList;
import java.util.List;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


public class Activity_Lances extends AppCompatActivity {

    private Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;
    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    private static ImageButton btn_voltar;
    private static ImageButton btn_share;
    public static int points;
    private static TextView tv_titulo;
    public static List<HMAux> arrayList_dados = new ArrayList<>();
    private static RecyclerView lv_itens;
    private static adapterHistoricoLances adapter;
    private static LinearLayoutManager linearLayoutManager;

    //Scroll Listenner
    public static List<HMAux> arrayList_dados_NextPage = new ArrayList<>();
    public static int sizePerPage = 10;
    public static int pageNumber = 1;
    private static boolean loading;
    public static TextView tvCredits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_recyclerview);
        pageNumber = 1;
        //Var padroes
        context = getBaseContext();
        context_activity = Activity_Lances.this;
        activity = Activity_Lances.this;
        window = activity.getWindow();
        arrayList_dados.clear();

        initVars();
        initActions();
        initWSConection(pageNumber);

    }

    private void initVars() {

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */
        lv_itens = findViewById(R.id.lv_itens);
        btn_voltar = findViewById(R.id.btn_voltar);
        btn_share = findViewById(R.id.btn_aux);
        tv_titulo = findViewById(R.id.tv_toolbar);
        tvCredits = findViewById(R.id.tv_credits);

    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */

        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_titulo.setText("Recompensas");

//        ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
    }

    public static void Set_Dados_Array() {

        linearLayoutManager = ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
        lv_itens.setLayoutManager(linearLayoutManager);
        adapter = new adapterHistoricoLances(
                activity,
                arrayList_dados
        );
        lv_itens.setAdapter(adapter);
        if (arrayList_dados.size() < sizePerPage) {
            adapter.addEndView();
            loading = true;
        } else {
            adapter.addFooterView();
        }
        lv_itens.addOnScrollListener(scrollListener);

    }

    public static void addDataArray() {
        if (arrayList_dados_NextPage.size() > 0) {
            adapter.removeLastItem();
            loading = false;
            adapter.addList(arrayList_dados_NextPage);
            if (arrayList_dados_NextPage.size() < sizePerPage) {
                adapter.addEndView();
                loading = true;
            } else {
                adapter.addFooterView();
            }
//            MsgUtil.Funcao_MSG(activity, "Page " + pageNumber);
        } else {
            adapter.removeLastItem();
            adapter.addEndView();
            loading = true;
//            Snackbar snackbar = Snackbar.make(view, "Sem dados", Snackbar.LENGTH_SHORT)
//                    .setCallback(new Snackbar.Callback() {
//                        @Override
//                        public void onDismissed(Snackbar transientBottomBar, int event) {
//                            super.onDismissed(transientBottomBar, event);
//                            loading = false;
//                            adapter_recycler_itens.addFooter();
//                        }
//                    });
//            snackbar.show();
        }
    }

    public static void noData() {

        linearLayoutManager = ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
        lv_itens.setLayoutManager(linearLayoutManager);

        adapter = new adapterHistoricoLances(
                activity,
                arrayList_dados
        );

        lv_itens.setAdapter(adapter);
//        adapter_recycler_itens_vertical.addFooterView();
        adapter.addEndView();
    }

    private static RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            if (!loading && linearLayoutManager.getItemCount() == (linearLayoutManager.findLastVisibleItemPosition() + 1)) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initWSConection(pageNumber);
                    }
                }, 1500);

                loading = true;
            }
        }
    };

    public static void initWSConection(int Page) {
        Dados_Ws(19, "quantidade_por_pagina=" + sizePerPage + "&pagina=" + String.valueOf(Page), null, activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
