package net.megamil.leilao24h.Usuario.Fragmentos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_ListView_Frag_MeusLeiloes;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Ativar_Leilao.Activity_Ativar_Leilao;
import net.megamil.leilao24h.Usuario.Pagina_Inicio;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;

import java.util.ArrayList;
import java.util.List;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


/**
 * Created by John on 09/12/2017.
 */

public class Fragment_Leiloes extends Fragment {

    private static Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;

    private static ImageButton btn_voltar;
    public static List<HMAux> arrayList_dados = new ArrayList<>();
    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    private static ListView lv_itens;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_listview, container, false);

        //Var padroes
        context = getActivity();
        context_activity = getActivity();
        activity = ((Pagina_Inicio) getActivity());
        window = activity.getWindow();

        initWSConection();
        initVars(view);
        initActions();

        return view;

    }

    private void initVars(View view) {

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */

        lv_itens = view.findViewById(R.id.lv_itens);

    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */

//        arrayList_dados = gerarDados(90);
//        Set_Dados_Array(arrayList_dados);


    }

    public static void Set_Dados_Array() {

//        arrayList_dados.add(arrayList);

        Adapter_ListView_Frag_MeusLeiloes adapter_leiloes = new Adapter_ListView_Frag_MeusLeiloes(
                activity,
                R.layout.celula_leiloes,
                arrayList_dados
        );

        lv_itens.setAdapter(adapter_leiloes);

        lv_itens.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                HMAux hmAux = (HMAux) adapterView.getItemAtPosition(i);

                Intent intent = new Intent(activity, Activity_Ativar_Leilao.class);

                intent.putExtra(HMAux.ID_POSITION, i);
                intent.putExtra(HMAux.INTENT_TYPE, 1);

                intent.putExtra(HMAux.ID_LEILAO, hmAux.get(HMAux.ID_LEILAO));
                intent.putExtra(HMAux.ID_PROCUTO, hmAux.get(HMAux.ID_PROCUTO));
                intent.putExtra(HMAux.NOME_PRODUTO, hmAux.get(HMAux.NOME_PRODUTO));
                intent.putExtra(HMAux.DESCRICAO_PRODUTO, hmAux.get(HMAux.DESCRICAO_PRODUTO));
                intent.putExtra(HMAux.VALOR_MINIMO, hmAux.get(HMAux.VALOR_MINIMO));
                intent.putExtra(HMAux.LANCE_MINIMO, hmAux.get(HMAux.LANCE_MINIMO));
                intent.putExtra(HMAux.INGRESSOS, hmAux.get(HMAux.INGRESSOS));
                intent.putExtra(HMAux.DATA_INICIO, hmAux.get(HMAux.DATA_INICIO));
                intent.putExtra(HMAux.DATA_FIM_PREVISTO, hmAux.get(HMAux.DATA_FIM_PREVISTO));
                intent.putExtra(HMAux.STATUS_LEILAO, hmAux.get(HMAux.STATUS_LEILAO));
                intent.putExtra(HMAux.IMAGENS_ARRAY, hmAux.get(HMAux.IMAGENS_ARRAY));


                ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");

            }
        });

    }

    private void initWSConection() {
        Dados_Ws(15, "quantidade_por_pagina=20&pagina=1", null, activity);
    }


}
