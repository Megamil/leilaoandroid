package net.megamil.leilao24h.Usuario.Fragmentos;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_Recycler_Itens_Vertical;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Pagina_Inicio;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RecyclerView;

import java.util.ArrayList;
import java.util.List;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


/**
 * Created by John on 09/12/2017.
 */

public class Fragment_Favoritos extends Fragment {

    private static Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;

    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    //Itens List RecyclerView
    private static LinearLayoutManager linearLayoutManager;
    private static RecyclerView recyclerView;
    private static Adapter_Recycler_Itens_Vertical adapter;
    public static List<HMAux> arrayList_dados = new ArrayList<>();

    //Scroll Listenner
    public static List<HMAux> arrayList_dados_NextPage = new ArrayList<>();
    public static int sizePerPage = 20;
    public static int pageNumber = 1;
    private static boolean loading;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);

        //Var padroes
        context = getActivity();
        context_activity = getActivity();
        activity = ((Pagina_Inicio) getActivity());
        window = activity.getWindow();
        pageNumber = 1;

        initWSConection(pageNumber);
        initVars(view);
        initActions();

        return view;

    }

    private void initVars(View view) {

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */

        recyclerView = view.findViewById(R.id.lv_itens);

    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */

    }

    public static void Set_Dados_Array() {

        linearLayoutManager = ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new Adapter_Recycler_Itens_Vertical(
                activity,
                arrayList_dados,
                2
        );
        recyclerView.setAdapter(adapter);
        if (arrayList_dados.size() < sizePerPage) {
            adapter.addEndView();
            loading = true;
        } else {
            adapter.addFooterView();
        }
        recyclerView.addOnScrollListener(scrollListener);

    }

    public static void addDataArray() {
        if (arrayList_dados_NextPage.size() > 0) {
            adapter.removeLastItem();
            loading = false;
            adapter.addList(arrayList_dados_NextPage);
            if (arrayList_dados_NextPage.size() < sizePerPage) {
                adapter.addEndView();
                loading = true;
            } else {
                adapter.addFooterView();
            }
        } else {
            adapter.removeLastItem();
            adapter.addEndView();
            loading = true;
        }
    }

    public static void noData() {

        linearLayoutManager = ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new Adapter_Recycler_Itens_Vertical(
                activity,
                arrayList_dados,
                2
        );

        recyclerView.setAdapter(adapter);

        adapter.addEndView();
    }

    private static RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            if (!loading && linearLayoutManager.getItemCount() == (linearLayoutManager.findLastVisibleItemPosition() + 1)) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initWSConection(pageNumber);
                    }
                }, 1500);

                loading = true;
            }
        }
    };

    public static void initWSConection(int pageNumber) {
        Dados_Ws(14, "quantidade_por_pagina=" + sizePerPage + "&pagina=" + pageNumber, null, activity);
    }

}