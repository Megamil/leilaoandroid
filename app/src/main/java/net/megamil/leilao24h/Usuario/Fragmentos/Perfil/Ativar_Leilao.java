package net.megamil.leilao24h.Usuario.Fragmentos.Perfil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_ListView_Frag_MeusLeiloes;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Ativar_Leilao.Activity_Ativar_Leilao;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;

import java.util.ArrayList;
import java.util.List;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


public class Ativar_Leilao extends AppCompatActivity {

    private static Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;

    private static ImageButton btn_voltar;
    public static List<HMAux> arrayList_dados = new ArrayList<>();
    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    private static ListView lv_itens;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_listview);

        initWSConection();
        initVars();
        initActions();


    }

    private void initVars() {

        //Var padroes
        context = getApplicationContext();
        context_activity = Ativar_Leilao.this;
        activity = Ativar_Leilao.this;
        window = getWindow();
        btn_voltar = findViewById(R.id.btn_voltar);

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */

        lv_itens = findViewById(R.id.lv_itens);

    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */
        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        arrayList_dados = gerarDados(90);
//        Set_Dados_Array(arrayList_dados);


    }

    public static void Set_Dados_Array() {

//        arrayList_dados.add(arrayList);

        Adapter_ListView_Frag_MeusLeiloes adapter_leiloes = new Adapter_ListView_Frag_MeusLeiloes(
                activity,
                R.layout.celula_leiloes,
                arrayList_dados
        );

        lv_itens.setAdapter(adapter_leiloes);

        lv_itens.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                HMAux hmAux = (HMAux) adapterView.getItemAtPosition(i);

                Intent intent = new Intent(activity, Activity_Ativar_Leilao.class);

                intent.putExtra(HMAux.ID_POSITION, i);
                intent.putExtra(HMAux.INTENT_TYPE, 2);

                intent.putExtra(HMAux.ID_PROCUTO, hmAux.get(HMAux.ID_PROCUTO));
                intent.putExtra(HMAux.NOME_PRODUTO, hmAux.get(HMAux.NOME_PRODUTO));
                intent.putExtra(HMAux.DESCRICAO_PRODUTO, hmAux.get(HMAux.DESCRICAO_PRODUTO));
                intent.putExtra(HMAux.NOME_CATEGORIA, hmAux.get(HMAux.NOME_CATEGORIA));

                intent.putExtra(HMAux.IMAGENS_ARRAY, hmAux.get(HMAux.IMAGENS_ARRAY));


                ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");

            }
        });
    }

    public void initWSConection() {
        Dados_Ws(8, "", null, Ativar_Leilao.this);
    }

    @Override
    public void onBackPressed() {
//        ToolBox_Chama_Activity.Funcao_Chama_TelaPrincipal(activity);
        super.onBackPressed();
        finish();
    }
}
