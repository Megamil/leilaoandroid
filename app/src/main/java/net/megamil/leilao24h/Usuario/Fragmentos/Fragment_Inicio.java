/*
 * Copyright (c) Developed by John Alves at 2018/10/31.
 */

package net.megamil.leilao24h.Usuario.Fragmentos;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_Recycler_Itens;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.Planos;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Cadastra_Leilao;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RecyclerView;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;

import java.util.ArrayList;
import java.util.List;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;

public class Fragment_Inicio extends Fragment implements View.OnClickListener, View.OnTouchListener {

    //var padroes
    private static Context context;
    private static Context context_activity;
    private static Activity activity;
    private static Window window;
    public static View view;

    //LinearLayout
    private static LinearLayout btn_vender;
    private static LinearLayout btn_comprar_ingresso;

    //textView
    public static TextView tv_credits;

    //SwipeRefreshLayout
    private static SwipeRefreshLayout refreshLayout;

    //RecyclerView itens Leilões á iniciar
    private static RecyclerView recyclerView;
    private static Adapter_Recycler_Itens adapter_recycler_itens;
    private static LinearLayoutManager linearLayoutManager;
    private static GridLayoutManager gridLayoutManager;
    public static List<HMAux> arrayList_dados = new ArrayList<>();
    public static List<HMAux> arrayList_dados_NextPage = new ArrayList<>();
    public static int sizePerPage = 20;
    public static int pageNumber = 1;
    private static int currentVisiblePosition = 0;
    private static int currentYPosition = 0;


    private static boolean loading;
    private static int loadTimes = 0;


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("page", 1);
//        outState.putParcelableArrayList("places", (List<? extends Parcelable>) arrayList_dados);

    }


    @Override
    public void onResume() {
        super.onResume();
        loading = false;
        pageNumber = 1;
        Log.w("OnResume", "Load = " + String.valueOf(loading));
        ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
        tv_credits.setText(prefs.getCredits());

        if (arrayList_dados.size() > 0) {
            arrayList_dados = new ArrayList<>();
        }
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_activity_inicio_inicio, container, false);

        context = getActivity();
        context_activity = getActivity();
        activity = getActivity();
        window = getActivity().getWindow();

        initVars(view);
        initActions();

        initWSConection(pageNumber);
        return view;
    }

    private void initVars(View view) {
        btn_vender = view.findViewById(R.id.btn_vender_agora);

        btn_comprar_ingresso = view.findViewById(R.id.btn_comprar_ingressos);

        recyclerView = view.findViewById(R.id.recycler_view_recycler_view);
        refreshLayout = view.findViewById(R.id.swipe_refresh_layout_recycler_view);

        tv_credits = view.findViewById(R.id.tv_credits);
    }

    private void initActions() {
        Funcao_ChamaCadastroLeilao(btn_vender);

        ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
        tv_credits.setText(prefs.getCredits());

        btn_comprar_ingresso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, Planos.class, null, false, false, "");
            }
        });

        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

//                if (arrayList_dados.size() > 0) {
//
//                    arrayList_dados.clear();
//                } else {
//                    arrayList_dados.clear();
//                }
                if (arrayList_dados.size() > 0) {
                    arrayList_dados.clear();
                    adapter_recycler_itens.removeAll();
                }
                pageNumber = 1;
                initWSConection(pageNumber);
                refreshLayout.setRefreshing(false);
            }
        });

//        gerarDados(100);
//        dados = new ArrayList<>();
//        dados = A_ToolBox_Gerador_De_Dados.gerarDados(100);
//        for (int i = 1; i <= 20; i++) {
////            Log.w("teste", String.valueOf(i));
//            dados.add(i + "");
//        }


//        loadTimes = 0;
//
//        if (Funcao_getScreenWidthDp(activity) >= 1200) {
//            final GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 3);
//            recyclerView.setLayoutManager(gridLayoutManager);
//        } else if (Funcao_getScreenWidthDp(activity) >= 800) {
//            final GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 2);
//            recyclerView.setLayoutManager(gridLayoutManager);
//        } else {
//            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
//            recyclerView.setLayoutManager(linearLayoutManager);
//        }
//
//        recyclerViewAdapter = new RecyclerViewAdapter(context);
//        recyclerView.setAdapter(recyclerViewAdapter);
//        recyclerViewAdapter.setItems(dados);
//        recyclerViewAdapter.addFooter();

//        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayout.HORIZONTAL, false);
//        recyclerView.setLayoutManager(linearLayoutManager);
//
//        adapter_recycler_itens = new Adapter_Recycler_Itens(
//                activity,
//                arrayList_dados
//        );
//
//        recyclerView.setAdapter(adapter_recycler_itens);


//        recyclerView.setAdapter(new NossoAdapter(
//                context,
//                R.layout.card_teste,
//                dados
//        ));
//
//        RecyclerView.LayoutManager layout = new LinearLayoutManager(context);
//        recyclerView.setLayoutManager(layout);


//        adapter_recycler_itens.setOnItemClickListener(new Adapter_Recycler_Itens.ClickListener() {
//            @Override
//            public void onItemClick(int position, View v) {
//
//                HMAux hmAux_click = dados.get(position);
//
//                String nome = hmAux_click.get(HMAux.NOME_ITEM);
//
//                Log.d("", "onItemClick position: " + position);
//                MsgUtil.Funcao_MSG(context, String.valueOf(position) + " " + nome);
//            }
//
//            @Override
//            public void onItemLongClick(int position, View v) {
//                Log.d("", "onItemLongClick pos = " + position);
//                MsgUtil.Funcao_MSG(context, String.valueOf(position));
//            }
//        });


//        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(adapter);
//        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(callback);
//        mItemTouchHelper.attachToRecyclerView(mRecyclerView);

//        swipeRefreshLayout.setColorSchemeResources(R.color.google_blue, R.color.google_green, R.color.google_red, R.color.google_yellow);
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
////                        if (color > 4) {
////                            color = 0;
////                        }
////                        adapter.setColor(++color);
//                        swipeRefreshLayout.setRefreshing(false);
//                    }
//                }, 2000);
//
//            }
//        });

//        recyclerView.addOnScrollListener(scrollListener);
    }
//
//    RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
//        @Override
//        public void onScrolled(final RecyclerView recyclerView, int dx, int dy) {
//            super.onScrolled(recyclerView, dx, dy);
//
//            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//            if (!loading && linearLayoutManager.getItemCount() == (linearLayoutManager.findLastVisibleItemPosition() + 1)) {
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (loadTimes <= 5) {
//                            recyclerViewAdapter.removeFooter();
//                            loading = false;
//                            recyclerViewAdapter.addItems(dados);
//                            recyclerViewAdapter.addFooter();
//                            loadTimes++;
//                        } else {
//                            recyclerViewAdapter.removeFooter();
//                            Snackbar.make(recyclerView, "nada", Snackbar.LENGTH_SHORT).setCallback(new Snackbar.Callback() {
//                                @Override
//                                public void onDismissed(Snackbar transientBottomBar, int event) {
//                                    super.onDismissed(transientBottomBar, event);
//                                    loading = false;
//                                    recyclerViewAdapter.addFooter();
//                                }
//                            }).show();
//                        }
//                    }
//                }, 1500);
//
//                loading = true;
//            }
//        }
//    };


    public static void Funcao_ChamaCadastroLeilao(View view) {
        ToolBox_Chama_Activity.Funcao_Chama_Activity(view, activity, Cadastra_Leilao.class, null, false, false, "");
    }

    public static void initWSConection(int Page) {
        Dados_Ws(10, "quantidade_por_pagina=" + sizePerPage + "&pagina=" + String.valueOf(Page), null, activity);
    }

    public static void Set_Dados_Array() {

//        linearLayoutManager = new LinearLayoutManager(activity, LinearLayout.HORIZONTAL, false);
//        recyclerView.setLayoutManager(linearLayoutManager);

        gridLayoutManager = ToolBox_RecyclerView.Funcao_GridLayoutManager(activity);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter_recycler_itens = new Adapter_Recycler_Itens(
                activity,
                arrayList_dados
        );
        recyclerView.setAdapter(adapter_recycler_itens);

        if (arrayList_dados.size() < sizePerPage) {
            adapter_recycler_itens.addEnd();
            loading = true;
        } else {
            adapter_recycler_itens.addFooter();
        }

        recyclerView.addOnScrollListener(scrollListener);


//        adapter_recycler_itens.setOnItemClickListener(new Adapter_Recycler_Itens.ClickListener() {
//            @Override
//            public void onItemClick(int position, View v) {
//
//                HMAux hmAux_click = arrayList_dados.get(position);
//
//                String id = hmAux_click.get(HMAux.ID_LEILAO);
//
////                Log.d("", "onItemClick position: " + position);
////                MsgUtil.Funcao_MSG(context, String.valueOf(position) + " " + nome);
//            }
//
//            @Override
//            public void onItemLongClick(int position, View v) {
//                Log.d("", "onItemLongClick pos = " + position);
//                MsgUtil.Funcao_MSG(context, String.valueOf(position));
//            }
//        });

    }

    public static void addDataArray() {
        if (arrayList_dados_NextPage.size() > 0) {
            adapter_recycler_itens.removeFooter();
            loading = false;
            adapter_recycler_itens.addItems(arrayList_dados_NextPage);
            if (arrayList_dados_NextPage.size() < sizePerPage) {
                adapter_recycler_itens.addEnd();
                loading = true;
            } else {
                adapter_recycler_itens.addFooter();
            }
//            MsgUtil.Funcao_MSG(activity, "Page " + pageNumber);
        } else {
            adapter_recycler_itens.removeFooter();
            adapter_recycler_itens.addEnd();
            loading = true;
//            Snackbar snackbar = Snackbar.make(view, "Sem dados", Snackbar.LENGTH_SHORT)
//                    .setCallback(new Snackbar.Callback() {
//                        @Override
//                        public void onDismissed(Snackbar transientBottomBar, int event) {
//                            super.onDismissed(transientBottomBar, event);
//                            loading = false;
//                            adapter_recycler_itens.addFooter();
//                        }
//                    });
//            snackbar.show();
        }
    }

    private static RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            currentYPosition = currentYPosition + dy;

            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
//            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            if (!loading && gridLayoutManager.getItemCount() == (gridLayoutManager.findLastVisibleItemPosition() + 1)) {

                currentVisiblePosition = gridLayoutManager.findLastCompletelyVisibleItemPosition() + 1;

                Log.w("Position", String.valueOf(currentVisiblePosition));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initWSConection(pageNumber);
                    }
                }, 1500);

                loading = true;
            }
        }
    };

//    private static RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
//        @Override
//        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//            super.onScrolled(recyclerView, dx, dy);
//
//            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//            if (!loading && linearLayoutManager.getItemCount() == (linearLayoutManager.findLastVisibleItemPosition() + 1)) {
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (loadTimes <= 5) {
//                            adapter_recycler_itens.removeFooter();
//                            loading = false;
////                            adapter_recycler_itens.addItems(arrayList_dados);
//                            adapter_recycler_itens.addFooter();
//                            MsgUtil.Funcao_MSG(activity, "load " + pageNumber);
//                            loadTimes++;
//                        } else {
//                            adapter_recycler_itens.removeFooter();
//                            Snackbar snackbar = Snackbar.make(view, "Sem dados", Snackbar.LENGTH_SHORT)
//                                    .setCallback(new Snackbar.Callback() {
//                                        @Override
//                                        public void onDismissed(Snackbar transientBottomBar, int event) {
//                                            super.onDismissed(transientBottomBar, event);
//                                            loading = false;
//                                            adapter_recycler_itens.addFooter();
//                                        }
//                                    });
//                            snackbar.show();
//                        }
//                    }
//                }, 1500);
//
//                loading = true;
//            }
//        }
//    };

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                ObjectAnimator upAnim = ObjectAnimator.ofFloat(view, "translationZ", 16);
                upAnim.setDuration(150);
                upAnim.setInterpolator(new DecelerateInterpolator());
                upAnim.start();
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                ObjectAnimator downAnim = ObjectAnimator.ofFloat(view, "translationZ", 0);
                downAnim.setDuration(150);
                downAnim.setInterpolator(new AccelerateInterpolator());
                downAnim.start();
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {

    }

}
