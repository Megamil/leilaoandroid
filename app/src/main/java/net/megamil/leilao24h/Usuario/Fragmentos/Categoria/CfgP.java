package net.megamil.leilao24h.Usuario.Fragmentos.Categoria;

import android.support.v4.app.Fragment;

/**
 * Created by nalmir on 04/12/2017.
 */

public class CfgP {

    private String titulo;
    private Fragment pagina;

    public CfgP(String titulo, Fragment pagina) {
        this.titulo = titulo;
        this.pagina = pagina;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Fragment getPagina() {
        return pagina;
    }

    public void setPagina(Fragment pagina) {
        this.pagina = pagina;
    }
}
