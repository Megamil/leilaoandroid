package net.megamil.leilao24h.Usuario.Fragmentos.Perfil;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Splash.Splash;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_Dialog_Categoria;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Imagem;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Views;
import net.megamil.leilao24h.Utils.ToolBox.toolbox_Modal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;

public class Cadastra_Leilao extends AppCompatActivity {

    //padrão
    private static Context context;
    private static Activity activity;

    //toolbar
    private TextView toolbar;
    private RelativeLayout rl_toolbar;
    private ImageButton imgButtonVoltar;

    //edittext
    private static EditText et_nome;
    private static EditText et_descricao;

    //textview
    private static TextView tv_categorias;

    //ScrollView
    private ScrollView sv_pai;

    //Button
    private static Button btn_add_update_leilao;

    //Dialog
    private static Dialog dialog;

    //Dados para Layout de Categorias
    public static ArrayList<HMAux> dados_categorias = new ArrayList<>(); // Dados em Memoria
    public static ArrayList<HMAux> dados_raw_categorias = new ArrayList<>(); // Dados em Memoria
    public static Boolean open_dialog = true;
    public static int add_array_categorias = 1;

    public static ListView listView_catgorias;

    public static JSONArray categoria_json = null;

    private static String id_categoria = "-1";
    private static String nome_categoria = "Categorias";

    //Dados para Layout de Imagens
    private static ImageView iv_1;
    private static ImageView iv_2;
    private static ImageView iv_3;
    private static ImageView iv_4;
    private static ImageView iv_5;
    private static ImageView iv_6;
    private static ImageView[] IMG = {iv_1, iv_2, iv_3, iv_4, iv_5, iv_6};

    private static RelativeLayout btn_addImage;

    private static RelativeLayout rl_1;
    private static RelativeLayout rl_2;
    private static RelativeLayout rl_3;
    private static RelativeLayout rl_4;
    private static RelativeLayout rl_5;
    private static RelativeLayout rl_6;
    private static RelativeLayout[] RL_IMG = {rl_1, rl_2, rl_3, rl_4, rl_5, rl_6};

    private static ArrayList<String> lista_de_img = new ArrayList<>();
    private static List<Boolean> arrayImgPosController = new ArrayList<>();
    private static int posFixed = -1;
    private static int usedOrNew = -1;

    //segmented
    private static LinearLayout ll_segmented;

    /////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_cadastra_cliente);

        initVars();
        initActions();
        ToolBox_Views.Novo_fecha_teclado_RelativeLayout(activity, rl_toolbar);
        ToolBox_Views.Novo_fecha_teclado_ScrollView(activity, sv_pai);

        Dados_Ws(6, "", null, activity);
    }

    private void initVars() {

        //padroes
        context = getApplicationContext();
        activity = Cadastra_Leilao.this;
        Splash.verifyIsLogued(activity);
        //toolbar
        toolbar = findViewById(R.id.tv_toolbar);
        imgButtonVoltar = findViewById(R.id.btn_politica_privacidade_voltar_login);
        rl_toolbar = findViewById(R.id.relativeLayout);

        //scroolview
        sv_pai = findViewById(R.id.sv_pai);

        //button
        btn_add_update_leilao = findViewById(R.id.btn_cadastra_leilao);
        btn_addImage = findViewById(R.id.btn_add_img);

        //edittext
        et_nome = (EditText) findViewById(R.id.et_nome);
        et_descricao = findViewById(R.id.et_descricao);

        //textview
        tv_categorias = findViewById(R.id.tv_categorias);

        //Layout de imagens
        iv_1 = findViewById(R.id.iv_1);
        iv_2 = findViewById(R.id.iv_2);
        iv_3 = findViewById(R.id.iv_3);
        iv_4 = findViewById(R.id.iv_4);
        iv_5 = findViewById(R.id.iv_5);
        iv_6 = findViewById(R.id.iv_6);
        IMG[0] = iv_1;
        IMG[1] = iv_2;
        IMG[2] = iv_3;
        IMG[3] = iv_4;
        IMG[4] = iv_5;
        IMG[5] = iv_6;

        rl_1 = findViewById(R.id.rl_1);
        rl_2 = findViewById(R.id.rl_2);
        rl_3 = findViewById(R.id.rl_3);
        rl_4 = findViewById(R.id.rl_4);
        rl_5 = findViewById(R.id.rl_5);
        rl_6 = findViewById(R.id.rl_6);
        RL_IMG[0] = rl_1;
        RL_IMG[1] = rl_2;
        RL_IMG[2] = rl_3;
        RL_IMG[3] = rl_4;
        RL_IMG[4] = rl_5;
        RL_IMG[5] = rl_6;

        //segmented
        ll_segmented = findViewById(R.id.segmented);

        TextView tv_novo = findViewById(R.id.tv_novo);
        TextView tv_usado = findViewById(R.id.tv_usado);

        Set_Segment(1, tv_novo, tv_usado);
        Set_Segment(2, tv_usado, tv_novo);

    }

    private void Set_Segment(final int funcao, final TextView textView1, final TextView textView2) {
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (funcao) {
                    case 1:
                        textView1.setTextColor(0xFFFFFFFF);
                        textView1.setBackground(getResources().getDrawable(R.drawable.segment_esquerdo_fundo));

                        textView2.setTextColor(getResources().getColor(R.color.colorPrimary));
                        textView2.setBackground(getResources().getDrawable(R.drawable.segment_direito));
                        usedOrNew = 0;
                        break;
                    case 2:
                        textView1.setTextColor(0xFFFFFFFF);
                        textView1.setBackground(getResources().getDrawable(R.drawable.segment_direito_fundo));

                        textView2.setTextColor(getResources().getColor(R.color.colorPrimary));
                        textView2.setBackground(getResources().getDrawable(R.drawable.segment_esquerdo));
                        usedOrNew = 1;
                        break;

                }
            }
        });
    }//fim


    private void initActions() {

        lista_de_img.clear();
        arrayImgPosController.clear();

        imgButtonVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChamaTelaCerta();
            }
        });

        tv_categorias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Funcao_AbreDialog_Categoria();
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////

        clickListennerModalImage(btn_addImage, 10);
        clickListennerModalImage(rl_1, 0);
        clickListennerModalImage(rl_2, 1);
        clickListennerModalImage(rl_3, 2);
        clickListennerModalImage(rl_4, 3);
        clickListennerModalImage(rl_5, 4);
        clickListennerModalImage(rl_6, 5);

        /////////////////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////////////

        btn_add_update_leilao.setOnClickListener(ToolBox_Verivifacao_Campos.buttonCheckConection(activity, onClickListener));

//        btn_add_update_leilao.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//
//                String Dados_Para_URL_GET = ""; // padrao
//                Map<String, String> Dados_Para_Parametros = new HashMap<>(); // padrao
//
//                Dados_Para_Parametros.put("fk_categoria", id_categoria);
//                Dados_Para_Parametros.put("nome_produto", et_nome.getText().toString().trim());
//                Dados_Para_Parametros.put("descricao_produto", et_descricao.getText().toString().trim());
//                Dados_Para_Parametros.put("imagem[]", lista_de_img.toString());
//
//                Dados_Ws(7, "", Dados_Para_Parametros, activity);
//            }
//        });
        /////////////////////////////////////////////////////////////////////////////////////////////

    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            boolean[] Array_De_Obrigatorios = {
                    ToolBox_Verivifacao_Campos.Funcao_verificaTamanhoArray(activity, lista_de_img),
                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_nome, et_nome.getText().toString().trim()),
                    ToolBox_Verivifacao_Campos.Funcao_verificaIdCategoria(activity, id_categoria),
                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_descricao, et_descricao.getText().toString().trim())
            };
            Boolean validação_final = ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios);

            if (validação_final == true) {

                String Dados_Para_URL_GET = ""; // padrao
                Map<String, String> Dados_Para_Parametros = new HashMap<>(); // padrao

                Dados_Para_Parametros.put("fk_categoria", id_categoria);
                Dados_Para_Parametros.put("nome_produto", et_nome.getText().toString().trim());
                Dados_Para_Parametros.put("descricao_produto", et_descricao.getText().toString().trim());
                Dados_Para_Parametros.put("imagem[]", lista_de_img.toString());

                if (usedOrNew != -1) {
                    Dados_Para_Parametros.put("produto_usado", String.valueOf(usedOrNew));
                    Dados_Ws(7, "", Dados_Para_Parametros, activity);
                } else {
                    MsgUtil.Funcao_MSG(activity, "Novo ou usado??");
                }

            }

        }

    };

    /////////////////////////////////////////////////////////////////////////////////////////////
//    Categorias

    public static void Funcao_AbreDialog_Categoria() {

        dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(R.layout.dialog_categorias);

        ImageView img_dialog_fullscreen_close = dialog.findViewById(R.id.btn_volta_modal);

        listView_catgorias = dialog.findViewById(R.id.lv_dialog_categoria);

        Adapter_Dialog_Categoria adapter_dialog = new Adapter_Dialog_Categoria(context,
                R.layout.celula_categorias,
                dados_raw_categorias
        );
        listView_catgorias.setAdapter(adapter_dialog);
//        Log.w("RAW", dados_raw_categorias.toString());
        listView_catgorias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                HMAux categorias = (HMAux) parent.getItemAtPosition(position);
                id_categoria = categorias.get(HMAux.ID);
                nome_categoria = categorias.get(HMAux.NOME_CATEGORIA);

                String get = "id_categoria=" + id_categoria;
                Map<String, String> Dados_Para_Parametros = new HashMap<>();
                Dados_Para_Parametros.put("subcategoria", "1");

                Dados_Ws(6, get, Dados_Para_Parametros, activity);

            }
        });


        img_dialog_fullscreen_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                open_dialog = true;
            }
        });

        dialog.show();
    }

    public static void Funcao_ListaCategoria() {

        Cadastra_Leilao.dados_categorias.clear();

        for (int i = 0; i < categoria_json.length(); i++) {
            try {
                JSONObject arrayInterno = categoria_json.getJSONObject(i);

                HMAux hmAux = new HMAux();
                //
                // Imita mini-registro
                hmAux.put(HMAux.ID, arrayInterno.getString("id_categoria")); // Primary Key
                hmAux.put(HMAux.NOME_CATEGORIA, arrayInterno.getString("nome_categoria"));
                hmAux.put(HMAux.FK_CATEGORIA_PAI, arrayInterno.getString("fk_categoria_pai"));
                //
                dados_categorias.add(hmAux);
                if (add_array_categorias == 1) {
                    dados_raw_categorias.add(hmAux);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        add_array_categorias = 0;
    }

    public static void Funcao_FinalizaCategoria() {

        if (dialog != null) {
            dialog.dismiss();
        }
        tv_categorias.setText(nome_categoria);
        open_dialog = true;

    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onBackPressed() {
        ChamaTelaCerta();
    }

    private void ChamaTelaCerta() {
        finish();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

//    Modal Abrir Dialog Fotos

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (((resultCode == RESULT_OK && reqCode == toolbox_Modal.REQUEST_LOAD_IMG)) && (data != null)) {
            Log.w("Entrei", "Galeria");
            ClipData clipData = data.getClipData();
            if (clipData != null) {
                for (int i = 0; i < clipData.getItemCount(); i++) {
                    ClipData.Item item = clipData.getItemAt(i);
                    Uri uri = item.getUri();
                    Boolean imagem_valida = ToolBox_Imagem.Funcao_Verifica_Tamanho_IMG(context, uri);
                    if (imagem_valida) {
                        toolbox_Modal.Carrega_Multiplas_Imagems_Selecionada(activity, uri, arrayPos());
                    }
                }
            } else {
                Uri uri = data.getData();
                Boolean imagem_valida = ToolBox_Imagem.Funcao_Verifica_Tamanho_IMG(context, uri);
                if (imagem_valida) {
                    toolbox_Modal.Carrega_Multiplas_Imagems_Selecionada(activity, uri, arrayPos());
                }
            }

        } else if (resultCode == RESULT_OK && reqCode == toolbox_Modal.REQUEST_IMAGE_CAPTURE) {
            Log.w("Entrei", "Camera");
            toolbox_Modal.Carrega_Multiplas_Imagems_Selecionada(activity, toolbox_Modal.Uri_Camera, arrayPos());
        } else {
            MsgUtil.Funcao_MSG(activity, "Foto não Capturada.");
        }
        System.gc();

    }

    private static void clickListennerModalImage(RelativeLayout rl_clicked, final int pos) {
        rl_clicked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pos == 10) {
                    posFixed = -1;
                } else {
                    posFixed = pos;
                }

                toolbox_Modal.ChamaModal(activity);
            }
        });
    }

    public static class Funcao_Add_Imagem implements Runnable {
        private Handler mHandler = new Handler(Looper.getMainLooper());
        Context context;
        Uri uri;
        int width;
        int posicao_array;

        public Funcao_Add_Imagem(Context context, Uri uri, int width, int pos) {
            this.context = context;
            this.uri = uri;
            this.width = width;
            this.posicao_array = pos;
        }

        @Override
        public void run() {

            try {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
//                        int maxSize = Utils.getMaxSize();
//                        int requestSize = Math.min(width, maxSize);
//                        Bitmap raw_bitmap = Utils.decodeSampledBitmapFromUri(context, uri, requestSize);
//                        Bitmap raw_bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
                        Log.w("Array Img Antes", String.valueOf(lista_de_img.size()));
                        if (lista_de_img.size() < 6) {
                            Glide.with(activity).asBitmap()
                                    .load(uri)
                                    .apply(new RequestOptions()
                                            .centerCrop())
                                    .into(IMG[posicao_array]);

                            RL_IMG[posicao_array].setVisibility(View.VISIBLE);
                            Log.w("posicao_array", String.valueOf(posicao_array));
                            if (arrayImgPosController.size() >= 6) {
                                btn_addImage.setVisibility(View.GONE);
                            }

                            Bitmap raw_bitmap = null;
                            try {
                                raw_bitmap = ToolBox_Imagem.Funcao_decodeUri(activity, uri);
                                String sha1Img = ToolBox_Imagem.Funcao_Imagem_encodeToBase64(raw_bitmap);

                                try {
                                    lista_de_img.remove(posicao_array);
                                    arrayImgPosController.remove(posicao_array);
                                    lista_de_img.set(posicao_array, sha1Img);
                                } catch (IndexOutOfBoundsException e) {
                                    lista_de_img.add(posicao_array, sha1Img);
                                }


                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
//                                Log.w("sha1Img Img", sha1Img);
                        }
                        Log.w("Array Img depois", String.valueOf(lista_de_img.size()));
//                        Log.w("Array Img", lista_de_img.toString());
                    }
                });
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static int arrayPos() {
        int pos = 0;
//        if (arrayImgPosController.size() > pos) {
//            pos = arrayImgPosController.size();
//        }
//        arrayImgPosController.add(pos, true);
        if (posFixed > -1) {
            pos = posFixed;
        } else {
            pos = arrayImgPosController.size() > 0 ? arrayImgPosController.size() : 0;
        }
        arrayImgPosController.add(pos, true);
        return pos;
    }

}


