package net.megamil.leilao24h.Usuario.Fragmentos.Categoria;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import net.megamil.leilao24h.R;

/**
 * Created by John on 09/12/2017.
 */

public class Fragment_1 extends Fragment {

    private static Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;

    private Button btn_n_simples;
    private Button btn_n_com_som;
    private Button btn_n_com_imagen;
    private Button btn_n_personalizada;

    private static ViewPager viewPager;
    private static TabLayout tabLayout;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_1, container, false);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        initVars(view);
        initActions();

        return view;
    }

    private void initVars(View view) {

        //Var padroes
        context = getActivity();
        context_activity = getActivity();
        activity = getActivity();
        window = getActivity().getWindow();

        btn_n_simples = view.findViewById(R.id.n_simples);
        btn_n_com_som = view.findViewById(R.id.n_simples);
        btn_n_com_imagen = view.findViewById(R.id.n_simples);
        btn_n_personalizada = view.findViewById(R.id.n_simples);


    }

    private void initActions() {

        Chama_Nortificação(btn_n_simples, 1, "Simples", "teste");

    }



    private static void Chama_Nortificação(Button button, int tipo, final String texto_cabecalho, final String texto_corpo) {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                NotificationUtils notificationUtils;
//                notificationUtils = new NotificationUtils(context);
//
//                Intent intent = new Intent(context, Fragment_Categorias.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                notificationUtils.showNotificationMessage(texto_cabecalho, texto_corpo, "0000-00-00 00:00:00", intent , "https://pbs.twimg.com/profile_images/972154872261853184/RnOg6UyU_400x400.jpg");
//           NotificationUtils.Funcao_Nortificacao(context ,  "teste" , "teste", hmAux);
            }
        });

    }

}
