package net.megamil.leilao24h.Usuario;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_Recycler_Itens_Vertical;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Busca.Adapter_Dialog_Busca;
import net.megamil.leilao24h.Usuario.Busca.DB_Busca;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Permissao;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;
import static net.megamil.leilao24h.Utils.ToolBox._ToolBox_Config_Inicial.Funcao_Verifica_Pastas;

public class Pagina_Busca extends AppCompatActivity {

    private static Context context;
    private static Activity activity;
    private Window window;
    private ConstraintLayout rl_pai;

    //searchbar
    private static TextView tv_busca;
    private LinearLayout ll_busca;
    private static Dialog dialog;

    private static ListView lv_itens;

    //Itens List RecyclerView
    private static LinearLayoutManager linearLayoutManager;
    private static RecyclerView recyclerView;
    private static Adapter_Recycler_Itens_Vertical adapter;
    private static int pageLayoutAdapter = 3;
    public static List<HMAux> arrayList_dados = new ArrayList<>();

    //Scroll Listenner
    public static List<HMAux> arrayList_dados_NextPage = new ArrayList<>();
    public static int sizePerPage = 20;
    public static int pageNumber = 1;
    private static boolean loading;
    public static String textSearsh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busca);

        Pagina_Busca.this.invalidateOptionsMenu();

        initVars();
        initActions();

        Boolean permissao = ToolBox_Permissao.Funcao_Verifica_Permissao(activity, ToolBox_Permissao.VERIFICA_WRITE_EXTERNAL_STORAGE);

        if (!permissao) {
//            ChamaModal(activity, Uri_Camera);
        }

        Funcao_Verifica_Pastas();
//        Funcao_Verifica_Permicoes_Do_APP(activity);

    }

    private void initVars() {

        context = getApplicationContext();
        window = getWindow();
        activity = Pagina_Busca.this;

        ll_busca = findViewById(R.id.ll_include_searchbar);
        tv_busca = findViewById(R.id.tv_include_busca);

        recyclerView = findViewById(R.id.lv_itens);

    }

    private void initActions() {

        Intent intent = getIntent();
        String raw_text = intent.getStringExtra("pesquisa");

        if (!raw_text.equals("null") || !raw_text.equals("")) {
            tv_busca.setText(raw_text);

            initWSConection(raw_text, pageNumber);
        }
        textSearsh = raw_text;

        ll_busca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                MsgUtil.Funcao_MSG(context , "Search");
                Funcao_AbreDialog_Busca();
            }
        });

    }

    static ArrayList<HMAux> dados_s = new ArrayList<>();
    static ArrayList<HMAux> dados_s_pesuisa = new ArrayList<>();
    private static ListView listView_busca;
    private static PopupWindow popupWindow;

    public static void Funcao_AbreDialog_Busca() {
        dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(R.layout.dialog_searchbar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogNoAnimation;
//        ImageView img_full_screen_dialog = fullscreenDialog.findViewById(R.id.img_full_screen_dialog);
//        Glide.with(getContext()).load(R.drawable.google_assistant).into(img_full_screen_dialog);
        ImageView img_dialog_fullscreen_close = dialog.findViewById(R.id.img_dialog_fullscreen_close);
        ImageView img_dialog_fullscreen_p_voz = dialog.findViewById(R.id.img_dialog_fullscreen_p_voz);
        final ImageView img_dialog_fullscreen_menu = dialog.findViewById(R.id.img_dialog_fullscreen_menu);

        final EditText editText = dialog.findViewById(R.id.et_busca);
        final DB_Busca db_busca = new DB_Busca(context);//conexao

        final RelativeLayout relativeLayout_busca = dialog.findViewById(R.id.rl_dialog_busca);
        final LinearLayout linearLayout_busca = dialog.findViewById(R.id.ll_dialog_buscas);

        listView_busca = dialog.findViewById(R.id.lv_dialog_buscas);

        dados_s = db_busca.Obter_Dados_Historico_Busca();
//        Log.w("Dados de Historico", dados_s.toString());

        if (dados_s.size() > 0) {
            linearLayout_busca.setVisibility(View.VISIBLE);
            relativeLayout_busca.setVisibility(View.GONE);
        } else {
            linearLayout_busca.setVisibility(View.GONE);
            relativeLayout_busca.setVisibility(View.VISIBLE);
        }

        Adapter_Dialog_Busca adapter_dialog_busca = new Adapter_Dialog_Busca(context,
                R.layout.celula_searchbar,
                dados_s
        );
        listView_busca.setAdapter(adapter_dialog_busca);

        listView_busca.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final HMAux hmAux = (HMAux) parent.getItemAtPosition(position);

                String texto_celula_pesquisa = hmAux.get(HMAux.BUSCA_TEXTO_PESQUISA);

                initWSConection(texto_celula_pesquisa, pageNumber);
                textSearsh = texto_celula_pesquisa;
                tv_busca.setText(texto_celula_pesquisa);
                dialog.dismiss();
            }
        });

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.dialog_serachbar_menu, null);
        popupWindow = new PopupWindow(popupView, 700, ViewGroup.LayoutParams.WRAP_CONTENT);

        img_dialog_fullscreen_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow.isShowing())
                    popupWindow.dismiss();
                else
                    popupWindow.showAsDropDown(img_dialog_fullscreen_menu, 50, 0);
            }
        });

        TextView menu1 = popupView.findViewById(R.id.dialog_searchbar_menu_1);

        menu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db_busca.DeletaBanco_Historico_Busca();
                popupWindow.dismiss();
                linearLayout_busca.setVisibility(View.GONE);
                relativeLayout_busca.setVisibility(View.VISIBLE);
            }
        });


        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {

                    String texto_de_pesquisa = editText.getText().toString().trim();

                    if (texto_de_pesquisa.isEmpty() || texto_de_pesquisa.equals("")) {
                        MsgUtil.Funcao_MSG(context, "Digite Algo Para Pesquisa");
                    } else {
                        DB_Busca db_busca = new DB_Busca(context);//conexao

                        db_busca.CriaBaseDeDados();
                        db_busca.Inserir_Historico_Busca(texto_de_pesquisa);

                        initWSConection(texto_de_pesquisa, pageNumber);
                        textSearsh = texto_de_pesquisa;

                        tv_busca.setText(texto_de_pesquisa);
                        dialog.dismiss();
                    }

                }
                return false;
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String texto_para_pesquisa = editText.getText().toString().trim();
                Filtro_De_Pesquisa(texto_para_pesquisa);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        img_dialog_fullscreen_p_voz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pesquisa_Por_Voz();
            }
        });

        img_dialog_fullscreen_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private static void Pesquisa_Por_Voz() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "O quer pesquisa?");
        try {
            activity.startActivityForResult(intent, 10);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(context, "Error ao Pesquisar",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public static void Filtro_De_Pesquisa(String name) {
        name = name.toLowerCase(Locale.getDefault());
        dados_s_pesuisa.clear();

        Adapter_Dialog_Busca adapter_dialog_busca = new Adapter_Dialog_Busca(context,
                R.layout.celula_searchbar,
                dados_s_pesuisa
        );

        listView_busca.setAdapter(adapter_dialog_busca);

        if (name.length() == 0) {

            Adapter_Dialog_Busca adapter_dialog_busca1 = new Adapter_Dialog_Busca(context,
                    R.layout.celula_searchbar,
                    dados_s
            );

            listView_busca.setAdapter(adapter_dialog_busca1);

        } else {
            for (HMAux dados_pesquisa : dados_s) {
                if (dados_pesquisa.get(HMAux.BUSCA_TEXTO_PESQUISA).toLowerCase(Locale.getDefault()).contains(name)) {
                    dados_s_pesuisa.add(dados_pesquisa);
                }
            }

        }

    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    DB_Busca db_busca = new DB_Busca(context);//conexao
                    db_busca.CriaBaseDeDados();
                    db_busca.Inserir_Historico_Busca(result.get(0));
                    initWSConection(result.get(0), pageNumber);
                    tv_busca.setText((result.get(0)));
                    textSearsh = result.get(0);
                    dialog.dismiss();
                }
                break;
            }

        }
    }

    public static void Set_Dados_Array() {

        linearLayoutManager = ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new Adapter_Recycler_Itens_Vertical(
                activity,
                arrayList_dados,
                pageLayoutAdapter
        );
        recyclerView.setAdapter(adapter);
        if (arrayList_dados.size() < sizePerPage) {
            adapter.addEndView();
            loading = true;
        } else {
            adapter.addFooterView();
        }
        recyclerView.addOnScrollListener(scrollListener);

    }

    public static void addDataArray() {
        if (arrayList_dados_NextPage.size() > 0) {
            adapter.removeLastItem();
            loading = false;
            adapter.addList(arrayList_dados_NextPage);
            if (arrayList_dados_NextPage.size() < sizePerPage) {
                adapter.addEndView();
                loading = true;
            } else {
                adapter.addFooterView();
            }
        } else {
            adapter.removeLastItem();
            adapter.addEndView();
            loading = true;
        }
    }

    public static void noData() {

        linearLayoutManager = ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new Adapter_Recycler_Itens_Vertical(
                activity,
                arrayList_dados,
                pageLayoutAdapter
        );

        recyclerView.setAdapter(adapter);

        adapter.addEndView();
    }

    private static RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            if (!loading && linearLayoutManager.getItemCount() == (linearLayoutManager.findLastVisibleItemPosition() + 1)) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initWSConection(textSearsh, pageNumber);
                    }
                }, 1500);

                loading = true;
            }
        }
    };

    public static void initWSConection(String raw_text, int pageNumber) {
        Dados_Ws(18, "busca=" + raw_text + "&quantidade_por_pagina=" + sizePerPage + "&pagina=" + pageNumber, null, activity);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}