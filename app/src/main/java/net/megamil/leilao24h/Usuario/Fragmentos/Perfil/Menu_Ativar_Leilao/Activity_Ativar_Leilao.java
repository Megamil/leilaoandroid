package net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Ativar_Leilao;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Adaptadores.ViewPager_IMG;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Carossel;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Data_e_Hora;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Sistema;

import java.util.HashMap;
import java.util.Map;

import faranjit.currency.edittext.CurrencyEditText;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;
import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos.Funcao_Converter_Dinhero_Ws;
import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos.tGetText;

public class Activity_Ativar_Leilao extends AppCompatActivity {

    private Context context;
    private Context context_activity;
    private Activity activity;
    private Window window;
    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    //TextViews
    private static TextView tv_title;
    private static TextView tv_status;
    private static TextView tv_data_inicio;
    private static TextView tv_hora_inicio;
    private static TextView tv_data_fim;
    private static TextView tv_hora_fim;
    private static TextView tvLabelAnser;

    //EditText
    private static EditText et_descricao;
    private static EditText et_ingressos;

    //CurrencyEditText
    private static CurrencyEditText et_valor_minimo;
    private static CurrencyEditText et_lance_minimo;

    //ImageButton
    private static ImageButton btn_close;

    //Button
    private static Button btn_iniciar;

    //String
    private static String ID_PROCUTO = "";

    //Carossel
    private LinearLayout dotsLinearLayout;
    private TextView imgCount;
    private String[] Array_imagens = null;
    private ViewPager mViewPager;
    private RelativeLayout rlControlImg;

    //ScroolView
    private ScrollView svLayout;

    //LinearLayout
    private LinearLayout llControl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ativar_item_leilao);

        //Var padroes
        context = getApplicationContext();
        context_activity = Activity_Ativar_Leilao.this;
        activity = Activity_Ativar_Leilao.this;
        window = getWindow();

        initVars();
        initActions();
        iniDados();
    }

    private void initVars() {

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */

        //ScrollView
        svLayout = findViewById(R.id.sv);

        //linearlayout
        llControl = findViewById(R.id.llControl);

        //textviews
        tv_title = findViewById(R.id.tv_title);
        tv_status = findViewById(R.id.tv_status);
        tv_data_inicio = findViewById(R.id.tela_agendamento_data);
        tv_hora_inicio = findViewById(R.id.tela_agendamento_hora);
        tv_data_fim = findViewById(R.id.tela_agendamento_data_fim);
        tv_hora_fim = findViewById(R.id.tela_agendamento_hora_fim);
        tvLabelAnser = findViewById(R.id.tvLabelAnser);

        //edittext
        et_descricao = findViewById(R.id.et_descricao);
        et_valor_minimo = findViewById(R.id.et_valor_minimo);
        et_lance_minimo = findViewById(R.id.et_lance_minimo);
        et_ingressos = findViewById(R.id.et_ingressos);

        //imagebutton
        btn_close = findViewById(R.id.btn_close);

        //buttons
        btn_iniciar = findViewById(R.id.btn_iniciar);

        //Carossel
        dotsLinearLayout = (LinearLayout) findViewById(R.id.dotLinear);
        imgCount = findViewById(R.id.img_count);
        mViewPager = (ViewPager) findViewById(R.id.vp_img_pager);
        rlControlImg = findViewById(R.id.rl_img);

    }

    private void initActions() {

        /*
         *  Inicio de Ações da Pagina
         */

        ToolBox_Data_e_Hora.Funcao_AbrirDataPicker(activity, tv_data_inicio, true, true);
        ToolBox_Data_e_Hora.Funcao_AbrirTimerPicker(activity, tv_hora_inicio, true);

        ToolBox_Data_e_Hora.Funcao_AbrirDataPicker(activity, tv_data_fim, true, true);
        ToolBox_Data_e_Hora.Funcao_AbrirTimerPicker(activity, tv_hora_fim, true);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void iniDados() {

        if (getIntent().getIntExtra(HMAux.INTENT_TYPE, -1) == 1) {

            btn_iniciar.setVisibility(View.GONE);

            String ID_LEILAO = getIntent().getStringExtra(HMAux.ID_LEILAO);
            String ID_PROCUTO = getIntent().getStringExtra(HMAux.ID_PROCUTO);
            String NOME_PRODUTO = getIntent().getStringExtra(HMAux.NOME_PRODUTO);
            String DESCRICAO_PRODUTO = getIntent().getStringExtra(HMAux.DESCRICAO_PRODUTO);
            String VALOR_MINIMO = getIntent().getStringExtra(HMAux.VALOR_MINIMO);
            String LANCE_MINIMO = getIntent().getStringExtra(HMAux.LANCE_MINIMO);
            String INGRESSOS = getIntent().getStringExtra(HMAux.INGRESSOS);
            String DATA_INICIO = getIntent().getStringExtra(HMAux.DATA_INICIO);
            String DATA_FIM_PREVISTO = getIntent().getStringExtra(HMAux.DATA_FIM_PREVISTO);
            String STATUS_LEILAO = getIntent().getStringExtra(HMAux.STATUS_LEILAO);
            String IMAGENS = getIntent().getStringExtra(HMAux.IMAGENS_ARRAY);

            tv_title.setText(NOME_PRODUTO);
            setTextWithClickableTextViewStatus(tv_status, STATUS_LEILAO);

            setTextWithClickableEditText(et_descricao, DESCRICAO_PRODUTO);
            setTextWithClickableEditText(et_valor_minimo, VALOR_MINIMO);
            setTextWithClickableEditText(et_lance_minimo, LANCE_MINIMO);
            setTextWithClickableEditText(et_ingressos, INGRESSOS);

            setTextWithClickableTextViewDayAndHours(tv_data_inicio, tv_hora_inicio, DATA_INICIO);
            setTextWithClickableTextViewDayAndHours(tv_data_fim, tv_hora_fim, DATA_FIM_PREVISTO);

            String[] Array_imagens = IMAGENS.split(",");

            ViewPager_IMG mCustomPagerAdapter = new ViewPager_IMG(this, Array_imagens);

            mViewPager.setAdapter(mCustomPagerAdapter);

            ToolBox_Carossel tc = new ToolBox_Carossel(
                    activity,
                    dotsLinearLayout,
                    mViewPager,
                    imgCount,
                    Array_imagens.length
            );

        } else if (getIntent().getIntExtra(HMAux.INTENT_TYPE, -1) == 2) {

            btn_iniciar.setVisibility(View.VISIBLE);
            llControl.setVisibility(View.GONE);

            tvLabelAnser.setText("Por quanto você quer vender?");
            tvLabelAnser.setVisibility(View.VISIBLE);

            ID_PROCUTO = getIntent().getStringExtra(HMAux.ID_PROCUTO);
            String NOME_PRODUTO = getIntent().getStringExtra(HMAux.NOME_PRODUTO);
            String DESCRICAO_PRODUTO = getIntent().getStringExtra(HMAux.DESCRICAO_PRODUTO);
            String NOME_CATEGORIA = getIntent().getStringExtra(HMAux.NOME_CATEGORIA);
            String IMAGENS = getIntent().getStringExtra(HMAux.IMAGENS_ARRAY);

            tv_title.setText(NOME_PRODUTO);
            et_descricao.setText(DESCRICAO_PRODUTO);
            tv_status.setText(NOME_CATEGORIA);

            String[] Array_imagens = IMAGENS.split(",");

            ViewPager_IMG mCustomPagerAdapter = new ViewPager_IMG(this, Array_imagens);

            mViewPager.setAdapter(mCustomPagerAdapter);

            ToolBox_Carossel tc = new ToolBox_Carossel(
                    activity,
                    dotsLinearLayout,
                    mViewPager,
                    imgCount,
                    Array_imagens.length
            );

            btn_iniciar.setOnClickListener(onClickListener_initArticle);
        } else if (getIntent().getIntExtra(HMAux.INTENT_TYPE, -1) == 3) {
//            svLayout.setVisibility(View.GONE);

            rlControlImg.setVisibility(View.GONE);
            llControl.setVisibility(View.GONE);

            ID_PROCUTO = getIntent().getStringExtra(HMAux.ID_PROCUTO);
            String nomeProduto = getIntent().getStringExtra(HMAux.NOME_PRODUTO);

            tv_title.setText(nomeProduto);
            tv_status.setText("Por quanto você quer vender?");
            btn_iniciar.setOnClickListener(onClickListener_initArticle);
        }


    }

    private void setTextWithClickableTextViewStatus(TextView textView, String string) {
        if (string.equals("Aprovado")) {
            textView.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        } else {
            textView.setTextColor(activity.getResources().getColor(R.color.google_app_red));
        }
        textView.setText(string);
    }

    private void setTextWithClickableTextViewDayAndHours(TextView day, TextView hour, String timestamp) {

        String[] strings = ToolBox_Data_e_Hora.Funcao_TimeStamp_Array(timestamp, true, false);

        day.setEnabled(false);
        day.setText(strings[0]);

        hour.setEnabled(false);
        hour.setText(strings[1]);
    }

    private void setTextWithClickableEditText(EditText editText, String string) {
        editText.setEnabled(false);
        editText.setText(string);
    }

    View.OnClickListener onClickListener_initArticle = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            initWSConection();
        }
    };

    private void initWSConection() {


        boolean conn = ToolBox_Verivifacao_Sistema.Funcao_Status_Conexao(context); // função para checar a conexao antes
        if (conn == true) {

            String valor_minimo = et_valor_minimo.getText().toString().trim();
            String lance_minimo = et_lance_minimo.getText().toString().trim();
            String ingressos = et_ingressos.getText().toString().trim();
            String data_inicio = ToolBox_Data_e_Hora.Funcao_Data_Hora_TimeStamp(tGetText(tv_data_inicio), tGetText(tv_hora_inicio));
            String data_fim = ToolBox_Data_e_Hora.Funcao_Data_Hora_TimeStamp(tGetText(tv_data_fim), tGetText(tv_hora_fim));

            boolean[] Array_De_Obrigatorios = new boolean[]{
//                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_ingressos, ingressos),
//                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_lance_minimo, lance_minimo),
                    ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(activity, et_valor_minimo, valor_minimo)
//                    ToolBox_Verivifacao_Campos.Funcao_Verifica_Data_Menor_que_Hoje(data_inicio),
//                    ToolBox_Verivifacao_Campos.Funcao_Verifica_Data_Menor_que_Hoje(data_fim),
//                    ToolBox_Verivifacao_Campos.Funcao_Verifica_Data_Final(activity, data_inicio, data_fim)
            };

            Boolean validação_final = ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios);

            if (validação_final == true) {

                String Dados_Para_URL_GET = "";
                Map<String, String> Dados_Para_Parametros = new HashMap<>();

                Dados_Para_Parametros.put("id_produto", ID_PROCUTO);
                Dados_Para_Parametros.put("valor_minimo", Funcao_Converter_Dinhero_Ws(valor_minimo));
//                Dados_Para_Parametros.put("lance_minimo", Funcao_Converter_Dinhero_Ws(lance_minimo));
//                Dados_Para_Parametros.put("ingressos", ingressos);
//                Dados_Para_Parametros.put("data_inicio", data_inicio);
//                Dados_Para_Parametros.put("data_fim", data_fim);

                Log.w("Array", Dados_Para_Parametros.toString());

                Dados_Ws(9, Dados_Para_URL_GET, Dados_Para_Parametros, activity);

            }

        } else {
            MsgUtil.Funcao_MSG(activity, context.getResources().getString(R.string.msg_global_sem_conexao));
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
