package net.megamil.leilao24h.Usuario;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Splash.Splash;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Busca.Adapter_Dialog_Busca;
import net.megamil.leilao24h.Usuario.Busca.DB_Busca;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Categorias;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Favoritos;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Inicio;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Leiloes;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Perfil;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.NotificationHelper;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Permissao;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Views;

import java.util.ArrayList;
import java.util.Locale;

import static net.megamil.leilao24h.Utils.ToolBox._ToolBox_Config_Inicial.Funcao_Verifica_Pastas;


public class Pagina_Inicio extends AppCompatActivity {

    private static Context context;
    private static Activity activity;
    private Window window;
    private ConstraintLayout rl_pai;

    private static Fragment_Inicio fPerfil;
    private static Fragment_Categorias fAgenda;
    private static Fragment_Favoritos fFinanceiro;
    private static Fragment_Leiloes fLeiloes;
    private static Fragment_Perfil fProcedimentos;


    //searchbar
    private static TextView tv_busca;
    private LinearLayout ll_busca;
    private static Dialog dialog;

    private int indice = 1;
    private static int indice_antigo = 1;

    BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        Pagina_Inicio.this.invalidateOptionsMenu();

        activity = Pagina_Inicio.this;
        Splash.verifyIsLogued(activity);

        initVars();
        initActions();

        Boolean permissao = ToolBox_Permissao.Funcao_Verifica_Permissao(activity, ToolBox_Permissao.VERIFICA_WRITE_EXTERNAL_STORAGE);

        if (permissao == true) {
//            ChamaModal(activity, Uri_Camera);
        }

        Funcao_Verifica_Pastas();
//        Funcao_Verifica_Permicoes_Do_APP(activity);


        setupBottomNavigationView();
        NotificationHelper.startPowerSaverIntent(this);
    }

    private void initVars() {

        context = getApplicationContext();
        window = getWindow();


        fPerfil = new Fragment_Inicio();
        fFinanceiro = new Fragment_Favoritos();
        fProcedimentos = new Fragment_Perfil();
        fLeiloes = new Fragment_Leiloes();
        fAgenda = new Fragment_Categorias();

        ll_busca = findViewById(R.id.ll_include_searchbar);
        tv_busca = findViewById(R.id.tv_include_busca);

        rl_pai = findViewById(R.id.rl_pai);
        navigationView = findViewById(R.id.bottomNavigation);
        ToolBox_Views.BottomNavigationView_disableShiftMode(navigationView);

    }

    private void initActions() {

        Intent intent = getIntent();
        int indce_raw = intent.getIntExtra("indice", 0);

        if (indce_raw != 0) {
            indice = indce_raw;
            Log.w("Novo Indice", String.valueOf(indice));
        }

        if (indce_raw == 2) {
            indice_antigo = 2;
        }
        setFragment(indice);

        ll_busca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                MsgUtil.Funcao_MSG(context , "Search");
                Funcao_AbreDialog_Busca();
            }
        });

    }

    static ArrayList<HMAux> dados_s = new ArrayList<>();
    static ArrayList<HMAux> dados_s_pesuisa = new ArrayList<>();
    private static ListView listView_busca;
    private static PopupWindow popupWindow;

    public static void Funcao_AbreDialog_Busca() {
        dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(R.layout.dialog_searchbar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogNoAnimation;
//        ImageView img_full_screen_dialog = fullscreenDialog.findViewById(R.id.img_full_screen_dialog);
//        Glide.with(getContext()).load(R.drawable.google_assistant).into(img_full_screen_dialog);
        ImageView img_dialog_fullscreen_close = dialog.findViewById(R.id.img_dialog_fullscreen_close);
        ImageView img_dialog_fullscreen_p_voz = dialog.findViewById(R.id.img_dialog_fullscreen_p_voz);
        final ImageView img_dialog_fullscreen_menu = dialog.findViewById(R.id.img_dialog_fullscreen_menu);

        final EditText editText = dialog.findViewById(R.id.et_busca);
        final DB_Busca db_busca = new DB_Busca(context);//conexao

        final RelativeLayout relativeLayout_busca = dialog.findViewById(R.id.rl_dialog_busca);
        final LinearLayout linearLayout_busca = dialog.findViewById(R.id.ll_dialog_buscas);

        listView_busca = dialog.findViewById(R.id.lv_dialog_buscas);

        dados_s = db_busca.Obter_Dados_Historico_Busca();
//        Log.w("Dados de Historico", dados_s.toString());

        if (dados_s.size() > 0) {
            linearLayout_busca.setVisibility(View.VISIBLE);
            relativeLayout_busca.setVisibility(View.GONE);
        } else {
            linearLayout_busca.setVisibility(View.GONE);
            relativeLayout_busca.setVisibility(View.VISIBLE);
        }

        Adapter_Dialog_Busca adapter_dialog_busca = new Adapter_Dialog_Busca(context,
                R.layout.celula_searchbar,
                dados_s
        );
        listView_busca.setAdapter(adapter_dialog_busca);

        listView_busca.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final HMAux hmAux = (HMAux) parent.getItemAtPosition(position);

                String texto_celula_pesquisa = hmAux.get(HMAux.BUSCA_TEXTO_PESQUISA);

                Chama_Tela_Busca(texto_celula_pesquisa);

                tv_busca.setText(texto_celula_pesquisa);
                dialog.dismiss();
            }
        });

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.dialog_serachbar_menu, null);
        popupWindow = new PopupWindow(popupView, 700, ViewGroup.LayoutParams.WRAP_CONTENT);

        img_dialog_fullscreen_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow.isShowing())
                    popupWindow.dismiss();
                else
                    popupWindow.showAsDropDown(img_dialog_fullscreen_menu, 50, 0);
            }
        });

        TextView menu1 = popupView.findViewById(R.id.dialog_searchbar_menu_1);

        menu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db_busca.DeletaBanco_Historico_Busca();
                popupWindow.dismiss();
                linearLayout_busca.setVisibility(View.GONE);
                relativeLayout_busca.setVisibility(View.VISIBLE);
            }
        });


        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    MsgUtil.Funcao_MSG(context , "Iniciando pesquisa");

                    String texto_de_pesquisa = editText.getText().toString().trim();

                    if (texto_de_pesquisa.isEmpty() || texto_de_pesquisa.equals("")) {
                        MsgUtil.Funcao_MSG(context, "Digite Algo Para Pesquisa");
                    } else {
                        DB_Busca db_busca = new DB_Busca(context);//conexao

                        db_busca.CriaBaseDeDados();
                        db_busca.Inserir_Historico_Busca(texto_de_pesquisa);

                        Chama_Tela_Busca(texto_de_pesquisa);

                        tv_busca.setText(texto_de_pesquisa);
                        dialog.dismiss();
                    }


                }
                return false;
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String texto_para_pesquisa = editText.getText().toString().trim();
                Filtro_De_Pesquisa(texto_para_pesquisa);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        img_dialog_fullscreen_p_voz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pesquisa_Por_Voz();
            }
        });

        img_dialog_fullscreen_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private static void Chama_Tela_Busca(String texto) {

        Intent intent = new Intent(activity, Pagina_Busca.class);
        intent.putExtra("pesquisa", texto);

        ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");

    }

    private static void Pesquisa_Por_Voz() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "O quer pesquisa?");
        try {
            activity.startActivityForResult(intent, 10);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(context, "Error ao Pesquisar",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public static void Filtro_De_Pesquisa(String name) {
        name = name.toLowerCase(Locale.getDefault());
        dados_s_pesuisa.clear();

        Adapter_Dialog_Busca adapter_dialog_busca = new Adapter_Dialog_Busca(context,
                R.layout.celula_searchbar,
                dados_s_pesuisa
        );

        listView_busca.setAdapter(adapter_dialog_busca);

        if (name.length() == 0) {

            Adapter_Dialog_Busca adapter_dialog_busca1 = new Adapter_Dialog_Busca(context,
                    R.layout.celula_searchbar,
                    dados_s
            );

            listView_busca.setAdapter(adapter_dialog_busca1);

        } else {
            for (HMAux dados_pesquisa : dados_s) {
                if (dados_pesquisa.get(HMAux.BUSCA_TEXTO_PESQUISA).toLowerCase(Locale.getDefault()).contains(name)) {
                    dados_s_pesuisa.add(dados_pesquisa);
                }
            }

        }

    }


    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    DB_Busca db_busca = new DB_Busca(context);//conexao
                    db_busca.CriaBaseDeDados();
                    db_busca.Inserir_Historico_Busca(result.get(0));
                    tv_busca.setText((result.get(0)));

                    Chama_Tela_Busca(result.get(0));

                    dialog.dismiss();
                }
                break;
            }

        }
    }


    private void setupBottomNavigationView() {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                switch (item.getItemId()) {
                    case R.id.usuario_menu_perfil:
                        if (indice != 1) {
                            indice = 1;
                            //
                            navigationView.setSelectedItemId(R.id.usuario_menu_perfil);
                            setFragment(indice);
//                            item_agenda_limpar.setVisible(false);

                        }
                        break;
                    case R.id.usuario_menu_agenda:
                        if (indice != 2) {
                            indice = 2;

                            setFragment(indice);
                        }
                        break;
                    case R.id.usuario_menu_financeiros:
                        if (indice != 3) {
                            indice = 3;
                            //
                            setFragment(indice);
                        }
                        break;
                    case R.id.usuario_menu_leiloes:
                        if (indice != 4) {
                            indice = 4;
                            //
                            setFragment(indice);
                        }
                        break;
                    case R.id.usuario_menu_procedimentos:
                        if (indice != 5) {
                            indice = 5;
                            //
                            setFragment(indice);
                        }

                        break;

                }
                updateNavigationBarState(item.getItemId());

                return true;
            }
        });
    }

    public void setFragment(int indice) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        if (indice_antigo > indice) {
            ft.setCustomAnimations(R.anim.left_enter, R.anim.left_exit);
        } else if (indice_antigo < indice) {
            ft.setCustomAnimations(R.anim.right_enter, R.anim.right_exit);
        }
//        else if(indice_antigo == indice){
//            ft.setCustomAnimations(R.anim.left_enter, R.anim.left_exit, R.anim.left_pop_enter, R.anim.left_pop_exit);
//        }

        switch (indice) {

            case 1:
                ft.replace(R.id.usuario_fragmento_inicial, fPerfil, "Frag Perfil");
                ft.commit();
                break;
            case 2:
                ft.replace(R.id.usuario_fragmento_inicial, fAgenda, "Frag Agenda");
                ft.commit();
                break;
            case 3:
                ft.replace(R.id.usuario_fragmento_inicial, fFinanceiro, "Frag Financeiro");
                ft.commit();
                break;
            case 4:
                ft.replace(R.id.usuario_fragmento_inicial, fLeiloes, "Frag Leiloes");
                ft.commit();
                break;
            case 5:
                ft.replace(R.id.usuario_fragmento_inicial, fProcedimentos, "Frag Procedimentos");
                ft.commit();
                break;


        }

        setIndiceMenu(indice);
        indice_antigo = indice;
    }


    private void updateNavigationBarState(int actionId) {
        Menu menu = navigationView.getMenu();

        for (int i = 0, size = menu.size(); i < size; i++) {
            MenuItem item = menu.getItem(i);
            item.setChecked(item.getItemId() == actionId);
        }
    }

    @Override
    protected void onResume() {
        setIndiceMenu(indice);
        super.onResume();
    }

    private void setIndiceMenu(int indice) {

        switch (indice) {
            case 1:
                navigationView.setSelectedItemId(R.id.usuario_menu_perfil);
                break;
            case 2:
                navigationView.setSelectedItemId(R.id.usuario_menu_agenda);
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
        }
    }


    @Override
    public void onBackPressed() {

//        if (popupWindow.isShowing()) {
//            popupWindow.dismiss();
//        } else
        if (indice != 1) {
            setFragment(1);
            indice = 1;

        }

    }
}