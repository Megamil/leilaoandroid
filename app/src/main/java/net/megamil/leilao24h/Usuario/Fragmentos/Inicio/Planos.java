package net.megamil.leilao24h.Usuario.Fragmentos.Inicio;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_Planos;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;

import java.util.ArrayList;
import java.util.List;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


public class Planos extends AppCompatActivity {

    private static Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;

    private static ImageButton btn_voltar;
    public static List<HMAux> arrayList_dados = new ArrayList<>();
    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    //ListView
    private static ListView lv_itens;

    //Textview
    private static TextView tv_toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_listview);

        //Var padroes
        context = getApplicationContext();
        context_activity = Planos.this;
        activity = Planos.this;
        window = getWindow();
        btn_voltar = findViewById(R.id.btn_voltar);

        initWSConection();
        initVars();
        initActions();


    }

    private void initVars() {

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */

        lv_itens = findViewById(R.id.lv_itens);
        tv_toolbar = findViewById(R.id.tv_toolbar);

    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */
        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_toolbar.setText("Planos");
//        arrayList_dados = gerarDados(90);
//        Set_Dados_Array(arrayList_dados);


    }

    public static void Set_Dados_Array() {

//        arrayList_dados.add(arrayList);

        Adapter_Planos adapter_leiloes = new Adapter_Planos(
                activity,
                R.layout.celula_plano,
                arrayList_dados
        );

        lv_itens.setAdapter(adapter_leiloes);

    }

    public void initWSConection() {
        Dados_Ws(12, "quantidade_por_pagina=100&pagina=1", null, activity);
    }

    @Override
    public void onBackPressed() {
//        ToolBox_Chama_Activity.Funcao_Chama_TelaPrincipal(activity);
        super.onBackPressed();
        finish();
    }

    public static void planosFinish() {
        activity.finish();
    }


}
