package net.megamil.leilao24h.Usuario.Adaptadores;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Arremates;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Recompensa;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;
import net.megamil.leilao24h.Utils.ToolBox.DialogUtil;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Imagem;

import java.util.List;
import java.util.Map;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;

public class adapterHistoricoRecompensa extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //First
    private static Activity activity;
    private int layout;
    private List<HMAux> dataBase;
    private HMAux hmAux = new HMAux();
    private LayoutInflater layoutInflater;
    private int viewPosition;

    //Second - View's Types
    private int NORMAL_TYPE = 0;
    private int FOOTER_TYPE = 1;
    private int END_TYPE = 2;
    private int ADS_TYPE = 3;

    /**
     * init Constructor with HMAux's List and Resorce
     *
     * @param activity activity pai
     * @param dataBase List de HMAux
     * @param layout   layout chamado de fora do metodo
     */
    public adapterHistoricoRecompensa(Activity activity, List<HMAux> dataBase, int layout) {
        this.activity = activity;
        this.dataBase = dataBase;
        this.layout = layout;
        this.layoutInflater = LayoutInflater.from(activity);
    }

    /**
     * init Constructor with HMAux's List
     *
     * @param activity activity pai
     * @param dataBase HMAux's list
     */
    public adapterHistoricoRecompensa(Activity activity, List<HMAux> dataBase) {
        this.activity = activity;
        this.dataBase = dataBase;
        this.layoutInflater = LayoutInflater.from(activity);
    }

    /**
     * @param list HMAux's list
     */
    public void addList(List<HMAux> list) {
        dataBase.addAll(list);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * @param hmAux add dados raw hmaux
     */
    public void addItem(HMAux hmAux) {
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * add raw item dados with position
     *
     * @param position int position
     * @param hmAux    raw dados
     */
    public void addItemAtPosition(int position, HMAux hmAux) {
        dataBase.add(position, hmAux);
        notifyItemInserted(position);
    }

    /**
     * remove at first position
     */
    public void removeFirstItem() {
        dataBase.remove(0);
        notifyItemRemoved(0);
    }

    /**
     * remove at last position
     */
    public void removeLastItem() {
        dataBase.remove(dataBase.size() - 1);
        notifyItemRemoved(dataBase.size() - 1);
    }

    /**
     * remove item at positon with parameter
     *
     * @param position
     */
    public void removeItemAtPosition(int position) {
        dataBase.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * add FooterView at RecyclerView
     */
    public void addFooterView() {
        hmAux.put("type", String.valueOf(FOOTER_TYPE));
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * add End at RecyclerView
     */
    public void addEndView() {
        hmAux.put("type", String.valueOf(END_TYPE));
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * add Google ADS at RecyclerView
     */
    public void addAdsView() {
        hmAux.put("type", String.valueOf(ADS_TYPE));
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param position
     * @return ViewType for load Resorce
     */
    @Override
    public int getItemViewType(int position) {
        HMAux s = dataBase.get(position);
        String typeView = s.get("type");

        if (s.containsKey("type")) {

            if (typeView.equals(String.valueOf(FOOTER_TYPE))) {
                return FOOTER_TYPE;
            } else if (typeView.equals(String.valueOf(END_TYPE))) {
                return END_TYPE;
            } else if (typeView.equals(String.valueOf(ADS_TYPE))) {
                return ADS_TYPE;
            } else {
                return NORMAL_TYPE;
            }

        } else {
            return NORMAL_TYPE;
        }
    }

    /**
     * @return HMAux's List size
     */
    @Override
    public int getItemCount() {
        return dataBase.size();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator animatorUP = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    animatorUP.setDuration(200);
                    animatorUP.setInterpolator(new DecelerateInterpolator());
                    animatorUP.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator animatorDOWN = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    animatorDOWN.setDuration(200);
                    animatorDOWN.setInterpolator(new AccelerateInterpolator());
                    animatorDOWN.start();
                    break;
            }
            return false;
        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * in class implements onMoveAndSwipedListener to use methods at down
     */

//    @Override
//    public boolean onItemMove(int fromPosition, int toPosition) {
//        Collections.swap(mItems, fromPosition, toPosition);
//        notifyItemMoved(fromPosition, toPosition);
//        return true;
//    }
//
//    @Override
//    public void onItemDismiss(final int position) {
//        mItems.remove(position);
//        notifyItemRemoved(position);
//
//        Snackbar.make(parentView, "Item Deleted", Snackbar.LENGTH_SHORT)
//                .setAction("Undo", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        addItem(position, mItems.get(position));
//                    }
//                }).show();
//    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == FOOTER_TYPE) {
            View view = layoutInflater.inflate(R.layout.celula_recycler_footer_vertical, parent, false);
            return new FooterViewHolder(view);
        } else if (viewType == END_TYPE) {
            View view = layoutInflater.inflate(R.layout.celula_recycler_end_vertical, parent, false);
            return new EndViewHolder(view);
        } else if (viewType == ADS_TYPE) {
            View view = layoutInflater.inflate(R.layout.celula_leilao_recompensa, parent, false);
            return new AdsViewHolder(view);
        } else {
            View view = layoutInflater.inflate(R.layout.celula_recompenda, parent, false);
            return new RecyclerViewHolder(view);
        }

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        hmAux = dataBase.get(position);
        if (holder instanceof FooterViewHolder) {
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            initActions_FooterView(viewHolder);
        } else if (holder instanceof EndViewHolder) {
            EndViewHolder viewHolder = (EndViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            initActions_EndView(viewHolder);
        } else if (holder instanceof AdsViewHolder) {
            AdsViewHolder viewHolder = (AdsViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            initActions_AdsView(viewHolder);
        } else {
            RecyclerViewHolder viewHolder = (RecyclerViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            initActions_NormalView(viewHolder);
        }
    }

    /**
     * ViewHolder FOOTER_TYPE
     */
    private class FooterViewHolder extends RecyclerView.ViewHolder {
        private View view;
        //init privates instances

        public FooterViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            //init instance's findViewById


            /**
             * Events at View
             */
            view.setOnClickListener(FOOTER_TYPE_ViewClick);
        }
    }

    /**
     * @param viewHolder
     */
    private void initActions_FooterView(FooterViewHolder viewHolder) {

    }

    private View.OnClickListener FOOTER_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ViewHolder END_VIEW
     */
    private class EndViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private Button buttonReload;

        public EndViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            buttonReload = view.findViewById(R.id.btnRetry);
            buttonReload.setOnClickListener(END_TYPE_ViewClick);
        }
    }

    private void initActions_EndView(EndViewHolder viewHolder) {

    }

    private View.OnClickListener END_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            removeLastItem();
            addFooterView();

            Activity_Arremates.initWSConection(Activity_Arremates.pageNumber);

        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ViewHolder ADS_VIEW
     */
    private class AdsViewHolder extends RecyclerView.ViewHolder {
        private View view;
        //init privates instances

        public AdsViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            //init instance's findViewById


            /**
             * Events at View
             */
            view.setOnClickListener(ADS_TYPE_ViewClick);
        }
    }

    private void initActions_AdsView(AdsViewHolder viewHolder) {

    }

    private View.OnClickListener ADS_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * ViewHolder NORMAL_TYPE
     */
    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private View view;

        // view
        private ImageView img_item;
        private CardView labelNew;
        private CardView labelCategory;
        private TextView labelCategoria;
        private TextView name_item;
        private TextView desc_item;
        private TextView ingressos;
        private TextView data_inicio;
        private ImageView btn_item;
        private RelativeLayout rl_img;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            //findViewById's

            img_item = itemView.findViewById(R.id.img_item);
            labelNew = itemView.findViewById(R.id.labelNew);
            labelCategory = itemView.findViewById(R.id.labelCategory);
            labelCategoria = itemView.findViewById(R.id.labelCategoria);
            name_item = itemView.findViewById(R.id.name_item);
            desc_item = itemView.findViewById(R.id.desc_item);
            ingressos = itemView.findViewById(R.id.ingressos);
            rl_img = itemView.findViewById(R.id.rl_img);

            /**
             * Events at View
             */
            view.setOnClickListener(ITEM_TYPE_ViewClick);
            view.setOnLongClickListener(NORMAL_TYPE_LongClick);
        }
    }

    private void initActions_NormalView(RecyclerViewHolder viewHolder) {


        //Cria uma animação para a entrada da celula , mView é a celula
//            Animation animation = AnimationUtils.loadAnimation(context, R.anim.anim_recycler_item_show);
//            recyclerViewHolder.mView.startAnimation(animation);

//            AlphaAnimation aa1 = new AlphaAnimation(1.0f, 0.1f);
//            aa1.setDuration(400);
//            recyclerViewHolder.rela_round.startAnimation(aa1);

//            recyclerViewHolder.pos_item.setText(hmAux.get(HMAux.ID));
        viewHolder.name_item.setText(this.hmAux.get(HMAux.NOME_PRODUTO));
        viewHolder.desc_item.setText(this.hmAux.get(HMAux.DESCRICAO_PRODUTO));
        viewHolder.ingressos.setText(this.hmAux.get(HMAux.PRODUTO_RECOMPENSA_CUSTO));
//        viewHolder.data_inicio.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(this.hmAux.get(HMAux.DATA_INICIO), false, false));
////            recyclerViewHolder.data_inicio.setText(hmAux.get(HMAux.DATA_INICIO));
        String id_leilao = this.hmAux.get(HMAux.ID_PRODUTO);
        //                .load(Conexao_Constantes.Base_URL_WS + "/upload/produtos/" + id_leilao + "/" + id_leilao + "_0.png")

        Glide.with(activity).asBitmap()
                .load(Conexao_Constantes.Base_URL_WS + this.hmAux.get(HMAux.IMAGENS))
//                    .load(Conexao_Constantes.Base_URL_WS + hmAux.get(HMAux.IMAGENS))
                .apply(new RequestOptions()
                        .centerCrop()
                )
                .into(viewHolder.img_item);
        Log.w("URL", this.hmAux.get(HMAux.IMAGENS));

        if (this.hmAux.get(HMAux.PRODUTO_USADO).equals("0")) {
            viewHolder.labelNew.setVisibility(View.VISIBLE);
        } else {
            viewHolder.labelNew.setVisibility(View.GONE);
        }

        if (!this.hmAux.get(HMAux.NOME_CATEGORIA).equals("")){
            viewHolder.labelCategory.setVisibility(View.VISIBLE);
            viewHolder.labelCategoria.setText(this.hmAux.get(HMAux.NOME_CATEGORIA));
        }else{
            viewHolder.labelCategory.setVisibility(View.VISIBLE);
            viewHolder.labelCategoria.setText("");
        }


//            final int favorito = Integer.parseInt(hmAux.get(HMAux.ITEM_FAVORITO));
//            Favorito(recyclerViewHolder.btn_item, favorito);
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
         *
         *  Caso tenha click em determinados lugares da celula, pode ser chamado aqui
         *  Neste caso , o mView esta referenciando a celula completa
         *
         */

        AlphaAnimation aa = new AlphaAnimation(0.1f, 1.0f);
        aa.setDuration(400);

        viewHolder.rl_img.startAnimation(aa);

    }
    private View.OnClickListener ITEM_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            viewPosition = (int) view.getTag();
            HMAux hmAux = dataBase.get(viewPosition);

            int value = Integer.parseInt(hmAux.get(HMAux.PRODUTO_RECOMPENSA_CUSTO));

            if (Recompensa.points < value) {
                MsgUtil.Funcao_MSG(activity, "Pontos insuficientes");
            } else {
                new AlertDialog.Builder(activity)
                        .setMessage("Adquirir?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                viewPosition = (int) view.getTag();
                                HMAux hmAux_click = dataBase.get(viewPosition);
                                Map<String, String> map = new ArrayMap<>();
                                map.put("tipo_recompensa", "2");
                                map.put("id_recompensa", hmAux_click.get(HMAux.ID_PRODUTO));
                                Dados_Ws(24, "", map, activity);
                            }
                        })
                        .setNegativeButton("Não", null)
                        .create()
                        .show();
            }

        }
    };

    private View.OnLongClickListener NORMAL_TYPE_LongClick = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            viewPosition = (int) v.getTag();
            HMAux hmAux = dataBase.get(viewPosition);
            DialogUtil.showDetails(activity, hmAux, viewPosition);
            return true;
        }
    };
    private String setTextNew(String s) {
        if (Integer.parseInt(s) == 0) {
            return "Novo";
        } else {
            return "Usado";
        }
    }

}
