package net.megamil.leilao24h.Usuario.Fragmentos.Perfil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.isseiaoki.simplecropview.util.Utils;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;
import net.megamil.leilao24h.Utils.ToolBox.A_ToolBox_Gerador_De_Dados;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Imagem;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Mascaras;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Permissao;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Sistema;
import net.megamil.leilao24h.Utils.ToolBox.toolbox_Modal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;
import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos.tGetSpinnerPosition;
import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos.tGetSpinnerPositionSex;
import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos.tGetText;
import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos.tGetTextPass;
import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos.tValidePass;
import static net.megamil.leilao24h.Utils.ToolBox.toolbox_Modal.OPEN_CROPVIEW;


public class Editar_Usuario extends AppCompatActivity {

    private static Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;
    private Bundle bundle;
    private static ToolBox_SharedPrefs_Usuario prefs;

    private static ImageButton btn_voltar;
    /////////////////////////////////////////////////////////////////////////////

    /*
     *  _Inicio de Variaveis
     */

    //CircleImageView
    private static CircleImageView img_usuario;
    private static CircleImageView img_plus;

    //editText's
    public static EditText et_nome_usuario;
    public static EditText et_email_usuario;
    public static EditText et_rg_usuario;
    public static EditText et_cpf_usuario;
    public static EditText et_celular_usuario;
    public static EditText et_telefone_usuario;
    public static EditText et_endereco_usuario;
    public static EditText et_cep_usuario;
    public static EditText et_numero_usuario;
    public static EditText et_complemento_usuario;
    public static EditText et_bairro_usuario;
    public static EditText et_cidade_usuario;
    public static EditText et_senha_usuario;
    public static EditText et_senha_conficacao_usuario;

    //Spinner's
    public static Spinner sp_sexo;
    public static Spinner sp_estado;

    //Button's
    private static Button btn_atualizar;

    //TextViwe's
    private static TextView tv_toolbar;

    //LinearLayout's
    private static LinearLayout ll_editar_dados;
    private static LinearLayout ll_editar_senha;

    //int's
    private static int intentType;
    private static int userID = 0;

    //boolean's
    private boolean iniSearshCep = false;
    private static boolean forceDataComplete = false;

    //String
    private static String fotoUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_usuario);

        initVars();
        initActions();

        bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("comp")) {
            iniIntentType(1);
            String jsonString = bundle.getString("comp");
            Log.w("DATA", jsonString);
            JSONObject jObj = null;
            try {
                jObj = new JSONObject(jsonString);
                setUserData(jObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            forceDataComplete = true;
        } else {
            iniIntentType(intentType);
            Funcao_PreEditar_Usuario();
            forceDataComplete = false;
        }

        setImgUser();

    }

    public static void setUserData(JSONObject jsonObject) throws JSONException {
//        String name = jsonObject.getString("nome_usuario");
        if (jsonObject.has("id_usuario")) {
            userID = jsonObject.getInt("id_usuario");
        }

        setImgUser();

        et_nome_usuario.setText(jsonObject.getString("nome_usuario"));
        et_email_usuario.setText(jsonObject.getString("email_usuario"));
        et_rg_usuario.setText(jsonObject.getString("rg_usuario"));
        et_cpf_usuario.setText(jsonObject.getString("cpf_usuario"));
        et_celular_usuario.setText(jsonObject.getString("celular_usuario"));
        et_telefone_usuario.setText(jsonObject.getString("telefone_usuario"));
        et_endereco_usuario.setText(jsonObject.getString("logradouro_usuario"));
        et_cep_usuario.setText(jsonObject.getString("cep_usuario"));
        et_numero_usuario.setText(jsonObject.getString("numero_usuario"));
        et_complemento_usuario.setText(jsonObject.getString("complemento_usuario"));
        et_bairro_usuario.setText(jsonObject.getString("bairro_usuario"));
        et_cidade_usuario.setText(jsonObject.getString("cidade_usuario"));

        sp_estado.setSelection(ToolBox_Verivifacao_Campos.Funcao_verificaSpinnerVazio(jsonObject.getString("estado_usuario")));
        sp_sexo.setSelection(ToolBox_Verivifacao_Campos.Funcao_verificaSpinnerVazioSex(jsonObject.getString("sexo_usuario")));
    }

    private void iniIntentType(int intentType) {
        if (intentType == 1) {
            tv_toolbar.setText("Editar Usuario");
            ll_editar_dados.setVisibility(View.VISIBLE);
            ll_editar_senha.setVisibility(View.GONE);
        } else {
            tv_toolbar.setText("Editar Senha");
            ll_editar_dados.setVisibility(View.GONE);
            ll_editar_senha.setVisibility(View.VISIBLE);
        }
    }


    private void initVars() {

        //Var padroes
        context = getApplicationContext();
        context_activity = Editar_Usuario.this;
        activity = Editar_Usuario.this;
        window = getWindow();

        btn_voltar = findViewById(R.id.btn_voltar);
        ////////////////////////////////////////////////////////////////////////

        /*
         * findViewById's
         */

        tv_toolbar = findViewById(R.id.tv_toolbar);
        btn_atualizar = findViewById(R.id.btn_atualizar);

        ll_editar_dados = findViewById(R.id.ll_editar_dados);
        ll_editar_senha = findViewById(R.id.ll_editar_senha);

        // Editar Usuario
        img_usuario = findViewById(R.id.login_frag_cadastro_et_usuario_img_principal);
        img_plus = findViewById(R.id.iv_pupila_foto_icone_foto_olho_viewpager_pronturio_esquerdo_f02);

        et_nome_usuario = findViewById(R.id.et_nome_usuario);
        et_email_usuario = findViewById(R.id.et_email_usuario);
        et_rg_usuario = findViewById(R.id.et_rg_tela_editar_usuario);
        et_cpf_usuario = findViewById(R.id.et_cpf_tela_editar_usuario);
        et_celular_usuario = findViewById(R.id.et_celular_tela_editar_usuario);
        et_telefone_usuario = findViewById(R.id.et_telefone_tela_editar_usuario);
        et_endereco_usuario = findViewById(R.id.et_endereco_tela_editar_usuario);
        et_cep_usuario = findViewById(R.id.et_nome_cep_editar_usuario);
        et_numero_usuario = findViewById(R.id.et_numero_tela_editar_usuario);
        et_complemento_usuario = findViewById(R.id.et_complemento_tela_editar_usuario);
        et_bairro_usuario = findViewById(R.id.et_bairro_tela_editar_usuario);
        et_cidade_usuario = findViewById(R.id.et_cidade_tela_editar_usuario);

        sp_estado = findViewById(R.id.sp_estados_tela_editar_usuario);
        sp_sexo = findViewById(R.id.sp_sexo_tela_editar_usuario);

        //Editar Senha
        et_senha_usuario = findViewById(R.id.et_senha_usuario);
        et_senha_conficacao_usuario = findViewById(R.id.et_senha_confirmacao_usuario);

    }

    private void initActions() {
        /*
         *  _Inicio de Ações da Pagina
         */


        intentType = getIntent().getIntExtra("type", 0);

        A_ToolBox_Gerador_De_Dados.Spinner_Estados_do_Brasil(activity, sp_estado);
        A_ToolBox_Gerador_De_Dados.Spinner_Sexo(activity, sp_sexo);

        et_celular_usuario.addTextChangedListener(ToolBox_Mascaras.Funcao_MascaraParaTelefone(et_celular_usuario));
        et_telefone_usuario.addTextChangedListener(ToolBox_Mascaras.Funcao_MascaraParaTelefone(et_telefone_usuario));

        et_cpf_usuario.addTextChangedListener(ToolBox_Mascaras.Funcao_InsereMascara_Universao(et_cpf_usuario, ToolBox_Mascaras.CONSTANTE_MASCARA_FORMAT_CPF));
        TextWatcher RGWatcher = ToolBox_Mascaras.Funcao_Dois_Teclados_Com_Mascaras(ToolBox_Mascaras.CONSTANTE_MASCARA_FORMAT_RG, et_rg_usuario, 10, InputType.TYPE_CLASS_NUMBER, InputType.TYPE_CLASS_TEXT);
        et_rg_usuario.addTextChangedListener(RGWatcher);
        et_cep_usuario.addTextChangedListener(ToolBox_Mascaras.Funcao_InsereMascara_Universao(et_cep_usuario, ToolBox_Mascaras.CONSTANTE_MASCARA_FORMAT_CEP));
        et_cep_usuario.addTextChangedListener(textCep);

        btn_atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Funcao_Atualizar();
            }
        });

        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        img_usuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean permissao = ToolBox_Permissao.Funcao_Verifica_Permissao(activity, ToolBox_Permissao.VERIFICA_CAMERA);

                if (permissao == true) {
                    toolbox_Modal.ChamaModal(activity, toolbox_Modal.Uri_Camera);
                }
            }
        });


    }


    private void Funcao_PreEditar_Usuario() {
        Dados_Ws(4, "", null, activity);
    }

    private void Funcao_Atualizar() {

        boolean conn = ToolBox_Verivifacao_Sistema.Funcao_Status_Conexao(context); // função para checar a conexao antes
        if (conn) {

//            String usuarios = et_nome_usuario.getText().toString().trim();
            boolean senha = false;
            if (intentType == 1) {
                senha = true;
            } else {
                senha = tValidePass(et_senha_usuario, et_senha_conficacao_usuario);
            }


            boolean[] Array_De_Obrigatorios = new boolean[]{
                    ToolBox_Verivifacao_Campos.isFieldObrigatory(activity, et_cidade_usuario),
                    ToolBox_Verivifacao_Campos.isFieldObrigatory(activity, et_bairro_usuario),
                    ToolBox_Verivifacao_Campos.isFieldObrigatory(activity, et_endereco_usuario),
                    ToolBox_Verivifacao_Campos.isFieldObrigatory(activity, et_numero_usuario),
                    ToolBox_Verivifacao_Campos.isFieldObrigatory(activity, et_cep_usuario),
                    ToolBox_Verivifacao_Campos.isFieldObrigatory(activity, et_celular_usuario),
                    ToolBox_Verivifacao_Campos.isFieldObrigatory(activity, et_cpf_usuario),
                    ToolBox_Verivifacao_Campos.isEmailValide(activity, et_email_usuario),
                    ToolBox_Verivifacao_Campos.isFieldObrigatory(activity, et_email_usuario),
                    ToolBox_Verivifacao_Campos.isFieldObrigatory(activity, et_nome_usuario),
                    senha
            };

//            Array_De_Obrigatorios[0] = ToolBox_Verivifacao_Campos.Funcao_Campo_Obrigatorio(context, et_nome_usuario, tGetText(et_nome_usuario));

            //verifica ArrayObrigarotios
            boolean validação_final = ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios);

            if (validação_final) {

                String Dados_Para_URL_GET = "";
                Map<String, String> Dados_Para_Parametros = new HashMap<>();

                if (forceDataComplete) {
                    Dados_Para_Parametros.put("id_usuario", String.valueOf(userID));
                }

                Dados_Para_Parametros.put("nome_usuario", tGetText(et_nome_usuario));
                Dados_Para_Parametros.put("sexo_usuario", tGetSpinnerPositionSex(sp_sexo));
                Dados_Para_Parametros.put("rg_usuario", "1");
//                Dados_Para_Parametros.put("rg_usuario", tGetText(et_rg_usuario));
                Dados_Para_Parametros.put("cpf_usuario", tGetText(et_cpf_usuario));
                Dados_Para_Parametros.put("celular_usuario", tGetText(et_celular_usuario));
                Dados_Para_Parametros.put("telefone_usuario", tGetText(et_telefone_usuario));
                Dados_Para_Parametros.put("cep_usuario", tGetText(et_cep_usuario));
                Dados_Para_Parametros.put("logradouro_usuario", tGetText(et_endereco_usuario));
                Dados_Para_Parametros.put("bairro_usuario", tGetText(et_bairro_usuario));
                Dados_Para_Parametros.put("cidade_usuario", tGetText(et_cidade_usuario));
                Dados_Para_Parametros.put("estado_usuario", tGetSpinnerPosition(sp_estado));
                Dados_Para_Parametros.put("numero_usuario", tGetText(et_numero_usuario));
                Dados_Para_Parametros.put("complemento_usuario", tGetText(et_complemento_usuario));
                Dados_Para_Parametros.put("email_usuario", tGetText(et_email_usuario));

                if (intentType == 2) {
                    Dados_Para_Parametros.put("senha_usuario", tGetTextPass(et_senha_usuario));
                }

                Dados_Ws(5, Dados_Para_URL_GET, Dados_Para_Parametros, activity);

            } else {
                MsgUtil.Funcao_TOAST(activity, "Este Campo Precisa ser Preenchido");
            }


        } else {
            MsgUtil.Funcao_TOAST(activity, context.getResources().getString(R.string.msg_global_sem_conexao));
        }

    }

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    TextWatcher textCep = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (i1 == 9) {

                funcaoCep();

            } else {

                iniSearshCep = true;
            }

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };


    private void funcaoCep() {

        if (iniSearshCep) {
            iniSearshCep = false;

            Dados_Ws(1001, "cep=" + tGetText(et_cep_usuario).replace("-", ""), null, activity);
        }

    }

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    @Override
    public void onBackPressed() {
//        ToolBox_Chama_Activity.Funcao_Chama_TelaPrincipal(activity);
        finish();
        super.onBackPressed();
    }

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

//        Log.w("Entrei", "ActivityResult");

        if (resultCode == RESULT_OK && reqCode == toolbox_Modal.REQUEST_LOAD_IMG) {
            Log.w("Entrei", "Galeria");

            Uri imageUri = data.getData();
//                Bitmap selectedImage = ToolBox_Imagem.Funcao_decodeUri(activity, imageUri);
            Boolean imagem_valida = ToolBox_Imagem.Funcao_Verifica_Tamanho_IMG(context, imageUri);
            if (imagem_valida) {

                if (OPEN_CROPVIEW) {
                    toolbox_Modal.ChamaCropView(activity, imageUri);
                } else {
                    toolbox_Modal.Carrega_Imagem_Selecionada(activity, imageUri, 1);
                }

            }


        } else if (resultCode == RESULT_OK && reqCode == toolbox_Modal.REQUEST_IMAGE_CAPTURE) {
            Log.w("Entrei", "Camera");

//            Uri uri = dados.getDados();
//            Bundle extras = dados.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("dados");
//
//            final String nome_foto = "principal.jpg";
//            final String imagem = _ToolBox_Config_Inicial.Constante_Pasta_Projeto_Imagens + nome_foto;
//            Uri imageUri_Cam = Novo_Uri_Imagem(context, Bitmap.CompressFormat.JPEG);
//            Bitmap bitmap = BitmapFactory.decodeFile(imagem);

            if (OPEN_CROPVIEW) {
                toolbox_Modal.ChamaCropView(activity, toolbox_Modal.Uri_Camera);
            } else {
                toolbox_Modal.Carrega_Imagem_Selecionada(activity, toolbox_Modal.Uri_Camera, 1);
            }

        } else {
            MsgUtil.Funcao_MSG(activity, "Foto não Capturada.");
        }
        System.gc();
    }

    //FUNCAO RESPONSAVEL POR REDIMENCIONAR A IMAGEM ANTES DE ADICIONAR A TELA
    //APOS REDIMENCIONAR , ESTA FUNÇÃO ADICIONA A IMAGEM A TELA

    public static class Redimencionar_Imagem implements Runnable {
        private Handler mHandler = new Handler(Looper.getMainLooper());
        Context context;
        Uri uri;
        ImageView imageView;
        int width;

        public Redimencionar_Imagem(Context context, Uri uri, int width) {
            this.context = context;
            this.uri = uri;
            this.width = width;

            Glide.with(activity).asBitmap()
                    .load(uri)
                    .apply(new RequestOptions().centerCrop())
                    .into(img_usuario);
        }

        @Override
        public void run() {
            final int raw_exifRotation = Utils.getExifOrientation(context, uri);

            final int exifRotation = Utils.getRotateDegreeFromOrientation(raw_exifRotation);

            ToolBox_SharedPrefs toolBox_sharedPrefs = new ToolBox_SharedPrefs(activity);
            String path = toolBox_sharedPrefs.getPhoto_path();
            Log.w("Path", path);

//            final int ori = toolbox_Modal.Funcao_Retorna_Orientacao_Imagem(context, uri, path);

            int maxSize = Utils.getMaxSize();
            int requestSize = Math.min(width, maxSize);
            try {
                final Bitmap raw_sampledBitmap = Utils.decodeSampledBitmapFromUri(context, uri, requestSize);

//                final Bitmap sampledBitmap = toolbox_Modal.Funcao_Rotaciona_Imagem(raw_sampledBitmap, ori);

                //AQUI CHAMO A FUNÇÃO PARA GUARDAR A IMAGEM EM UM CHAR64 OU DECODIFICAR ELA
                //Encode_Decode_Imagem64(sampledBitmap);

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {

//                        Bitmap bitmap = null; // nao carrega a imagem - le informacoes da imagem options
//                        try {
//                            bitmap = ToolBox_Imagem.Funcao_decodeUri(activity, uri);
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        }


//                        try {
//                            FutureTarget<Bitmap> futureTarget =
//                                    Glide.with(context)
//                                            .asBitmap()
//                                            .load(uri)
//                                            .submit(400, 400);
//
//                            Bitmap bitmap = futureTarget.get();

                        fotoUser = ToolBox_Imagem.Funcao_Imagem_encodeToBase64(raw_sampledBitmap);

                        Map<String, String> map = new ArrayMap<>();
                        map.put("foto", fotoUser);

                        Dados_Ws(16, "", map, activity);


//                        } catch (ExecutionException e) {
//
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        img_usuario.setImageMatrix(Utils.getMatrixFromExifOrientation(exifRotation));
//                        img_usuario.setImageBitmap(sampledBitmap);

//                        Picasso.get().load(uri)
////                                .placeholder(R.drawable.images).error(R.drawable.ic_launcher)
//                                .into(img_usuario);

//                        Glide
//                                .with(activity)
//                                .load(raw_sampledBitmap)
//                                .apply(
//                                        new RequestOptions().dontAnimate()
//                                )
//                                .into(img_usuario);
//                                .get();

//                        Glide.with(activity).load(sampledBitmap).into(img_usuario);
//
//                        Picasso.with(this).load(uri).into(imageView);

                    }
                });
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    public static void setImgUser() {

        if (forceDataComplete) {
            ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
            String imgBase64 = prefs.getImgUser();
            if (!imgBase64.equals("") || !imgBase64.equals("null")) {
                Bitmap bitmap = ToolBox_Imagem.Funcao_Imagem_decodeBase64(imgBase64);

                Glide.with(activity).asBitmap().load(bitmap).into(img_usuario);
            }
        } else {

            prefs = new ToolBox_SharedPrefs_Usuario(context);

            if (!prefs.getURL_IMAGE_FACEBOOK().isEmpty()) {
                Glide.with(activity).asBitmap()
                        .load(prefs.getURL_IMAGE_FACEBOOK())
                        .apply(new RequestOptions().centerCrop())
                        .into(img_usuario);
            } else {
                Glide.with(activity).asBitmap().load(Conexao_Constantes.URL_WS_FOTO_PERFIL + userID + ".png").into(img_usuario);
            }
        }
    }

}
