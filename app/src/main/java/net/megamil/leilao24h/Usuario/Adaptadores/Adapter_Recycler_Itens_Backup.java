package net.megamil.leilao24h.Usuario.Adaptadores;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.Leilao;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Data_e_Hora;

import java.util.List;

/**
 * Created by zhang on 2016.08.07.
 */
public class Adapter_Recycler_Itens_Backup extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static Activity activity; // Context
    private int resource; // Layout.xml da celula
    private static List<HMAux> dados; // List Dados referenciado
    private LayoutInflater mInflater;
    private HMAux hmAux = new HMAux(); // HashMap Referencia dos dados em memoria


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     *
     *  Adapter com a referencia do layout enviada na chamada do adapter
     *
     */
//    public Adapter_Recycler_Itens(Activity activity, int resource, List<HMAux> dados) {
//        this.activity = activity;
//        this.resource = resource;
//        this.dados = dados;
//        this.mInflater = LayoutInflater.from(activity);
//    }
//
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            View view = mInflater.inflate(resource, parent, false);
//            return new RecyclerViewHolder(view);
//    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     *
     *  Adapter com a referencia do layout Aqui
     *
     */
    public Adapter_Recycler_Itens_Backup(Activity activity, List<HMAux> dados) {
        this.activity = activity;
        this.dados = dados;
        this.mInflater = LayoutInflater.from(activity);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.celula_leilao, parent, false);
        return new RecyclerViewHolder(view);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RecyclerViewHolder) {
            final RecyclerViewHolder recyclerViewHolder = (RecyclerViewHolder) holder;

            hmAux = dados.get(position); // carrega os dados da celula

            //Cria uma animação para a entrada da celula , mView é a celula
//            Animation animation = AnimationUtils.loadAnimation(context, R.anim.anim_recycler_item_show);
//            recyclerViewHolder.mView.startAnimation(animation);

//            AlphaAnimation aa1 = new AlphaAnimation(1.0f, 0.1f);
//            aa1.setDuration(400);
//            recyclerViewHolder.rela_round.startAnimation(aa1);

//            recyclerViewHolder.pos_item.setText(hmAux.get(HMAux.ID));
            recyclerViewHolder.name_item.setText(hmAux.get(HMAux.NOME_PRODUTO));
            recyclerViewHolder.desc_item.setText(hmAux.get(HMAux.DESCRICAO_PRODUTO));
            recyclerViewHolder.ingressos.setText(hmAux.get(HMAux.INGRESSOS));
            recyclerViewHolder.data_inicio.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(hmAux.get(HMAux.DATA_INICIO), false, false));
//            recyclerViewHolder.data_inicio.setText(hmAux.get(HMAux.DATA_INICIO));
            String id_leilao = hmAux.get(HMAux.ID_LEILAO);
            Glide.with(activity).asBitmap()
                    .load(Conexao_Constantes.Base_URL_WS + "/leilao/upload/produtos/" + id_leilao + "/" + id_leilao + "_0.png")
//                    .load(Conexao_Constantes.Base_URL_WS + hmAux.get(HMAux.IMAGENS))
                    .apply(new RequestOptions()
                            .centerCrop()
                    )
                    .into(recyclerViewHolder.img_item);
            Log.w("URL", hmAux.get(HMAux.IMAGENS));
//            final int favorito = Integer.parseInt(hmAux.get(HMAux.ITEM_FAVORITO));
//            Favorito(recyclerViewHolder.btn_item, favorito);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /*
             *
             *  Caso tenha click em determinados lugares da celula, pode ser chamado aqui
             *  Neste caso , o mView esta referenciando a celula completa
             *
             */

            AlphaAnimation aa = new AlphaAnimation(0.1f, 1.0f);
            aa.setDuration(400);

            recyclerViewHolder.rl_img.startAnimation(aa);
            recyclerViewHolder.mView.setOnTouchListener(touchListener);
            recyclerViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    HMAux hmAux_click = dados.get(position);

                    int pos = position;
//                    int i = Integer.parseInt(hmAux_click.get(HMAux.ID));
//                    Toast.makeText(activity, "Items da POS = " + pos + " " + hmAux_click.get(HMAux.NOME_ITEM), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(activity, Leilao.class);

                    intent.putExtra(HMAux.ID_LEILAO, hmAux_click.get(HMAux.ID_LEILAO));
                    intent.putExtra(HMAux.ID_PROCUTO, hmAux_click.get(HMAux.ID_PROCUTO));
                    intent.putExtra(HMAux.NOME_PRODUTO, hmAux_click.get(HMAux.NOME_PRODUTO));
                    intent.putExtra(HMAux.DESCRICAO_PRODUTO, hmAux_click.get(HMAux.DESCRICAO_PRODUTO));
                    intent.putExtra(HMAux.VALOR_MINIMO, hmAux_click.get(HMAux.VALOR_MINIMO));
                    intent.putExtra(HMAux.LANCE_MINIMO, hmAux_click.get(HMAux.LANCE_MINIMO));
                    intent.putExtra(HMAux.INGRESSOS, hmAux_click.get(HMAux.INGRESSOS));
                    intent.putExtra(HMAux.DATA_INICIO, hmAux_click.get(HMAux.DATA_INICIO));
                    intent.putExtra(HMAux.DATA_FIM_PREVISTO, hmAux_click.get(HMAux.DATA_FIM_PREVISTO));
                    intent.putExtra(HMAux.FAVORITO, hmAux_click.get(HMAux.FAVORITO));
                    intent.putExtra(HMAux.IMAGENS_ARRAY, hmAux_click.get(HMAux.IMAGENS_ARRAY));
                    intent.putExtra(HMAux.ID_POSITION, pos);

                    activity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation
                            (activity, recyclerViewHolder.rl_img, "shareView").toBundle());


                }
            });

            recyclerViewHolder.btn_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Add_Favorito(recyclerViewHolder.btn_item, position);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return dados.size();
    }

    private static ClickListener clickListener;


    private class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private View mView;
        // view
        private ImageView img_item;
        private TextView pos_item;
        private TextView name_item;
        private TextView desc_item;
        private TextView ingressos;
        private TextView data_inicio;
        private ImageView btn_item;
        private RelativeLayout rl_img;

        private RecyclerViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            mView = itemView;
            // inicia findViewById's

            img_item = itemView.findViewById(R.id.img_item);
//            pos_item = itemView.findViewById(R.id.pos_item);
            name_item = itemView.findViewById(R.id.name_item);

            desc_item = itemView.findViewById(R.id.desc_item);
            ingressos = itemView.findViewById(R.id.ingressos);
            data_inicio = itemView.findViewById(R.id.data_inicio);

            btn_item = itemView.findViewById(R.id.btn_favorito);
            rl_img = itemView.findViewById(R.id.rl_img);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return true;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        Adapter_Recycler_Itens_Backup.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

        void onItemLongClick(int position, View v);
    }

    private static void Favorito(ImageView btn_item, int i) {

        AlphaAnimation alphaAnimationShowIcon = new AlphaAnimation(0.2f, 1.0f);
        alphaAnimationShowIcon.setDuration(500);

        if (i == 1) {
            btn_item.setImageResource(R.drawable.ic_favorite_black_24dp);
            btn_item.startAnimation(alphaAnimationShowIcon);
        } else {
            btn_item.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            btn_item.startAnimation(alphaAnimationShowIcon);
        }

    }

    private static void Add_Favorito(ImageView btn_item, int pos) {

        HMAux hmAux = dados.get(pos);
        int i = Integer.parseInt(hmAux.get(HMAux.ITEM_FAVORITO));

        AlphaAnimation alphaAnimationShowIcon = new AlphaAnimation(0.2f, 1.0f);
        alphaAnimationShowIcon.setDuration(500);

        if (i == 1) {
            btn_item.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            btn_item.startAnimation(alphaAnimationShowIcon);
            hmAux.put(HMAux.ITEM_FAVORITO, "0");

            Toast.makeText(activity, "Retirado dos Favoritos", Toast.LENGTH_SHORT).show();

        } else {
            btn_item.setImageResource(R.drawable.ic_favorite_black_24dp);
            btn_item.startAnimation(alphaAnimationShowIcon);
            hmAux.put(HMAux.ITEM_FAVORITO, "1");

            Toast.makeText(activity, "Favorito", Toast.LENGTH_SHORT).show();

        }
    }

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator upAnim = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    upAnim.setDuration(200);
                    upAnim.setInterpolator(new DecelerateInterpolator());
                    upAnim.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator downAnim = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    downAnim.setDuration(200);
                    downAnim.setInterpolator(new AccelerateInterpolator());
                    downAnim.start();
                    break;
            }
            return false;
        }
    };
}
