package net.megamil.leilao24h.Usuario.Fragmentos.Perfil;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Arremates;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Lances;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Pagamento;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico.Activity_Recompensas;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


public class Historico extends AppCompatActivity {

    private Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;
    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    private ImageButton btnBack;
    private Button btnLances;
    private Button btnArremates;
    private Button btnRecompensas;
    private Button btnPagamento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico);

        //Var padroes
        context = getBaseContext();
        context_activity = Historico.this;
        activity = Historico.this;
        window = activity.getWindow();

        initVars();
        initActions();

    }

    private void initVars() {

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */

        btnBack = findViewById(R.id.btnBack);
        btnLances = findViewById(R.id.btn_lances);
        btnArremates = findViewById(R.id.btn_arremates);
        btnRecompensas = findViewById(R.id.btn_recompensas);
        btnPagamento = findViewById(R.id.btn_pagamento);
    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */

        btnBack.setOnClickListener(onClickBtnBack);
        btnLances.setOnClickListener(onClickLances);
        btnArremates.setOnClickListener(onClickArremate);
        btnRecompensas.setOnClickListener(onClickRecompensas);
        btnPagamento.setOnClickListener(onClickPagamento);

    }

    View.OnClickListener onClickBtnBack = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onBackPressed();
        }
    };

    View.OnClickListener onClickLances = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, Activity_Lances.class , null , false , false, "");
        }
    };

    View.OnClickListener onClickArremate = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, Activity_Arremates.class , null , false , false, "");
        }
    };

    View.OnClickListener onClickRecompensas = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, Activity_Recompensas.class , null , false , false, "");
        }
    };

    View.OnClickListener onClickPagamento  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, Activity_Pagamento.class , null , false , false, "");
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
