package net.megamil.leilao24h.Usuario.Adaptadores;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Categorias;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Favoritos;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.Leilao;
import net.megamil.leilao24h.Usuario.Pagina_Busca;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Data_e_Hora;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Imagem;

import java.util.List;

public class Adapter_Recycler_Itens_Vertical extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //First
    private Activity activity;
    private int layout;
    private List<HMAux> dataBase;
    private HMAux hmAux = new HMAux();
    private LayoutInflater layoutInflater;
    private int viewPosition;

    //Second - View's Types
    private int NORMAL_TYPE = 0;
    private int FOOTER_TYPE = 1;
    private int END_TYPE = 2;
    private int ADS_TYPE = 3;

    /**
     * init Constructor with HMAux's List and Resorce
     *
     * @param activity activity pai
     * @param dataBase List de HMAux
     * @param layout   layout chamado de fora do metodo
     */
    public Adapter_Recycler_Itens_Vertical(Activity activity, List<HMAux> dataBase, int layout) {
        this.activity = activity;
        this.dataBase = dataBase;
        this.layout = layout;
        this.layoutInflater = LayoutInflater.from(activity);
    }

    /**
     * init Constructor with HMAux's List
     *
     * @param activity activity pai
     * @param dataBase HMAux's list
     */
    public Adapter_Recycler_Itens_Vertical(Activity activity, List<HMAux> dataBase) {
        this.activity = activity;
        this.dataBase = dataBase;
        this.layoutInflater = LayoutInflater.from(activity);
    }

    /**
     * @param list HMAux's list
     */
    public void addList(List<HMAux> list) {
        dataBase.addAll(list);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * @param hmAux add dados raw hmaux
     */
    public void addItem(HMAux hmAux) {
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    /**
     * add raw item dados
     *
     * @param position int position
     * @param hmAux    raw dados
     */
    public void addItemAtPosition(int position, HMAux hmAux) {
        dataBase.add(position, hmAux);
        notifyItemInserted(position);
    }

    /**
     * remove at first position
     */
    public void removeFirstItem() {
        dataBase.remove(0);
        notifyItemRemoved(0);
    }

    /**
     * remove at last position
     */
    public void removeLastItem() {
        dataBase.remove(dataBase.size() - 1);
        notifyItemRemoved(dataBase.size());
    }

    /**
     * remove item at positon with parameter
     *
     * @param position
     */
    public void removeItemAtPosition(int position) {
        dataBase.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * remove all items at RecyclerView
     */
    public void clear() {
        final int size = dataBase.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                dataBase.remove(0);
            }
            notifyItemRemoved(0);
        }
    }

    /**
     * add FooterView at RecyclerView
     */
    public void addFooterView() {
        hmAux.put("type", String.valueOf(FOOTER_TYPE));
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    public void addEndView() {
        hmAux.put("type", String.valueOf(END_TYPE));
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }

    public void addAdsView() {
        hmAux.put("type", String.valueOf(ADS_TYPE));
        dataBase.add(hmAux);
        notifyItemInserted(dataBase.size() - 1);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator animatorUP = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    animatorUP.setDuration(200);
                    animatorUP.setInterpolator(new DecelerateInterpolator());
                    animatorUP.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator animatorDOWN = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    animatorDOWN.setDuration(200);
                    animatorDOWN.setInterpolator(new AccelerateInterpolator());
                    animatorDOWN.start();
                    break;
            }
            return false;
        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * in class implements onMoveAndSwipedListener to use methods at down
     */

//    @Override
//    public boolean onItemMove(int fromPosition, int toPosition) {
//        Collections.swap(mItems, fromPosition, toPosition);
//        notifyItemMoved(fromPosition, toPosition);
//        return true;
//    }
//
//    @Override
//    public void onItemDismiss(final int position) {
//        mItems.remove(position);
//        notifyItemRemoved(position);
//
//        Snackbar.make(parentView, "Item Deleted", Snackbar.LENGTH_SHORT)
//                .setAction("Undo", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        addItem(position, mItems.get(position));
//                    }
//                }).show();
//    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == FOOTER_TYPE) {
            View view = layoutInflater.inflate(R.layout.celula_recycler_footer_vertical, parent, false);
            return new FooterViewHolder(view);
        } else if (viewType == END_TYPE) {
            View view = layoutInflater.inflate(R.layout.celula_recycler_end_vertical, parent, false);
            return new EndViewHolder(view);
        } else if (viewType == ADS_TYPE) {
            View view = layoutInflater.inflate(R.layout.celula_leilao_vertical, parent, false);
            return new AdsViewHolder(view);
        } else {
            View view = layoutInflater.inflate(R.layout.celula_leilao_vertical, parent, false);
            return new RecyclerViewHolder(view);
        }

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        hmAux = dataBase.get(position);
        if (holder instanceof FooterViewHolder) {
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setOnClickListener(FOOTER_TYPE_ViewClick);
            viewHolder.view.setTag(position);
            initActions_FooterView(viewHolder, hmAux);

        } else if (holder instanceof EndViewHolder) {
            EndViewHolder viewHolder = (EndViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setOnClickListener(END_TYPE_ViewClick);
            viewHolder.view.setTag(position);
            initActions_EndView(viewHolder, hmAux);

        } else if (holder instanceof AdsViewHolder) {
            AdsViewHolder viewHolder = (AdsViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setOnClickListener(ADS_TYPE_ViewClick);
            viewHolder.view.setTag(position);
            initActions_AdsView(viewHolder, hmAux);

        } else {
            RecyclerViewHolder viewHolder = (RecyclerViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            initActions_NormalView(viewHolder, hmAux);
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param position
     * @return ViewType for load Resorce
     */
    @Override
    public int getItemViewType(int position) {
        HMAux s = dataBase.get(position);
        String typeView = s.get("type");

        if (s.containsKey("type")) {

            if (typeView.equals(String.valueOf(FOOTER_TYPE))) {
                return FOOTER_TYPE;
            } else if (typeView.equals(String.valueOf(END_TYPE))) {
                return END_TYPE;
            } else if (typeView.equals(String.valueOf(ADS_TYPE))) {
                return ADS_TYPE;
            } else {
                return NORMAL_TYPE;
            }

        } else {
            return NORMAL_TYPE;
        }

    }

    /**
     * @return HMAux's List size
     */
    @Override
    public int getItemCount() {
        return dataBase.size();
    }

    /**
     * ViewHolder FOOTER_TYPE
     */
    private class FooterViewHolder extends RecyclerView.ViewHolder {
        private View view;

        public FooterViewHolder(View itemView) {
            super(itemView);
            view = itemView;
        }
    }

    /**
     * @param viewHolder
     * @param hmAux
     */
    private void initActions_FooterView(FooterViewHolder viewHolder, HMAux hmAux) {

    }

    private View.OnClickListener FOOTER_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ViewHolder END_VIEW
     */
    private class EndViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private Button buttonReload;

        public EndViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            buttonReload = view.findViewById(R.id.btnRetry);
            buttonReload.setOnClickListener(END_TYPE_ViewClick);
        }
    }

    private void initActions_EndView(EndViewHolder viewHolder, HMAux hmAux) {

    }

    private View.OnClickListener END_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            removeLastItem();
            addFooterView();

            if (layout == 1) {
                Fragment_Categorias.initWSConection(Fragment_Categorias.pageNumber);
            } else if (layout == 2) {
                Fragment_Favoritos.initWSConection(Fragment_Favoritos.pageNumber);
            } else if (layout == 3) {
                Pagina_Busca.initWSConection(Pagina_Busca.textSearsh, Pagina_Busca.pageNumber);
            }

        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ViewHolder ADS_VIEW
     */
    private class AdsViewHolder extends RecyclerView.ViewHolder {
        private View view;

        public AdsViewHolder(View itemView) {
            super(itemView);
            view = itemView;
        }
    }

    private void initActions_AdsView(AdsViewHolder viewHolder, HMAux hmAux) {

    }

    private View.OnClickListener ADS_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * ViewHolder NORMAL_TYPE
     */
    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private View view;

        // view
        private ImageView img_item;
        private TextView pos_item;
        private TextView name_item;
        private TextView desc_item;
        private TextView ingressos;
        private TextView data_inicio;
        private ImageView btn_item;
        private RelativeLayout rl_img;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            //findViewById's

            img_item = itemView.findViewById(R.id.img_item);
            name_item = itemView.findViewById(R.id.name_item);
            desc_item = itemView.findViewById(R.id.desc_item);
            ingressos = itemView.findViewById(R.id.ingressos);
            data_inicio = itemView.findViewById(R.id.data_inicio);
            btn_item = itemView.findViewById(R.id.btn_favorito);
            rl_img = itemView.findViewById(R.id.rl_img);

            /**
             * Events at View
             */
            view.setOnClickListener(NORMAL_TYPE_ViewClick);
        }
    }

    private void initActions_NormalView(final RecyclerViewHolder viewHolder, HMAux hmAux) {


        //Cria uma animação para a entrada da celula , mView é a celula
//            Animation animation = AnimationUtils.loadAnimation(context, R.anim.anim_recycler_item_show);
//            recyclerViewHolder.mView.startAnimation(animation);

//            AlphaAnimation aa1 = new AlphaAnimation(1.0f, 0.1f);
//            aa1.setDuration(400);
//            recyclerViewHolder.rela_round.startAnimation(aa1);

//            recyclerViewHolder.pos_item.setText(hmAux.get(HMAux.ID));
        viewHolder.name_item.setText(viewHolder.name_item.getText()+this.hmAux.get(HMAux.NOME_PRODUTO));
        viewHolder.desc_item.setText(viewHolder.desc_item.getText()+this.hmAux.get(HMAux.DESCRICAO_PRODUTO));
        viewHolder.ingressos.setText(viewHolder.ingressos.getText()+this.hmAux.get(HMAux.PRODUTO_RECOMPENSA_CUSTO));
        viewHolder.data_inicio.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(this.hmAux.get(HMAux.DATA_INICIO), false, false));
//            recyclerViewHolder.data_inicio.setText(hmAux.get(HMAux.DATA_INICIO));
        String id_leilao = this.hmAux.get(HMAux.ID_LEILAO);
        String url = Conexao_Constantes.Base_URL_WS + hmAux.get(HMAux.IMAGENS);
        ToolBox_Imagem.initGlide(activity, url, viewHolder.img_item);
//        Glide.with(activity).asBitmap()
//                .load(Conexao_Constantes.Base_URL_WS + "/leilao/upload/produtos/" + id_leilao + "/" + id_leilao + "_0.png")
////                    .load(Conexao_Constantes.Base_URL_WS + hmAux.get(HMAux.IMAGENS))
//                .apply(new RequestOptions()
//                        .centerCrop()
//                )
//                .into(viewHolder.img_item);
//        Log.w("URL", this.hmAux.get(HMAux.IMAGENS));
//            final int favorito = Integer.parseInt(hmAux.get(HMAux.ITEM_FAVORITO));
//            Favorito(recyclerViewHolder.btn_item, favorito);
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
         *
         *  Caso tenha click em determinados lugares da celula, pode ser chamado aqui
         *  Neste caso , o mView esta referenciando a celula completa
         *
         */

        AlphaAnimation aa = new AlphaAnimation(0.1f, 1.0f);
        aa.setDuration(400);

        viewHolder.rl_img.startAnimation(aa);

    }

    private View.OnClickListener NORMAL_TYPE_ViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            viewPosition = (int) view.getTag();
//            MsgUtil.Funcao_MSG(activity, String.valueOf(viewPosition));

            HMAux hmAux_click = dataBase.get(viewPosition);

            Intent intent = new Intent(activity, Leilao.class);

            intent.putExtra(HMAux.ID_LEILAO, hmAux_click.get(HMAux.ID_LEILAO));
            intent.putExtra(HMAux.ID_PROCUTO, hmAux_click.get(HMAux.ID_PROCUTO));
            intent.putExtra(HMAux.NOME_PRODUTO, hmAux_click.get(HMAux.NOME_PRODUTO));
            intent.putExtra(HMAux.DESCRICAO_PRODUTO, hmAux_click.get(HMAux.DESCRICAO_PRODUTO));
            intent.putExtra(HMAux.VALOR_MINIMO, hmAux_click.get(HMAux.VALOR_MINIMO));
            intent.putExtra(HMAux.LANCE_MINIMO, hmAux_click.get(HMAux.LANCE_MINIMO));
            intent.putExtra(HMAux.INGRESSOS, hmAux_click.get(HMAux.INGRESSOS));
            intent.putExtra(HMAux.DATA_INICIO, hmAux_click.get(HMAux.DATA_INICIO));
            intent.putExtra(HMAux.DATA_FIM_PREVISTO, hmAux_click.get(HMAux.DATA_FIM_PREVISTO));
            intent.putExtra(HMAux.FAVORITO, hmAux_click.get(HMAux.FAVORITO));
            intent.putExtra(HMAux.AVALIACAO, hmAux_click.get(HMAux.AVALIACAO));
            intent.putExtra(HMAux.NOME_USUARIO, hmAux_click.get(HMAux.NOME_USUARIO));
            intent.putExtra(HMAux.PRODUTO_USADO, hmAux_click.get(HMAux.PRODUTO_USADO));
            intent.putExtra(HMAux.IMAGENS_ARRAY, hmAux_click.get(HMAux.IMAGENS_ARRAY));
            intent.putExtra(HMAux.ID_POSITION, viewPosition);
            activity.startActivity(intent);

        }
    };


}
