package net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Historico;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Adaptadores.adapterHistoricoPagamento;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RecyclerView;

import java.util.ArrayList;
import java.util.List;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


public class Activity_Pagamento extends AppCompatActivity {


    private static Activity activity;
    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    private ImageButton btn_voltar;
    private TextView tv_titulo;
    public static List<HMAux> arrayList_dados = new ArrayList<>();
    private static RecyclerView lv_itens;
    private static adapterHistoricoPagamento adapter;
    private static LinearLayoutManager linearLayoutManager;

    //Scroll Listenner
    public static List<HMAux> arrayList_dados_NextPage = new ArrayList<>();
    public static int sizePerPage = 2;
    public static int pageNumber = 1;
    private static boolean loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_recyclerview);
        pageNumber = 1;
        //Var padroes
        activity = Activity_Pagamento.this;
        arrayList_dados.clear();

        initVars();
        initActions();
        initWSConection();

    }

    private void initVars() {

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */
        lv_itens = findViewById(R.id.lv_itens);
        btn_voltar = findViewById(R.id.btn_voltar);
        tv_titulo = findViewById(R.id.tv_toolbar);
    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */

        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_titulo.setText("Histórico de Pagamentos");

        ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
    }

    public static void Set_Dados_Array() {

        linearLayoutManager = ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
        lv_itens.setLayoutManager(linearLayoutManager);
        adapter = new adapterHistoricoPagamento(
                activity,
                arrayList_dados
        );
        lv_itens.setAdapter(adapter);
        if (arrayList_dados.size() < sizePerPage) {
            adapter.addEndView();
            loading = true;
        } else {
            adapter.addFooterView();
        }

    }

    public static void initWSConection() {
        Dados_Ws(27, "", null, activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
