package net.megamil.leilao24h.Usuario.Adaptadores;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.Activity_Item;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;

import java.util.List;

/**
 * Created by John on 21/11/2017.
 */

public class Adapter_Leiloes_Favorito extends BaseAdapter {

    private Activity activity;
    private int resource;
    private List<HMAux> dados;
    private View mView;
    private HMAux hmAux = null;

    private LayoutInflater mInflater;

    private ImageView img;

    public interface IAdapterPosts {
        void onStatusChanged(String id, boolean status);
    }

    private IAdapterPosts delegate;

    public void setOnStatusChangedListener(IAdapterPosts delegate) {
        this.delegate = delegate;
    }

    public Adapter_Leiloes_Favorito(Activity activity, int resource, List<HMAux> dados) {
        this.activity = activity;
        this.resource = resource;
        this.dados = dados;
        this.mInflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return dados.size();
    }

    @Override
    public Object getItem(int position) {
        return dados.get(position);
    }

    @Override
    public long getItemId(int position) {
        HMAux hmAux = dados.get(position);

        return Long.parseLong(hmAux.get(HMAux.ID));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(resource, parent, false);
        }

        // Acessar os dados de uma posicao especirfica (position)
        hmAux = dados.get(position);

        // Acessar a Layout Celula

        TextView tv_nome = (TextView)
                convertView.findViewById(R.id.tv_nome);

        TextView tv_descricao = (TextView)
                convertView.findViewById(R.id.tv_descricao);

        TextView tv_categorias = (TextView)
                convertView.findViewById(R.id.tv_categorias);

        img = (ImageView)
                convertView.findViewById(R.id.iv_item);

        // Juntar Dados / Layout
        tv_nome.setText(hmAux.get(HMAux.NOME_PRODUTO));
        tv_descricao.setText(hmAux.get(HMAux.DESCRICAO_PRODUTO));
        tv_categorias.setText(hmAux.get(HMAux.NOME_CATEGORIA));

        Glide.with(activity).asBitmap()
                .load("http://31.220.57.245" + hmAux.get(HMAux.IMAGENS))
                .apply(new RequestOptions()
                        .centerCrop()
                )
                .into(img);

        mView = convertView;
        mView.setOnTouchListener(touchListener);
//        mView.setOnClickListener(clickListener);
//        mView.setOnLongClickListener(onLongClickListener);

//        mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                String id_ = hmAux.get(HMAux.ID_PROCUTO);
////                Log.w("lv", "click " + id_);
//
//                Intent intent = new Intent(activity, Leilao.class);
//
//                intent.putExtra(HMAux.ID_POSITION, position);
//                intent.putExtra(HMAux.ID_LEILAO, hmAux.get(HMAux.ID_LEILAO));
//                intent.putExtra(HMAux.ID_PROCUTO, hmAux.get(HMAux.ID_PROCUTO));
//
//                intent.putExtra(HMAux.NOME_PRODUTO, hmAux.get(HMAux.NOME_PRODUTO));
//                intent.putExtra(HMAux.DESCRICAO_PRODUTO, hmAux.get(HMAux.DESCRICAO_PRODUTO));
//                intent.putExtra(HMAux.VALOR_MINIMO, hmAux.get(HMAux.VALOR_MINIMO));
//                intent.putExtra(HMAux.LANCE_MINIMO, hmAux.get(HMAux.LANCE_MINIMO));
//                intent.putExtra(HMAux.INGRESSOS, hmAux.get(HMAux.INGRESSOS));
//                intent.putExtra(HMAux.DATA_INICIO, hmAux.get(HMAux.DATA_INICIO));
//                intent.putExtra(HMAux.DATA_FIM_PREVISTO, hmAux.get(HMAux.DATA_FIM_PREVISTO));
//                intent.putExtra(HMAux.FAVORITO, hmAux.get(HMAux.FAVORITO));
//                intent.putExtra(HMAux.IMAGENS_ARRAY, hmAux.get(HMAux.IMAGENS_ARRAY));
//
//                ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");
//
//            }
//        });

        return convertView;
    }

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator upAnim = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    upAnim.setDuration(200);
                    upAnim.setInterpolator(new DecelerateInterpolator());
                    upAnim.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator downAnim = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    downAnim.setDuration(200);
                    downAnim.setInterpolator(new AccelerateInterpolator());
                    downAnim.start();
                    break;
            }
            return false;
        }
    };

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

//            hmAux = dados.get(position);

            String id_ = hmAux.get(HMAux.ID_PROCUTO);
            Log.w("lv", "click " + id_);

            Intent intent = new Intent(activity, Activity_Item.class);
            intent.putExtra(HMAux.ID, id_);

//            ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");

            activity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation
                    (activity, img, "shareView").toBundle());
        }
    };

    View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            String id_ = hmAux.get(HMAux.ID_PROCUTO);

            MsgUtil.Funcao_MSG(activity, "Long " + id_);
            return true;
        }
    };


}
