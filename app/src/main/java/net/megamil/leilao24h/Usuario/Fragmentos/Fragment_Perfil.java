package net.megamil.leilao24h.Usuario.Fragmentos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Afiliados;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Ativar_Leilao;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Editar_Usuario;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Historico;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Recompensa;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_VersaoApp;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;

import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity.Funcao_Chama_Activity;

/**
 * Created by John on 09/12/2017.
 */

public class Fragment_Perfil extends Fragment {

    private Context context;
    private Context context_activity;
    private Activity activity;
    private Window window;

    private TextView tv_nome;
    private ConstraintLayout btn_criarLailao;

    private static LinearLayout ll_btn_editar_perfil;
    private static LinearLayout ll_btn_editar_senha;

    private static LinearLayout ll_btn_ativar_leilao;
    private static LinearLayout ll_btn_afiliados;
    private static LinearLayout ll_btn_recompensa;
    private static LinearLayout ll_btn_historico;

    private static LinearLayout ll_btn_sair;
    private static LinearLayout ll_btn_debug;

    private static LinearLayout ll_versao;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_activity_inicio_perfil, container, false);

        initVars(view);
        initActions();
        ToolBox_VersaoApp.setVersion(activity, ll_versao, ll_btn_debug);
        return view;
    }

    private void initVars(View view) {

        //Var padroes
        context = getActivity();
        context_activity = getActivity();
        activity = getActivity();
        window = getActivity().getWindow();

        tv_nome = view.findViewById(R.id.fragment_perfil_nome_usuario);
        btn_criarLailao = view.findViewById(R.id.btn_criar_leilao);

        ll_btn_editar_perfil = view.findViewById(R.id.btn_editar_perfil);
        ll_btn_editar_senha = view.findViewById(R.id.btn_editar_senha);

        ll_btn_ativar_leilao = view.findViewById(R.id.btn_ativar_leilao);
        ll_btn_afiliados = view.findViewById(R.id.btn_afiliados);
        ll_btn_recompensa = view.findViewById(R.id.btn_recompensa);
        ll_btn_historico = view.findViewById(R.id.btn_historico);

        ll_btn_sair = view.findViewById(R.id.btn_sair);
        ll_btn_debug = view.findViewById(R.id.btn_menu_debug);

        ll_versao = view.findViewById(R.id.ll_versao);

    }

    private void initActions() {

        ToolBox_SharedPrefs_Usuario prefs = new ToolBox_SharedPrefs_Usuario(context);
        tv_nome.setText("Ola " + prefs.getNOME_USUARIO());
//        //Cria a tarefa para ser executada em background
//        SocketAsyncTask task = new SocketAsyncTask(this);
//
//        //Aqui eu chamo a execução. Como é em background, por isso citei de usar um EventBus ou outra
//        //forma de poder recuperar a informação. Não é a melhor forma, mas de forma ilustrativa, vou
//        //criar um método para receber a informação e aproveitar na activity.
//        task.execute("Dados enviados para o servidor");

        Fragment_Inicio.Funcao_ChamaCadastroLeilao(btn_criarLailao);


//        perfil
        Intent intent_user = new Intent(activity , Editar_Usuario.class);
        intent_user.putExtra("type" , 1);
        Funcao_Chama_Activity(ll_btn_editar_perfil, activity, null, intent_user, false, false, "");

        Intent intent_pass = new Intent(activity , Editar_Usuario.class);
        intent_pass.putExtra("type" , 2);
        Funcao_Chama_Activity(ll_btn_editar_senha, activity, null, intent_pass, false, false, "");

//       conta
        Funcao_Chama_Activity(ll_btn_ativar_leilao, activity, Ativar_Leilao.class, null, false, false, "");
        Funcao_Chama_Activity(ll_btn_afiliados, activity, Afiliados.class, null, false, false, "");
        Funcao_Chama_Activity(ll_btn_recompensa, activity, Recompensa.class, null, false, false, "");
        Funcao_Chama_Activity(ll_btn_historico, activity, Historico.class, null, false, false, "");


        ll_btn_sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToolBox_Chama_Activity.Funcao_Logout(activity);
            }
        });

//        ToolBox_VersaoApp.Funcao_Liberar_Debug(activity , ll_versao , ll_btn_debug);
    }

//    @Override
//    public void postResult(String result) {
//        //Neste método você pega o resultado da asynctask e aproveita de alguma forma
//
//        tv_socket.setText(result);
//    }

}
//
//interface IAsyncHandler {
//    void postResult(String result);
//}
