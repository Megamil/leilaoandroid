package net.megamil.leilao24h.Usuario.Adaptadores;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.Activity_Suporte.WebView;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Mascaras;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;

import java.util.List;

/**
 * Created by nalmir on 21/11/2017.
 */

public class Adapter_Planos extends BaseAdapter {

    private Activity activity;
    private int resource;
    private List<HMAux> dados;
    private View mView;

    private LayoutInflater mInflater;

    public interface IAdapterPosts {
        void onStatusChanged(String id, boolean status);
    }

    private IAdapterPosts delegate;

    public void setOnStatusChangedListener(IAdapterPosts delegate) {
        this.delegate = delegate;
    }

    public Adapter_Planos(Activity activity, int resource, List<HMAux> dados) {
        this.activity = activity;
        this.resource = resource;
        this.dados = dados;
        this.mInflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return dados.size();
    }

    @Override
    public Object getItem(int position) {
        return dados.get(position);
    }

    @Override
    public long getItemId(int position) {
        HMAux hmAux = dados.get(position);

        return Long.parseLong(hmAux.get(HMAux.ID));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(resource, parent, false);
        }

        // Acessar os dados de uma posicao especirfica (position)
        final HMAux hmAux = dados.get(position);

        TextView tv_nome = (TextView)
                convertView.findViewById(R.id.celula_plano_nome);

        TextView tv_valor = (TextView)
                convertView.findViewById(R.id.celula_plano_valor);

        TextView tv_data = (TextView)
                convertView.findViewById(R.id.celula_plano_desc);

        TextView tv_data_texto = (TextView)
                convertView.findViewById(R.id.celula_plano_nome_text);

        // Juntar Dados / Layout
        tv_nome.setText(hmAux.get(HMAux.QUANTIDADE_INGRESSO));
        tv_valor.setText(ToolBox_Mascaras.Funcao_Conveter_Para_Moeda(hmAux.get(HMAux.VALOR_PACOTE)));
        tv_data.setText(hmAux.get(HMAux.DESCRICAO_PACOTE));

        if (Integer.parseInt(hmAux.get(HMAux.QUANTIDADE_INGRESSO)) > 0) {
            tv_data_texto.setText("Ingressos");
        } else {
            tv_data_texto.setText("Ingresso");
        }

        mView = convertView;

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ToolBox_SharedPrefs_Usuario prefs = new ToolBox_SharedPrefs_Usuario(activity);
                String id = prefs.getID_USUARIO();

                Intent intent = new Intent(activity, WebView.class);
                intent.putExtra("credited" , hmAux.get(HMAux.QUANTIDADE_INGRESSO));
                intent.putExtra("url", Conexao_Constantes.URL_WS_PACOTES + id + "/" + hmAux.get(HMAux.ID_PACOTE_INGRESSO));

                ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");

            }
        });

        return convertView;
    }

}
