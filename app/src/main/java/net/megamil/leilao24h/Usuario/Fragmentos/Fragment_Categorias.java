package net.megamil.leilao24h.Usuario.Fragmentos;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_Dialog_Categoria;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_Recycler_Itens_Vertical;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Utils.ToolBox.Animation.tResizeAnimation;
import net.megamil.leilao24h.Utils.ToolBox.Animation.tRotateAnimation;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Cadastra_Leilao.add_array_categorias;
import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;

/**
 * Created by John on 09/12/2017.
 */

public class Fragment_Categorias extends Fragment {

    private Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;

    private static ImageView ic_drop;

    private static LinearLayout breadcrumbWrapper;
    private static LinearLayout btnBackCateg;
    private static TextView textBackCateg;

    private static ArrayList<String> nameCateg = new ArrayList<>();
    private static ArrayList<String> idCateg = new ArrayList<>();

    private static LinearLayout btn_selectCateg;
    private static LinearLayout ll_modalSelectCateg;
    public static Boolean modalCategShow = false;

    public static JSONArray categoria_json = null;
    public static ArrayList<HMAux> dados_categorias = new ArrayList<>(); // Dados em Memoria
    public static ArrayList<HMAux> dados_raw_categorias = new ArrayList<>(); // Dados em Memoria
    public static ListView listView_catgorias;

    public static ProgressBar progressBar;
    private static String id_categoria;
    private static String nome_categoria;
    private static TextView tv_nome_categ;

    private static tRotateAnimation rotateAnimation;

    //Itens List RecyclerView
    private static LinearLayoutManager linearLayoutManager;
    private static RecyclerView recyclerView;
    private static Adapter_Recycler_Itens_Vertical adapter;
    public static List<HMAux> arrayList_dados = new ArrayList<>();

    //Scroll Listenner
    public static List<HMAux> arrayList_dados_NextPage = new ArrayList<>();
    public static int sizePerPage = 2;
    public static int pageNumber = 1;
    private static boolean loading;

    @Override
    public void onPause() {
        super.onPause();
        modalCategShow = false;
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//        tRotateAnimation rotateAnimation = new tRotateAnimation(ic_drop);
//
//        if (modalCategShow) {
////            Log.w("Slide", "hide");
//
//            tResizeAnimation resizeAnimation = new tResizeAnimation(
//                    ll_modalSelectCateg,
//                    -900,
//                    900
//            );
//            resizeAnimation.setDuration(500);
//            ll_modalSelectCateg.startAnimation(resizeAnimation);
//
//            rotateAnimation.topArrow();
//
//        } else {
////            Log.w("Slide", "show");
//
//            tResizeAnimation resizeAnimation = new tResizeAnimation(
//                    ll_modalSelectCateg,
//                    900,
//                    0
//            );
//            resizeAnimation.setDuration(500);
//            ll_modalSelectCateg.startAnimation(resizeAnimation);
//
//            rotateAnimation.bottonArrow();
//        }
//        modalCategShow = !modalCategShow;
//    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_activity_inicio_categoria, container, false);

        initVars(view);
        initActions();

        Dados_Ws(601, "", null, activity);

        breadcrumbWrapper = view.findViewById(R.id.breadcrumb_wrapper);
//        breadcrumbBar = (ViewGroup) view.findViewById(R.id.breadcrumbs);

        return view;
    }

    private void initVars(View view) {

        //Var padroes
        context = getActivity();
        context_activity = getActivity();
        activity = getActivity();
        window = getActivity().getWindow();

        btn_selectCateg = view.findViewById(R.id.ll_selectCateg);
        ll_modalSelectCateg = view.findViewById(R.id.ll_modalSelectCateg);
        ic_drop = view.findViewById(R.id.ic_drop);
        listView_catgorias = view.findViewById(R.id.lv_dialog_categoria);
        progressBar = view.findViewById(R.id.pb);
        tv_nome_categ = view.findViewById(R.id.tv_nome_categ);
        textBackCateg = view.findViewById(R.id.textBackCateg);
        btnBackCateg = view.findViewById(R.id.btnBackCateg);

        recyclerView = view.findViewById(R.id.rv_itens);

    }

    private void initActions() {

//        onSlideViewButtonClick();

        Adapter_Dialog_Categoria adapter_dialog = new Adapter_Dialog_Categoria(context,
                R.layout.celula_categorias,
                dados_raw_categorias
        );
        listView_catgorias.setAdapter(adapter_dialog);

        listView_catgorias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                pageNumber = 1;
                loading = false;

                HMAux categorias = (HMAux) parent.getItemAtPosition(position);
                id_categoria = categorias.get(HMAux.ID);
                nome_categoria = categorias.get(HMAux.NOME_CATEGORIA);

//                tv_nome_categ.setText(nome_categoria);

//                addBreadCumb(nome_categoria, id_categoria);
//                addBackButton(nome_categoria, id_categoria);

                progressBar.setVisibility(View.VISIBLE);
                listView_catgorias.setVisibility(View.GONE);

                String get = "id_categoria=" + id_categoria;
                Map<String, String> Dados_Para_Parametros = new HashMap<>();
                Dados_Para_Parametros.put("add", "1");
                Dados_Para_Parametros.put("id", id_categoria);
                Dados_Para_Parametros.put("quantidade_por_pagina", String.valueOf(sizePerPage));
                Dados_Para_Parametros.put("pagina", String.valueOf(pageNumber));

                Dados_Ws(601, get, Dados_Para_Parametros, activity);

            }
        });

        btn_selectCateg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.w("Clicked", "OK");
                onSlideViewButtonClick();
            }
        });

        btnBackCateg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BackButton();
            }
        });

        rotateAnimation = new tRotateAnimation(ic_drop);

    }

    public static void addBackButton() {
        int idPos = idCateg.size();

        if (idPos == 0) {
            idCateg.add("0");
            nameCateg.add("Root");

            idCateg.add(id_categoria);
            nameCateg.add(nome_categoria);

        } else {

            int id = Integer.parseInt(idCateg.get(idPos - 1));

            if (id != Integer.parseInt(id_categoria)) {
                idCateg.add(id_categoria);
                nameCateg.add(nome_categoria);
            }

        }

        if (nameCateg.size() > 1) {
            btnBackCateg.setVisibility(View.VISIBLE);
            textBackCateg.setText(nome_categoria);
        }

        Log.w("Array de Categ", nameCateg.toString());
        Log.w("Array de idCateg", idCateg.toString());

    }

    private void BackButton() {
//        Log.w("Array de Categ", nameCateg.toString());
//        Log.w("Array de idCateg", idCateg.toString());

        progressBar.setVisibility(View.VISIBLE);
        listView_catgorias.setVisibility(View.GONE);

        int idPos = idCateg.size() - 1;

        String get = "id_categoria=" + idCateg.get(idPos);
        Map<String, String> Dados_Para_Parametros = new HashMap<>();
        Dados_Para_Parametros.put("back", "1");
        Dados_Para_Parametros.put("id", id_categoria);
        Dados_Para_Parametros.put("quantidade_por_pagina", String.valueOf(sizePerPage));
        Dados_Para_Parametros.put("pagina", String.valueOf(pageNumber));

        if (idCateg.size() <= 1) {
            btnBackCateg.setVisibility(View.GONE);
            Dados_Ws(601, "", null, activity);
        } else {
            Dados_Ws(601, get, Dados_Para_Parametros, activity);
        }

    }

    public static void removeArray() {

        if (idCateg.size() > 0) {
            int idPos = idCateg.size() - 1;

            textBackCateg.setText(nameCateg.get(idPos));
            idCateg.remove(idPos);
            nameCateg.remove(idPos);

            Log.w("Array de Categ", nameCateg.toString());
            Log.w("Array de idCateg", idCateg.toString());
        }

        progressBar.setVisibility(View.GONE);
        listView_catgorias.setVisibility(View.VISIBLE);

    }

    private void addBreadCumb(String nome_categoria, String id_categoria) {
//      Deixado de lado por falta de Vale Refeição
        int idPos = idCateg.size();
        int namePos = nameCateg.size();

        if (idPos == 0) {
            idCateg.add("0");
        }

        if (namePos == 0) {
            nameCateg.add("Root");
        }

        idCateg.add(id_categoria);
        nameCateg.add(nome_categoria);

        ImageView imageView = new ImageView(activity);
        imageView.setBackground(getResources().getDrawable(R.drawable.ic_keyboard_arrow_right_black_24dp));

        TextView textView = new TextView(activity);
        textView.setText(nome_categoria);

        breadcrumbWrapper.addView(imageView);
        breadcrumbWrapper.addView(textView);

    }

    public void onSlideViewButtonClick() {

        if (modalCategShow) {
//            Log.w("Slide", "hide");
            animationHide();
        } else {
//            Log.w("Slide", "show");
            animationShow();
        }
//        modalCategShow = !modalCategShow;
    }

    public static void Funcao_FinalizaCategoria() {
//        int idPos = idCateg.size() - 1;
//        int namePos = nameCateg.size() - 1;
//
//        idCateg.remove(idPos);
//        nameCateg.remove(namePos);
//
//        textBackCateg.setText(nameCateg.get(namePos - 1));

        animationHide();
//
//        progressBar.setVisibility(View.GONE);
//        listView_catgorias.setVisibility(View.VISIBLE);
    }

    private void animationShow() {
        tResizeAnimation resizeAnimation = new tResizeAnimation(
                ll_modalSelectCateg,
                900,
                0
        );
        resizeAnimation.setDuration(500);
        ll_modalSelectCateg.startAnimation(resizeAnimation);
        rotateAnimation.bottonArrow();
        modalCategShow = true;

        progressBar.setVisibility(View.GONE);
        listView_catgorias.setVisibility(View.VISIBLE);
    }

    public static void animationHide() {

        tResizeAnimation resizeAnimation = new tResizeAnimation(
                ll_modalSelectCateg,
                -900,
                900
        );
        resizeAnimation.setDuration(500);
        ll_modalSelectCateg.startAnimation(resizeAnimation);
        rotateAnimation.topArrow();
        modalCategShow = false;

    }

    public static void Funcao_ListaCategoria() {

        dados_categorias.clear();

        for (int i = 0; i < categoria_json.length(); i++) {
            try {
                JSONObject arrayInterno = categoria_json.getJSONObject(i);

                HMAux hmAux = new HMAux();
                //
                // Imita mini-registro
                hmAux.put(HMAux.ID, arrayInterno.getString("id_categoria")); // Primary Key
                hmAux.put(HMAux.NOME_CATEGORIA, arrayInterno.getString("nome_categoria"));
                hmAux.put(HMAux.FK_CATEGORIA_PAI, arrayInterno.getString("fk_categoria_pai"));
                //
                dados_categorias.add(hmAux);
                if (add_array_categorias == 1) {
                    dados_raw_categorias.add(hmAux);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        add_array_categorias = 0;

        progressBar.setVisibility(View.GONE);
        listView_catgorias.setVisibility(View.VISIBLE);

    }

    public static void Set_Dados_Array() {

        linearLayoutManager = ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new Adapter_Recycler_Itens_Vertical(
                activity,
                arrayList_dados,
                1
        );
        recyclerView.setAdapter(adapter);
        if (arrayList_dados.size() < sizePerPage) {
            adapter.addEndView();
            loading = true;
        } else {
            adapter.addFooterView();
        }
        recyclerView.addOnScrollListener(scrollListener);

    }

    public static void addDataArray() {
        if (arrayList_dados_NextPage.size() > 0) {
            adapter.removeLastItem();
            loading = false;
            adapter.addList(arrayList_dados_NextPage);
            if (arrayList_dados_NextPage.size() < sizePerPage) {
                adapter.addEndView();
                loading = true;
            } else {
                adapter.addFooterView();
            }
        } else {
            adapter.removeLastItem();
            adapter.addEndView();
            loading = true;
        }
    }

    public static void noData() {

        linearLayoutManager = ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new Adapter_Recycler_Itens_Vertical(
                activity,
                arrayList_dados,
                1
        );

        recyclerView.setAdapter(adapter);

        adapter.addEndView();
    }

    private static RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            if (!loading && linearLayoutManager.getItemCount() == (linearLayoutManager.findLastVisibleItemPosition() + 1)) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initWSConection(pageNumber);
                    }
                }, 1500);

                loading = true;
            }
        }
    };

    public static void initWSConection(int Page) {
        Dados_Ws(17, "quantidade_por_pagina=" + sizePerPage + "&pagina=" + String.valueOf(Page) + "&categoria=" + id_categoria, null, activity);
    }

}
