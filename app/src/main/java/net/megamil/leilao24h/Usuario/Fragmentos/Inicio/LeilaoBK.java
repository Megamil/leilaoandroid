package net.megamil.leilao24h.Usuario.Fragmentos.Inicio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Adaptadores.ViewPager_IMG;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Inicio;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Carossel;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Data_e_Hora;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Mascaras;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RingTone;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.supercharge.shimmerlayout.ShimmerLayout;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;
import static net.megamil.leilao24h.Utils.ToolBox.A_ToolBox_Gerador_De_Dados.getSpinnerDataFromID;
import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Data_e_Hora.MillisecondsToDate;

public class LeilaoBK extends AppCompatActivity {

    //init Page

    @SuppressLint("StaticFieldLeak")
    private static Activity activity;

    /////////////////////////////////////////////////////////////////////////////

    /*
     *  Inicio de Variaveis
     */

    //TextViwe
    private TextView tv_share_view_tip;
    private TextView tv_titulo;
    private TextView tv_estado;
    private TextView valor_principal;
    private TextView valor_secundario;
    private TextView conometro;
    private TextView valor_atual;
    private TextView valor_atualizado;
    @SuppressLint("StaticFieldLeak")
    private static TextView finaliza;
    @SuppressLint("StaticFieldLeak")
    public static TextView descricao;
    private TextView leiloero;
    private TextView tvLabelNew;
    private TextView ultimo_lance;
    @SuppressLint("StaticFieldLeak")
    public static TextView credits;
    @SuppressLint("StaticFieldLeak")
    public static TextView labelEndDate;
    @SuppressLint("StaticFieldLeak")
    public static TextView tvLabelChonometer;

    //ImageView
    private ImageView btn_favorito;

    //ImageButton
    private ImageButton btn_close;

    //CircleImageView
//    private CircleImageView img_leiloero;

    //LinearLayout
    @SuppressLint("StaticFieldLeak")
    private static LinearLayout btn_historico;
    private LinearLayout llcredits;
    @SuppressLint("StaticFieldLeak")
    private static LinearLayout ll_itens_dialog;

    //Dialog
    private static Dialog dialog_bids = null;

    //Button
    @SuppressLint("StaticFieldLeak")
    public static Button btn_dar_lance;

    //RatingBar
    private RatingBar rb_avaliaty;

    //ShimmerLayout
    private static ShimmerLayout shimmerFrameLayout;

    //Vars
    private static String id_leilao;
    private static String currentValue;
    private static String currentBidValue;
    private static String DATA_INICIO;
    private static String DATA_FIM_PREVISTO;
    //
    private static int id_pos;
    private static int nortification;
    //
    private static boolean bl_favorito = true;
    //
    private static List<String> bids = new ArrayList<>();

    //Socket and vars
    private Socket mSocket;

    //Carossel
    private LinearLayout dotsLinearLayout;
    private TextView imgCount;
    //    private int[] mResources = null;
    private String[] Array_imagens = null;
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leilao);

        //Var padroes
        activity = LeilaoBK.this;

        id_leilao = getIntent().getStringExtra(HMAux.ID_LEILAO);
        id_pos = getIntent().getIntExtra(HMAux.ID_POSITION, -1);
        nortification = getIntent().getIntExtra(HMAux.NORTIFICATION, -1);

        initVars();
        iniDados();
        initWSConection();
        initActions();
        iniSocket();


    }

    private void initVars() {

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */
        tv_titulo = findViewById(R.id.tv_titulo);
        tv_estado = findViewById(R.id.tv_estado);
        valor_principal = findViewById(R.id.valor_principal);
        valor_secundario = findViewById(R.id.valor_secundario);
        conometro = findViewById(R.id.conometro);
        valor_atual = findViewById(R.id.valor_atual);
        valor_atualizado = findViewById(R.id.valor_atualizado);
        finaliza = findViewById(R.id.finaliza);
        descricao = findViewById(R.id.descricao);
        leiloero = findViewById(R.id.leiloero);
//        vendedor = findViewById(R.id.vendedor);
        ultimo_lance = findViewById(R.id.ultimo_lance);
        tvLabelNew = findViewById(R.id.tvLabelNew);
        labelEndDate = findViewById(R.id.labelEndDate);

        llcredits = findViewById(R.id.llcredits);
        credits = findViewById(R.id.credits);

        tvLabelChonometer = findViewById(R.id.tvLabelChonometer);

        btn_favorito = findViewById(R.id.btn_favorito);
        btn_close = findViewById(R.id.btn_close);

        tv_share_view_tip = findViewById(R.id.tv_share_view_tip);

        btn_historico = findViewById(R.id.btn_historico);

        dotsLinearLayout = findViewById(R.id.dotLinear);
        imgCount = findViewById(R.id.img_count);
        mViewPager = findViewById(R.id.vp_img_pager);

        btn_dar_lance = findViewById(R.id.btn_dar_lance);

        rb_avaliaty = findViewById(R.id.rb_avaliaty);
        rb_avaliaty.setEnabled(false);

        shimmerFrameLayout = findViewById(R.id.shimmer_view_container);
//        shimmerFrameLayout.startShimmer();

    }

    private void iniDados() {

        if (nortification == -1) {
//            String ID_PROCUTO = getIntent().getStringExtra(HMAux.ID_PROCUTO);
            String NOME_PRODUTO = getIntent().getStringExtra(HMAux.NOME_PRODUTO);
            String DESCRICAO_PRODUTO = getIntent().getStringExtra(HMAux.DESCRICAO_PRODUTO);
            String VALOR_MINIMO = ToolBox_Mascaras.Funcao_Mascara_Moeda_NumberFormat(getIntent().getStringExtra(HMAux.VALOR_MINIMO));
            String LANCE_MINIMO = ToolBox_Mascaras.Funcao_Mascara_Moeda_NumberFormat(getIntent().getStringExtra(HMAux.LANCE_MINIMO));
            currentBidValue = getIntent().getStringExtra(HMAux.LANCE_MINIMO);
//            String INGRESSOS = getIntent().getStringExtra(HMAux.INGRESSOS);
            DATA_INICIO = getIntent().getStringExtra(HMAux.DATA_INICIO);
            DATA_FIM_PREVISTO = getIntent().getStringExtra(HMAux.DATA_FIM_PREVISTO);
            String FAVORITO = getIntent().getStringExtra(HMAux.FAVORITO);
            String USADO = getIntent().getStringExtra(HMAux.PRODUTO_USADO);
            String IMAGENS = getIntent().getStringExtra(HMAux.IMAGENS_ARRAY);

            String ESTADO_USUARIO = getIntent().getStringExtra(HMAux.ESTADO_USUARIO);
            tv_estado.setText(getSpinnerDataFromID(ESTADO_USUARIO));

            leiloero.setText(getIntent().getStringExtra(HMAux.NOME_USUARIO));
            rb_avaliaty.setRating(Float.parseFloat(getIntent().getStringExtra(HMAux.AVALIACAO)));

            tvLabelNew.setText(setTextNew(USADO));

            Array_imagens = IMAGENS.split(",");

            bl_favorito = !FAVORITO.equals("0");

            Status_Favorito(btn_favorito);

            String[] raw_valor = VALOR_MINIMO.split(",");

            valor_principal.setText(raw_valor[0].replaceAll("[^\\d.]", ""));
//        valor_secundario.setText(raw_valor[1]);

            tv_titulo.setText(NOME_PRODUTO);
            valor_atual.setText(VALOR_MINIMO);
            valor_atualizado.setText(LANCE_MINIMO);

            descricao.setText(DESCRICAO_PRODUTO);
            llcredits.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, Planos.class, null, false, true, "Deseja comprar mais ingressos?");
                }
            });
        }

        ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
        credits.setText(prefs.getCredits());

    }

    private void initWSConection() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(HMAux.ID_LEILAO, id_leilao);
        Dados_Ws(11, "", parametros, activity);
    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */

        if (bids.size() > 0) {
            bids.clear();
        }

        ViewPager_IMG mCustomPagerAdapter = new ViewPager_IMG(this, Array_imagens);

        mViewPager.setAdapter(mCustomPagerAdapter);

        new ToolBox_Carossel(
                activity,
                dotsLinearLayout,
                mViewPager,
                imgCount,
                Array_imagens.length
        );


        Add_Favorito(btn_favorito);

        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(1500);
        alphaAnimation.setStartOffset(1000);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tv_share_view_tip.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        tv_share_view_tip.startAnimation(alphaAnimation);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_dar_lance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                MsgUtil.Funcao_Dialog_OK(activity, "Tenha em mente que todas as licitações são vinculativas, e será obrigado a cumprir com o pagamento, caso venha a ganhar o lote! \n" +
//                        "O valor da licitação, inclui os custos de envio e a taxa de leilão.", "Comfirmar", "Cancelar", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        FunctionEmitBid();
//                    }
//                });

                modalBid();
            }
        });


        btn_historico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModalHistory();
            }
        });

        showBtnHistory();

    }

    private void iniSocket() {

        final ToolBox_SharedPrefs_Usuario prefs = new ToolBox_SharedPrefs_Usuario(activity);
        Log.w("joinRoom", prefs.getTOKEN() + "  :  " + id_leilao);


        String server = Conexao_Constantes.SOCKET_WS;
//        String server = "http://31.220.57.245:3005";
//        String server = "http://192.168.1.105:3005";
        try {
            mSocket = IO.socket(server);
        } catch (URISyntaxException ignored) {
        }

        Log.w("joinRoom", prefs.getTOKEN() + "  :  " + id_leilao);
        mSocket.emit("joinRoom", prefs.getTOKEN(), id_leilao); // enviar

        mSocket.on("currentBid", currentBid);
        mSocket.on("initRoom", initRoom);
        mSocket.on("newBid", newBid);

//        Boolean inicio = ToolBox_Verivifacao_Campos.Funcao_Verifica_Data_Menor_que_Agora(DATA_INICIO);
//        if (!inicio) {
        mSocket.on("dateStart", dateStart);
//        } else {
        mSocket.on("dateEnd", dateEnd);
//        }

        mSocket.on("cronometro", chronometer);
        mSocket.on("por_usuarios", perUsers);

        mSocket.on("winner", winner);
        checkSocketStatus();
        mSocket.connect();
        stopShimmer();
    }

    //=================================================================================================================================================================================================================================
    //
    // Socket
    //
    //=================================================================================================================================================================================================================================

    private Emitter.Listener currentBid = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    currentValue = data;
                    Log.w("currentBid", String.valueOf(data));
                    String[] valor = data.split(",");

                    valor_principal.setText(valor[0]);

                    if (valor[1].equals("null") || valor[1].equals("")) {
                        valor_secundario.setText("00");
                    } else {
                        valor_secundario.setText(valor[1]);
                    }
                }
            });
        }
    };

    private static int i1 = 0;
    private Emitter.Listener dateStart = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    i1 = Integer.parseInt(data);
                    if (i1 > 0) {
                        MillisecondsToDate(activity, conometro, btn_dar_lance, Long.valueOf(data), 1, false);
                    }
                    Log.w("dateStart", data);
                }
            });
        }

    };

    private Emitter.Listener dateEnd = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    if (i1 <= 0) {
                        MillisecondsToDate(activity, conometro, btn_dar_lance, Long.valueOf(data), 2, false);
                    }
                    Log.w("dateEnd", data);

                    Boolean init = i1 > 0;
                    started(init);
                }
            });
        }

    };

    private void runResponse(final String status, final Object[] args) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (args.length > 0) {
                    String data = String.valueOf(args[0]);
                    Log.w(status, data);
                } else {
                    Log.w(status, "init");
                }
            }
        });
    }

    private void FunctionEmitBid() {
        mSocket.emit("bid"); // enviar
        ToolBox_RingTone.Funcao_Play(activity);
    }

    private String chrometerValue;
    private Emitter.Listener chronometer = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    chrometerValue = data;
                    Log.w("chronometer", data);
                }
            });
        }

    };

    private Emitter.Listener perUsers = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    if (data.equals("1")) {
                        //usuarios
                        startedPerUsers(chrometerValue);
                    }

                    Log.w("perUsers", data);
                }
            });
        }

    };

    private Emitter.Listener winner = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    Log.w("Winner", data);
                    MsgUtil.Funcao_BottomSheetDialog(activity, data, clickListener_winner, null);
                }
            });
        }

    };

    View.OnClickListener clickListener_winner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onBackPressed();
        }
    };

    private Emitter.Listener initRoom = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    Log.w("initRoom", data);

                    bids.add(data);
                    showBtnHistory();
//                    ultimo_lance.setText(dados);
//                    ultimo_lance.setVisibility(View.VISIBLE);
                }
            });
        }

    };

    private Emitter.Listener newBid = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    Log.w("UltimoLance", data);
                    bids.add(data);

                    if (dialog_bids != null) {
                        addTextView(data);
                    }

                    showBtnHistory();

                    ultimo_lance.setText(data);
                    ultimo_lance.setVisibility(View.VISIBLE);
                }
            });
        }

    };

    //=================================================================================================================================================================================================================================
    //
    // Socket's Util
    //
    //=================================================================================================================================================================================================================================

    private void checkSocketStatus() {

        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_CONNECT", args);
            }
        });

        mSocket.on(Socket.EVENT_CONNECTING, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_CONNECTING", args);
            }
        });

        mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_DISCONNECT", args);
            }
        });

        mSocket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_ERROR", args);
            }
        });

        mSocket.on(Socket.EVENT_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_MESSAGE", args);
            }
        });


//        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_CONNECT_TIMEOUT", args);
//            }
//        });
//
//
//        mSocket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_CONNECT_ERROR" , args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_PING, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_PING", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_PONG, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_PONG", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECT", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECT_ATTEMPT, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECT_ATTEMPT", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECT_ERROR, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECT_ERROR", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECTING, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECTING", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECT_FAILED, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECT_FAILED", args);
//            }
//        });

    }

    //=================================================================================================================================================================================================================================
    //
    // History
    //
    //=================================================================================================================================================================================================================================

    private void ModalHistory() {

        dialog_bids = new Dialog(activity);
        dialog_bids.setContentView(R.layout.dialog_historico);
        ll_itens_dialog = dialog_bids.findViewById(R.id.sv_itens);
        for (int i = 0; i < bids.size(); i++) {
            TextView textView = new TextView(activity);
            textView.setText(bids.get(i));

            ll_itens_dialog.addView(textView);
        }

        Objects.requireNonNull(dialog_bids.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog_bids.show();
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_bids.dismiss();
            }
        });

    }

    private void showBtnHistory() {
        if (bids.size() > 0) {
            btn_historico.setVisibility(View.VISIBLE);
        } else {
            btn_historico.setVisibility(View.GONE);
        }
    }

    //=================================================================================================================================================================================================================================
    //
    // Util's page
    //
    //=================================================================================================================================================================================================================================

    @SuppressLint("SetTextI18n")
    public static void started(Boolean init) {
        finaliza.setVisibility(View.VISIBLE);
        labelEndDate.setText("Fim do Leilão");
        if (init) {
            tvLabelChonometer.setText("Inicia em:");
            tvLabelChonometer.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            finaliza.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(DATA_INICIO, false, false));
            btn_dar_lance.setVisibility(View.VISIBLE);
        } else {
            tvLabelChonometer.setText("");
            tvLabelChonometer.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            finaliza.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(DATA_FIM_PREVISTO, false, false));
//            btn_dar_lance.setVisibility(View.VISIBLE);
        }
    }

    @SuppressLint("SetTextI18n")
    private void startedPerUsers(String chrometerValue) {
        tvLabelChonometer.setText("");
        labelEndDate.setText("Aguardando usuários");
        finaliza.setVisibility(View.VISIBLE);
        btn_dar_lance.setVisibility(View.VISIBLE);
        MillisecondsToDate(activity, conometro, btn_dar_lance, Long.valueOf(chrometerValue), 2, true);
    }

    @SuppressLint("SetTextI18n")
    private void modalBid() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        @SuppressLint("InflateParams") final View dialogbox = activity.getLayoutInflater().inflate(R.layout.dialog_bid, null);

        ImageView btn_close = dialogbox.findViewById(R.id.btn_close);
        TextView tvBid = dialogbox.findViewById(R.id.tvBidLabel);
        Button bidConfirm = dialogbox.findViewById(R.id.bidConfirm);

        builder.setView(dialogbox);
        final Dialog dialog = builder.create();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Float value = Float.parseFloat(currentValue.replace(".", "").replace(",", ".")) + Float.parseFloat(currentBidValue);

        tvBid.setText("Confirmar lance de " + ToolBox_Mascaras.Funcao_Mascara_Moeda_NumberFormat(String.valueOf(value)));

        bidConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FunctionEmitBid();
                dialog.dismiss();
            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

//    private void checkValue(String i) {
//
//        float value = Float.parseFloat(i.replaceAll("[^\\d.]", ""));
//        Log.w("Value", String.valueOf(value));
////        return i;
//    }

    private void addTextView(String string) {
        TextView textView = new TextView(activity);
        textView.setText(string);

        ll_itens_dialog.addView(textView);
    }

    public void stopShimmer() {
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
//                what ever you do here will be done after 3 seconds delay.
//                if (shimmerFrameLayout.isShimmerStarted()) {
                Log.w("Shimmer", "Stop");
                shimmerFrameLayout.stopShimmerAnimation();
//                }
            }
        };
        handler.postDelayed(r, 3000);
    }

    private static void Status_Favorito(ImageView btn_item) {

        AlphaAnimation alphaAnimationShowIcon = new AlphaAnimation(0.2f, 1.0f);
        alphaAnimationShowIcon.setDuration(500);

//        btn_item.clearAnimation();
//        TranslateAnimation translation;
//        translation = new TranslateAnimation(0f, 0F, 0f, getDisplayHeight());
//        translation.setStartOffset(500);
//        translation.setDuration(2000);
//        translation.setFillAfter(true);
//
//        translation.setInterpolator(new BounceInterpolator());
//        btn_item.startAnimation(translation);

        if (!bl_favorito) {
            btn_item.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            btn_item.startAnimation(alphaAnimationShowIcon);
            bl_favorito = true;
//            Toast.makeText(activity, "Retirado dos Favoritos", Toast.LENGTH_SHORT).show();

        } else {
            btn_item.setImageResource(R.drawable.ic_favorite_black_24dp);
            btn_item.startAnimation(alphaAnimationShowIcon);
            bl_favorito = false;
//            Toast.makeText(activity, "Favorito", Toast.LENGTH_SHORT).show();

        }


    }

    private static void Add_Favorito(final ImageView btn_item) {


        btn_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Status_Favorito(btn_item);

                Map<String, String> Dados_Para_Parametros = new HashMap<>();

                Dados_Para_Parametros.put("id_leilao", id_leilao);

                HMAux hmAux = Fragment_Inicio.arrayList_dados.get(id_pos);
                if (!bl_favorito) {
                    hmAux.put(HMAux.FAVORITO, "3");
                } else {
                    hmAux.put(HMAux.FAVORITO, "0");
                }

                Dados_Ws(13, "", Dados_Para_Parametros, activity);
            }


        });
    }

    private String setTextNew(String s) {
        if (Integer.parseInt(s) == 0) {
            return "Novo";
        } else {
            return "Usado";
        }
    }

    //=================================================================================================================================================================================================================================
    //
    // Override
    //
    //=================================================================================================================================================================================================================================

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mSocket.disconnect();
        finish();
    }
}
