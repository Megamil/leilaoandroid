package net.megamil.leilao24h.Usuario.Busca;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;

import java.util.ArrayList;

/**
 * Created by nalmir on 21/11/2017.
 */

public class Adapter_Dialog_Busca extends BaseAdapter {

    private Context context;
    private int resource;
    private ArrayList<HMAux> dados;

    private LayoutInflater mInflater;

    public interface IAdapterPosts {
        void onStatusChanged(String id, boolean status);
    }

    private IAdapterPosts delegate;

    public void setOnStatusChangedListener(IAdapterPosts delegate) {
        this.delegate = delegate;
    }

    public Adapter_Dialog_Busca(Context context, int resource, ArrayList<HMAux> dados) {
        this.context = context;
        this.resource = resource;
        this.dados = dados;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return dados.size();
    }

    @Override
    public Object getItem(int position) {
        return dados.get(position);
    }

    @Override
    public long getItemId(int position) {
        HMAux hmAux = dados.get(position);

        return Long.parseLong(hmAux.get(HMAux.ID));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(resource, parent, false);
        }

        // Acessar os dados de uma posicao especirfica (position)
        final HMAux hmAux = dados.get(position);
//

        TextView tv_data = (TextView)
                convertView.findViewById(R.id.tv_nome_celula_searchview);

        // Juntar Dados / Layout
//        Log.w("Dado", hmAux.get(HMAux.BUSCA_TEXTO_PESQUISA));

        tv_data.setText(hmAux.get(HMAux.BUSCA_TEXTO_PESQUISA));

        return convertView;
    }

}
