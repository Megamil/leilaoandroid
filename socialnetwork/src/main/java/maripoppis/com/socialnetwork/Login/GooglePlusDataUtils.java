
/*
 * Copyright (c) Developed by John Alves at 2018/10/24.
 */

package maripoppis.com.socialnetwork.Login;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

/**
 * Created by John on 10/05/2018.
 */

public class GooglePlusDataUtils {

    public static GoogleLoginCallback callback;

    private static GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build();

    public static int LOGIN_GOOGLE_ONRESULT_CODE = 100;

    public static void initLogin(final Activity activity) {

        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        activity.startActivityForResult(signInIntent, LOGIN_GOOGLE_ONRESULT_CODE);

    }

    private void revokeAccess(Activity activity, GoogleSignInClient mGoogleSignInClient) {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
//                        Toast.makeText(activity, "Google access revoked.", Toast.LENGTH_SHORT).show();
//                        getProfileInformation(null);
                    }
                });
    }


    private void doGoogleSignOut(final Activity activity) {
        final GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        revokeAccess(activity, mGoogleSignInClient);
                    }
                });
    }


    // verifica se ainda esta logado , este metodo e chamado no onStart da activivity
    // se retornar dados , significa que ainda esta logado
    // se retornar null , usuario naoesta mais logado

    public static void Funcao_Verifica_Se_Esta_Logado(Activity activity) {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(activity);
//        updateUI(account);
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
//        if (requestCode == LOGIN_GOOGLE_ONRESULT_CODE) {
//            // The Task returned from this call is always completed, no need to attach
//            // a listener.
//            handleSignInResult(task);
//        }
//    }

    //
    public static void handleSignInResult(Intent data) {

        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

        try {

            GoogleSignInAccount account = task.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            callback.GoogleLoginSuccess(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
//            Log.w("Login com Google", "signInResult:failed code=" + e.getStatusCode());
            callback.GoogleLoginFail(e);
        }
    }

    public static void setCallback(GoogleLoginCallback mCallback) {
        callback = mCallback;
    }

    public interface GoogleLoginCallback {
        void GoogleLoginSuccess(GoogleSignInAccount account);

        void GoogleLoginFail(ApiException e);
    }

}


