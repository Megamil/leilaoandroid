package maripoppis.com.socialnetwork.Google;

import android.app.Activity;
import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

/**
 * Created by John on 10/05/2018.
 */

public class Google_Login {


//    Entre no link e crie seu app
//    https://developers.google.com/identity/sign-in/android/start-integrating
//
//    Uns dos passos é adicionar hash sha1 do seu certificado local
//
//    Agora baixe este arquivo:
//    https://code.google.com/archive/p/openssl-for-windows/downloads
//    Crie uma pasta em C: com o nome OpenSSL e extraia os arquivos nela
//     *****(Cuidado para não criar um subpasta com o mesmo nome do arquivo. rar) somente clique em Extrair Aqui!
//
//    Após isto, você deverá gerar duas chaves para o facebook,
//    Tenha em mente que você também deverá ter o Java jdk instalado em sua máquina:
//    Download:
//    http://www.oracle.com/technetwork/pt/java/javase/downloads/index.html
//
//    Localize onde está o keystore do seu android.
//    No meu caso está em:
//    C:\Users\John\.android\debug.keystore
//    Copie este caminho.
//
//    //
//    Localize a pasta bin do jdk em sua máquina, normalmente encontrada como:
//    C:\Program Files\Java\jdk1.8.0_151\bin
//    Você deverá abrir um prompt de comando nesta pasta ou navegar até ela via cmd:
//            ***Testei via power shell e deu erro, recomendo navegar via CMD
//    Voce pode segurar o botão “Shift” e dar um clique com o botão direito, um menu atalho para o CMD ou Power Shell ira aparecer,
//    Caso o CMD não aparecer, poderá aqui via pesquisa e digitar o comando:
//    cd C:\Program Files\Java\jdk1.8.0_151\bin
//    Que irá parar dentro da pasta bin
//     *** não esqueça de modificar o caminho para o mesmo da sua maquina
//    //
//    Agora no prompt cole:
//    keytool -J-Duser.language=en -exportcert -alias androiddebugkey -keystore C:\Users\John\.android\debug.keystore -list –v
//    Um password sera requerido, por padrão é “android”, digite e aperte enter.
//
//
//    Uma lista ira aparecer assim :
//    Enter keystore password:
//    Alias name: androiddebugkey
//    Creation date: Nov 28, 2017
//    Entry type: PrivateKeyEntry
//    Certificate chain length: 1
//    Certificate[1]:
//    Owner: C=US, O=Android, CN=Android Debug
//    Issuer: C=US, O=Android, CN=Android Debug
//    Serial number: 1
//    Valid from: Tue Nov 28 18:54:16 BRST 2017 until: Thu Nov 21 18:54:16 BRST 2047
//    Certificate fingerprints:
//    MD5:  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//    SHA1: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//    SHA256: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//    Signature algorithm name: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//    Subject Public Key Algorithm: 1024-bit RSA key
//    Version: 1
//
//    Pegue o código sha1 e adicione no google

//    Owner: C=US, O=Android, CN=Android Debug
//    Issuer: C=US, O=Android, CN=Android Debug
//    Serial number: 1
//    Valid from: Tue Nov 28 18:54:16 BRST 2017 until: Thu Nov 21 18:54:16 BRST 2047
//    Certificate fingerprints:
//    MD5:  D7:10:2C:60:68:4B:2B:93:8E:63:7F:59:63:EE:B0:D1
//    SHA1: 9F:89:19:6A:5D:A6:90:A5:A4:82:42:BA:51:4A:46:97:96:C8:65:D2
//    SHA256: BA:9C:C7:94:44:64:6B:DC:93:0F:5D:56:71:CE:44:18:7C:51:B2:55:A2:09:D3:EB:84:9B:F4:52:5E:86:CE:9B
//    Signature algorithm name: SHA1withRSA
//    Subject Public Key Algorithm: 1024-bit RSA key
//    Version: 1


//    337991135340-acsnjqe86s0s35jv3hghmhmsfbjdoni5.apps.googleusercontent.com
//    Client Secret
//    CpZ3pXk6np_28Aape7sWHnQC

    private static GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build();
    private static GoogleSignInClient mGoogleSignInClient;
    private static int LOGIN_GOOGLE_ONRESULT_CODE = 100;

    public static void Funcao_Login_Google(final Activity activity) {

        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        activity.startActivityForResult(signInIntent, LOGIN_GOOGLE_ONRESULT_CODE);

    }

    // verifica se ainda esta logado , este metodo e chamado no onStart da activivity
    // se retornar dados , significa que ainda esta logado
    // se retornar null , usuario naoesta mais logado

    public static void Funcao_Verifica_Se_Esta_Logado(Activity activity){
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(activity);
//        updateUI(account);
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
//        if (requestCode == LOGIN_GOOGLE_ONRESULT_CODE) {
//            // The Task returned from this call is always completed, no need to attach
//            // a listener.
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);
//        }
//    }

//
//    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
//        try {
//            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
//
//            // Signed in successfully, show authenticated UI.
//            updateUI(account);
//        } catch (ApiException e) {
//            // The ApiException status code indicates the detailed failure reason.
//            // Please refer to the GoogleSignInStatusCodes class reference for more information.
//            Log.w("Login com Google", "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
//        }
//    }

}


