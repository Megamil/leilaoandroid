package maripoppis.com.socialnetwork.FaceBook;

import android.app.Activity;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by John on 10/05/2018.
 */

public class FB_Login {

    private static String token_facebook;
    private static String fb_id = "";
    private static String fb_email = "";

    private LoginButton loginButton;

    static CallbackManager callbackManager = CallbackManager.Factory.create();

    public static void Funcao_Login_FB(final Activity activity, LoginButton button) {

        callbackManager = CallbackManager.Factory.create();
//        loginButton.setReadPermissions(Arrays.asList(idDoFace));
        button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("SUCESSO!");
                AccessToken accessToken = loginResult.getAccessToken();
                token_facebook = String.valueOf(accessToken);
                LoginFace(accessToken);
            }

            @Override
            public void onCancel() {
                System.out.println("CANCELADO!");
            }

            @Override
            public void onError(FacebookException error) {
                System.out.println("ERRO");
            }
        });
    }


    private static void LoginFace(AccessToken accessToken) {

        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                try {

                    fb_id = object.getString("id");
                    fb_email = object.getString("email");
                    Boolean verified = object.getBoolean("verified");

                    String name = object.getString("name");
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");

                    String gender = object.getString("gender");
                    String birthday = object.getString("birthday");

                    String link = object.getString("link");

                    JSONObject location = object.getJSONObject("location");
                    String id_location = location.getString("id");
                    String name_location = location.getString("name");
                    String country_locale = object.getString("locale");

                    int timezone = object.getInt("timezone");
                    String updated_time = object.getString("updated_time");


//                Log.w("id face", id+email);

                } catch (
                        JSONException e)

                {
                    e.printStackTrace();
                }

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();

    }


    public static void Funcao_Verifica_Logado_FB() {

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("LOGADO PELO FACEBOOK");
            }

            @Override
            public void onCancel() {
                System.out.println("LOGIN DO FACEBOOK CANCELADO");
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("ERRO NO LOGIN DO FACEBOOK");
            }
        });
    }
}
