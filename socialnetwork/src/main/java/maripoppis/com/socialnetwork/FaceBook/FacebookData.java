/*
 * Copyright (c) Developed by John Alves at 2018/10/28.
 */

package maripoppis.com.socialnetwork.FaceBook;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

public class FacebookData implements Serializable {

    //==============================================================================================
    //
    // Structure Response
    //
    //==============================================================================================

    //Json Structure Example

//    {
//            "id": "579156356",
//            "name": "Vishal Jethani",
//            "first_name": "Vishal",
//            "last_name": "Jethani",
//            "link": "https://www.facebook.com/vishal.jethani",
//            "username": "vishal.jethani",
//
//   "hometown": {
//            "id": "106442706060302",
//            "name": "Pune, Maharashtra"
//    },
//   "location": {
//        "id": "106377336067638",
//        "name": "Bangalore, India"
//    },
//            "bio": "bye bye to bad characters",
//            "gender": "male",
//            "relationship_status": "Single",
//            "timezone": 5.5,
//            "locale": "en_GB",
//            "verified": true,
//            "updated_time": "2012-06-15T05:33:31+0000",
//            "type": "user"
//    }

//    W/FB: {Response:  responseCode: 200, graphObject: {"id":"2037594996552257","name":"Jonatas Alves","email":"jonny255d@hotmail.com","last_name":"Alves","first_name":"Jonatas"}, error: null}


    //==============================================================================================
    //
    // init
    //
    //==============================================================================================

    private String id;
    private String profile_pic;
    private String email;
    private String link;
    private String name;
    private String first_name;
    private String last_name;
    private String gender;
    private String birthday;
    private String locale;
    private String timezone;
    private String updated_time;

    private location location;

    //==============================================================================================
    //
    // Constructor
    //
    //==============================================================================================

    public FacebookData(String id, String profile_pic, String email, String link, String name, String first_name, String last_name, String gender, String birthday, String locale, String timezone, String updated_time, FacebookData.location location) {
        this.id = id;
        this.profile_pic = profile_pic;
        this.email = email;
        this.link = link;
        this.name = name;
        this.first_name = first_name;
        this.last_name = last_name;
        this.gender = gender;
        this.birthday = birthday;
        this.locale = locale;
        this.timezone = timezone;
        this.updated_time = updated_time;
        this.location = location;
    }

    public FacebookData() {
        id = getStringError();
        profile_pic = getStringError();
        email = getStringError();
        link = getStringError();
        name = getStringError();
        first_name = getStringError();
        last_name = getStringError();
        gender = getStringError();
        birthday = getStringError();
        locale = getStringError();
        timezone = getStringError();
        updated_time = getStringError();
        location = getLocationError();
    }


    //==============================================================================================
    //
    // Getters and Setters
    //
    //==============================================================================================

    public static String getKeyIntent() {
        return "facebookdataforintent";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String idFB) {

        URL profile_pic = null;
        try {
            profile_pic = new URL(FacebookDataUtils.getProfilePicUrl(idFB));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        this.profile_pic = String.valueOf(profile_pic);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(String updated_time) {
        this.updated_time = updated_time;
    }

    public FacebookData.location getLocation() {
        return location;
    }

    public void setLocation(FacebookData.location location) {
        this.location = location;
    }

    //==============================================================================================
    //
    // Utils
    //
    //==============================================================================================

    private int getIntError() {
        return -99;
    }

    private String getStringError() {
        return "";
    }

    private boolean getBooleanError() {
        return false;
    }

    private location getLocationError() {
        return null;
    }

    //==============================================================================================
    //
    // toString
    //
    //==============================================================================================

    @NonNull
    @Override
    public String toString() {
        return "FacebookData{" +
                "id='" + id + '\'' +
                ", profile_pic='" + profile_pic + '\'' +
                ", email='" + email + '\'' +
                ", link='" + link + '\'' +
                ", name='" + name + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", gender='" + gender + '\'' +
                ", birthday='" + birthday + '\'' +
                ", locale='" + locale + '\'' +
                ", timezone='" + timezone + '\'' +
                ", updated_time='" + updated_time + '\'' +
                ", location=" + location +
                '}';
    }


    //==============================================================================================
    //
    // Location
    //
    //==============================================================================================

    public class location implements Serializable {

        public location(String fbLocationId, String fbLocationname) {
            this.fbLocationId = fbLocationId;
            this.fbLocationname = fbLocationname;
        }

        public location() {
            fbLocationId = getStringError();
            fbLocationname = getStringError();
        }

        private String fbLocationId = "id";
        private String fbLocationname = "name";

        public String getFbLocationId() {
            return fbLocationId;
        }

        public void setFbLocationId(String fbLocationId) {
            this.fbLocationId = fbLocationId;
        }

        public String getFbLocationname() {
            return fbLocationname;
        }

        public void setFbLocationname(String fbLocationname) {
            this.fbLocationname = fbLocationname;
        }

        @NonNull
        @Override
        public String toString() {
            return "location{" +
                    "fbLocationId='" + fbLocationId + '\'' +
                    ", fbLocationname='" + fbLocationname + '\'' +
                    '}';
        }
    }

}
