
/*
 * Copyright (c) Modifications and updates started on 2018/$today.mount/25, by programmer Jonn Alves, about the Naville Brasil contract.
 */

package maripoppis.com.socialnetwork.FaceBook;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class FacebookDataUtils {

    //Json Structure Example

//    {
//            "id": "579156356",
//            "name": "Vishal Jethani",
//            "first_name": "Vishal",
//            "last_name": "Jethani",
//            "link": "https://www.facebook.com/vishal.jethani",
//            "username": "vishal.jethani",
//
//   "hometown": {
//            "id": "106442706060302",
//            "name": "Pune, Maharashtra"
//    },
//   "location": {
//        "id": "106377336067638",
//        "name": "Bangalore, India"
//    },
//            "bio": "bye bye to bad characters",
//            "gender": "male",
//            "relationship_status": "Single",
//            "timezone": 5.5,
//            "locale": "en_GB",
//            "verified": true,
//            "updated_time": "2012-06-15T05:33:31+0000",
//            "type": "user"
//    }

//    W/FB: {Response:  responseCode: 200, graphObject: {"id":"2037594996552257","name":"Jonatas Alves","email":"jonny255d@hotmail.com","last_name":"Alves","first_name":"Jonatas"}, error: null}


    /*
     * 1     private CallbackManager facebookCallbackManager;
     * 2
     */

    //init
    private static String fb_id = "";
    private static FacebookDataCallback callback;
    private static CallbackManager callbackManager;

    //==============================================================================================
    //
    // Cosntantes Keys
    //
    //==============================================================================================

    private static String fbId = "id";
    private static String fbProfilePic = "profile_pic";
    private static String fbEmail = "email";
    private static String fbLink = "link";
    private static String fbName = "name";
    private static String fbFirstName = "first_name";
    private static String fbLastName = "last_name";
    private static String fbGender = "gender";
    private static String fbBirthday = "birthday";

    private static String fbLocale = "locale";
    private static String fbTimezone = "timezone";
    private static String fbUpdatedTime = "updated_time";

    private static String fbJsonLocation = "location";
    private static String fbLocationId = "id";
    private static String fbLocationname = "name";
    private static String fbLocationCountry = "locale";


    //==============================================================================================
    //
    // Util's
    //
    //==============================================================================================

    public static String getProfilePicUrl(String idFacebookProfile) {
        return "https://graph.facebook.com/" + idFacebookProfile + "/picture?type=large";
    }

    private static String verifieJsonArray(JSONObject object, String verifie) {

        if (object.has(verifie)) {
            try {
                return object.getString(verifie);
            } catch (JSONException e) {
                e.printStackTrace();
                return "";
            }
        } else {
            return "";
        }
    }

    private static int verifieJsonArrayINT(JSONObject object, String verifie) {
        if (object.has(verifie)) {
            try {
                return object.getInt(verifie);
            } catch (JSONException e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            return 0;
        }
    }

    private static JSONObject verifieJsonArrayJSON(JSONObject object, String verifie) {
        if (object.has(verifie)) {
            try {
                return object.getJSONObject(verifie);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    private static boolean JsonExist(JSONObject object) {
        if (object != null && object.length() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //==============================================================================================
    //
    // @Functions
    //
    //==============================================================================================

    public static void getFacebookData(AccessToken accessToken) {

        final Bundle bundle = new Bundle();
        final FacebookData facebookData = new FacebookData();

        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                fb_id = verifieJsonArray(object, fbId);

                if (!fb_id.equals("")) {

                    bundle.putString("idFacebook", fb_id);

                    facebookData.setId(verifieJsonArray(object, fbId));
                    facebookData.setProfile_pic(verifieJsonArray(object, fbId));
                    facebookData.setEmail(verifieJsonArray(object, fbEmail));
                    facebookData.setName(verifieJsonArray(object, fbName));
                    facebookData.setFirst_name(verifieJsonArray(object, fbFirstName));
                    facebookData.setLast_name(verifieJsonArray(object, fbLastName));
                    facebookData.setGender(verifieJsonArray(object, fbGender));
                    facebookData.setLink(verifieJsonArray(object, fbLink));
                    facebookData.setLocale(verifieJsonArray(object, fbLocale));
                    facebookData.setBirthday(verifieJsonArray(object, fbBirthday));
                    facebookData.setTimezone(String.valueOf(verifieJsonArrayINT(object, fbTimezone)));
                    facebookData.setUpdated_time(verifieJsonArray(object, fbUpdatedTime));

                    JSONObject location = verifieJsonArrayJSON(object, fbJsonLocation);
                    if (JsonExist(location)) {
                        facebookData.getLocation().setFbLocationId(verifieJsonArray(location, fbLocationId));
                        facebookData.getLocation().setFbLocationname(verifieJsonArray(location, fbLocationname));
                    }

                    callback.FacebookDataSuccess(facebookData);
                }
            }
        });
        bundle.putString(
                "fields",
                "id,name,link,email,gender,last_name,first_name,locale,timezone,updated_time,verified"
        );
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();
    }

    //==============================================================================================
    //
    // setLogin Button
    //
    //==============================================================================================

    public static void setLoginButtom(LoginButton loginButton) {
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
    }

    //==============================================================================================
    //
    // init Facebookcallback
    //
    //==============================================================================================

    public static void startFacebookLogin(Activity activity, CallbackManager callbackManager) {
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getFacebookData(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.w("FB Cancel", "");
            }

            @Override
            public void onError(FacebookException error) {
                Log.w("FB Error", error.getMessage());
            }
        });
    }

    //==============================================================================================
    //
    // AccessToken
    //
    //==============================================================================================

    public static AccessToken verifyTokem() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            return accessToken;
        } else {
            return null;
        }
    }

    private void deleteAccessToken() {
        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {

                if (currentAccessToken == null) {
                    //User logged out
                    LoginManager.getInstance().logOut();
                }
            }
        };
    }

    //==============================================================================================
    //
    // Logout
    //
    //==============================================================================================

    public static void FacebookLogout() {
        LoginManager.getInstance().logOut();
    }

    //==============================================================================================
    //
    // Interfaces
    //
    //==============================================================================================

    public static void setCallback(FacebookDataCallback mCallback) {
        callback = mCallback;
    }

    public interface FacebookDataCallback {
        void FacebookDataSuccess(FacebookData facebookData);
    }


}
