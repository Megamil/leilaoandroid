/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.socialnetwork.FaceBook;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;

public class FacebookAcountKitUtils {
    /*
    *Passo 1
    https://developers.facebook.com/docs/accountkit/android
    Após criar o app , deverá criar sua ACCOUNT_KIT_CLIENT_TOKEN

    https://developers.facebook.com/apps/
    clique no seu app

     apos , abrir a DashBoard , clique em Account Kit/configuraçoes no menu lateral
     clique em avançar e selecione o valor no campo " Token do cliente do Account Kit "

     Agora baixe este arquivo:
     https://code.google.com/archive/p/openssl-for-windows/downloads
     Crie uma pasta em C: com o nome OpenSSL e extraia os arquivos nela
     *****(Cuidado para não criar uma subpasta com o mesmo nome do arquivo .rar) somente clique em Extrair Aqui!

     Após isto, você deverá gerar duas chaves para o facebook,
     Tenha em mente que você também deverá ter o Java jdk instalado em sua máquina:
     Download:
     http://www.oracle.com/technetwork/pt/java/javase/downloads/index.html

     Localize onde esta o keystore do seu android.
     No meu caso esta em:
     C:\Users\John\.android\debug.keystore
     Copie este caminho.

     //
     Localize a pasta bin do jdk em sua máquina, normalmente encontrada como:
     C:\Program Files\Java\jdk1.8.0_151\bin
     Você deverá abrir um prompt de comando nesta pasta ou navegar até ela via cmd:
     ***Testei via power shell e deu erro, recomendo navegar via CMD
     Voce pode segurar o botão “Shift” e dar um clique com o botão direito , um menu atalho para o CMD ou Power Shell ira aparecer ,
     Caso o CMD não aparecer, poderá aqui via pesquisa e digitar o comando:
     cd C:\Program Files\Java\jdk1.8.0_151\bin
     Que irá parar dentro da pasta bin
     *** não esqueça de modificar o caminho para o mesmo da sua maquina
     //
     Agora no prompt cole:
     keytool -exportcert -alias androiddebugkey -keystore C:\Users\John\.android\debug.keystore | C:\OpenSSL\bin\openssl sha1 -binary | C:\OpenSSL\bin\openssl base64
     Digite uma senha para voce guardar sua keystore ,
     Irá gerar o primeiro hash.

     Agora digite:
     keytool -exportcert -alias KEY_GERADA_NO_COMANDO_ANTERIOR  -keystore C:\Users\John\.android | C:\OpenSSL\bin\openssl sha1 -binary | C:\OpenSSL\bin\openssl base64

     Isso irá gerar a seguda hash .

     Agora na dashboard , clique em account kit/início rapido e no passo
     4. Adicione seu desenvolvimento e os hashes de chave de lançamento
     Adicione as duas hashs geradas anteriormente

     */

    View view;
    public static int APP_REQUEST_CODE = 99;

    public static void smsAtivation(Activity activity) {
        final Intent intent = new Intent(activity, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.CODE
                ); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        activity.startActivityForResult(intent, APP_REQUEST_CODE);
    }

    public void onEmailLoginFlow(Activity activity) {
        final Intent intent = new Intent(activity, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.EMAIL,
                        AccountKitActivity.ResponseType.CODE); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        activity.startActivityForResult(intent, 101);
    }

//    @Override
//    public void onActivityResult(int reqCode, int resultCode, Intent data) {
//        super.onActivityResult(reqCode, resultCode, data);
//
//        if (reqCode == APP_REQUEST_CODE) { // confirm that this response matches your request
//            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
//            String toastMessage;
//            if (loginResult.getError() != null) {
//                toastMessage = loginResult.getError().getErrorType().getMessage();
//            } else if (loginResult.wasCancelled()) {
//                toastMessage = "SMS CANCELADA";
//            } else {
//                if (loginResult.getAccessToken() != null) {
//                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
//                } else {
//                    toastMessage = String.format("DEU CERTO", loginResult.getAuthorizationCode().substring(0, 10));
////                    ativarViaSms();
//                }
//
//                // If you have an authorization code, retrieve it from
//                // loginResult.getAuthorizationCode()
//                // and pass it to your server and exchange it for an access token.
//
//                // Success! Start your next activity...
////                Intent intent = new Intent(this, MainActivity.class);
////                startActivity(intent);
//            }
//
//
//            AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
//                @Override
//                public void onSuccess(final Account account) {
//                    // Get Account Kit ID
//                    String accountKitId = account.getId();
//
//                    // Get phone number
//                    PhoneNumber phoneNumber = account.getPhoneNumber();
//                    if (phoneNumber != null) {
//                        String phoneNumberString = phoneNumber.toString();
//                    }
//
//                    // Get email
//                    String email = account.getEmail();
//                }
//
//                @Override
//                public void onError(final AccountKitError error) {
//                    // Handle Error
//                }
//            });
//        }


    }
