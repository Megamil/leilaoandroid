
/*
 * Copyright (c) Developed by John Alves at 2018/10/31.
 */

package net.megamil.library.ActivitySuport;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import net.megamil.library.R;
import net.megamil.library.Utils.ActivityUtil;
import net.megamil.library.Utils.GlideUtil;

public class WebViewActivity extends AppCompatActivity {


    //init
    private WebView webView;
    LinearLayout llWebview;

    //Load Layout
    CoordinatorLayout loadingLayout;
    ImageView imgFavicon;
    ProgressBar progressBar;

    //TermsOfUser
    Button aceitar;
    Button rejeitar;
    LinearLayout llTerms;

    private static TermsOfUseCallback callback;

    private static String keyUrl = "urlintent";
    private static String keyTerms = "termsIntent";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        initVars();
        initActions();

    }

    private void initVars() {

        //init
        webView = findViewById(R.id.webviewActivity);
        llWebview = findViewById(R.id.llWebview);

        //Load Layout
        loadingLayout = findViewById(R.id.loading);
        imgFavicon = findViewById(R.id.label_favicon);
        progressBar = findViewById(R.id.progress);

        //TermsOfUser
        aceitar = findViewById(R.id.aceitar);
        rejeitar = findViewById(R.id.rejeitar);
        llTerms = findViewById(R.id.linearLayout);

    }


    private void initActions() {
        loadWebViewLoad(webView, getIntent().getStringExtra(keyUrl));
    }

    //==============================================================================================
    //
    // Webview init
    //
    //==============================================================================================

    @SuppressLint("SetJavaScriptEnabled")
    private void loadWebViewLoad(final WebView webview, String url) {

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setSupportMultipleWindows(true);

        webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showLoading();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                showWebview();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }

        });

        webview.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);
                GlideUtil.initGlide(WebViewActivity.this, icon, imgFavicon);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                setProgressBar(newProgress);
            }

        });
        webview.loadUrl(url);
    }

    //==============================================================================================
    //
    // Utils
    //
    //==============================================================================================

    private void showLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
        llWebview.setVisibility(View.GONE);
    }

    private void showWebview() {
        loadingLayout.setVisibility(View.GONE);
        llWebview.setVisibility(View.VISIBLE);
        //
        progressBar.setProgress(0);
        progressBar.setSecondaryProgress(0);

        if (getIntent().hasExtra(keyTerms)) {
            initTermsOfUser();
        }
    }

    private void setProgressBar(int newProgress) {
        progressBar.setProgress(newProgress);
        progressBar.setSecondaryProgress(newProgress + 10);
    }

    //==============================================================================================
    //
    // @Override
    //
    //==============================================================================================

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
            finish();
        }
    }


    //==============================================================================================
    //
    // @Interface for Terms of user
    //
    //==============================================================================================

    public static void setTermsCallback(TermsOfUseCallback termsOfUseCallback) {
        callback = termsOfUseCallback;
    }

    public interface TermsOfUseCallback {
        void TermsOfUseAccept();

        void TermsOfUseRegected();
    }

    private void initTermsOfUser() {

        llTerms.setVisibility(View.VISIBLE);

        aceitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.TermsOfUseAccept();
                finish();
            }
        });

        rejeitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.TermsOfUseRegected();
                finish();
            }
        });

    }

    //==============================================================================================
    //
    // StartWeb View
    //
    //==============================================================================================

    /**
     * @param activity
     * @param url
     * @param finish
     */
    public static void callActivityWebView(Activity activity, String url, final Boolean finish) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(keyUrl, url);
        ActivityUtil.callActivity(activity, intent, finish);
    }

    /**
     * @param activity
     * @param url
     * @param finish
     */
    public static void callActivityWebViewForTermOfUser(Activity activity, String url, final Boolean finish) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(keyUrl, url);
        intent.putExtra(keyTerms, true);
        ActivityUtil.callActivity(activity, intent, finish);
    }
}
