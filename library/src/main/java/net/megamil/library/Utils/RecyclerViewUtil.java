/*
 * Copyright (c) Developed by John Alves at 2018/10/24.
 */

package net.megamil.library.Utils;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Display;

public class RecyclerViewUtil {

    public static GridLayoutManager getGridLayoutManager(Activity activity) {

        GridLayoutManager gridLayoutManager = null;

        if (getScreenWidthDp(activity) >= 1200) {
            gridLayoutManager = new GridLayoutManager(activity, 3);
        } else if (getScreenWidthDp(activity) >= 800) {
            gridLayoutManager = new GridLayoutManager(activity, 2);
        } else {
            gridLayoutManager = new GridLayoutManager(activity, 2);
        }

        return gridLayoutManager;
    }

    //    private static int getScreenWidthDp(Activity activity) {
//        DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
//        return (int) (displayMetrics.widthPixels / displayMetrics.density);
//    }
    private static int getScreenWidthDp(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        return (int) (display.getWidth() / display.getHeight());
    }

    public static LinearLayoutManager getLayoutManagerVertical(Activity activity) {
        return new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
    }

    public static LinearLayoutManager getLayoutManagerHorizontal(Activity activity) {
        return new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
    }

    public static LinearLayoutManager getLayoutManagerVerticalReverse(Activity activity) {
        return new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, true);
    }

    public static LinearLayoutManager getLayoutManagerHorizontalReverse(Activity activity) {
        return new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, true);
    }
}
