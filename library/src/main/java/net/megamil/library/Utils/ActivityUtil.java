/*
 * Copyright (c) Developed by John Alves at 2018/10/28.
 */

package net.megamil.library.Utils;

import android.app.Activity;
import android.content.Intent;

public class ActivityUtil {

    /**
     * @param activity
     * @param intent
     * @param finish
     */
    public static void callActivity(Activity activity, final Intent intent, final Boolean finish) {
        initIntent(activity, verifieStatusIntent(intent), finish);
    }

    /**
     *
     * @param activity
     * @param classActivity
     * @param finish
     */
    public static void callActivity(Activity activity, final Class classActivity, final Boolean finish) {
        initIntent(activity, verifieStatusIntent(new Intent(activity, classActivity)), finish);
    }


    /**
     * @param intent
     * @return
     */
    private static Intent verifieStatusIntent(Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    /**
     * @param activity
     * @param intent
     * @param finish
     */
    private static void initIntent(Activity activity, Intent intent, boolean finish) {
        activity.startActivity(intent);
        if (finish) {
            activity.finish();
        }
    }


}
