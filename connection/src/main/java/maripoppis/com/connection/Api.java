/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection;

import maripoppis.com.connection.Model.SubModels.Login;
import maripoppis.com.connection.Model.WSResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {

    //Login, Recover Password and Sing In
    @POST("login_usuario")
    Call<WSResponse> setLogin(@Header("Authorization") String authKey, @Body Login login);

    @POST("login_usuario")
    Call<WSResponse> setLoginSocialNetwork(@Body Login login);

    @POST("ativar_sms")
    Call<WSResponse> smsActivate(@Body Login login);

//    @GET(newWs + "wsMagazines?limit=10")
//    Call<WSResponse> getMagazines(@Query("page") int wsPage);
//
//    //
//    @POST(newWs + "wsCreateUser")
//    Call<WSResponse> createNewUser(@Body Usuario usuarios);
//
//    @POST(newWs + "wsForgotPassword")
//    Call<WSResponse> setRecoverPassword(@Body Usuario usuarios);

    //Inside in app!


    //==============================================================================================
    //
    // News Connections
    //
    //==============================================================================================

}

