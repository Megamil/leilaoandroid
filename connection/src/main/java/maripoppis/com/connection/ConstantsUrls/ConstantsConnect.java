/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection.ConstantsUrls;

public class ConstantsConnect {

    private static String SERVER = "http://api.leilao24h.com.br";
    private static String PROJECT_NAME = "/";

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    private static String URL_BASE = SERVER + PROJECT_NAME;

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    private static String urlBaseSocket = SERVER + ":3005";
    private static String urlProjectController = URL_BASE + "controller_webservice/";
    private static String urlBuyTicket = URL_BASE + "comprar_ingressos/";
    private static String urlPhotoProfile = URL_BASE + "upload/perfil/foto/";

    //==============================================================================================

    public static String getBaseUrl() {
        return getUrlProjectController();
    }

    //privacity Policy
    public static String getUrlPrivaciryPolicy() {
        return "http://api.leilao24h.com.br/privacidade";
    }

    //Terms Of Users
    public static String getUrlTermOfUse() {
        return "http://api.leilao24h.com.br/termos";
    }

    public static String getUrlBaseSocket() {
        return urlBaseSocket;
    }

    public static String getUrlProjectController() {
        return urlProjectController;
    }

    public static String getUrlBuyTicket() {
        return urlBuyTicket;
    }

    public static String getUrlPhotoProfile() {
        return urlPhotoProfile;
    }


}
