/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection.Model.SubModels;

import java.io.Serializable;

import static maripoppis.com.connection.ConnectionUtils.ModelUtils.getStringError;

public class Login implements Serializable {


    private String id_usuario;
    private String celular_usuario;
    private String user;
    private String pass;
    private String token;
    private String facebook_usuario;

    public Login(String user, String pass, String token, String facebook_usuario) {
        this.user = user;
        this.pass = pass;
        this.token = token;
        this.facebook_usuario = facebook_usuario;
    }

    public Login(String token, String facebook_usuario) {
        this.token = token;
        this.facebook_usuario = facebook_usuario;
    }

    public Login(String user, String pass, String token) {
        this.user = user;
        this.pass = pass;
        this.token = token;
    }

    public Login() {
        user = getStringError();
        pass = getStringError();
        token = getStringError();
        facebook_usuario = getStringError();
    }

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    @Override
    public String toString() {
        return "Login{" +
                "id_usuario='" + id_usuario + '\'' +
                ", user='" + user + '\'' +
                ", pass='" + pass + '\'' +
                ", token='" + token + '\'' +
                ", facebook_usuario='" + facebook_usuario + '\'' +
                '}';
    }
//==============================================================================================
    //
    //
    //
    //==============================================================================================

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFacebook_usuario() {
        return facebook_usuario;
    }

    public void setFacebook_usuario(String facebook_usuario) {
        this.facebook_usuario = facebook_usuario;
    }
}
