/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import maripoppis.com.connection.Model.SubModels.Login;
import maripoppis.com.connection.Model.SubModels.usuarios;

import static maripoppis.com.connection.ConnectionUtils.ModelUtils.getDataNullError;
import static maripoppis.com.connection.ConnectionUtils.ModelUtils.getIntError;
import static maripoppis.com.connection.ConnectionUtils.ModelUtils.getStringError;

public class WSResponse implements Serializable {

    //Vars ws
    private int status;
    private String resultado;
    private String creditos;
    private String emoji;
    private dados dados;
    // Others
    private String id_usuario;
    private String celular_usuario;

    public WSResponse() {
        this.status = getIntError();
        this.resultado = getStringError();
        this.emoji = getStringError();
        this.dados = getDataNullError();
    }

    public WSResponse(int status, String resultado, String emoji, dados dados) {
        this.status = status;
        this.resultado = resultado;
        this.emoji = emoji;
        this.dados = dados;
    }

    @Override
    public String toString() {
        return "WSResponse{" +
                "status=" + status +
                ", resultado='" + resultado + '\'' +
                ", creditos='" + creditos + '\'' +
                ", emoji='" + emoji + '\'' +
                ", dados=" + dados +
                ", id_usuario='" + id_usuario + '\'' +
                ", celular_usuario='" + celular_usuario + '\'' +
                '}';
    }

    //Class Data WS
    //==============================================================================================
    public static class dados implements Serializable {

//             dados":{"id_usuario":"13"
//                     "code":"MTM=",
//         "nome_usuario":"Jonny alves",
//         "email_usuario":"jonny255a@hotmail.com",
//         "celular_usuario":"11999235688","telefone_usuario":"","fk_grupo_usuario":"3","ativo_usuario":"1","token_acesso":"8831ce7aeb67e606bbc7049d550be373f9856b8313","ativado_sms":"1","bloquear":"0"}}


        @SerializedName("id_usuario")
        @Expose
        private String idUsuario;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("nome_usuario")
        @Expose
        private String nomeUsuario;
        @SerializedName("email_usuario")
        @Expose
        private String emailUsuario;
        @SerializedName("celular_usuario")
        @Expose
        private String celularUsuario;
        @SerializedName("telefone_usuario")
        @Expose
        private String telefoneUsuario;
        @SerializedName("fk_grupo_usuario")
        @Expose
        private String fkGrupoUsuario;
        @SerializedName("ativo_usuario")
        @Expose
        private String ativoUsuario;
        @SerializedName("token_acesso")
        @Expose
        private String tokenAcesso;
        @SerializedName("ativado_sms")
        @Expose
        private String ativadoSms;
        @SerializedName("bloquear")
        @Expose
        private String bloquear;

        public String getIdUsuario() {
            return idUsuario;
        }

        public void setIdUsuario(String idUsuario) {
            this.idUsuario = idUsuario;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getNomeUsuario() {
            return nomeUsuario;
        }

        public void setNomeUsuario(String nomeUsuario) {
            this.nomeUsuario = nomeUsuario;
        }

        public String getEmailUsuario() {
            return emailUsuario;
        }

        public void setEmailUsuario(String emailUsuario) {
            this.emailUsuario = emailUsuario;
        }

        public String getCelularUsuario() {
            return celularUsuario;
        }

        public void setCelularUsuario(String celularUsuario) {
            this.celularUsuario = celularUsuario;
        }

        public String getTelefoneUsuario() {
            return telefoneUsuario;
        }

        public void setTelefoneUsuario(String telefoneUsuario) {
            this.telefoneUsuario = telefoneUsuario;
        }

        public String getFkGrupoUsuario() {
            return fkGrupoUsuario;
        }

        public void setFkGrupoUsuario(String fkGrupoUsuario) {
            this.fkGrupoUsuario = fkGrupoUsuario;
        }

        public String getAtivoUsuario() {
            return ativoUsuario;
        }

        public void setAtivoUsuario(String ativoUsuario) {
            this.ativoUsuario = ativoUsuario;
        }

        public String getTokenAcesso() {
            return tokenAcesso;
        }

        public void setTokenAcesso(String tokenAcesso) {
            this.tokenAcesso = tokenAcesso;
        }

        public String getAtivadoSms() {
            return ativadoSms;
        }

        public void setAtivadoSms(String ativadoSms) {
            this.ativadoSms = ativadoSms;
        }

        public String getBloquear() {
            return bloquear;
        }

        public void setBloquear(String bloquear) {
            this.bloquear = bloquear;
        }

        Login login;

        public Login getLogin() {
            return login;
        }

        public void setLogin(Login login) {
            this.login = login;
        }

        @Override
        public String toString() {
            return "dados{" +
                    "login=" + login +
                    '}';
        }
    }


    //Setter and Getter
    //==============================================================================================

    public String getCreditos() {
        return creditos;
    }

    public void setCreditos(String creditos) {
        this.creditos = creditos;
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getCelular_usuario() {
        return celular_usuario;
    }

    public void setCelular_usuario(String celular_usuario) {
        this.celular_usuario = celular_usuario;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getEmoji() {
        return emoji;
    }

    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }

    public dados getDados() {
        return dados;
    }

    public void setDados(dados dados) {
        this.dados = dados;
    }

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    private usuarios usuarios;

    public maripoppis.com.connection.Model.SubModels.usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(maripoppis.com.connection.Model.SubModels.usuarios usuarios) {
        this.usuarios = usuarios;
    }

}
